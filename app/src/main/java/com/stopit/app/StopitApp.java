package com.stopit.app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.SaveCallback;
import com.parse.gcm.ParseGCM;
import com.stanko.tools.DeviceInfo;
import com.stanko.tools.ResHelper;
import com.stopit.R;
import com.stopit.db.DBHelper;
import com.stopit.event.AppOnForegroundEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;


public class StopitApp extends MultiDexApplication implements Application.ActivityLifecycleCallbacks {

    public static boolean isProd = false;
    private static Context sAppContext; // Application Context could be kept in static field!
    private static boolean isAppOnForeground = true;
    private int numStarted = 0;
    private Locale locale;

    public static Context getAppContext() {
        return sAppContext;
    }

    public static void cleanUpMemory() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    public static boolean getAppForegraundState() {
        return isAppOnForeground;
    }

    @Override
    public void onCreate() {
        android.util.Log.i(StopitApp.class.getSimpleName(), "onCreate()");
        long start = System.currentTimeMillis();
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        sAppContext = getApplicationContext();
        initHelpers();
        initParse();
        registerActivityLifecycleCallbacks(this);
        clearGlideCash();
        /*checkLocale();*/

        // Needed updated Provider for corner cases with HTTP request using TSL on Android API19
//        com.google.android.gms.security.ProviderInstaller.installIfNeededAsync(sAppContext, new ProviderInstaller.ProviderInstallListener() {
//            @Override
//            public void onProviderInstalled() {
//                Log.d("StopitApp", "Provider installed");
//            }
//
//            @Override
//            public void onProviderInstallFailed(int i, Intent intent) {
//                Log.d("StopitApp", "Provider installation failed");
//            }
//        });

        //android.util.Log.i("StopitApp", "App onCreate() took time: " + (System.currentTimeMillis() - start));
    }

    /*private void checkLocale() {
        final String lang = Locale.getDefault().getLanguage();
        final String selectedLanguage = SharedPrefsHelper.getSelectedAppLanguage(getBaseContext());
        if (TextUtils.isEmpty(selectedLanguage) || selectedLanguage.equals(lang))
            return;

        final Resources resources = getBaseContext().getResources();
        Configuration config = resources.getConfiguration();
        this.locale = new Locale(selectedLanguage);
        Locale.setDefault(locale);
        config.setLocale(locale);
        resources.updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }*/

    private void initHelpers() {
        DBHelper.init(sAppContext);
        DeviceInfo.init(sAppContext);
        ResHelper.init(sAppContext);
        //firebase token
        /*String token = FirebaseInstanceId.getInstance().getToken();
        Log.i("stopit_app", "FCM Token: " + token);*/
    }

//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        if (locale != null) {
//            newConfig.setLocale(locale);
//            Locale.setDefault(locale);
//            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
//        }
//    }

//    @Override
//    protected void attachBaseContext(Context context) {
//        super.attachBaseContext(LocaleHelper.setLocale(context));
//    }

    private void clearGlideCash() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Glide.get(getAppContext()).clearDiskCache();
            }
        }).start();
    }

    private void initParse() {
        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);
        Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);
        if (isProd) {
            //Prod Key

            Parse.Configuration.Builder builder = new Parse.Configuration.Builder(getApplicationContext())
                    .applicationId("X1oDeeFYY0mx4iqhG5q5UBHEkxdkV2hadg6iIuoF")
                    .clientKey("uCrwnlsLenEUZRLiA6yQXG0PNIpM9X498EQn74wy")
                    .server("https://stopitappstore-772.nodechef.com/parse/");

            Parse.initialize(builder.build());
            ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
            parseInstallation.put("GCMSenderId", getResources().getString(R.string.prod_google_app_id));
            parseInstallation.setPushType("gcm");
            parseInstallation.increment("badge", 0);

        } else {
            //pre-prod Key
            Parse.Configuration.Builder builder = new Parse.Configuration.Builder(getApplicationContext())
                    .applicationId("HCNCJA2dVNKGZznt0CSXZ0IAYKUbXll5H0kmW95u")
                    .clientKey("xoikgUOCkfDjNk8vqzm3VDFgtocIYDtXwHEJ8uG6")
                    .server("https://stopitpreprod-772.nodechef.com/parse/");

            Parse.initialize(builder.build());
            ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
            parseInstallation.put("GCMSenderId", getResources().getString(R.string.preprod_google_app_id));
            parseInstallation.setPushType("gcm");
            parseInstallation.increment("badge", 0);
        }

        Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);
        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                ParseGCM.register(getApplicationContext());
                if (e != null)
                    Log.e("StopitApp ", e.getMessage());
            }
        });
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {
        if (numStarted == 0) {
            // app went to foreground
            setAppIsOnForeground(true);
            //Toast.makeText(this, "false", Toast.LENGTH_SHORT).show();
        }
        numStarted++;
    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {
        numStarted--;
        if (numStarted == 0) {
            // app went to background
            setAppIsOnForeground(false);
            //Toast.makeText(this, "true", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    void setAppIsOnForeground(final boolean isOnForeground) {
        Log.i("StopitApp", "isAppIsOnBackground = " + isOnForeground);

        isAppOnForeground = isOnForeground;
        if (isAppOnForeground) {
            EventBus.getDefault().post(new AppOnForegroundEvent());
        }
        //unregisterAllReceivers();
    }


//    private static void clearStorage() {
//        boolean hasPermissions = Build.VERSION.SDK_INT < 23; // LOLLIPOP and lower
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            hasPermissions = ContextCompat.checkSelfPermission(sAppContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
//        }
//        final File cacheDir = SDCardHelper.getCacheDir();
//        if (hasPermissions && cacheDir.exists() && FileUtils.isWritable(cacheDir)) {
//            final Thread clearStorageThread = new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    FileUtils.deleteFiles(cacheDir);
//                }
//            });
//            clearStorageThread.setName("ClearAppStorageThread");
//            clearStorageThread.setPriority(Process.THREAD_PRIORITY_BACKGROUND);
//            clearStorageThread.start();
//        }
//    }

//        ResHelper.init(sAppContext);
//        DeviceInfo.init(sAppContext);
//        PermissionsHelper.init(sAppContext);
//        SDCardHelper.init(sAppContext);
//        NotificationsHelper.init(sAppContext);
//        LocaleHelper.setLocale(sAppContext, appLocale);
//        start = System.currentTimeMillis();


//    public static boolean isTablet() {
//        Log.i("called from: " + Log.getMethodName() + " isTablet: " + DeviceInfo.isTablet() + " !forceSmartPhoneVersion(): " + !SharedPrefsHelper.forceSmartPhoneVersion());
//        return DeviceInfo.isTablet() && !SharedPrefsHelper.forceSmartPhoneVersion();
//    }


//    public static boolean checkIfFreeSdCardSpaceAvailableAndInform(final boolean isNeedToastShown) {
//        long availableSpace = SDCardHelper.getAvailableSpace();
//        final boolean isAvailable = availableSpace >= ActivityExtraKeys.PD_REQUIRED_SPACE_MIN;
//
//        if (isNeedToastShown && !isAvailable)
//            showToast(R.string.toast_no_sdcard_space);
//        return isAvailable;
//    }


}