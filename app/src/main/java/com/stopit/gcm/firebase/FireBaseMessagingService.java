package com.stopit.gcm.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.parse.ParseInstallation;

public class FireBaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d("FireBaseMessagingService", "From: "+remoteMessage.getFrom());
        Log.d("FireBaseMessagingService", "Notification Body: " + remoteMessage.getNotification().getBody());
        Log.d("FireBaseMessagingService", "Notification Data Payload: " + remoteMessage.getNotification());
        Log.d("FireBaseMessagingService", "To: " + remoteMessage.getTo());
        Log.d("FireBaseMessagingService", "Message-id: "+ remoteMessage.getMessageId());
        Log.d("FireBaseMessagingService", "Message-type: "+ remoteMessage.getMessageType());
        //        showNotification(remoteMessage.getNotification().getBody());

    }

    public void onNewToken(String token)
    {
        super.onNewToken(token);
        Log.d("FireBaseMessagingService", "New Token: " + token);
//        ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
//        parseInstallation.put("deviceToken", token);
//        parseInstallation.saveInBackground();
    }
}


