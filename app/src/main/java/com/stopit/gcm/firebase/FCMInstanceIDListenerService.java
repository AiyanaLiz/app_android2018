package com.stopit.gcm.firebase;

import android.util.Log;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.parse.ParseInstallation;

public class FCMInstanceIDListenerService extends FirebaseInstanceIdService {

       // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.i("Firebase","Push test onTokenRefresh() got FCM(GCM) token: " + refreshedToken);
        //CMHelper.onTokenRefresh(refreshedToken);
        //sendRegistrationToServer(refreshedToken);
        ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
        parseInstallation.put("deviceToken", refreshedToken);
        parseInstallation.saveInBackground();
    }


}
