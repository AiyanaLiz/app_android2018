package com.stopit.gcm.firebase;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FireBaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

    //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

    //Displaying token on logcat
        Log.i("MyFirebaseIIDService","Refreshed token: " + refreshedToken);
    }

}
