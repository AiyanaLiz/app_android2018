package com.stopit.gcm.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.stopit.R;
import com.stopit.activity.MainActivity;

public class MyFirebaseService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseService.class.getSimpleName();

//    @Override
//    public void onMessageReceived(RemoteMessage remoteMessage) {
//        Log.d(TAG, "onMessageReceived: " + remoteMessage.getData().get("message"));
//        //Toast.makeText(StopitApp.getAppContext(), "Notification received via Firebase onMessageReceived", Toast.LENGTH_SHORT).show();
////        Log.d(TAG, "remoteMessage" + remoteMessage.getData());
////        Log.d(TAG, "APS: " + remoteMessage.getData().get("aps"));
////
////        String id, title, msg, type, image, sound;
////        try {
////            if (remoteMessage.getData() != null) {
////                id = remoteMessage.getData().get("id");
////                title = remoteMessage.getData().get("title");
////                msg = remoteMessage.getData().get("msg");
////                type = remoteMessage.getData().get("type");
////                image = remoteMessage.getData().get("image");
////                sound = remoteMessage.getData().get("sound");
////                sendNotification();
////            }
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//        sendNotification(remoteMessage.getData().get("message"));
//    }

//    private void sendNotification(String messageBody) {
//        Intent intent = new Intent(this, MainActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0/*Request code*/, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//
//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setContentTitle("Stopit FCM test")
//                .setContentText(messageBody)
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setContentIntent(pendingIntent);
//
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationManager.notify(0/*ID of notification*/, notificationBuilder.build());
//    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "Push onMessageReceived: " + remoteMessage.getData().get("message"));

    }

    public void onNewToken(String token )
    {
        super.onNewToken(token);
        Log.d(TAG, "Push test onNewToken: " + token);
    }
}


