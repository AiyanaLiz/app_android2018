package com.stopit.gcm.parse_gcm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;
import com.stopit.api.chat.SmackService;
import com.stopit.app.StopitApp;

import org.json.JSONObject;

import static com.stopit.app.Constants.INBOX_NOTIFICATION_BROADCAST_ID;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_NOTIFICATION_ALERT;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_NOTIFICATION_MSG_ID;

/**
 * This class handles inbox type push message i.e. push which has video or some website links as
 * payload.
 */
public class StopitGCMBroadcastReceiver extends ParsePushBroadcastReceiver {
    public static final String ACTION = "com.example.package.MESSAGE";
    public static final String PARSE_EXTRA_DATA_KEY = "com.parse.Data";
    public static final String PARSE_JSON_CHANNEL_KEY = "com.parse.Channel";
    private static final String APS_KEY = "aps";
    private static final String URL_KEY = "url";
    private static final String ALERT_KEY = "alert";
    private static final String MESSAGE_ID_KEY = "message_id";


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("StopitGCMBroadc", "Push ****** GCMBroadCastReceiver ***");
        if (!StopitApp.getAppForegraundState()) {
            super.onReceive(context, intent);

        } else {
            final Bundle bundle = intent.getExtras();
            if (bundle == null || TextUtils.isEmpty(bundle.getString(PARSE_EXTRA_DATA_KEY)))
                return;

            try {
                JSONObject json = new JSONObject(bundle.getString(PARSE_EXTRA_DATA_KEY));
//            if (json.has(APS_KEY)) {
//                JSONObject aps = json.getJSONObject(APS_KEY);
//                if (aps.has(ALERT_KEY) && json.has(URL_KEY)) {
//                    String alert = aps.getString(ALERT_KEY);
//                    String url = json.getString(URL_KEY);
//                    notifyUser(context, alert, url);
//                }
//            }


                if (json.has(MESSAGE_ID_KEY)) {
                    String messageId = json.getString(MESSAGE_ID_KEY);
                    String alert = json.getString(ALERT_KEY);

                    final String action;
                    if (!messageId.isEmpty()) {
                        action = INBOX_NOTIFICATION_BROADCAST_ID;
                    } else {
                        action = SmackService.PUSH_NEW_MESSAGE;
                    }

                    final Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(action);
                    //broadcastIntent.putExtra("notification", "true");
                    broadcastIntent.putExtra(ST_EXTRAS_KEY_NOTIFICATION_ALERT, alert);
                    broadcastIntent.putExtra(ST_EXTRAS_KEY_NOTIFICATION_MSG_ID, messageId);

                    StopitApp.getAppContext().sendBroadcast(broadcastIntent);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
