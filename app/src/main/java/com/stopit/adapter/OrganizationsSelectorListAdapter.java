package com.stopit.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.stopit.R;
import com.stopit.models.Organization;

import java.util.List;

/**
 * Created by Alenka on 17.06.2018.
 */

public class OrganizationsSelectorListAdapter extends BaseQuickAdapter<Organization, BaseViewHolder> {


    public OrganizationsSelectorListAdapter(final List<Organization> entities) {
        super(R.layout.item_organization_selector, entities);
    }


    @Override
    protected void convert(final BaseViewHolder viewHolder, final Organization item) {
        viewHolder.setText(R.id.org_selector_title, item.getName());
    }
}
