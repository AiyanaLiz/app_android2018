package com.stopit.adapter;

import android.view.View;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.stopit.R;
import com.stopit.models.messenger.ChatMessage;

import java.util.List;

import static com.stopit.app.Constants.MESSAGE_TYPE_ADMIN_MESSAGE;
import static com.stopit.app.Constants.MESSAGE_TYPE_USER_MESSAGE;


public class MessengerAdapter extends BaseMultiItemQuickAdapter<ChatMessage, BaseViewHolder> {

//    /**
//     * Displays clicked image or PDF in a full screen viewer
//     */
//    private final View.OnClickListener userPhotoClickListener = new View.OnClickListener() {
//        @Override
//        public void onClick(View view) {
//
//        }
//    };


    public MessengerAdapter(final List<ChatMessage> messages) {
        super(messages);
        addItemType(MESSAGE_TYPE_USER_MESSAGE, R.layout.item_chat_user_text_message);
        addItemType(MESSAGE_TYPE_ADMIN_MESSAGE, R.layout.item_chat_admin_message);
    }

    @Override
    protected void convert(BaseViewHolder holder, ChatMessage message) {
        final int itemType = message.getItemType();

        switch (itemType) {
            case MESSAGE_TYPE_ADMIN_MESSAGE:
                holder.setText(R.id.chat_message_text, message.getMessage());
                holder.setText(R.id.chat_time_text, message.getMessageDateFormatted());
                break;

            default:
                holder.setText(R.id.chat_user_message_text, message.getMessage());
                holder.setText(R.id.chat_user_time_text, message.getMessageDateFormatted());
        }
    }
}