package com.stopit.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.stopit.R;
import com.stopit.models.OrgsTreeEntity;
import com.stopit.views.StopitTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alenka on 17.06.2018.
 */

public class OrganizationsBaseAdapter extends RecyclerView.Adapter<OrganizationsBaseAdapter.OrgViewHolder> implements Filterable {
    private OrganizationsFilter mFilter;
    private List<OrgsTreeEntity> allData;
    private List<OrgsTreeEntity> filteredData;
    private LayoutInflater inflater;
    private View.OnClickListener itemClicked;


    public OrganizationsBaseAdapter(final Context context, final List<OrgsTreeEntity> organizations, final View.OnClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.allData = new ArrayList<>(organizations);
        this.filteredData = organizations;
        this.itemClicked = listener;
    }

    @Override
    public OrgViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_organizations_list, parent, false);
        return new OrgViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrgViewHolder viewHolder, int position) {
        final OrgsTreeEntity item = getItem(position);

        viewHolder.root.setTag(item);
        viewHolder.root.setOnClickListener(itemClicked);
        viewHolder.nameTextView.setText(item.getName());

        final int iconRes = item.isParent() ? R.drawable.ic_arrow_gray : 0;
        viewHolder.nameTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, iconRes, 0);
    }

    public OrgsTreeEntity getItem(final int position) {
        return filteredData.get(position);
    }

    @Override
    public int getItemCount() {
        return filteredData.size();
    }

    public void setItems(List<OrgsTreeEntity> items) {
        filteredData.addAll(items);
        notifyDataSetChanged();
    }

    public void clearItems() {
        filteredData.clear();
        notifyDataSetChanged();
    }


//    @Override
//    protected void convert(BaseViewHolder viewHolder, OrgsTreeEntity item) {
//        viewHolder.itemView.setTag(item);
//        viewHolder.itemView.setOnClickListener(listener);
//        final TextView orgTxt = viewHolder.getView(R.id.organization_title);
//        orgTxt.setText(item.getName());
//        final boolean hasNext = true;//item.hasNext();
//        final int drawableRes = hasNext ? R.drawable.ic_arrow_gray : 0;
//        orgTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_gray, 0);
//    }

    public Filter getFilter() {
        if (mFilter == null)
            mFilter = new OrganizationsFilter();

        return mFilter;
    }

    class OrgViewHolder extends RecyclerView.ViewHolder {
        private View root;
        private StopitTextView nameTextView;

        public OrgViewHolder(View itemView) {
            super(itemView);

            this.root = itemView;
            nameTextView = itemView.findViewById(R.id.organization_title);
        }
    }

    private class OrganizationsFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();

            //clear selection
            if (TextUtils.isEmpty(filterString)) {
                results.values = new ArrayList<>(allData);
                results.count = allData.size();
                return results;
            }

            int count = allData.size();
            final List<OrgsTreeEntity> resultsList = new ArrayList<>();

            OrgsTreeEntity item;
            for (int i = 0; i < count; i++) {
                item = allData.get(i);
                final String orgName = item.getName();
                if (!TextUtils.isEmpty(orgName) && orgName.toLowerCase().contains(filterString)) {
                    resultsList.add(item);
                }
            }

            results.values = resultsList;
            results.count = resultsList.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results == null || results.values == null) {
                filteredData = new ArrayList<>();
            } else
                filteredData = (ArrayList<OrgsTreeEntity>) results.values;

            notifyDataSetChanged();
        }
    }
}
