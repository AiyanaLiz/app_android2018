package com.stopit.adapter;

import android.view.View;

import com.andexert.library.RippleView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.stopit.R;
import com.stopit.models.Incident;

import java.util.List;

/**
 * Created by Alenka on 17.06.2018.
 */

public class IncidentsListAdapter<T extends Incident> extends BaseQuickAdapter<T, BaseViewHolder> {
    private RippleView.OnRippleCompleteListener viewIncidentListener;
    private RippleView.OnRippleCompleteListener mediaClickListener;
    private RippleView.OnRippleCompleteListener messengerListener;


    public IncidentsListAdapter(final List<T> incidents, final RippleView.OnRippleCompleteListener viewIncidentListener,
                                final RippleView.OnRippleCompleteListener mediaClickListener,
                                final RippleView.OnRippleCompleteListener messengerListener) {
        super(R.layout.item_incidents_list, incidents);
        this.viewIncidentListener = viewIncidentListener;
        this.mediaClickListener = mediaClickListener;
        this.messengerListener = messengerListener;
    }

    @Override
    protected void convert(BaseViewHolder viewHolder, Incident incident) {
        viewHolder.setText(R.id.incident_id, incident.getIdFormatted());
        viewHolder.setText(R.id.incident_date, incident.getDateFormatted(incident.getCreationDate()));
        viewHolder.setText(R.id.incident_media_counter, mContext.getString(R.string.media_counter_format, incident.getAttachmentsCount()));

        final int newMessagesCount = incident.getMessagesCount();
        if (newMessagesCount == 0)
            viewHolder.setText(R.id.incident_messenger_btn, mContext.getString(R.string.messenger_btn_format_zero));
        else
            viewHolder.setText(R.id.incident_messenger_btn, mContext.getString(R.string.messenger_btn_format, newMessagesCount));

        final Long tag = incident.getId();

        //Add media+
        final RippleView addMediaRipple = viewHolder.getView(R.id.incident_add_media_ripple);
        if (incident.isClosed()) {
            addMediaRipple.setVisibility(View.INVISIBLE);
            addMediaRipple.setTag(null);
            addMediaRipple.setOnRippleCompleteListener(null);
        } else {
            addMediaRipple.setVisibility(View.VISIBLE);
            addMediaRipple.setOnRippleCompleteListener(mediaClickListener);
            addMediaRipple.setTag(tag);
        }

        //status icon
        viewHolder.setBackgroundRes(R.id.incident_status_txt, incident.getStatusBackground());
        viewHolder.setText(R.id.incident_status_txt, incident.getStatusTxtResource());

        //bottom button view
        final RippleView detailsRipple = viewHolder.getView(R.id.incident_details_ripple);
        detailsRipple.setTag(tag);
        detailsRipple.setOnRippleCompleteListener(viewIncidentListener);

        //bottom button messages
        final RippleView messengerRipple = viewHolder.getView(R.id.incident_messenger_ripple);
        messengerRipple.setTag(tag);
        messengerRipple.setOnRippleCompleteListener(messengerListener);
    }
}
