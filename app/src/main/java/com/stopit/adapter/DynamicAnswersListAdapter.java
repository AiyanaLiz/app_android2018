package com.stopit.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.stopit.R;
import com.stopit.models.DynamicAnswer;

import java.util.List;

/**
 * Created by Alenka on 17.06.2018.
 */

public class DynamicAnswersListAdapter<T extends DynamicAnswer> extends BaseQuickAdapter<T, BaseViewHolder> {


    public DynamicAnswersListAdapter(final List<T> answers) {
        super(R.layout.item_dynamic_multi_choice_list, answers);
    }

    public DynamicAnswersListAdapter(final List<T> answers, final boolean singleChoice) {
        super(R.layout.item_dynamic_single_choice_list, answers);
    }

    @Override
    protected void convert(BaseViewHolder viewHolder, DynamicAnswer answer) {
        viewHolder.setText(R.id.item_answer_txt, answer.getLabel());
        viewHolder.setChecked(R.id.item_answer_chk, answer.isSelected());
    }
}
