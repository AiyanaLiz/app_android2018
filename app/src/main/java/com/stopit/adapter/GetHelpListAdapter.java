package com.stopit.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.andexert.library.RippleView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.stopit.R;
import com.stopit.models.CrisisCenter;
import com.stopit.utils.BrandingHelper;
import com.stopit.utils.DateUtils;
import com.stopit.utils.FontHelper;
import com.stopit.views.StopitTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Alenka on 05.07.2018.
 */

public class GetHelpListAdapter<T extends CrisisCenter> extends BaseQuickAdapter<T, BaseViewHolder> {
    private RippleView.OnRippleCompleteListener itemSelectedListener;

    public GetHelpListAdapter(final List<T> contacts, final RippleView.OnRippleCompleteListener itemSelected) {
        super(R.layout.item_get_help_list, contacts);
        this.itemSelectedListener = itemSelected;
    }

    @Override
    protected void convert(BaseViewHolder viewHolder, CrisisCenter item) {
        if (item == null)
            return;

        final AppCompatImageView iconView = viewHolder.getView(R.id.get_help_contact_icon);
        iconView.setImageResource(item.getIconRes());
        if(BrandingHelper.isBrandingColor()){
            BrandingHelper.setIconColor(iconView);
        }

        viewHolder.setImageResource(R.id.get_help_contact_icon, item.getIconRes());
        viewHolder.setText(R.id.get_help_contact_title, item.getName());
        final RippleView rootView = (RippleView) viewHolder.itemView;
        rootView.setTag(item);
        rootView.setOnRippleCompleteListener(itemSelectedListener);

        final StopitTextView titleTxt = viewHolder.getView(R.id.get_help_contact_title);
        titleTxt.setText(item.getName());
        final String fontPath;
        if(item.isPriorityContact()){
            fontPath = "fonts/Montserrat-Bold.ttf";
        }else{
            fontPath = "fonts/Montserrat-Medium.ttf";
        }
        FontHelper.setCustomFont(titleTxt, fontPath, titleTxt.getContext());
    }
}
