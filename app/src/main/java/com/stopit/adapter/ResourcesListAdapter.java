package com.stopit.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.stopit.R;
import com.stopit.models.InformationResource;
import com.stopit.utils.Imager;

import java.util.List;

/**
 * Created by Alenka on 05.07.2018.
 */

public class ResourcesListAdapter<T extends InformationResource> extends BaseQuickAdapter<T, BaseViewHolder> {
    private View.OnClickListener itemClickListener;
    private Context context;


    public ResourcesListAdapter(final Context context, final List<T> incidents, final View.OnClickListener itemClickListener) {
        super(R.layout.item_info_resource_list, incidents);
        this.itemClickListener = itemClickListener;
        this.context = context;
    }


    @Override
    protected void convert(BaseViewHolder viewHolder, InformationResource item) {
        final ImageView imageView = viewHolder.getView(R.id.info_resource_image_view);
        Imager.loadResourceIcon(context, item.getImageUrl(), imageView);
        viewHolder.setText(R.id.info_resource_title_txt, item.getTitle());
        viewHolder.setText(R.id.info_resource_description_txt, item.getDescription());
        final View readMoreTxt = viewHolder.getView(R.id.info_resource_more_btn);
        if (!TextUtils.isEmpty(item.getWebUrl())) {
            readMoreTxt.setTag(item);
            readMoreTxt.setOnClickListener(itemClickListener);
        } else {
            readMoreTxt.setTag(null);
            readMoreTxt.setOnClickListener(null);
        }
    }
}
