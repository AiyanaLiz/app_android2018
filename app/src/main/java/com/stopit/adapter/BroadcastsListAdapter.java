package com.stopit.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.View;

import com.andexert.library.RippleView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.stopit.R;
import com.stopit.models.BroadcastMessage;
import com.stopit.utils.FontHelper;
import com.stopit.views.StopitTextView;

import java.util.List;

import static com.stopit.app.Constants.BROADCAST_MODE_DEFAULT;
import static com.stopit.app.Constants.BROADCAST_MODE_EDIT;

/**
 * Created by Alenka on 17.06.2018.
 */

public class BroadcastsListAdapter<T extends BroadcastMessage> extends BaseQuickAdapter<T, BaseViewHolder> {

    private Context context;
    private RippleView.OnRippleCompleteListener listener;
    private int colorBlack;
    private int colorGray;
    private int mode;


    public BroadcastsListAdapter(final Context context, final List<T> messages, final RippleView.OnRippleCompleteListener listener) {
        super(R.layout.item_notifications_list, messages);
        this.context = context;
        this.listener = listener;

        final Resources res = context.getResources();
        this.colorBlack = res.getColor(R.color.stopit_black);
        this.colorGray = res.getColor(R.color.list_item_date_gray);
        this.mode = BROADCAST_MODE_DEFAULT;
    }

    @Override
    protected void convert(BaseViewHolder viewHolder, BroadcastMessage item) {
        final RippleView rippleView = viewHolder.getView(R.id.notifications_ripple_view);
        rippleView.setTag(item);
        rippleView.setOnRippleCompleteListener(listener);

        final StopitTextView titleView = viewHolder.getView(R.id.notification_label_txt);
        final String font;
        final int textColor;
        if (item.isRead()) {
            font = "fonts/Montserrat-Medium.ttf";
            textColor = colorGray;
        } else {
            font = "fonts/Montserrat-SemiBold.ttf";
            textColor = colorBlack;
        }
        FontHelper.setCustomFont(titleView, font, titleView.getContext());
        titleView.setTextColor(textColor);

        final View statusIconView = viewHolder.getView(R.id.notification_status_icon);
        statusIconView.setVisibility(item.isRead() ? View.GONE : View.VISIBLE);

        viewHolder.setText(R.id.notification_label_txt, item.getTitle());
        viewHolder.setText(R.id.notification_time_txt, item.getDateFormatted());

        viewHolder.setVisible(R.id.notification_arrow, mode == BROADCAST_MODE_DEFAULT);
        final View chkContainer = viewHolder.getView(R.id.notification_chk_container);
        chkContainer.setVisibility(mode == BROADCAST_MODE_EDIT ? View.VISIBLE : View.GONE);
//        viewHolder.setVisible(R.id.notification_chk, mode == BROADCAST_MODE_EDIT);
        viewHolder.setChecked(R.id.notification_chk, item.isSelected());
    }

    public void setMode(final int mode) {
        this.mode = mode;
        notifyDataSetChanged();
    }
}
