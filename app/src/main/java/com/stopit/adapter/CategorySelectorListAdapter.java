package com.stopit.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.stopit.R;
import com.stopit.models.Organization;
import com.stopit.models.ResourceCategory;

import java.util.List;

/**
 * Created by Alenka on 17.06.2018.
 */

public class CategorySelectorListAdapter extends BaseQuickAdapter<ResourceCategory, BaseViewHolder> {


    public CategorySelectorListAdapter(final List<ResourceCategory> entities) {
        super(R.layout.item_organization_selector, entities);
    }


    @Override
    protected void convert(final BaseViewHolder viewHolder, final ResourceCategory item) {
        viewHolder.setText(R.id.org_selector_title, item.getName());
    }
}
