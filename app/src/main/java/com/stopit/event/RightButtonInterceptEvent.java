package com.stopit.event;

import android.os.Bundle;

/**
 * Created by Alenka on 17.07.2018.
 */

public class RightButtonInterceptEvent {

    public Bundle extras;

    public RightButtonInterceptEvent(final Bundle extras){
        this.extras = extras;
    }
}
