package com.stopit.event;

import android.os.Bundle;

/**
 * Created by Alenka on 17.07.2018.
 */

public class UnreadInboxMessageEvent {
    public Bundle extras;

    public UnreadInboxMessageEvent(final Bundle bundle) {
        this.extras = bundle;
    }
}
