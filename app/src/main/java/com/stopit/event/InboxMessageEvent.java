package com.stopit.event;

import android.os.Bundle;

/**
 * Created by Alenka on 17.07.2018.
 */

public class InboxMessageEvent {
    public Bundle extras;

    public InboxMessageEvent(final Bundle extras) {
        this.extras = extras;
    }
}
