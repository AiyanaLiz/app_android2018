package com.stopit.event;

import android.os.Bundle;

/**
 * Created by Alenka on 17.07.2018.
 */

public class UnreadChatMessageEvent {
    public Bundle extras;

    public UnreadChatMessageEvent(final Bundle bundle) {
        this.extras = bundle;
    }
}
