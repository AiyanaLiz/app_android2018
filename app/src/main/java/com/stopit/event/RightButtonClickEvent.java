package com.stopit.event;

import android.os.Bundle;

/**
 * Created by Alenka on 17.07.2018.
 */

public class RightButtonClickEvent {

    public Bundle extras;

    public RightButtonClickEvent(final Bundle extras){
        this.extras = extras;
    }
}
