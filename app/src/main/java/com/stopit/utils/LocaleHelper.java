package com.stopit.utils;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;

import com.stopit.activity.MainActivity;

import java.util.Locale;

public class LocaleHelper {

    public static void setLocale(final Context context, final String language) {
        final Locale locale = new Locale(language);
        Locale.setDefault(locale);

        SharedPrefsHelper.saveSelectedAppLanguage(context, language);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.setLocale(locale);
        res.updateConfiguration(config, res.getDisplayMetrics());
        final Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
        //return context.createConfigurationContext(config);
    }
}
