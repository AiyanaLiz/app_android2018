package com.stopit.utils;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by Alenka on 12.07.2018.
 */

public class NetworkStateHelper {



    public static boolean isNetworkAvailable(final Context context) {
        return isNetworkConnected(context) && isHostReachable();
    }


    private static boolean isNetworkConnected(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm != null && cm.getActiveNetworkInfo() != null;
    }

    public static boolean isHostReachable() {
//        try {
//            InetAddress ipAddr = InetAddress.getByName("google.com");
//            return !ipAddr.getHostName().equals("");
//
//        } catch (Exception e) {
//            return false;
//        }
        return true;
    }
}
