package com.stopit.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.stopit.R;
import com.stopit.activity.StopitActivity;
import com.stopit.models.VersionStatus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class VersionController {

    public static boolean isDeviceOsSupported(final VersionStatus versionStatus) {
        return android.os.Build.VERSION.SDK_INT >= versionStatus.getMinOsVersion();
    }

    public static boolean isAppVersionSupported(final Context context, final VersionStatus versionStatus) {
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return true;
        }
        final long appversion = pInfo.versionCode;
        return appversion >= versionStatus.getMinVersion() && appversion >= versionStatus.getCurrentVersion();
    }
}
