package com.stopit.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class SharedPrefsHelper {
    private static final String TERMS_OF_USE_ACCEPTED_FOR = "termsOfUseAcceptedFor";
    private static final String STOPIT_PREFS = "StopitPreferences";
    private static final String OLD_STOPIT_PREFS = "MyPreferences";
    private static final String ST_KEY_INTELLICODE = "INTELLICODE";
    private static final String ST_KEY_ORG_TYPE = "ORG_TYPE";
    private static final String ST_KEY_LANGUAGE = "stopitAppLanguage";

    public static void setTermsOfUseAccepted(Context context, final long orgId) {
        SharedPreferences preferences = context.getSharedPreferences(STOPIT_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(getKey(orgId), orgId);
        editor.apply();
    }

    public static void setTermsOfUseDeclined(Context context, final long orgId) {
        SharedPreferences preferences = context.getSharedPreferences(STOPIT_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(getKey(orgId));
        editor.apply();
    }

    public static boolean isTermsOfUseAccepted(Context context, final long orgId) {
        SharedPreferences preferences = context.getSharedPreferences(STOPIT_PREFS, Context.MODE_PRIVATE);
        final long acceptedForOrgId = preferences.getLong(getKey(orgId), 0);
        return acceptedForOrgId != 0;
    }

    private static String getKey(final long orgId) {
        return TERMS_OF_USE_ACCEPTED_FOR + String.valueOf(orgId);
    }

    //saves org id with access code as a key
    public static void saveAccessCodeForOrganization(Context context, final String accessCode, final long orgId) {
        SharedPreferences preferences = context.getSharedPreferences(STOPIT_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(accessCode, orgId);
        editor.apply();
    }


    public static long getOrgIdByAccessCode(final Context context, final String accessCode) {
        SharedPreferences preferences = context.getSharedPreferences(STOPIT_PREFS, Context.MODE_PRIVATE);
        return preferences.getLong(accessCode, 0);
    }

    public static String getOldAppIntellicodeSaved(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OLD_STOPIT_PREFS, Context.MODE_PRIVATE);
        final String savedIntellicode = preferences.getString(ST_KEY_INTELLICODE, null);
        Log.d("Stopit 2018", "Stopit old intellicode "+savedIntellicode);
        return savedIntellicode;
    }

//    public static String getOldAppOrgIdSaved(Context context) {
//        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
//        final String savedIntellicode = preferences.getString("INTELLICODE", null);
//        Log.d("Stopit 2018", "Stopit old intellicode "+savedIntellicode);
//        return savedIntellicode;
//    }

    public static void removeOldAppIntellicode(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OLD_STOPIT_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(ST_KEY_INTELLICODE);
        editor.remove(ST_KEY_ORG_TYPE);
        editor.apply();
    }

    public static String getSelectedAppLanguage(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(STOPIT_PREFS, Context.MODE_PRIVATE);
        final String language = preferences.getString(ST_KEY_LANGUAGE, null);
        Log.d("PrefsHelper" , "PrefsHelper restored language "+language);
        return language;
    }

    public static void saveSelectedAppLanguage(Context context, final String language) {
        SharedPreferences preferences = context.getSharedPreferences(STOPIT_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ST_KEY_LANGUAGE, language);
        editor.apply();
        Log.d("PrefsHelper" , "PrefsHelper saved language "+language);
    }
}
