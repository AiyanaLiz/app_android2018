package com.stopit.utils;

import com.stopit.models.AWS;
import com.stopit.models.IncidentAttachment;
import com.stopit.models.messenger.ChatMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alenka on 27.06.2018.
 */

public class StubDataHelper {

//    public static List<MultiItemEntity> getOrganizationsExpandableList() {
//        final List<MultiItemEntity> organizationsList = new ArrayList<>();
//        HeadquartersEntity entity1 = new HeadquartersEntity("first");
//        entity1.addSubItem(new OrganizationEntity("sub 1_1"));
//        entity1.addSubItem(new OrganizationEntity("sub 1_2"));
//        entity1.addSubItem(new OrganizationEntity("sub 1_3"));
//        entity1.addSubItem(new OrganizationEntity("sub 1_4"));
//
//        HeadquartersEntity entity2 = new HeadquartersEntity("second");
//        entity2.addSubItem(new OrganizationEntity("sub 2_1"));
//        entity2.addSubItem(new OrganizationEntity("sub 2_2"));
//        entity2.addSubItem(new OrganizationEntity("sub 2_3"));
//        entity2.addSubItem(new OrganizationEntity("sub 2_4"));
//        entity2.addSubItem(new OrganizationEntity("sub 2_5"));
//        entity2.addSubItem(new OrganizationEntity("sub 2_6"));
//        entity2.addSubItem(new OrganizationEntity("sub 2_7"));
//
//        HeadquartersEntity entity3 = new HeadquartersEntity("third");
//        entity3.addSubItem(new OrganizationEntity("sub 3_1"));
//        entity3.addSubItem(new OrganizationEntity("sub 3_2"));
//        entity3.addSubItem(new OrganizationEntity("sub 3_3"));
//        entity3.addSubItem(new OrganizationEntity("sub 3_4"));
//        entity3.addSubItem(new OrganizationEntity("sub 3_5"));
//        entity3.addSubItem(new OrganizationEntity("sub 3_6"));
//
//        organizationsList.add(entity1);
//        organizationsList.add(entity2);
//        organizationsList.add(entity3);
//        return organizationsList;
//    }

//    public static List<Organization> getOrganizationsList() {
//        final List<Organization> organizationsList = new ArrayList<>();
//        organizationsList.add(new Organization("school"));
//        organizationsList.add(new Organization("school - 1"));
//        organizationsList.add(new Organization("test school 2"));
//        organizationsList.add(new Organization("test school 3"));
//
//        return organizationsList;
//    }

//    public static List<OrgsTreeEntity> getOrgsTree() {
//        final List<OrgsTreeEntity> organizationsList = new ArrayList<>();
//        organizationsList.add(new OrgsTreeEntity("school"));
//        organizationsList.add(new OrgsTreeEntity("school - 1"));
//        organizationsList.add(new OrgsTreeEntity("test school 2"));
//        organizationsList.add(new OrgsTreeEntity("test school 3"));
//
//        return organizationsList;
//    }

//    public static List<InformationResource> getNewsList() {
//        final List<InformationResource> newsList = new ArrayList<>();
//
//        newsList.add(new InformationResource(1, "bulling news", "12345678987"));
//        newsList.add(new InformationResource(2, "incidents news", "aaaaaaaaaa"));
//        newsList.add(new InformationResource(3, "how to avoid", "bbbbbbbbbbb"));
//        newsList.add(new InformationResource(4, "new victim", "vvvvvvvvvvvv"));
//        newsList.add(new InformationResource(5, "other news", "tttttttttt"));
//
//        return newsList;
//    }

//    public static List<CrisisCenter> getHelpContactList() {
//        final List<CrisisCenter> contactsList = new ArrayList<>();
//
//        contactsList.add(new CrisisCenter(1, "call center", "12345678987", null, null));
//        contactsList.add(new CrisisCenter(2, "email resourse", null, null, "call_center@gmail.com"));
//        contactsList.add(new CrisisCenter(3, "web link resource with long long extra long extra extra long name", null, null, "www.google.com"));
//        contactsList.add(new CrisisCenter(2, "mail to resource", null, null, "call_center@gmail.com"));
//        contactsList.add(new CrisisCenter(2, "new get help contact", null, null, "call_center@gmail.com"));
//
//        return contactsList;
//    }

//    public static List<DynamicQuestion> getQuestions() {
//        final List<DynamicQuestion> contactsList = new ArrayList<>();
//        final ArrayList<DynamicAnswer> answers = new ArrayList<>();
//        answers.add(new DynamicAnswer(1, "answer1", 1));
//        answers.add(new DynamicAnswer(2, "answer2", 5));
//        answers.add(new DynamicAnswer(3, "answer3", 4));
//        answers.add(new DynamicAnswer(4, "answer4", 2));
//        answers.add(new DynamicAnswer(5, "answer5", 3));
//        answers.add(new DynamicAnswer(6, "answer6", 6));
//        answers.add(new DynamicAnswer(7, "answer7", 7));
//
//        contactsList.add(new DynamicQuestion(1, "What is your name", Constants.DYNAMIC_QUESTION_TYPE_TEXTBOX_ID, null));
//        contactsList.add(new DynamicQuestion(2, "Send report anonymously", Constants.DYNAMIC_QUESTION_TYPE_CHECKBOX_ID, null));
//        contactsList.add(new DynamicQuestion(3, "What is your favourite food", Constants.DYNAMIC_QUESTION_TYPE_CHK_LIST_ID, answers));
//        contactsList.add(new DynamicQuestion(4, "When did this happend", Constants.DYNAMIC_QUESTION_TYPE_DATE_ID, null));
//        contactsList.add(new DynamicQuestion(5, "What is your favourite color", Constants.DYNAMIC_QUESTION_TYPE_DROPDOWN_ID, answers));
//        contactsList.add(new DynamicQuestion(6, "What is your email", Constants.DYNAMIC_QUESTION_TYPE_EMAIL_ID, null));
//        contactsList.add(new DynamicQuestion(7, "What is your favourite music", Constants.DYNAMIC_QUESTION_TYPE_RADIO_ID, answers));
//        contactsList.add(new DynamicQuestion(8, "What is your favourite lesson", Constants.DYNAMIC_QUESTION_TYPE_RATING_ID, answers));
//        return contactsList;
//    }
//
//    public static List<DynamicAnswer> getDynamicAnswers() {
//        final List<DynamicAnswer> answers = new ArrayList<>();
//
//        answers.add(new DynamicAnswer("answer1"));
//        answers.add(new DynamicAnswer("answer2"));
//        answers.add(new DynamicAnswer("answer3"));
//        answers.add(new DynamicAnswer("answer4"));
//        answers.add(new DynamicAnswer("answer5"));
//        answers.add(new DynamicAnswer("answer6"));
//
//        return answers;
//    }
//
//    public static List<Incident> getIncidentsList() {
//        final List<Incident> incidentsList = new ArrayList<>();
//        incidentsList.add(new Incident(111, System.currentTimeMillis(), "Pending", "description"));
//        incidentsList.add(new Incident(222, System.currentTimeMillis(), "Pending", "description"));
//        incidentsList.add(new Incident(333, System.currentTimeMillis(), "Pending", "description"));
//        incidentsList.add(new Incident(444, System.currentTimeMillis(), "Pending", "description"));
//        incidentsList.add(new Incident(555, System.currentTimeMillis(), "Pending", "description"));
//
//        return incidentsList;
//    }

//    public static List<BroadcastMessage> getBroadcastMessages() {
//        final List<BroadcastMessage> organizationsList = new ArrayList<>();
//        organizationsList.add(new BroadcastMessage("2343292:38da0650-7696-4a49-ac91-b68f232de204", "Message 1",
//                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
//                "2018-08-10T09:55:10.000000Z", 1));
////        organizationsList.add(new BroadcastMessage("109577:02733fb2-bd12-4d65-b817-c33e4f0be8cd1", "Message 2", "details 2", "2018-08-10T10:55:10.000000Z", 7));
////        organizationsList.add(new BroadcastMessage("109577:02733fb2-bd12-4d65-b817-c33e4f0be8cd2", "Message 3", "details 3", "2018-08-10T09:55:10.000000Z", 2));
////        organizationsList.add(new BroadcastMessage("109577:02733fb2-bd12-4d65-b817-c33e4f0be8cd3", "Message 4", "details 4", "2018-08-10T08:55:10.000000Z", 1));
//
//        return organizationsList;
//    }

//    public static List<IncidentAttachment> getAttachments() {
//        final ArrayList<IncidentAttachment> attachments = new ArrayList<>();
//        attachments.add(new IncidentAttachment("jpg", "path"));
//        attachments.add(new IncidentAttachment("png", "path"));
//        return attachments;
//    }

//    public static List<ChatMessage> getChatMessages() {
//        final ArrayList<ChatMessage> messages = new ArrayList<>();
//        messages.add(new ChatMessage(1,
//                "Through the above picture, we can see that the second line is the third line item also ImageView, this time ImageView can be reused, we can use GridLayoutManager", "25.08.17"));
//        messages.add(new ChatMessage(2, "Recently few hours before i solved the problem and the problem is in Manifest file. I just changed \"com.google.firebase.MESSAGE_EVENT\" to \"com.google.firebase.MESSAGING_EVENT\"..that solved my problem..by the way thanks for your effort!",
//                "24.07.18"));
//        messages.add(new ChatMessage(3,
//                "Th",
//                "15.06.18"));
//        messages.add(new ChatMessage(4,
//                "Te",
//                "12:53"));
//        return messages;
//    }

//    public static String getStubBgUrl() {
//        //return "http://umelyremont.ru/imgumel/osteklenie_fasadov_zdanij.jpg";
//        return "https://images.pexels.com/photos/940308/pexels-photo-940308.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260";
//        //return "http://caucatalog.narod.ru/mtxt/mtxt_23.jpg";
//    }
//
//    public static String getStubLogoUrl() {
//        return "";
//        //return "https://logos.flamingtext.com/City-Logos/Gallows-Logo.png";
//        //return "http://bipbap.ru/wp-content/uploads/2017/06/tmb_145037_6611.jpg";
//        //return "http://www.radionetplus.ru/uploads/posts/2013-07/1374306712_krasivye-oboi-1.jpg";
//    }
//
//    public static AWS getPreprodAws() {
//        //staging
//        return new AWS("us-east-1:4eca380b-943d-4149-870e-45cbd8032a93", "us-east-1", "stopit-private-staging");
//        //current production
//        //return new AWS("us-east-1:d20c19ac-2bd4-4cc2-98fb-99b317631ee9", "us-east-1", "stopit-private");
//    }
}
