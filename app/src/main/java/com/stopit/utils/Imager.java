package com.stopit.utils;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.signature.ObjectKey;
import com.stanko.tools.DeviceInfo;
import com.stanko.tools.ImageUtils;
import com.stanko.tools.ResHelper;
import com.stopit.R;
import com.stopit.activity.OnDownloadFinishedCallback;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.stopit.app.Constants.BRANDING_IMG_GIF_FILE_SUFIX;
import static com.stopit.app.Constants.BRANDING_IMG_JPG_FILE_SUFIX;
import static com.stopit.app.Constants.BRANDING_IMG_PNG_FILE_SUFIX;

public class Imager {

    private static final float SCREEN_WIDTH_INDEX = 0.8f;
    private static final float SCREEN_WIDTH_SMALL_INDEX = 0.45f;

    public static void loadTumbnail(final Context context, final String path, final ImageView target) {
        Glide.with(context)
                .load(path)
                //.thumbnail(0.1f)
                .apply(new RequestOptions().override(72, 72).centerCrop())
                .into(target);
    }

    public static void loadNewsIcon(final Context context, final String path, final ImageView target) {
        Glide.with(context)
                .load(path)
                .apply(new RequestOptions().override(72, 72).centerCrop().placeholder(R.drawable.ic_placeholder_small))
                .into(target);
    }

    public static void loadResourceIcon(final Context context, final String path, final ImageView target) {
        Glide.with(context)
                .load(path)
                .apply(new RequestOptions().centerCrop().placeholder(R.drawable.ic_placeholder_big))
                .into(target);
    }

    public static void loadImageFromFile(final Context context, final String filePath, final ImageView imageView) {
        Glide.with(context)
                .load(filePath)
                .apply(new RequestOptions().skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).centerInside().format(DecodeFormat.PREFER_ARGB_8888))
                .into(imageView);
    }


    public static void downloadImage(final Context context, final String url, final String filePath, final OnDownloadFinishedCallback callback) {
        final int displayWidth = DeviceInfo.getDisplayPortraitWidth();
        final int displayHeight = DeviceInfo.getDisplayPortraitHeight();
        Glide.with(context)
                .asBitmap()
                .load(url)
                .apply(new RequestOptions().override(displayWidth, displayHeight).format(DecodeFormat.PREFER_ARGB_8888))
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap bitmap, Transition<? super Bitmap> transition) {
                        final String extension = url.substring(url.lastIndexOf(".", url.length()));
                        Log.d("Imager", "bg image width " + bitmap.getWidth());
                        Log.d("Imager", "bg image height " + bitmap.getHeight());

                        saveFile(bitmap, filePath, extension);

                        final ImageView target = new ImageView(context);
                        target.setVisibility(View.INVISIBLE);
                        target.setImageBitmap(bitmap);

                        if (callback != null)
                            callback.onDownloadFinished();
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        if (callback != null)
                            callback.onDownloadFinished();
                    }
                });
    }

    public static void downloadLogoImage(final Context context, final String url, final String bigLogoPath,
                                         final String smallLogoPath,
                                         final OnDownloadFinishedCallback callback) {
        Glide.with(context)
                .asBitmap()
                .load(url)
                .apply(new RequestOptions().signature(new ObjectKey(System.currentTimeMillis())))
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap bitmap, Transition<? super Bitmap> transition) {
                        final String extension = url.substring(url.lastIndexOf(".", url.length()));
                        final ImageView target = new ImageView(context);
                        target.setVisibility(View.INVISIBLE);

                        Log.d("Imager", "logo image width " + bitmap.getWidth());
                        Log.d("Imager", "logo image height " + bitmap.getHeight());

                        final Bitmap bigLogoBitmap = getLogoResized(bitmap);
                        saveFile(bigLogoBitmap, bigLogoPath, extension);
                        target.setImageBitmap(bigLogoBitmap);

                        final Bitmap smallLogoBitmap = getSmallLogoResized(bitmap);
                        saveFile(smallLogoBitmap, smallLogoPath, extension);
                        target.setImageBitmap(smallLogoBitmap);

                        if (callback != null)
                            callback.onDownloadFinished();
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        if (callback != null)
                            callback.onDownloadFinished();
                    }
                });

//        Glide.with(context)
//                //.asGif()
//                .download(url)
//                .into(new SimpleTarget<File>() {
//                          @Override
//                          public void onResourceReady(@NonNull File resource, @Nullable Transition<? super File> transition) {
//
//                          }
//                      });
//        File file = new File(fileName);
//
//        FileOutputStream fileWriter = new FileOutputStream(file);
//        fileWriter.write(bytes);
//        fileWriter.flush();
//        fileWriter.close();
    }


    private static Bitmap getLogoResized(final Bitmap initialBitmap) {
        final int maxLogoWidth = ResHelper.getDimensPx(R.dimen.max_logo_width);
        final int maxLogoHeight = ResHelper.getDimensPx(R.dimen.max_logo_height);
        Log.d("Imager", "maxWidth big logo " + maxLogoWidth);
        Log.d("Imager", "maxHeight big logo " + maxLogoHeight);

        int imageHeight = initialBitmap.getHeight();
        int imageWidth = initialBitmap.getWidth();
        final float ratio = (float) imageWidth / (float) imageHeight;

        if (imageWidth <= maxLogoWidth && imageHeight <= maxLogoHeight) {
            return initialBitmap;
        }

        //if logo still too long - downsize once more
        if (imageWidth > maxLogoWidth) {
            imageWidth = maxLogoWidth;
            imageHeight = (int) (maxLogoWidth / ratio);
        }

        if (imageHeight > maxLogoHeight) {
            imageHeight = maxLogoHeight;
            imageWidth = (int) ((float) maxLogoHeight * ratio);
        }

        final Bitmap resizedBitmap = ImageUtils.getResizedBitmap(initialBitmap, imageHeight, imageWidth);
        Log.d("Imager", "resizedBitmap big logo width " + resizedBitmap.getWidth() + " height " + resizedBitmap.getHeight());
        return resizedBitmap;
    }


    private static Bitmap getSmallLogoResized(final Bitmap initialBitmap) {
        int maxLogoHeight = ResHelper.getDimensPx(R.dimen.max_small_logo_height);
        int maxLogoWidth = ResHelper.getDimensPx(R.dimen.max_small_logo_width);
        Log.d("Imager", "maxWidth small logo " + maxLogoWidth);
        Log.d("Imager", "maxHeight small logo " + maxLogoHeight);

        int imageWidth = initialBitmap.getWidth();
        int imageHeight = initialBitmap.getHeight();
        final float ratio = (float) imageWidth / (float) imageHeight;

        //if logo fits the toolbar logo space
        if (imageWidth <= maxLogoWidth && imageHeight <= maxLogoHeight)
            return initialBitmap;

        //if logo is too height - downsize
        if (imageHeight > maxLogoHeight) {
            imageHeight = maxLogoHeight;
            imageWidth = (int) ((float) maxLogoHeight * ratio);
        }

        //if logo still too long - downsize once more
        if (imageWidth > maxLogoWidth) {
            imageWidth = maxLogoWidth;
            imageHeight = (int) ((float) maxLogoWidth / ratio);
        }

        final Bitmap resizedBitmap = ImageUtils.getResizedBitmap(initialBitmap, imageHeight, imageWidth);
        Log.d("Imager", "resizedBitmap small logo width " + resizedBitmap.getWidth() + " height " + resizedBitmap.getHeight());
        return resizedBitmap;
    }

    private static void saveFile(final Bitmap bitmap, final String path, final String extension) {
        final String pngFilePath = path + BRANDING_IMG_PNG_FILE_SUFIX;
        final String jpgFilePath = path + BRANDING_IMG_JPG_FILE_SUFIX;
        final String gifFilePath = path + BRANDING_IMG_GIF_FILE_SUFIX;

        final File pngFile = new File(pngFilePath);
        if (pngFile.exists() && pngFile.canWrite()) {
            final boolean deleteResult = pngFile.delete();
            Log.d("Imager", "delete result " + deleteResult + " file path " + path);
        }

        final File jpgFile = new File(jpgFilePath);
        if (jpgFile.exists() && jpgFile.canWrite()) {
            final boolean deleteResult = jpgFile.delete();
            Log.d("Imager", "delete result " + deleteResult + " file path " + path);
        }

        final File gifFile = new File(gifFilePath);
        if (gifFile.exists() && gifFile.canWrite()) {
            final boolean deleteResult = gifFile.delete();
            Log.d("Imager", "delete result " + deleteResult + " file path " + path);
        }

        if (extension.contains("png"))
            ImageUtils.saveBitmapToPNGFile(bitmap, pngFile);
        else if (extension.contains("gif"))
            saveBitmapToGIFFile(bitmap, gifFile);
        else
            ImageUtils.saveBitmapToJPEGFile(bitmap, jpgFile, 100);
    }

    private static void saveBitmapToGIFFile(final Bitmap bitmap, final File gifFile) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);

            byte[] bitmapByteArray = outputStream.toByteArray();

            FileOutputStream fos = new FileOutputStream(gifFile);
            fos.write(bitmapByteArray);

            fos.flush();
            fos.close();

            outputStream.flush();
            outputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
