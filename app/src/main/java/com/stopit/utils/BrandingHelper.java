package com.stopit.utils;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.support.v4.graphics.ColorUtils;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;

import com.kyleduo.switchbutton.SwitchButton;
import com.stopit.R;
import com.stopit.app.StopitApp;
import com.stopit.db.DBHelper;
import com.stopit.models.Branding;
import com.stopit.views.StopitRadioButton;

import java.io.File;

import static com.stopit.app.Constants.BRANDING_IMG_JPG_FILE_SUFIX;
import static com.stopit.app.Constants.BRANDING_IMG_PNG_FILE_SUFIX;

/**
 * Created by Alenka on 06.07.2018.
 */

public class BrandingHelper {
    private static final String BRANDING_BG_FILE_NAME = "background_";
    private static final String BRANDING_LOGO_FILE_NAME = "logo_";
    private static final String BRANDING_SMALL_LOGO_FILE_NAME = "small_logo_";

    private static Branding instance;

    //no extension
    public static String getPathForBackground(final long organizationId) {
        return StopitApp.getAppContext().getFilesDir()
                + File.separator
                + BRANDING_BG_FILE_NAME
                + String.valueOf(organizationId);
    }

    //no extension
    public static String getPathForLogo(final long organizationId) {
        return StopitApp.getAppContext().getFilesDir()
                + File.separator
                + BRANDING_LOGO_FILE_NAME
                + String.valueOf(organizationId);
    }


    //no extension
    public static String getPathForSmallLogo(final long organizationId) {
        return StopitApp.getAppContext().getFilesDir()
                + File.separator
                + BRANDING_SMALL_LOGO_FILE_NAME
                + String.valueOf(organizationId);
    }

    public static void init() {
        instance = DBHelper.getBranding();
    }

    public static boolean isBranding() {
        if (instance == null)
            init();

        return instance != null;
    }

    public static boolean isBrandingColor() {
        return false;
    }

    public static int getMainColor(Context context) {
        return context.getResources().getColor(R.color.colorPrimary);
    }

    public static Branding getBranding() {
        if (instance == null)
            init();

        return instance;
    }


    public static void clearBranding(final boolean isClearAssetsNeeded) {
        instance = null;
        if (isClearAssetsNeeded)
            clearBrandingAssets();
    }

    public static void clearBrandingAssets() {
        final long currentOrgId = DBHelper.getCurrentOrganizationId();
        if (currentOrgId == 0)
            return;

        //delete branding assets
        final File smallLogoFilePNG = new File(getPathForSmallLogo(currentOrgId) + BRANDING_IMG_PNG_FILE_SUFIX);
        if (smallLogoFilePNG.exists() && smallLogoFilePNG.canWrite()) {
            final boolean deleted = smallLogoFilePNG.delete();
            Log.d("BrandingHelper", "deleted png small logo " + deleted);
        }

        final File smallLogoFileJPG = new File(getPathForSmallLogo(currentOrgId) + BRANDING_IMG_JPG_FILE_SUFIX);
        if (smallLogoFileJPG.exists() && smallLogoFileJPG.canWrite()) {
            final boolean deleted = smallLogoFileJPG.delete();
            Log.d("BrandingHelper", "deleted jpg small logo " + deleted);
        }

        final File logoFilePNG = new File(getPathForLogo(currentOrgId) + BRANDING_IMG_PNG_FILE_SUFIX);
        if (logoFilePNG.exists() && logoFilePNG.canWrite()) {
            final boolean deleted = logoFilePNG.delete();
            Log.d("BrandingHelper", "deleted png logo " + deleted);
        }

        final File logoFileJPG = new File(getPathForLogo(currentOrgId) + BRANDING_IMG_JPG_FILE_SUFIX);
        if (logoFileJPG.exists() && logoFileJPG.canWrite()) {
            final boolean deleted = logoFileJPG.delete();
            Log.d("BrandingHelper", "deleted jpg logo " + deleted);
        }

        final File bgFilePNG = new File(getPathForBackground(currentOrgId) + BRANDING_IMG_PNG_FILE_SUFIX);
        if (bgFilePNG.exists() && bgFilePNG.canWrite())
            bgFilePNG.delete();

        final File bgFileJPG = new File(getPathForBackground(currentOrgId) + BRANDING_IMG_JPG_FILE_SUFIX);
        if (bgFileJPG.exists() && bgFileJPG.canWrite())
            bgFileJPG.delete();
    }


    public static void setBackgroundDrawableColor(final View viewToBrand) {
        final int color = BrandingHelper.getMainColor(viewToBrand.getContext());
        Drawable background = viewToBrand.getBackground();
        if (background == null)
            return;

        if (background instanceof ShapeDrawable) {
            ShapeDrawable shapeDrawable = (ShapeDrawable) background;
            shapeDrawable.getPaint().setColor(color);

        } else if (background instanceof GradientDrawable) {
            GradientDrawable gradientDrawable = (GradientDrawable) background;
            gradientDrawable.setColor(color);

        } else if (background instanceof ColorDrawable) {
            // alpha value may need to be set again after this call
            ColorDrawable colorDrawable = (ColorDrawable) background;
            colorDrawable.setColor(color);

        } else if (background instanceof LayerDrawable) {
            LayerDrawable layerDrawable = (LayerDrawable) background;
            GradientDrawable drawable = (GradientDrawable) layerDrawable.findDrawableByLayerId(R.id.button_layer);
            if (drawable != null)
                drawable.setColor(color);
        }
    }

    public static void setBorderColor(final View viewToBrand) {
        final int color = BrandingHelper.getMainColor(viewToBrand.getContext());
        Drawable background = viewToBrand.getBackground();
        if (background != null && background instanceof GradientDrawable) {
            GradientDrawable gradientDrawable = (GradientDrawable) background;
            gradientDrawable.setStroke(3, color);
        }
    }

    public static void setBackground(final View viewToBrand) {
        final int color = BrandingHelper.getMainColor(viewToBrand.getContext());
        viewToBrand.setBackgroundColor(color);
    }

    public static void setTextColor(final AppCompatTextView viewToBrand) {
        final int color = BrandingHelper.getMainColor(viewToBrand.getContext());
        viewToBrand.setTextColor(color);
    }

    public static void setTextColor(final AppCompatEditText viewToBrand) {
        final int color = BrandingHelper.getMainColor(viewToBrand.getContext());
        viewToBrand.setTextColor(color);
        viewToBrand.setHintTextColor(color);
    }

    public static void setEditTextColor(final AppCompatEditText viewToBrand) {
        final int color = BrandingHelper.getMainColor(viewToBrand.getContext());
        viewToBrand.setTextColor(color);
    }

    public static ColorStateList getTextStatesColors(Context context, final int enabledColorRes) {
        final Resources res = context.getResources();
        final int activeColor = BrandingHelper.isBrandingColor() ? BrandingHelper.getMainColor(context)
                : res.getColor(R.color.colorPrimary);
        final int enabledColor = res.getColor(enabledColorRes);

        return new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_checked},
                        new int[]{android.R.attr.state_selected},
                        new int[]{android.R.attr.state_pressed},
                        new int[]{-android.R.attr.state_checked}},
                new int[]{
                        activeColor, activeColor, activeColor, enabledColor});
    }

    public static void setCheckBoxBtnColor(final AppCompatCheckBox viewToBrand) {
        Context context = viewToBrand.getContext();
        final int color = BrandingHelper.getMainColor(context);

        Drawable checkedDrawable = changeIconColor(context, R.drawable.ic_checkbox_big, color);
        if (checkedDrawable == null)
            return;

        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked},
                checkedDrawable);
        states.addState(new int[]{},
                context.getResources().getDrawable(R.drawable.ic_checkbox_big_inactive));
        viewToBrand.setButtonDrawable(states);
    }

    public static void setDropdownBtnColor(final CheckBox viewToBrand) {
        Context context = viewToBrand.getContext();
        final int color = BrandingHelper.getMainColor(context);

        Drawable checkedDrawable = changeIconColor(context, R.drawable.ic_dropdown_up, color);
        Drawable uncheckedDrawable = changeIconColor(context, R.drawable.ic_dropdown_down, color);
        if (checkedDrawable == null || uncheckedDrawable == null)
            return;

        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked}, checkedDrawable);
        states.addState(new int[]{}, uncheckedDrawable);
        viewToBrand.setButtonDrawable(states);
    }

    public static void changeRadioBtnStyle(final Context context, final StopitRadioButton viewToBrand, final int iconNormalRes, final int iconCheckedRes) {
        final int color = BrandingHelper.getMainColor(context);
        final int colorEnabled = context.getResources().getColor(R.color.tab_text_gray);

        Drawable checkedDrawable = changeIconColor(context, iconCheckedRes, color);
        if (checkedDrawable == null)
            return;

        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked},
                checkedDrawable);
        states.addState(new int[]{},
                context.getResources().getDrawable(iconNormalRes));
        viewToBrand.setCompoundDrawablesWithIntrinsicBounds(null, states, null, null);

        ///// text settings
        final ColorStateList textStates = new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_checked},
                        new int[]{-android.R.attr.state_checked}},
                new int[]{
                        color, colorEnabled});
        viewToBrand.setTextColor(textStates);
    }

    public static StateListDrawable getTabDrawableStateList(final Context context, final int iconNormalRes, final int iconActiveRes) {
        final Resources resources = context.getResources();
        final Drawable normalDrawable = resources.getDrawable(iconNormalRes);

        Drawable activeDrawable = null;
        if (isBrandingColor())
            activeDrawable = changeIconColor(context, iconActiveRes, getMainColor(context));

        if (activeDrawable == null)
            activeDrawable = resources.getDrawable(iconActiveRes);

        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked}, activeDrawable);
        states.addState(new int[]{android.R.attr.state_selected}, activeDrawable);
        states.addState(new int[]{android.R.attr.state_pressed}, activeDrawable);
        states.addState(new int[]{-android.R.attr.state_checked}, normalDrawable);

        return states;
    }

    public static void setSwitchColor(final Context context, final SwitchButton btn) {
        final int color = BrandingHelper.getMainColor(context);
        final int semitransparentColor = ColorUtils.setAlphaComponent(color, 60);
        final int colorEnabled = context.getResources().getColor(R.color.delimiter_gray);
        final int colorEnabledSemitr = context.getResources().getColor(R.color.delimiter_gray_opacity_40);

        ///// thumb color states
        final ColorStateList thumbStates = new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_checked},
                        new int[]{-android.R.attr.state_checked}},
                new int[]{
                        color, colorEnabled});

        btn.setThumbColor(thumbStates);

        ///// track color states
        final ColorStateList trackStates = new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_checked},
                        new int[]{-android.R.attr.state_checked}},
                new int[]{
                        semitransparentColor, colorEnabledSemitr});
        btn.setBackColor(trackStates);
    }

    /////////////////  icons /////////////////////////////

    public static void setIconColor(final AppCompatImageView imageView) {
        Context context = imageView.getContext();
        final int color = BrandingHelper.getMainColor(context);

        Drawable icon = imageView.getDrawable();
        icon = changeDrawableColor(context, icon, color);
        if (icon == null)
            return;

        imageView.setImageDrawable(icon);
    }

    public static void setIconColor(final ImageButton imageBtn) {
        Context context = imageBtn.getContext();
        final int color = BrandingHelper.getMainColor(context);

        Drawable icon = imageBtn.getDrawable();
        icon = changeDrawableColor(context, icon, color);
        if (icon == null)
            return;

        imageBtn.setImageDrawable(icon);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////


    public static Drawable changeIconColor(final Context context, final int iconRes, final int color) {
        Drawable mDrawable = context.getResources().getDrawable(iconRes);
        if (mDrawable != null) {
            mDrawable = changeDrawableColor(context, mDrawable, color);
        }
        return mDrawable;
    }

    private static Drawable changeDrawableColor(final Context context, final Drawable drawable, final int color) {
        drawable.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP));

        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
            return drawable;

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        Drawable layer = new LayerDrawable(new Drawable[]{drawable});
        layer.setBounds(0, 0, layer.getIntrinsicWidth(), layer.getIntrinsicHeight());
        layer.draw(canvas);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    public static String getHomeScreenMessage() {
        return isBranding() ? instance.getHomeMessage() : null;
    }

}
