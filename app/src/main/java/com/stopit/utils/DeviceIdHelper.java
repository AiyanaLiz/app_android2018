package com.stopit.utils;

import android.content.Context;
import android.provider.Settings;

/**
 * Created by Alenka on 11.07.2018.
 */

public class DeviceIdHelper {

    public static String getDeviceID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }
}
