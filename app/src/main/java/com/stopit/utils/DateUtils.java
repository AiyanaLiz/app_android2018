package com.stopit.utils;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy", Locale.getDefault());
    private static final SimpleDateFormat newIncidentDateFormat = new SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault());
    private static final SimpleDateFormat mediaFileDateFormat = new SimpleDateFormat("MM_dd_yyyy_HHmmss", Locale.getDefault());
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
    //private static final SimpleDateFormat helpContactDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault());
    private static final SimpleDateFormat incidentDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault());
    private static final SimpleDateFormat incidentDateFormatTZ = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
    private static final SimpleDateFormat incidentDateFormatLong = new SimpleDateFormat("MM.dd.yy KK:mm a", Locale.getDefault());
    private static final SimpleDateFormat dateFormatShort = new SimpleDateFormat("MM.dd.yy", Locale.getDefault());
    /*2018-08-23 07:22:14*/
    private static final SimpleDateFormat chatMessageDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    private static final SimpleDateFormat chatDateFormat = new SimpleDateFormat("yyyy-MM-dd h:mm:ss a", Locale.getDefault());


    public static String getMediaFilePrefixFormatted(final long date) {
        if (date <= 0)
            return "";

        return mediaFileDateFormat.format(date);
    }

    public static String getIncidentDateFormatted(String dateString) {
        if (TextUtils.isEmpty(dateString))
            return "";

        final Date date;
        try {
            date = incidentDateFormatTZ.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

        return dateFormatShort.format(date);
    }

    public static String getDateFormatted(String dateString) {
        if (TextUtils.isEmpty(dateString))
            return "";

        final Date date;
        try {
            date = incidentDateFormatTZ.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

        return dateFormatShort.format(date);
    }

    public static String getDateFormatted(long millis) {
        return dateFormatShort.format(millis);
    }

    public static String getChatDateFormatted(long millis) {
        return chatDateFormat.format(millis);
    }

    public static long getDateMillis(final String dateString) {
        if (TextUtils.isEmpty(dateString))
            return 0;


        try {
            final Date date = incidentDateFormatTZ.parse(dateString);
            return date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static long getChatMessageDateMillis(final String dateString) {
        if (TextUtils.isEmpty(dateString))
            return 0;

        try {
            //chatMessageDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            final Date date = chatMessageDateFormat.parse(dateString);

            return date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
