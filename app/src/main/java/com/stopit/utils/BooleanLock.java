package com.stopit.utils;

/**
 * Created by Alenka on 12.07.2018.
 */

public class BooleanLock {
    private boolean isLocked;

    public BooleanLock() {
    }

    public synchronized boolean setRunning() {
        if (this.isLocked) {
            return false;
        } else {
            this.isLocked = true;
            return true;
        }
    }

    public synchronized boolean isRunning() {
        return this.isLocked;
    }

    public synchronized void setFinished() {
        this.isLocked = false;
    }

    public synchronized boolean isFinished() {
        return !this.isLocked;
    }

}
