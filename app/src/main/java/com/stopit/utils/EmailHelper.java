package com.stopit.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alenka on 23.03.2018.
 */

public class EmailHelper {
    private static final String[] knownPackages = new String[]{"com.google.android.gm", "com.google.android.apps.inbox", "com.appple.app.email", "ru.mail.mailapp", "com.microsoft.office.outlook", "com.my.mail", "com.yahoo.mobile.client.android.mail", "com.cloudmagic.mail", "cz.seznam.email", "com.email.email", "com.trtf.blue", "me.bluemail.mail", "com.fsck.k9", "com.mailboxapp", "com.syntomo.email", "org.kman.aquamail", "com.mobincube.android.sc_15ibz", "ru.yandex.mail", "net.daum.android.solmail", "com.boxer.email", "com.aol.mobile.aolapp", "com.mobincube.android.sc_gaz7l", "com.xiaomistudio.tools.finalmail", "com.mail.emails", "co.itspace.emailproviders", "de.gmx.mobile.android.mail", "com.yahoo.mobile.client.android.im", "com.mail.mobile.android.mail", "com.asus.email", "com.maildroid", "com.wemail", "de.web.mobile.android.mail", "com.onegravity.k10.free", "com.dicklucifer.email", "de.freenet.mail", "com.qs.enhancedemail", "com.feistapps.anonymousemail", "com.onegravity.k10.pro2", "com.gloxandro.birdmail", "com.kaitenmail", "com.android.email", "com.sec.android.email", "com.htc.android.mail"};
    private static String EMH_DEFAULT_PICKER_TITLE = "Send EMail using:";
    private static String EMH_DEFAULT_SECURITY_EXCEPTION_ERROR_MESSAGE = "Sending EMail has been forbidden by permissions";
    private static String EMH_DEFAULT_NO_ASSOCIATED_APP_ERROR_MESSAGE = "No EMail app has been found";


    /**
     * Checks if given EMail address might be a valid one.
     *
     * @param email - String, EMail address to check
     * @return
     */
    public static boolean isValidEmail(final String email) {
        if (TextUtils.isEmpty(email))
            return false;
        // https://en.wikipedia.org/wiki/Domain_Name_System
        // a label may contain zero to 63 characters
        // subdivisions may have up to 127 levels
        // but total domain name 253 chars max
        final String expression = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,63}";
        final Pattern p = Pattern.compile(expression);
        final Matcher m = p.matcher(email);
        if (m.matches()) {
            final String domainPart = expression.substring(expression.indexOf("@") + 1);
            final String[] domainParts = domainPart.split(".");
            if (domainParts.length < 127) {
                // check labels
                for (String domainLabel : domainParts) {
                    if (domainLabel.length() < 1 || domainLabel.length() > 63)
                        return false;
                }
                return domainPart.length() < 254;
            } else
                return false;
        } else
            return false;
    }

    public static boolean sendEmail(Context context, String receiver, String subject, String securityExceptionMessage, String noAssociatedAppErrorMessage) {
        boolean isEmailSent = false;
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri data = Uri.parse("mailto:" + receiver + "?subject=" + subject + "&body=");
            intent.setData(data);
            context.startActivity(intent);
            isEmailSent = true;
//            Intent i = new Intent(Intent.ACTION_SEND);
//            i.setType("message/rfc822");
//            i.putExtra(Intent.EXTRA_EMAIL, new String[]{receiver});
//            i.putExtra(Intent.EXTRA_SUBJECT, subject);
//            context.startActivity(Intent.createChooser(i, "Send mail..."));
//            isEmailSent = true;

        } catch (ActivityNotFoundException ex) {
            Toast.makeText(context, noAssociatedAppErrorMessage, Toast.LENGTH_SHORT).show();
            isEmailSent = false;

        } catch (SecurityException e) {
            Toast.makeText(context, securityExceptionMessage, Toast.LENGTH_SHORT).show();
            isEmailSent = false;
        }
        return isEmailSent;
    }
}
