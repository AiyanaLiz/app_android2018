package com.stopit.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.stopit.activity.StopitActivity;

import java.io.File;
import java.io.IOException;

/**
 * Created by Alenka on 28.06.2018.
 */

public class CameraHelper {

    public static boolean hasCamera() {
        return Camera.getNumberOfCameras() > 0;
    }

    public static boolean isExternalStorageAvailable(){
        //available & writable
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }







}
