package com.stopit.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.os.Build;

import com.stopit.R;
import com.stopit.activity.StopitActivity;

import pub.devrel.easypermissions.EasyPermissions;

import static com.stopit.app.Constants.LOCATION_REQUEST_PERMISSION_CODE;

/**
 * Created by Alenka on 02.07.2018.
 */

public class StopitPermissionsHelper {

    public static boolean isCameraPermitted(final Activity activity){
        return EasyPermissions.hasPermissions(activity, Manifest.permission.CAMERA);
    }

    public static void requestCameraPermission(final Activity activity, final int requestCode){
        EasyPermissions.requestPermissions(activity, activity.getString(R.string.camera_permission_msg), requestCode, Manifest.permission.CAMERA);
    }

    public static boolean isExternalStorageReadWritePermitted(final Activity activity){
        return EasyPermissions.hasPermissions(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    public static void requestExternalStoragePermission(final Activity activity, final int requstPermissionCode){
        EasyPermissions.requestPermissions(activity, activity.getString(R.string.sdcard_permission_msg),
                requstPermissionCode, //EXTERNAL_STORAGE_REQUEST_PERMISSION_CODE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    public static boolean isLocationPermitted(final Activity activity){
        return EasyPermissions.hasPermissions(activity, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    public static void requestLocationPermission(final Activity activity){
        EasyPermissions.requestPermissions(activity, activity.getString(R.string.location_permission_msg), LOCATION_REQUEST_PERMISSION_CODE, Manifest.permission.ACCESS_FINE_LOCATION);
    }

}
