package com.stopit.utils;

import android.text.TextUtils;
import android.util.Base64;

import com.google.gson.JsonObject;
import com.stopit.db.DBHelper;
import com.stopit.models.User;

import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import static com.stopit.api.ApiKeys.ST_API_KEY_DATA;


public class EncryptionHelper {

    private static final byte[] iv = getFixedIV();

    public static String getSpecialKey() {
        char[] special_chars = new char[]{'b', 'd', '2', '5', '3', '0', 'a', '7', '4', 'w', '1', '9', 'b', 'c', '0', '6', 'a', 'l', 'x', 'g', 'h', '8', 'k', 's', 'b', 'd', '2', '5', '3', '0', 'a', '7'};
        return new String(special_chars);
    }

    private static byte[] getFixedIV() {
        char[] iv = new char[]{'0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'};
        return new String(iv).getBytes();
    }

    public static String getRandomIV(int len) {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        return uuid.substring(len);
    }

    public static String getEncryptionSalt() {
        String encryptionSalt = null;
        final User user = DBHelper.getUser();
        if (user != null) {
            final String intellicode = user.getIntelliCode();
            final String salt = user.getSalt();
            if (!TextUtils.isEmpty(intellicode) && !TextUtils.isEmpty(salt))
                encryptionSalt = intellicode + salt; //"20151c5464c7"+"ba65c4e6c26d45ee806f";//intellicode + salt;
        }
        return encryptionSalt;
    }

    ////////////////////////// with key only, no IV //////////////////////////////////////////

    public static String encryptNoIV(final String key, String stringToEncrypt) throws Exception {
        byte[] input = stringToEncrypt.getBytes("utf-8");
        SecretKeySpec skc = new SecretKeySpec(key.getBytes("utf-8"), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, skc);

        byte[] cipherText = new byte[cipher.getOutputSize(input.length)];
        int ctLength = cipher.update(input, 0, input.length, cipherText, 0);
        cipher.doFinal(cipherText, ctLength);
        return Base64.encodeToString(cipherText, Base64.CRLF);
    }

    public static String decryptNoIV(String encrypted) throws Exception {
        final String seed = getSpecialKey();

        byte[] encryptedBytes = Base64.decode(encrypted.getBytes("utf-8"), Base64.CRLF);
        byte[] keyb = seed.getBytes("utf-8");
        SecretKeySpec skey = new SecretKeySpec(keyb, "AES");
        Cipher dcipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        dcipher.init(Cipher.DECRYPT_MODE, skey);

        byte[] clearbyte = dcipher.doFinal(encryptedBytes);
        return new String(clearbyte);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////// with key (intellicode and salt) and fixed IV //////////////////////////

    public static String encrypt(String key, String content) throws Exception {
        byte[] input = content.getBytes("utf-8");

        SecretKeySpec skc = new SecretKeySpec(key.getBytes("utf-8"), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, skc, new IvParameterSpec(iv));

        byte[] cipherText = new byte[cipher.getOutputSize(input.length)];
        int ctLength = cipher.update(input, 0, input.length, cipherText, 0);
        cipher.doFinal(cipherText, ctLength);
        return Base64.encodeToString(cipherText, Base64.CRLF);
    }

    public static String decrypt(final String key, String encrypted) throws Exception {
        if (TextUtils.isEmpty(key))
            return null;

        byte[] encryptedBytes = Base64.decode(encrypted.getBytes("utf-8"), Base64.CRLF);
        byte[] keyb = key.getBytes("utf-8");
        SecretKeySpec skey = new SecretKeySpec(keyb, "AES");
        Cipher dcipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        dcipher.init(Cipher.DECRYPT_MODE, skey, new IvParameterSpec(iv));

        byte[] clearbyte = dcipher.doFinal(encryptedBytes);
        return new String(clearbyte);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////// with random IV len 16 /////////////////////////////////////////

    public static String encrypt(String key, String content, String ivVar) throws Exception {
        byte[] input = content.getBytes("utf-8");

        SecretKeySpec skc = new SecretKeySpec(key.getBytes("utf-8"), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, skc, new IvParameterSpec(ivVar.getBytes()));

        byte[] cipherText = new byte[cipher.getOutputSize(input.length)];
        int ctLength = cipher.update(input, 0, input.length, cipherText, 0);
        cipher.doFinal(cipherText, ctLength);
        return ivVar + Base64.encodeToString(cipherText, Base64.CRLF);
    }

    public static String decrypt(String key, String encrypted, String ivVar) throws Exception {

        byte[] encryptedBytes = Base64.decode(encrypted.getBytes("utf-8"), Base64.CRLF);
        byte[] keyb = key.getBytes("utf-8");
        SecretKeySpec skey = new SecretKeySpec(keyb, "AES");
        Cipher dcipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        dcipher.init(Cipher.DECRYPT_MODE, skey, new IvParameterSpec(ivVar.getBytes()));

        byte[] clearbyte = dcipher.doFinal(encryptedBytes);
        return new String(clearbyte);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    //logout
    public static JsonObject getEncryptedRandomIVRequestData(final JsonObject body) {
        final JsonObject encryptedJson = new JsonObject();
        final String randomIV = EncryptionHelper.getRandomIV(16);
        final String specialKey = EncryptionHelper.getSpecialKey();

        try {
            encryptedJson.addProperty(ST_API_KEY_DATA, EncryptionHelper.encrypt(specialKey, body.toString(), randomIV));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return encryptedJson;
    }

    public static String getEncryptedDataForCheckin(final JsonObject params) {
        try {
            return EncryptionHelper.encrypt(getSpecialKey(), params.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String getEncryptedData(final JsonObject params) {
        try {
            return EncryptionHelper.encrypt(getEncryptionSalt(), params.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDecryptedData(final String encryptedString) {
        if (TextUtils.isEmpty(encryptedString))
            return null;

        try {
            return EncryptionHelper.decrypt(EncryptionHelper.getEncryptionSalt(), encryptedString);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


//    public static void test(final String encrypted) {
//        String decryptedNoIV = null;
//        String decryptedFixedIV = null;
//        String decryptedRandomIV = null;
//
//        try {
//            decryptedNoIV = decryptNoIV(encrypted);
//            decryptedFixedIV = decrypt(encrypted);
//            decryptedRandomIV = decrypt(getEncryptionSalt(), encrypted, getRandomIV(16));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        Log.d("Encryption", "decryptedNoIV_" + decryptedNoIV);
//        Log.d("Encryption", "decryptedFixedIV" + decryptedFixedIV);
//        Log.d("Encryption", "decryptedRandomIV" + decryptedRandomIV);
//    }

}
