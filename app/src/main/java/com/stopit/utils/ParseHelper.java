package com.stopit.utils;

import android.text.TextUtils;
import android.util.Log;

import com.parse.ParsePush;
import com.google.firebase.messaging.FirebaseMessaging;
import com.stopit.db.DBHelper;
import com.stopit.models.Organization;

import static com.stopit.app.Constants.CHANNEL_PREFIX_ORG_ID;
import static com.stopit.app.Constants.CHANNEL_PREFIX_USER;

public class ParseHelper {

    public static void subscribeForParsePush() {
        final String intellicode = DBHelper.getIntellicode();
        if (!TextUtils.isEmpty(intellicode))
            ParsePush.subscribeInBackground(getUserChannel(intellicode));
            FirebaseMessaging.getInstance().subscribeToTopic(getUserChannel(intellicode));
            Log.d("FireBase","SubscribeTopic Intellicode: "+intellicode);

        final Organization currentOrg = DBHelper.getCurrentOrganization();
        if (currentOrg != null && currentOrg.getId() != 0)
            ParsePush.subscribeInBackground(getOrgChannel(currentOrg.getId()));
            FirebaseMessaging.getInstance().subscribeToTopic(getOrgChannel(currentOrg.getId()));
            Log.d("FireBase","SubscribeTopic Org: "+currentOrg.getId());
    }

    public static void unsubscribeForParsePush() {
        final String intellicode = DBHelper.getIntellicode();
        if (!TextUtils.isEmpty(intellicode)) {
            //try {
            ParsePush.unsubscribeInBackground(getUserChannel(intellicode));
            Log.d("ParseHelper", "ParseHelper unsubscribeInBackground "+intellicode);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        }

        final Organization currentOrg = DBHelper.getCurrentOrganization();
        if (currentOrg != null) {
            //try {
            ParsePush.unsubscribeInBackground(getOrgChannel(currentOrg.getId()));
            Log.d("ParseHelper", "ParseHelper unsubscribeInBackground "+currentOrg.getId());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        }
    }

    private static String getOrgChannel(final long orgId) {
        return CHANNEL_PREFIX_ORG_ID + String.valueOf(orgId);
    }

    private static String getUserChannel(final String intellicode) {
        return CHANNEL_PREFIX_USER + intellicode;
    }
}
