package com.stopit.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.stopit.R;
import com.stopit.activity.MainActivity;

import static com.stopit.app.Constants.INBOX_NOTIFICATION_BROADCAST_ACTION;

public class NotificationHelper {
    static final String SN_CHANNEL = "stopit_notification";
    static final String SN_NAME = "stopit_notification_name";

    public static void notifyUser(final Context mContext, final String alert) {
        Intent intent = new Intent(mContext.getApplicationContext(), MainActivity.class);
        //intent.setAction(INBOX_NOTIFICATION_BROADCAST_ACTION);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 12346, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(mContext.getApplicationContext(), SN_CHANNEL);
        mBuilder
                .setSmallIcon(R.drawable.ic_push)
                .setContentTitle(mContext.getResources().getString(R.string.app_name))
                .setContentText(alert)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManager mNotificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        if (mNotificationManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(SN_CHANNEL,
                        SN_NAME, NotificationManager.IMPORTANCE_DEFAULT);
                mNotificationManager.createNotificationChannel(channel);
            }
            mNotificationManager.notify(0, mBuilder.build());
        }
    }

    public static void notifyUserNewBroadcastMessage(final Context mContext, final String alert) {
        Intent intent = new Intent(mContext.getApplicationContext(), MainActivity.class);
        intent.setAction(INBOX_NOTIFICATION_BROADCAST_ACTION);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 12345, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(mContext.getApplicationContext(), SN_CHANNEL);
        mBuilder
                .setSmallIcon(R.drawable.ic_push)
                .setContentTitle(mContext.getResources().getString(R.string.app_name))
                .setContentText(alert)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManager mNotificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        if (mNotificationManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(SN_CHANNEL,
                        SN_NAME, NotificationManager.IMPORTANCE_DEFAULT);
                mNotificationManager.createNotificationChannel(channel);
            }
            mNotificationManager.notify(0, mBuilder.build());
        }
    }
}
