package com.stopit.api.chat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.stopit.R;
import com.stopit.db.DBHelper;
import com.stopit.models.messenger.ChatMessage;
import com.stopit.models.messenger.Conversation;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.roster.RosterListener;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.muc.InvitationListener;
import org.jivesoftware.smackx.muc.MucEnterConfiguration.Builder;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;
import org.jivesoftware.smackx.muc.packet.MUCUser;
import org.jivesoftware.smackx.ping.PingFailedListener;
import org.jivesoftware.smackx.ping.PingManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.receipts.ReceiptReceivedListener;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.EntityJid;
import org.jxmpp.jid.Jid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.jid.parts.Resourcepart;
import org.jxmpp.stringprep.XmppStringprepException;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;

import static com.stopit.app.Constants.ST_DEFAULT_MESSENGER_PORT;


public class SmackConnection implements ConnectionListener,
        RosterListener, PingFailedListener, ReceiptReceivedListener {

    private static final String TAG = "SMACK";
    /////////////// joined conversation ///////////////////////
    public static Conversation LAST_JOINED_CONVERSATION = null;
    private final Context context;
    private final String mPassword;
    private final String mUsername;
    private final String mServiceName;
    private final int port;
    private final String roomUrl;
    private final long roomId; //the same as incidentId

    public MultiUserChat xmppRoom;
    public AbstractXMPPConnection mAbstractXMPPConnection;
    private BroadcastReceiver mReceiver;

    public SmackConnection(Context pContext) {
        Log.i(TAG, "ChatConnection()");

        context = pContext;
        mPassword = PreferenceManager.getDefaultSharedPreferences(context).getString("xmpp_password", null);
        port = PreferenceManager.getDefaultSharedPreferences(context).getInt("xmpp_port", ST_DEFAULT_MESSENGER_PORT);

        String jid = PreferenceManager.getDefaultSharedPreferences(context).getString("xmpp_jid", null);
        roomUrl = PreferenceManager.getDefaultSharedPreferences(context).getString("xmpp_room_url", null);
        roomId = PreferenceManager.getDefaultSharedPreferences(context).getLong("xmpp_room_id", 0);

        mServiceName = (jid != null) ? jid.split("@")[1] : "";
        mUsername = (jid != null) ? jid.split("@")[0] : "";
    }

    @Override
    public void onReceiptReceived(Jid fromJid, Jid toJid, String receiptId, Stanza receipt) {
        Log.d("Received", receiptId);
    }

    public AbstractXMPPConnection passConnection() {
        return mAbstractXMPPConnection;
    }

    public void connect() throws IOException, XMPPException, SmackException {
        Log.i(TAG, "connect()");

        showXmppProgress(context.getResources().getString(R.string.message_initializing));
        XMPPTCPConnectionConfiguration config = null;
        try {
            config = XMPPTCPConnectionConfiguration.builder()
                    .setResource("STOPit Android")
                    .setUsernameAndPassword(mUsername, mPassword)
                    .setSecurityMode(ConnectionConfiguration.SecurityMode.required)
                    .setCustomSSLContext(SmackSSLContext.createContext(context))
                    .setHost(mServiceName)
                    .setPort(ST_DEFAULT_MESSENGER_PORT) //hardcoded value!
                    .setSendPresence(true)
                    .setXmppDomain(mServiceName).build();


        } catch (KeyStoreException e) {
            showXMPPErrorDialog();
            e.printStackTrace();

//            String codeLine = Integer.toString(Thread.currentThread().getStackTrace()[2].getLineNumber());
//            AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_XMPP_Error,AnalyticsManager.On_Chat,codeLine,true,null);

        } catch (NoSuchAlgorithmException e) {
            showXMPPErrorDialog();
            e.printStackTrace();
//            String codeLine = Integer.toString(Thread.currentThread().getStackTrace()[2].getLineNumber());
//            AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_XMPP_Error,AnalyticsManager.On_Chat,codeLine,true,null);

        } catch (KeyManagementException e) {
            showXMPPErrorDialog();
            e.printStackTrace();
//            String codeLine = Integer.toString(Thread.currentThread().getStackTrace()[2].getLineNumber());
//            AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_XMPP_Error,AnalyticsManager.On_Chat,codeLine,true,null);

        }
        /*ProviderManager.addExtensionProvider(DeliveryReceipt.ELEMENT, DeliveryReceipt.NAMESPACE, new DeliveryReceipt.Provider());
        ProviderManager.addExtensionProvider(DeliveryReceiptRequest.ELEMENT, new DeliveryReceiptRequest().getNamespace(), new DeliveryReceiptRequest.Provider());
        ProviderManager.addExtensionProvider("received", DeliveryReceipt.NAMESPACE, new DeliveryReceiptRequest.Provider());*/
        mAbstractXMPPConnection = new XMPPTCPConnection(config);
        //Set ConnectionListener here to catch initial connect();
        mAbstractXMPPConnection.addConnectionListener(this);
        mAbstractXMPPConnection.setReplyTimeout(10000);

        joinXMPPRoom();

        removeXmppProgressDialog();
        PingManager.setDefaultPingInterval(300); //Ping every 5 minutes
        PingManager pingManager = PingManager.getInstanceFor(mAbstractXMPPConnection);
        pingManager.registerPingFailedListener(this);

        //ChatManager.getInstanceFor(mAbstractXMPPConnection).addChatListener(this);
        Roster.getInstanceFor(mAbstractXMPPConnection).addRosterListener(this);
    }

    public void joinXMPPRoom() {
        try {
            if (!mAbstractXMPPConnection.isConnected()) {
                mAbstractXMPPConnection.connect();
            }
            if (mAbstractXMPPConnection.isConnected()) {
                Resourcepart resource = Resourcepart.from("STOPit Android");
                if (!mAbstractXMPPConnection.isAuthenticated()) {
                    mAbstractXMPPConnection.login(mUsername, mPassword, resource);
                }
                if (mAbstractXMPPConnection.isAuthenticated()) {
                    System.out.println("Authenticated");
                    if (!TextUtils.isEmpty(roomUrl)) {
                        joinMultiUserChatRoom(roomUrl);
                    } else {
                        xmppRoomJoinedFailed();
                    }
                } else {
                    System.out.println("Not Authenticated");
                }
            }

        } catch (InterruptedException e) {
            showXMPPErrorDialog();
            e.printStackTrace();
//            String codeLine = Integer.toString(Thread.currentThread().getStackTrace()[2].getLineNumber());
//            AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_XMPP_Error,AnalyticsManager.On_Chat,codeLine,true,null);
        } catch (SmackException e) {
            showXMPPErrorDialog();
            e.printStackTrace();
//            String codeLine = Integer.toString(Thread.currentThread().getStackTrace()[2].getLineNumber());
//            AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_XMPP_Error,AnalyticsManager.On_Chat,codeLine,true,null);
        } catch (Exception e) {
            e.printStackTrace();
            removeXmppProgressDialog();
            showXMPPErrorDialog();
//            String codeLine = Integer.toString(Thread.currentThread().getStackTrace()[2].getLineNumber());
//            AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_XMPP_Error,AnalyticsManager.On_Chat,codeLine,true,null);
        }

        try {
            setupSendMessageReceiver();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void joinMultiUserChatRoom(String roomID) throws XmppStringprepException {

        //check if connected
        if (SmackService.getState().equals(SmackConnection.ConnectionState.DISCONNECTED)) {
            return;
        }

        // Get the MultiUserChatManager
        MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(mAbstractXMPPConnection);

        // Create a MultiUserChat using an XMPPConnection for a room
        EntityBareJid roomJid = null;
        try {
            roomJid = JidCreate.entityBareFrom(roomID);
        } catch (XmppStringprepException e) {
            e.printStackTrace();
            showXMPPErrorDialog();
        }

        try {
            xmppRoom = manager.getMultiUserChat(roomJid);
            Resourcepart rp = Resourcepart.from(mUsername);
            Builder builder = xmppRoom.getEnterConfigurationBuilder(rp);
            //builder.requestMaxStanzasHistory(100);
            builder.withPassword(mPassword);
            xmppRoom.join(builder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            manager.addInvitationListener(new InvitationListener() {
                @Override
                public void invitationReceived(XMPPConnection conn, MultiUserChat room, EntityJid inviter, String reason, String password, Message message, MUCUser.Invite invitation) {
                    System.out.println("Invitation Received!");
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (xmppRoom.isJoined()) {
            System.out.println("joined");
            listenForUpcomingMessagesInRoom();
            xmppRoomJoinedSendBroadcast();

        } else {
            System.out.println("not joined");
            xmppRoomJoinedFailed();
        }
    }

    public void disconnect() {
        Log.i(TAG, "disconnect()");
        try {
            if (mAbstractXMPPConnection != null) {
                mAbstractXMPPConnection.disconnect();
            }
        } catch (Exception e) {
            SmackService.sConnectionState = ConnectionState.DISCONNECTED;
            e.printStackTrace();
//            String codeLine = Integer.toString(Thread.currentThread().getStackTrace()[2].getLineNumber());
//            AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_XMPP_Error,AnalyticsManager.On_Chat,codeLine,true,null);
        }

        mAbstractXMPPConnection = null;
        if (mReceiver != null) {
            context.unregisterReceiver(mReceiver);
            mReceiver = null;
        }
    }

    private void rebuildRoster() {
        //TODO check behavior
        if(mAbstractXMPPConnection == null)
            return;

        ArrayList<String> mRoster = new ArrayList<>();
        Roster roster = Roster.getInstanceFor(mAbstractXMPPConnection);
        String status;
        for (RosterEntry entry : roster.getEntries()) {
            if (roster.getPresence(entry.getJid()).isAvailable()) {
                status = "Online";
            } else {
                status = "Offline";
            }
            //String message = entry.getUser()+ ": " + status;
            String message = entry.getJid() + ": " + status;
            mRoster.add(message);
        }

        Intent intent = new Intent(SmackService.NEW_ROSTER);
        intent.setPackage(context.getPackageName());
        intent.putStringArrayListExtra(SmackService.BUNDLE_ROSTER, mRoster);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        }
        context.sendBroadcast(intent);
    }

    private void setupSendMessageReceiver() {
        if (mReceiver != null) {
            context.unregisterReceiver(mReceiver);
            mReceiver = null;
        }
        mReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (TextUtils.equals(action, SmackService.SEND_MESSAGE)) {
                    try {
                        sendMessage(intent.getStringExtra(SmackService.BUNDLE_MESSAGE_BODY),
                                intent.getStringExtra(SmackService.BUNDLE_TO),
                                intent.getStringExtra(SmackService.MESSAGE_UUID),
                                intent.getLongExtra(SmackService.MESSAGE_DATE, 0),
                                intent.getLongExtra(SmackService.MESSAGE_INCIDENT_ID, 0));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(SmackService.SEND_MESSAGE);
        context.registerReceiver(mReceiver, filter);
    }

    private void sendMessage(final String body, final String toJid, final String uuid, final long dateMillis, final long incidentId) {
        boolean isMessageSent = false;
        Log.i(TAG, "sendMessage()");
        EntityBareJid jid = null;
        try {
            jid = JidCreate.entityBareFrom(toJid);
        } catch (XmppStringprepException e) {
            showXMPPErrorDialog();
            e.printStackTrace();
        }

        try {
            Message message = new Message(jid, Message.Type.groupchat);
            message.setBody(body);
            message.setStanzaId(uuid);
            message.setType(Message.Type.groupchat);
            message.addExtension(new DeliveryReceiptRequest());
            if (xmppRoom != null) {
                xmppRoom.sendMessage(message);
                isMessageSent = true;
            }
        } catch (SmackException.NotConnectedException e) {
            showMessageNotSentDialog();
            e.printStackTrace();
//            String codeLine = Integer.toString(Thread.currentThread().getStackTrace()[2].getLineNumber());
//            AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_XMPP_Error,AnalyticsManager.On_Chat,codeLine,true,null);

        } catch (InterruptedException e) {
            e.printStackTrace();
            showMessageNotSentDialog();
//            String codeLine = Integer.toString(Thread.currentThread().getStackTrace()[2].getLineNumber());
//            AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_XMPP_Error,AnalyticsManager.On_Chat,codeLine,true,null);
        } catch (Exception e) {
            e.printStackTrace();
            showMessageNotSentDialog();
//            String codeLine = Integer.toString(Thread.currentThread().getStackTrace()[2].getLineNumber());
//            AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_XMPP_Error,AnalyticsManager.On_Chat,codeLine,true,null);
        }

        if (isMessageSent) {
            final ChatMessage sentMessage = new ChatMessage(body, dateMillis, uuid, incidentId, false);
            DBHelper.save(sentMessage);
        } else {
            showMessageNotSentDialog();
        }

        try {
            String docitUser = DBHelper.getDocitAdminName();
            EntityBareJid roomJID = JidCreate.entityBareFrom(docitUser);
            xmppRoom.invite(roomJID, "Messenger Invite");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listenForUpcomingMessagesInRoom() {
        xmppRoom.addMessageListener(new MessageListener() {
            @Override
            public void processMessage(Message message) {
                Log.d("Message", "received");
                final Message.Type type = message.getType();
                final String body = message.getBody();
                final String uniqueId = message.getStanzaId();

                if ((!type.equals(Message.Type.groupchat))
                        || TextUtils.isEmpty(body)
                        || TextUtils.isEmpty(uniqueId)
                        || DBHelper.isChatMessageWithId(uniqueId)) {

                    //ignore invalid or outcomming message
                    Log.d("Message", "listenForUpcomingMessagesInRoom ignored");
                    return;
                }

                final ChatMessage incomingMessage = new ChatMessage(body, System.currentTimeMillis(), uniqueId, roomId, true);
                DBHelper.save(incomingMessage);
            }
        });
    }

    @Override
    public void connected(XMPPConnection connection) {
        SmackService.sConnectionState = ConnectionState.CONNECTED;
        Log.i(TAG, "connected()");
        /*DeliveryReceiptManager deliveryReceiptManager = DeliveryReceiptManager.getInstanceFor(connection);
        deliveryReceiptManager.addReceiptReceivedListener(this);*/

    }

    //ConnectionListener

    @Override
    public void authenticated(XMPPConnection connection, boolean resumed) {
        SmackService.sConnectionState = ConnectionState.CONNECTED;
        Log.i(TAG, "authenticated()");
    }

    @Override
    public void connectionClosed() {
        SmackService.sConnectionState = ConnectionState.DISCONNECTED;
        Log.i(TAG, "connectionClosed()");
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        SmackService.sConnectionState = ConnectionState.DISCONNECTED;
        Log.i(TAG, "connectionClosedOnError()");
//        String codeLine = Integer.toString(Thread.currentThread().getStackTrace()[2].getLineNumber());
//        AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_XMPP_Error,AnalyticsManager.On_Chat,codeLine,true,null);
    }

    @Override
    public void entriesAdded(Collection<Jid> addresses) {
        Log.i(TAG, "entriesAdded()");
        rebuildRoster();
    }

    //RosterListener

    @Override
    public void entriesUpdated(Collection<Jid> addresses) {
        Log.i(TAG, "entriesUpdated()");
        rebuildRoster();
    }

    @Override
    public void entriesDeleted(Collection<Jid> addresses) {
        Log.i(TAG, "entriesDeleted()");
        rebuildRoster();
    }

    @Override
    public void presenceChanged(Presence presence) {
        Log.i(TAG, "presenceChanged()");
        rebuildRoster();
    }

    @Override
    public void pingFailed() {
        Log.i(TAG, "pingFailed()");
//
//        String codeLine = Integer.toString(Thread.currentThread().getStackTrace()[2].getLineNumber());
//        AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_XMPP_Error,AnalyticsManager.On_Chat,codeLine,true,null);
    }

    //PingFailedListener

    private void removeXmppProgressDialog() {
        context.sendBroadcast(new Intent(SmackService.AFTER_CONNECTION));
    }

    private void xmppRoomJoinedSendBroadcast() {
        context.sendBroadcast(new Intent(SmackService.XMPP_ROOM_JOINED));
    }

    private void xmppRoomJoinedFailed() {
        context.sendBroadcast(new Intent(SmackService.XMPP_ROOM_JOINED_FAILED));
    }

    private void showXmppProgress(String message) {
        context.sendBroadcast(new Intent(SmackService.BEFORE_CONNECTION));
    }

    private void showXMPPErrorDialog() {
        context.sendBroadcast(new Intent(SmackService.XMPP_ERROR));
    }

    private void showMessageNotSentDialog() {
        context.sendBroadcast(new Intent(SmackService.OUTCOMING_MESSAGE_NOT_SENT));
    }

    public enum ConnectionState {
        CONNECTED, CONNECTING, RECONNECTING, DISCONNECTED
    }

}
