package com.stopit.api.chat;

import android.content.Context;
import android.os.Build;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

public class SmackSSLContext {

    public static SSLContext createContext(Context ctx) throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        KeyStore trustStore = null;
            try {
                CertificateFactory cf = CertificateFactory.getInstance("X.509");
                InputStream is = ctx.getResources().getAssets().open("certificate/sf_bundle-g2-g1.crt");
                InputStream caInput = new BufferedInputStream(is);
                Certificate ca;

                try {
                    ca = cf.generateCertificate(caInput);
                    //System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
                } finally {
                    caInput.close();
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2){
                    trustStore = KeyStore.getInstance("AndroidKeyStore");
                }else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH && Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    //trustStore = KeyStore.getInstance("AndroidCAStore");
                    String keyStoreType = KeyStore.getDefaultType();
                    trustStore = KeyStore.getInstance(keyStoreType);
                }else {
                    trustStore = KeyStore.getInstance("BKS");
                }

                trustStore.load(null, null);
                trustStore.setCertificateEntry("ca", ca);

            } catch (IOException e) {
                e.printStackTrace();
                String codeLine = Integer.toString(Thread.currentThread().getStackTrace()[2].getLineNumber());
                //AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_XMPP_Error,AnalyticsManager.On_Chat,codeLine,true,null);
            } catch (CertificateException e) {
                e.printStackTrace();
                String codeLine = Integer.toString(Thread.currentThread().getStackTrace()[2].getLineNumber());
                //AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_XMPP_Error,AnalyticsManager.On_Chat,codeLine,true,null);
            }catch(Exception e){
                e.printStackTrace();
                String codeLine = Integer.toString(Thread.currentThread().getStackTrace()[2].getLineNumber());
                //AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_XMPP_Error,AnalyticsManager.On_Chat,codeLine,true,null);
            }

        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(trustStore);
        SSLContext sslContext = SSLContext.getInstance("TLS");
        //sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
        sslContext.init(null, trustManagerFactory.getTrustManagers(), null);
        return sslContext;
    }
}
