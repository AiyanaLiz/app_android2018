package com.stopit.api.chat;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;


import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.IOException;

public class SmackService extends Service {


    public static final String NEW_MESSAGE = "stopitcyberbully.newmessage";
    public static final String PUSH_NEW_MESSAGE = "stopitcyberbully.pushnewmessage";
    public static final String SEND_MESSAGE = "stopitcyberbully.sendmessage";
    public static final String NEW_ROSTER = "stopitcyberbully.newroster";
    public static final String BEFORE_CONNECTION = "stopitcyberbully.beforexmppconnection";
    public static final String XMPP_ROOM_JOINED = "stopitcyberbully.xmpproomjoined";
    public static final String XMPP_ROOM_JOINED_FAILED = "stopitcyberbully.xmpproomjoinedfailed";
    public static final String RECEIVED_MESSAGE_NOTIFICATION = "stopitcyberbully.receivednewmessage";
    public static final String AFTER_CONNECTION = "stopitcyberbully.afterxmppconnection";
    public static final String XMPP_ERROR = "stopitcyberbully.xmpperror";
    public static final String OUTCOMING_MESSAGE_NOT_SENT = "stopitcyberbully.outcomingmessagenotsent";
    //public static final String MESSENGER_ERROR = "stopitcyberbully.messengererror";

    public static final String BUNDLE_FROM_JID = "b_from";
    public static final String BUNDLE_MESSAGE_BODY = "b_body";
    public static final String BUNDLE_ROSTER = "b_body";
    public static final String BUNDLE_TO = "b_to";
    public static final String MESSAGE_UUID = "b_id";
    public static final String MESSAGE_DATE = "b_date";
    public static final String MESSAGE_INCIDENT_ID = "b_incident_id";

    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public SmackService getService() {
            // Return this instance of LocalService so clients can call public methods
            return SmackService.this;
        }
    }

    public static SmackConnection.ConnectionState sConnectionState;

    public static SmackConnection.ConnectionState getState() {
        if(sConnectionState == null){
            return SmackConnection.ConnectionState.DISCONNECTED;
        }
        return sConnectionState;
    }

    private boolean mActive;
    private Thread mThread;
    private Handler mTHandler;
    private SmackConnection mConnection;

    public SmackService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        start();
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stop();
    }


    public void restart() {

        mActive = false;
        if (mTHandler != null) {
            mTHandler.post(new Runnable() {

                @Override
                public void run() {
                    if (mConnection != null) {
                        mConnection.disconnect();
                        initConnection();
                    }
                }
            });
        }else{
            start();
        }
    }

    public void start() {
        if (!mActive) {
            mActive = true;

            // Create ConnectionThread Loop
            if (mThread == null || !mThread.isAlive()) {
                mThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Looper.prepare();
                        mTHandler = new Handler();
                        initConnection();
                        Looper.loop();
                    }

                });
                mThread.start();
            }
        }
    }

    public void stop() {
        mActive = false;
        if (mTHandler != null) {
            mTHandler.post(new Runnable() {

                @Override
                public void run() {
                    if (mConnection != null) {
                        mConnection.disconnect();
                    }
                }
            });
        }
    }

    public void initConnection() {
        if(mConnection == null){
            mConnection = new SmackConnection(getApplicationContext());
        }

        try {
            if (SmackService.getState().equals(SmackConnection.ConnectionState.DISCONNECTED)){
            mConnection.connect();
        }
        //TODO commented
//        else if(StopitApplication.networkDisconnected){
//            StopitApplication.networkDisconnected = false;
//            mConnection.connect();
//
//        }
        } catch (IOException | SmackException | XMPPException e) {
            e.printStackTrace();
        }
    }

    public void joinGivenXMPPRoom (){

        if(mConnection == null){
            mConnection = new SmackConnection(this);
            try {
                if (SmackService.getState().equals(SmackConnection.ConnectionState.DISCONNECTED)){
                    mConnection.connect();
                }
                //TODO commented
//                else if(StopitApplication.networkDisconnected){
//                    StopitApplication.networkDisconnected = false;
//                    mConnection.connect();
//
//                }
            } catch (IOException | SmackException | XMPPException e) {
                e.printStackTrace();
            }
        } else {
            mConnection.joinXMPPRoom();
        }
    }
}
