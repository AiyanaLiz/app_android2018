package com.stopit.api;

import android.support.annotation.IntDef;

import com.stopit.api.adapters.AccountApiAdapter;
import com.stopit.api.adapters.BrandingApiAdapter;
import com.stopit.api.adapters.BroadcastApiAdapter;
import com.stopit.api.adapters.IncidentsApiAdapter;
import com.stopit.api.adapters.MessengerApiAdapter;
import com.stopit.api.adapters.ResourcesApiAdapter;
import com.stopit.api.app_version.ConfigApiAdapter;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


public final class ApiManager {

    private static AccountApiAdapter sAccountApiAdapter;
    private static BrandingApiAdapter sBrandingApiAdapter;
    private static IncidentsApiAdapter sIncidentsApiAdapter;
    private static BroadcastApiAdapter sBroadcastsApiAdapter;
    private static ResourcesApiAdapter sResourcesApiAdapter;
    private static MessengerApiAdapter sMessengerApiAdapter;
    private static ConfigApiAdapter sConfigApiAdapter;

    public static void reset() {
        sAccountApiAdapter = null;
        sBrandingApiAdapter = null;
        sIncidentsApiAdapter = null;
        sBroadcastsApiAdapter = null;
        sResourcesApiAdapter = null;
        sMessengerApiAdapter = null;
        sConfigApiAdapter = null;
    }

    public static AccountApiAdapter getAccountAdapter() {
        if (sAccountApiAdapter == null)
            sAccountApiAdapter = new AccountApiAdapter();
        return sAccountApiAdapter;
    }

    public static BrandingApiAdapter getBrandingAdapter() {
        if (sBrandingApiAdapter == null)
            sBrandingApiAdapter = new BrandingApiAdapter();
        return sBrandingApiAdapter;
    }

    public static IncidentsApiAdapter getIncidentsAdapter() {
        if (sIncidentsApiAdapter == null)
            sIncidentsApiAdapter = new IncidentsApiAdapter();
        return sIncidentsApiAdapter;
    }

    public static BroadcastApiAdapter getBroadcastsAdapter() {
        if (sBroadcastsApiAdapter == null)
            sBroadcastsApiAdapter = new BroadcastApiAdapter();
        return sBroadcastsApiAdapter;
    }

    public static ResourcesApiAdapter getResourcesAdapter() {
        if (sResourcesApiAdapter == null)
            sResourcesApiAdapter = new ResourcesApiAdapter();
        return sResourcesApiAdapter;
    }

    public static MessengerApiAdapter getMessengerApiAdapter() {
        if (sMessengerApiAdapter == null)
            sMessengerApiAdapter = new MessengerApiAdapter();
        return sMessengerApiAdapter;
    }

    public static ConfigApiAdapter getConfigApiAdapter() {
        if (sConfigApiAdapter == null)
            sConfigApiAdapter = new ConfigApiAdapter();
        return sConfigApiAdapter;
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({
            AdapterUniqueKey.LOGIN_ADAPTER,
            AdapterUniqueKey.BRANDING_ADAPTER,
            AdapterUniqueKey.INCIDENTS_ADAPTER,
            AdapterUniqueKey.BROADCASTS_ADAPTER,
            AdapterUniqueKey.RESOURCES_ADAPTER,
            AdapterUniqueKey.MESSENGER_ADAPTER,
            AdapterUniqueKey.CONFIG_ADAPTER
    })


    public @interface AdapterUniqueKey {
        int LOGIN_ADAPTER = 128;
        int INCIDENTS_ADAPTER = 129;
        int BRANDING_ADAPTER = 130;
        int BROADCASTS_ADAPTER = 131;
        int RESOURCES_ADAPTER = 132;
        int MESSENGER_ADAPTER = 133;
        int CONFIG_ADAPTER = 134;
    }
}
