package com.stopit.api;

import android.support.annotation.NonNull;



public interface IApiCallBackSuccess<E> {

    void onApiSuccess(@NonNull final E response);

}