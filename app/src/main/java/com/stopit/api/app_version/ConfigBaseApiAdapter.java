package com.stopit.api.app_version;


import android.support.annotation.CallSuper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.readystatesoftware.chuck.ChuckInterceptor;
import com.stopit.BuildConfig;
import com.stopit.app.StopitApp;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

abstract class ConfigBaseApiAdapter<ApiService> {

    public static final int TIME_OUT = 10000; // ms
    public static String BASE_URL = "https://statuscheck.stopitsolutions.com";

    private final ApiService mApiService;
    private final Gson mGson;

    ConfigBaseApiAdapter() {
        final OkHttpClient.Builder okClientBuilder = new OkHttpClient.Builder()
                .connectTimeout(TIME_OUT, TimeUnit.MILLISECONDS)
                .readTimeout(TIME_OUT, TimeUnit.MILLISECONDS)
                .writeTimeout(TIME_OUT, TimeUnit.MILLISECONDS);
        /*addInterceptors(okClientBuilder);*/

        final GsonBuilder gsonBuilder = new GsonBuilder();
        registerTypeAdapters(gsonBuilder.setLenient());
        mGson = gsonBuilder.create();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(mGson))
                .client(okClientBuilder.build())
                .build();
        mApiService = retrofit.create(provideServiceClass());
    }

    static String getBaseUrl() {
        return BASE_URL;
    }

    abstract Class<ApiService> provideServiceClass();

    @CallSuper
    void addInterceptors(final OkHttpClient.Builder okClient) {
        if (BuildConfig.DEBUG) {
            okClient
//                    .addInterceptor(
//                            new LoggingInterceptor.Builder()
//                                    .loggable(true)
//                                    .setLevel(Level.BASIC)
//                                    .log(Platform.INFO)
//                                    .request("API_Request")
//                                    .response("API_Response")
//                                    .build()
//                    )
                    .addInterceptor(new ChuckInterceptor(StopitApp.getAppContext()));
        }
    }

    void registerTypeAdapters(final GsonBuilder gsonBuilder) {
    }

    ApiService getService() {
        return mApiService;
    }

    Gson getGson() {
        return mGson;
    }

}
