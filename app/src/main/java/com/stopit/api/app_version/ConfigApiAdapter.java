package com.stopit.api.app_version;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.JsonObject;
import com.stopit.api.IApiCallBackError;
import com.stopit.api.IApiCallBackSuccess;
import com.stopit.api.adapters.ApiError;
import com.stopit.api.services.ConfigApiService;
import com.stopit.models.data_wrappers.VersionStatusWrapper;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfigApiAdapter extends ConfigBaseApiAdapter<ConfigApiService> {

    /* *
     * Check response and transfer it to incomingCallback. If incomingCallback == null,
     * or apiCallbackHandler.needToHandle() return false - response not be transfer.
     */
    Callback<VersionStatusWrapper> checkResponse(final IApiCallBackSuccess<VersionStatusWrapper> callbackSuccess, final IApiCallBackError callbackError) {
        return new Callback<VersionStatusWrapper>() {
            @Override
            public void onResponse(@NonNull final Call<VersionStatusWrapper> call, @NonNull final Response<VersionStatusWrapper> response) {
                Log.i("StopitApiAdapter", String.format(Locale.US, "Success = %b, Code = %d", response.isSuccessful(), response.code()));
                callbackError.hideApiInProgressView();
                if (response.isSuccessful()) {
                    Log.d("response ", "responce string " + response.toString());
                    VersionStatusWrapper responseBody = response.body();
                    if (responseBody == null) {
                        if (callbackError.needToHandle()) {
                            if (responseBody == null)
                                callbackError.onError(call, new ApiError(response));
                            else
                                callbackError.onError(call, new ApiError(responseBody));
                        }
                    } else
                        callbackSuccess.onApiSuccess(responseBody);

                } else {
                    callbackError.onError(call, new ApiError(response));
                }
            }

            @Override
            public void onFailure(@NonNull final Call<VersionStatusWrapper> call, @NonNull final Throwable t) {
                callbackError.hideApiInProgressView();
                Log.i("StopitApiAdapter", "onFailure " + t);
                if (callbackError.needToHandle()) {
                    callbackError.onError(call, new ApiError(t));
                }
            }
        };
    }

    @Override
    Class<ConfigApiService> provideServiceClass() {
        return ConfigApiService.class;
    }

    public void requestAppInfo(final IApiCallBackSuccess<VersionStatusWrapper> callBackSuccess,
                               final IApiCallBackError callBackError) {

        callBackError.showApiInProgressView();
        getService().requestAppInfo().enqueue(checkResponse(callBackSuccess, callBackError));
    }
}
