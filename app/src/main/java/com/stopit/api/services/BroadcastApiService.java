package com.stopit.api.services;

import com.google.gson.JsonObject;
import com.stopit.models.data_wrappers.BroadcastListWrapper;
import com.stopit.models.data_wrappers.BroadcastMessageWrapper;
import com.stopit.models.data_wrappers.StopitResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface BroadcastApiService {

    // get all broadcast messages
    @POST("/v2/intellicodes/inbox/InboxList")
    Call<StopitResponse<BroadcastListWrapper>> getMessagesList(@Body JsonObject body);

    @POST("/v2/intellicodes/inbox/update")
    Call<StopitResponse<BroadcastMessageWrapper>> updateMessage(@Body JsonObject body);

    @POST("/v2/intellicodes/inbox/get_inbox_message")
    Call<StopitResponse<BroadcastMessageWrapper>> getMessageById(@Body JsonObject body);

    @POST("/v2/intellicodes/inbox/deleteInboxMessage")
    Call<StopitResponse<BroadcastMessageWrapper>> deleteMessageById(@Body JsonObject body);

    @POST("/v2/intellicodes/inbox/get_message_url")
    Call<StopitResponse<String>> getMessageUrl(@Body JsonObject body);

    @POST("/v2/intellicodes/inbox/count")
    Call<StopitResponse<String>> getUnreadInboxCount(@Body JsonObject body);





}
