package com.stopit.api.services;

import com.google.gson.JsonObject;
import com.stopit.models.Incident;
import com.stopit.models.data_wrappers.IncidentDataWrapper;
import com.stopit.models.data_wrappers.IncidentListDataWrapper;
import com.stopit.models.data_wrappers.QuestionsDataWrapper;
import com.stopit.models.data_wrappers.ResourcesListDataWrapper;
import com.stopit.models.data_wrappers.StopitResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface ResourcesApiService {

    //get information res
    @POST("/v5/intellicodes/resources")
    Call<StopitResponse<ResourcesListDataWrapper>> getInformationResources(@Body JsonObject body);

}
