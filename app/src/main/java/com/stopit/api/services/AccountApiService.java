package com.stopit.api.services;

import com.google.gson.JsonObject;
import com.stopit.models.data_wrappers.StopitResponse;
import com.stopit.models.data_wrappers.UserData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface AccountApiService {

    // sign in/ log in to existing account
    @POST("/v5/intellicodes/register")
    Call<StopitResponse<UserData>> register(@Body JsonObject body);

    // sign in/ log in to existing account
    @POST("/v5/intellicodes/register/{entity_id}")
    Call<StopitResponse<UserData>> register(@Path("entity_id") long orgId,  @Body JsonObject body);

    @POST("/v5/intellicodes/accept_terms_of_use")
    Call<StopitResponse<Void>> acceptTermsOfUse(@Body JsonObject body);

    @POST("/v5/intellicodes/check_in")
    Call<StopitResponse<String>> statusCheck(@Body JsonObject body);

//    @POST("/v2/devices/unencrypted_logouts")
//    Call<StopitResponse<UserData>> logoutUnencrypted(@Body JsonObject body);

    @POST("/v2/devices/logouts")
    Call<StopitResponse<UserData>> logout(@Body JsonObject body);

//    @POST("/v2/devices/logout1")
//    Call<StopitResponse<UserData>> logoutNew(@Body JsonObject body);

}
