package com.stopit.api.services;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;


public interface BrandingApiService {

    @GET
    Call<ResponseBody> imageDownload(@Url String fileUrl);

}
