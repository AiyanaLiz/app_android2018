package com.stopit.api.services;

import com.google.gson.JsonObject;
import com.stopit.models.DynamicQuestion;
import com.stopit.models.Incident;
import com.stopit.models.data_wrappers.IncidentDataWrapper;
import com.stopit.models.data_wrappers.IncidentListDataWrapper;
import com.stopit.models.data_wrappers.QuestionsDataWrapper;
import com.stopit.models.data_wrappers.StopitResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface IncidentsApiService {

    // submit new incident
    @POST("/v5/intellicodes/incidents")
    Call<StopitResponse<IncidentDataWrapper>> submitIncident(@Body JsonObject body);

    @POST("/v5/intellicodes/incidents/list")
    Call<StopitResponse<String>> getIncidentsList(@Body JsonObject body);

    @POST("/v5/intellicodes/incidents/{incident_id}/view")
    Call<StopitResponse<String>> getIncidentById(@Path("incident_id") long incidentId, @Body JsonObject body);

    @POST("/v5/intellicodes/incidents/lookup")
    Call<StopitResponse<IncidentDataWrapper>> getIncidentByIdAndCode(@Body JsonObject body);

    @POST("/v5/intellicodes/incidents/{incident_id}")
    Call<StopitResponse<IncidentDataWrapper>> updateIncident(@Path("incident_id") long incidentId, @Body JsonObject body);

    @POST("/v5/intellicodes/incidents/questions")
    Call<StopitResponse<String>> getDynamicQuestions(@Body JsonObject body);


}
