package com.stopit.api.services;

import com.google.gson.JsonObject;
import com.stopit.models.data_wrappers.ResourcesListDataWrapper;
import com.stopit.models.data_wrappers.StopitResponse;
import com.stopit.models.data_wrappers.VersionStatusWrapper;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface ConfigApiService {

    @GET("/statuschecknew.json")
    Call<VersionStatusWrapper> requestAppInfo();

}
