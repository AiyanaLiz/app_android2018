package com.stopit.api.services;

import com.google.gson.JsonObject;
import com.stopit.models.data_wrappers.BroadcastListWrapper;
import com.stopit.models.data_wrappers.StopitResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface MessengerApiService {

    // get all broadcast messages
    @POST("/v4/intellicodes/getchathistory")
    Call<StopitResponse<String>> getChatHistory(@Body JsonObject body);

    @POST("/v4/intellicodes/updatemsgs")
    Call<StopitResponse<Void>> updateMessages(@Body JsonObject body);

    @POST("/v4/intellicodes/getunreadcount")
    Call<StopitResponse<String>> getTotalUnreadMessagesCount(@Body JsonObject body);

}
