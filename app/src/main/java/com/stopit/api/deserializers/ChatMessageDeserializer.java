package com.stopit.api.deserializers;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.stopit.db.DBHelper;
import com.stopit.utils.DateUtils;

import java.lang.reflect.Type;

public class ChatMessageDeserializer<ChatMessage> implements JsonDeserializer<ChatMessage> {

    @Override
    public ChatMessage deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
            throws JsonParseException {

        //deserialize string date as long
        final JsonObject jsonObject = je.getAsJsonObject();
        if (jsonObject.has("sentDate")) {
            final String date = jsonObject.get("sentDate").getAsString();

            final long dateMillis = DateUtils.getChatMessageDateMillis(date);
            jsonObject.addProperty("sentDate", dateMillis);
        }

        //initialize incidentId from toJid property
        if (jsonObject.has("toJID")) {
            final String toJid = jsonObject.get("toJID").getAsString();
            if (!TextUtils.isEmpty(toJid)) {
                final String[] jidArr = toJid.split("@");
                jsonObject.addProperty("incidentId", jidArr[0]);
            }
        }

        //initialize isIncoming - fromJid contains sender id
        final long currentUserId = DBHelper.getCurrentUserId();

        if (jsonObject.has("fromJID") && currentUserId != 0) {
            final String fromJid = jsonObject.get("fromJID").getAsString();
            if (!TextUtils.isEmpty(fromJid)) {
                final String[] jidArr = fromJid.split("_");
                long senderId = 0;
                try {
                    senderId = Long.valueOf(jidArr[0]);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                if (senderId != currentUserId) {
                    jsonObject.addProperty("incoming", true);
                }
            }
        }

        if (jsonObject.has("stanza")) {
            final String stanza = jsonObject.get("stanza").getAsString();
            if (!TextUtils.isEmpty(stanza)) {
                final String result = stanza.replace("<message id=", "")
                        .replace("> </message>", "")
                        .replaceAll("\\\"", "");
                if (!TextUtils.isEmpty(result)) {
                    jsonObject.addProperty("stanza", result);
                }
            }
        }
        return new Gson().fromJson(jsonObject, type);
    }
}