package com.stopit.api.deserializers;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.stopit.utils.DateUtils;

import java.lang.reflect.Type;

import static com.stopit.app.Constants.INBOX_MSG_STATUS_DELETED;
import static com.stopit.app.Constants.INBOX_MSG_STATUS_NEW;
import static com.stopit.app.Constants.INBOX_MSG_STATUS_READ;

public class BroadcastMessageDeserializer<BroadcastMessage> implements JsonDeserializer<BroadcastMessage> {
    @Override
    public BroadcastMessage deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
            throws JsonParseException {

        final JsonObject jsonObject = je.getAsJsonObject();
        if (jsonObject.has("created_date")) {
            String date = jsonObject.get("created_date").getAsString();

            final long dateMillis = DateUtils.getDateMillis(date);
            jsonObject.addProperty("created_date", dateMillis);
        }
        if (jsonObject.has("message_status_id")) {
            int realStatus = jsonObject.get("message_status_id").getAsInt();

            int status = 0;
            switch (realStatus) {
                case 2:
                case 7:
                    status = INBOX_MSG_STATUS_READ;
                    break;
                case 5:
                    status = INBOX_MSG_STATUS_DELETED;
                    break;
                default:
                    status = INBOX_MSG_STATUS_NEW;
            }
            jsonObject.addProperty("message_status_id", status);
        }
        return new Gson().fromJson(jsonObject, type);
    }
}