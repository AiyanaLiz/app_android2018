package com.stopit.api;

/**
 * Created by Alenka on 20.07.2018.
 */

public interface ApiKeys {

    String ST_API_KEY_ACCESS_CODE = "access_code";
    String ST_API_KEY_INTELLICODE = "intellicode";
    String ST_API_KEY_SOURCE_ID = "source_id";
    String ST_API_KEY_INCIDENT = "incident";
    String ST_API_KEY_PAGE_SIZE = "pageSize";
    String ST_API_KEY_PAGE_NUM = "pageNum";
    String ST_API_KEY_SORT_DIRECTION = "sortDirection";
    String ST_API_KEY_INBOX_ID = "inbox_id";
    String ST_API_KEY_RETRIEVAL_CODE = "retrieval_code";
    String ST_API_KEY_PASS_CODE = "pass_code";
    String ST_API_KEY_INCIDENT_ID = "incident_id";
    String ST_API_KEY_ATTACHMENTS = "attachments";
    String ST_API_KEY_DATA = "data";
    String ST_API_KEY_MESSAGE_ID = "message_id";
    String ST_API_KEY_ACTION = "action";
    String ST_API_KEY_DELETE = "delete";
    String ST_API_KEY_UNREAD = "unread";
    String ST_API_KEY_READ = "read";
    String ST_API_KEY_ROOM_ID = "roomID";
    String ST_API_KEY_CONVERSATION_ID = "conversationID";
    String ST_API_KEY_CHAT_USERNAME = "chat_username";
    String ST_API_KEY_CHAT_MESSAGE_ID = "messageId";
    String ST_API_KEY_SORT_BY = "sortby";
    public static final String ST_API_KEY_PAYLOAD = "payload";
}
