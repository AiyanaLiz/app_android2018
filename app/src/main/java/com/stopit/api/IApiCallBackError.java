package com.stopit.api;

import android.support.annotation.NonNull;

import com.stopit.api.adapters.ApiError;

import retrofit2.Call;


public interface IApiCallBackError {

    boolean needToHandle();

    void showApiInProgressView();

    void hideApiInProgressView();

    void onError(@NonNull final Call call, @NonNull final ApiError error);

    /**
     * Called only when there is no Internet connection
     */
    void onFailure(@NonNull final Call call, @NonNull final Throwable t);

}
