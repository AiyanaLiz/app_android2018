package com.stopit.api.adapters;


import android.text.TextUtils;
import android.util.Log;

import com.google.gson.JsonObject;
import com.stopit.api.IApiCallBackError;
import com.stopit.api.IApiCallBackSuccess;
import com.stopit.api.services.AccountApiService;
import com.stopit.db.DBHelper;
import com.stopit.models.data_wrappers.StopitResponse;
import com.stopit.models.data_wrappers.UserData;
import com.stopit.utils.EncryptionHelper;

import org.json.JSONObject;

import static com.stopit.api.ApiKeys.ST_API_KEY_ACCESS_CODE;
import static com.stopit.api.ApiKeys.ST_API_KEY_DATA;
import static com.stopit.api.ApiKeys.ST_API_KEY_INTELLICODE;


public class AccountApiAdapter extends StopitApiAdapter<AccountApiService> {

    @Override
    Class<AccountApiService> provideServiceClass() {
        return AccountApiService.class;
    }

    public void register(final IApiCallBackSuccess<StopitResponse<UserData>> callBackSuccess,
                         final IApiCallBackError callBackError,
                         final String accessCode) {

        callBackError.showApiInProgressView();

        final JsonObject jsonParams = new JsonObject();
        jsonParams.addProperty(ST_API_KEY_ACCESS_CODE, accessCode);

        Log.d("AccountApiAdapter", "register params: " + jsonParams);
        getService().register(jsonParams).enqueue(checkResponse(callBackSuccess, callBackError));
    }

    public void register(final IApiCallBackSuccess<StopitResponse<UserData>> callBackSuccess,
                         final IApiCallBackError callBackError,
                         final long selectedOrgId) {

        callBackError.showApiInProgressView();
        final JsonObject jsonParams = new JsonObject();
        getService().register(selectedOrgId, jsonParams).enqueue(checkResponse(callBackSuccess, callBackError));
    }

    public void acceptTermsOfUse(final IApiCallBackSuccess<StopitResponse<Void>> callBackSuccess,
                                 final IApiCallBackError callBackError, final String intellicode) {

        callBackError.showApiInProgressView();
        final JsonObject jsonParams = new JsonObject();
        jsonParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        Log.d("AccountApiAdapter", "params: " + jsonParams);
        getService().acceptTermsOfUse(jsonParams).enqueue(checkResponse(callBackSuccess, callBackError));
    }

//    public void statusCheck(final IApiCallBackSuccess<StopitResponse<String>> callBackSuccess,
//                            final IApiCallBackError callBackError) {
//
//        final String intellicode = DBHelper.getIntellicode();
//        statusCheck(callBackSuccess, callBackError, intellicode);
//    }

    public void statusCheck(final IApiCallBackSuccess<StopitResponse<String>> callBackSuccess,
                            final IApiCallBackError callBackError, final String intellicode) {

        callBackError.showApiInProgressView();
        final JsonObject jsonParams = new JsonObject();
        jsonParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        final JsonObject encrypted = new JsonObject();
        encrypted.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedDataForCheckin(jsonParams));
        encrypted.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        getService().statusCheck(encrypted).enqueue(checkResponse(callBackSuccess, callBackError));
    }

    public void changeOrganization(final IApiCallBackSuccess<StopitResponse<String>> callBackSuccess,
                                   final IApiCallBackError callBackError,
                                   final String intellicode) {

        callBackError.showApiInProgressView();
        final JsonObject jsonParams = new JsonObject();
        jsonParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        final JsonObject encrypted = new JsonObject();
        encrypted.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedDataForCheckin(jsonParams));
        encrypted.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        getService().statusCheck(encrypted).enqueue(checkResponse(callBackSuccess, callBackError));
    }

    public void logout(final IApiCallBackSuccess<StopitResponse<UserData>> callBackSuccess,
                       final IApiCallBackError callBackError) {

        final String intellicode = DBHelper.getIntellicode();
        if(TextUtils.isEmpty(intellicode))
            return;

        callBackError.showApiInProgressView();
        final JsonObject jsonParams = new JsonObject();
        jsonParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        final JsonObject encrypted = EncryptionHelper.getEncryptedRandomIVRequestData(jsonParams);
        encrypted.addProperty(ST_API_KEY_INTELLICODE, DBHelper.getIntellicode());

        Log.d("AccountApiAdapter", "params: " + jsonParams);
        getService().logout(encrypted).enqueue(checkResponse(callBackSuccess, callBackError));
    }
}