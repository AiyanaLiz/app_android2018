package com.stopit.api.adapters;


import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.stopit.api.IApiCallBackError;
import com.stopit.api.IApiCallBackSuccess;
import com.stopit.api.services.IncidentsApiService;
import com.stopit.db.DBHelper;
import com.stopit.models.Incident;
import com.stopit.models.IncidentAttachment;
import com.stopit.models.data_wrappers.IncidentDataWrapper;
import com.stopit.models.data_wrappers.StopitResponse;
import com.stopit.utils.EncryptionHelper;

import java.util.List;

import static com.stopit.api.ApiKeys.ST_API_KEY_ATTACHMENTS;
import static com.stopit.api.ApiKeys.ST_API_KEY_DATA;
import static com.stopit.api.ApiKeys.ST_API_KEY_INCIDENT;
import static com.stopit.api.ApiKeys.ST_API_KEY_INCIDENT_ID;
import static com.stopit.api.ApiKeys.ST_API_KEY_INTELLICODE;
import static com.stopit.api.ApiKeys.ST_API_KEY_PASS_CODE;
import static com.stopit.api.ApiKeys.ST_API_KEY_SOURCE_ID;


public class IncidentsApiAdapter extends StopitApiAdapter<IncidentsApiService> {
    //hardcoded app id value
    private static final int SOURCE_ID = 1;


    @Override
    Class<IncidentsApiService> provideServiceClass() {
        return IncidentsApiService.class;
    }

    public void submitIncident(final IApiCallBackSuccess<StopitResponse<IncidentDataWrapper>> callBackSuccess,
                               final IApiCallBackError callBackError,
                               final Incident incident) {

        callBackError.showApiInProgressView();

        final String intellicode = DBHelper.getIntellicode();
        final JsonObject jsonData = new JsonObject();
        jsonData.addProperty(ST_API_KEY_INTELLICODE, intellicode);
        jsonData.addProperty(ST_API_KEY_SOURCE_ID, SOURCE_ID);
        jsonData.add(ST_API_KEY_INCIDENT, getGson().toJsonTree(incident));

//        final JsonObject encryptedBody = new JsonObject();
//        encryptedBody.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedData(jsonData));
//        encryptedBody.addProperty(ST_API_KEY_INTELLICODE, intellicode);
//        final String s = EncryptionHelper.getEncryptedData(jsonData);//EncryptionHelper.getDecryptedData(EncryptionHelper.getEncryptedData(jsonData));
//        Log.d("test", "test" + s);

        /*
        final JsonObject q1 = new JsonObject();
        q1.addProperty("answer_id", 0);
        q1.addProperty("answer_text", "августа 09, 2018");
        q1.addProperty("question_id", 3);
        q1.addProperty("question_text", "When did this happen?");

        final JsonObject q2 = new JsonObject();
        q2.addProperty("answer_id", 0);
        q2.addProperty("answer_text", "No");
        q2.addProperty("question_id", 13);
        q2.addProperty("question_text", "Did you take the anti-bullying course?");

        final JsonArray qArr = new JsonArray();
        qArr.add(q1);
        qArr.add(q2);

        final JsonObject incidentObj = new JsonObject();
        incidentObj.addProperty("id", 0);
        incidentObj.addProperty("attachment_count", 0);
        incidentObj.addProperty("message_count", 0);
        incidentObj.addProperty("org_id", 1070);
        incidentObj.addProperty("report_message", "uuuuuuu");
        //incidentObj.add("questions", qArr);

        final JsonObject result = new JsonObject();
        result.addProperty("intellicode", DBHelper.getIntellicode());
        result.addProperty("source_id", 1);
        result.add("incident", incidentObj);

        final JsonObject encryptedBody = new JsonObject();
        encryptedBody.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedData(result));
        encryptedBody.addProperty(ST_API_KEY_INTELLICODE, DBHelper.getIntellicode());
        final String s = EncryptionHelper.getDecryptedData(EncryptionHelper.getEncryptedData(result));
        Log.d("test", "test"+s);
*/

        getService().submitIncident(jsonData).enqueue(checkResponse(callBackSuccess, callBackError));
    }

    public void requestIncidentsList(final IApiCallBackSuccess<StopitResponse<String>> callBackSuccess,
                                     final IApiCallBackError callBackError) {

        callBackError.showApiInProgressView();
        final String intellicode = DBHelper.getIntellicode();
        final JsonObject jsonParams = new JsonObject();
        jsonParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        final JsonObject encryptedBody = new JsonObject();
        encryptedBody.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedData(jsonParams));
        encryptedBody.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        Log.d("IncidentsApiAdapter", "params: " + jsonParams);
        getService().getIncidentsList(encryptedBody).enqueue(checkResponse(callBackSuccess, callBackError));
    }

    public void getIncidentById(final IApiCallBackSuccess<StopitResponse<String>> callBackSuccess,
                                final IApiCallBackError callBackError,
                                final long incidentId) {

        callBackError.showApiInProgressView();
        final String intellicode = DBHelper.getIntellicode();

        final JsonObject jsonParams = new JsonObject();
        jsonParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        final JsonObject encryptedBody = new JsonObject();
        encryptedBody.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedData(jsonParams));
        encryptedBody.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        Log.d("IncidentsApiAdapter", "params: " + jsonParams);
        getService().getIncidentById(incidentId, encryptedBody).enqueue(checkResponse(callBackSuccess, callBackError));
    }

    public void getIncidentByIdAndCode(final IApiCallBackSuccess<StopitResponse<IncidentDataWrapper>> callBackSuccess,
                                       final IApiCallBackError callBackError,
                                       final long incidentId,
                                       final String code) {

        callBackError.showApiInProgressView();
        final JsonObject jsonParams = getParamsWithIntellicode();
        jsonParams.addProperty(ST_API_KEY_INCIDENT_ID, incidentId);
        jsonParams.addProperty(ST_API_KEY_PASS_CODE, code);

        Log.d("IncidentsApiAdapter", "params: " + jsonParams);
        getService().getIncidentByIdAndCode(jsonParams).enqueue(checkResponse(callBackSuccess, callBackError));
    }

    public void updateIncident(final IApiCallBackSuccess<StopitResponse<IncidentDataWrapper>> callBackSuccess,
                               final IApiCallBackError callBackError,
                               final long incidentId,
                               final List<IncidentAttachment> attachments) {

        callBackError.showApiInProgressView();
        final String intellicode = DBHelper.getIntellicode();

        final JsonObject jsonIncident = new JsonObject();
        jsonIncident.add(ST_API_KEY_ATTACHMENTS, getGson().toJsonTree(attachments));

        final JsonObject jsonParams = new JsonObject();
        jsonParams.add(ST_API_KEY_INCIDENT, jsonIncident);
        jsonParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);

//        final JsonObject encryptedBody = new JsonObject();
//        encryptedBody.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedData(jsonParams));
//        encryptedBody.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        Log.d("IncidentsApiAdapter", "params: " + jsonParams);
        getService().updateIncident(incidentId, jsonParams).enqueue(checkResponse(callBackSuccess, callBackError));
    }

    public void getDynamicQuestions(final IApiCallBackSuccess<StopitResponse<String>> callBackSuccess,
                                    final IApiCallBackError callBackError) {

        callBackError.showApiInProgressView();
        final String intellicode = DBHelper.getIntellicode();
        final JsonObject jsonParams = new JsonObject();
        jsonParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        final JsonObject encryptedBody = new JsonObject();
        encryptedBody.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedData(jsonParams));
        encryptedBody.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        Log.d("IncidentsApiAdapter", "params: " + jsonParams);
        getService().getDynamicQuestions(encryptedBody).enqueue(checkResponse(callBackSuccess, callBackError));
    }
}