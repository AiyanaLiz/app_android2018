package com.stopit.api.adapters;


import android.util.Log;

import com.stopit.api.IApiCallBackError;
import com.stopit.api.IApiCallBackSuccess;
import com.stopit.api.services.BrandingApiService;

import okhttp3.ResponseBody;

public class BrandingApiAdapter extends StopitApiAdapter<BrandingApiService> {

    @Override
    Class<BrandingApiService> provideServiceClass() {
        return BrandingApiService.class;
    }

    public void imageDownload(final IApiCallBackSuccess<ResponseBody> callBackSuccess,
                              final IApiCallBackError callBackError,
                              final String url) {

        callBackError.showApiInProgressView();
        Log.d("BrandingApiAdapter", "url: " + url);
        getService().imageDownload(url).enqueue(checkDownloadResponse(callBackSuccess, callBackError));
    }
}