package com.stopit.api.adapters;


import android.util.Log;

import com.google.gson.JsonObject;
import com.stopit.api.IApiCallBackError;
import com.stopit.api.IApiCallBackSuccess;
import com.stopit.api.services.ResourcesApiService;
import com.stopit.db.DBHelper;
import com.stopit.models.data_wrappers.IncidentDataWrapper;
import com.stopit.models.data_wrappers.ResourcesListDataWrapper;
import com.stopit.models.data_wrappers.StopitResponse;
import com.stopit.utils.EncryptionHelper;

import static com.stopit.api.ApiKeys.ST_API_KEY_DATA;
import static com.stopit.api.ApiKeys.ST_API_KEY_INTELLICODE;


public class ResourcesApiAdapter extends StopitApiAdapter<ResourcesApiService> {


    @Override
    Class<ResourcesApiService> provideServiceClass() {
        return ResourcesApiService.class;
    }

    public void getInformationResources(final IApiCallBackSuccess<StopitResponse<ResourcesListDataWrapper>> callBackSuccess,
                                        final IApiCallBackError callBackError) {

        callBackError.showApiInProgressView();

        final String intellicode = DBHelper.getIntellicode();
        final JsonObject jsonParams = new JsonObject();
        jsonParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);

//        final JsonObject encryptedBody = new JsonObject();
//        encryptedBody.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedData(jsonParams));
//        encryptedBody.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        Log.d("ResourcesApiAdapter", "params: " + jsonParams);
        getService().getInformationResources(jsonParams).enqueue(checkResponse(callBackSuccess, callBackError));
    }
}