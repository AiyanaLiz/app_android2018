package com.stopit.api.adapters;


import android.text.TextUtils;
import android.util.Log;

import com.google.gson.JsonObject;
import com.stopit.api.IApiCallBackError;
import com.stopit.api.IApiCallBackSuccess;
import com.stopit.api.services.BroadcastApiService;
import com.stopit.db.DBHelper;
import com.stopit.models.data_wrappers.BroadcastListWrapper;
import com.stopit.models.data_wrappers.BroadcastMessageWrapper;
import com.stopit.models.data_wrappers.StopitResponse;
import com.stopit.utils.EncryptionHelper;

import static com.stopit.api.ApiKeys.ST_API_KEY_ACTION;
import static com.stopit.api.ApiKeys.ST_API_KEY_DATA;
import static com.stopit.api.ApiKeys.ST_API_KEY_DELETE;
import static com.stopit.api.ApiKeys.ST_API_KEY_INTELLICODE;
import static com.stopit.api.ApiKeys.ST_API_KEY_MESSAGE_ID;
import static com.stopit.api.ApiKeys.ST_API_KEY_READ;
import static com.stopit.api.ApiKeys.ST_API_KEY_UNREAD;


public class BroadcastApiAdapter extends StopitApiAdapter<BroadcastApiService> {

    @Override
    Class<BroadcastApiService> provideServiceClass() {
        return BroadcastApiService.class;
    }

    public void getMessagesList(final IApiCallBackSuccess<StopitResponse<BroadcastListWrapper>> callBackSuccess,
                                final IApiCallBackError callBackError) {

        final String intellicode = DBHelper.getIntellicode();
        if(TextUtils.isEmpty(intellicode))
            return;

        callBackError.showApiInProgressView();

        final JsonObject jsonParams = new JsonObject();
        jsonParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);
//        jsonParams.addProperty(ST_API_KEY_PAGE_SIZE, "1");
//        jsonParams.addProperty(ST_API_KEY_PAGE_NUM, "1");
//        jsonParams.addProperty(ST_API_KEY_SORT_BY, "date");
//        jsonParams.addProperty(ST_API_KEY_SORT_DIRECTION, "ascending");

        final JsonObject encryptedBody = new JsonObject();
        encryptedBody.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedData(jsonParams));
        encryptedBody.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        Log.d("BroadcastApiAdapter", "params: " + jsonParams);
        getService().getMessagesList(encryptedBody).enqueue(checkResponse(callBackSuccess, callBackError));
    }

    public void updateMessage(final IApiCallBackSuccess<StopitResponse<BroadcastMessageWrapper>> callBackSuccess,
                              final IApiCallBackError callBackError,
                              final String messageId, final boolean isMessageRead) {

        callBackError.showApiInProgressView();

        final String intellicode = DBHelper.getIntellicode();
        final JsonObject jsonParams = new JsonObject();
        jsonParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);
        jsonParams.addProperty(ST_API_KEY_MESSAGE_ID, messageId);
        jsonParams.addProperty(ST_API_KEY_ACTION, isMessageRead ? ST_API_KEY_UNREAD : ST_API_KEY_READ);

        final JsonObject encryptedBody = new JsonObject();
        encryptedBody.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedData(jsonParams));
        encryptedBody.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        getService().updateMessage(encryptedBody).enqueue(checkResponse(callBackSuccess, callBackError));
    }

    public void getMessageById(final IApiCallBackSuccess<StopitResponse<BroadcastMessageWrapper>> callBackSuccess,
                               final IApiCallBackError callBackError,
                               final String messageId) {

        callBackError.showApiInProgressView();
        final String intellicode = DBHelper.getIntellicode();

        final JsonObject jsonParams = new JsonObject();
        jsonParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);
        jsonParams.addProperty(ST_API_KEY_MESSAGE_ID, messageId);

        final JsonObject encryptedBody = new JsonObject();
        encryptedBody.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedData(jsonParams));
        encryptedBody.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        Log.d("BroadcastApiAdapter", "params: " + jsonParams);
        getService().getMessageById(encryptedBody).enqueue(checkResponse(callBackSuccess, callBackError));
    }

    public void getMessageMediaUrl(final IApiCallBackSuccess<StopitResponse<String>> callBackSuccess,
                               final IApiCallBackError callBackError,
                               final String messageId) {

        callBackError.showApiInProgressView();
        final String intellicode = DBHelper.getIntellicode();

        final JsonObject jsonParams = new JsonObject();
        jsonParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);
        jsonParams.addProperty(ST_API_KEY_MESSAGE_ID, messageId);

        final JsonObject encryptedBody = new JsonObject();
        encryptedBody.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedData(jsonParams));
        encryptedBody.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        Log.d("BroadcastApiAdapter", "params: " + jsonParams);
        getService().getMessageUrl(encryptedBody).enqueue(checkResponse(callBackSuccess, callBackError));
    }

    public void deleteMessageById(final IApiCallBackSuccess<StopitResponse<BroadcastMessageWrapper>> callBackSuccess,
                                  final IApiCallBackError callBackError,
                                  final String messageId) {

        callBackError.showApiInProgressView();
        final String intellicode = DBHelper.getIntellicode();

        final JsonObject jsonParams = new JsonObject();
        jsonParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);
        jsonParams.addProperty(ST_API_KEY_MESSAGE_ID, messageId);
        jsonParams.addProperty(ST_API_KEY_ACTION, ST_API_KEY_DELETE);

        final JsonObject encryptedBody = new JsonObject();
        encryptedBody.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedData(jsonParams));
        encryptedBody.addProperty(ST_API_KEY_INTELLICODE, intellicode);
        getService().deleteMessageById(encryptedBody).enqueue(checkResponse(callBackSuccess, callBackError));
    }

    public void getUnreadInboxCount(final IApiCallBackSuccess<StopitResponse<String>> callBackSuccess,
                                   final IApiCallBackError callBackError) {

        callBackError.showApiInProgressView();
        final String intellicode = DBHelper.getIntellicode();

        final JsonObject jsonParams = new JsonObject();
        jsonParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        final JsonObject encryptedBody = new JsonObject();
        encryptedBody.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedData(jsonParams));
        encryptedBody.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        Log.d("BroadcastApiAdapter", "params: " + jsonParams);
        getService().getUnreadInboxCount(encryptedBody).enqueue(checkResponse(callBackSuccess, callBackError));
    }
}