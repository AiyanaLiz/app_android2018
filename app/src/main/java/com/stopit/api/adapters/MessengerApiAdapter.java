package com.stopit.api.adapters;


import android.util.Log;

import com.google.gson.JsonObject;
import com.stopit.api.IApiCallBackError;
import com.stopit.api.IApiCallBackSuccess;
import com.stopit.api.services.MessengerApiService;
import com.stopit.db.DBHelper;
import com.stopit.models.data_wrappers.StopitResponse;
import com.stopit.utils.EncryptionHelper;

import static com.stopit.api.ApiKeys.ST_API_KEY_DATA;
import static com.stopit.api.ApiKeys.ST_API_KEY_INTELLICODE;
import static com.stopit.api.ApiKeys.ST_API_KEY_ROOM_ID;


public class MessengerApiAdapter extends StopitApiAdapter<MessengerApiService> {

    @Override
    Class<MessengerApiService> provideServiceClass() {
        return MessengerApiService.class;
    }

    public void getChatHistory(final IApiCallBackSuccess<StopitResponse<String>> callBackSuccess,
                               final IApiCallBackError callBackError, final long incidentId) {

        callBackError.showApiInProgressView();

        final String intellicode = DBHelper.getIntellicode();
        final JsonObject jsonData = new JsonObject();
        jsonData.addProperty(ST_API_KEY_ROOM_ID, incidentId);

        final JsonObject encryptedParams = new JsonObject();
        encryptedParams.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedData(jsonData));
        encryptedParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        Log.d("BroadcastApiAdapter", "params: " + encryptedParams);
        getService().getChatHistory(encryptedParams).enqueue(checkResponse(callBackSuccess, callBackError));
    }

    public void updateMessages(final IApiCallBackSuccess<StopitResponse<Void>> callBackSuccess,
                               final IApiCallBackError callBackError, final long incidentId) {

        callBackError.showApiInProgressView();

        final String intellicode = DBHelper.getIntellicode();
        final JsonObject jsonData = new JsonObject();
        jsonData.addProperty(ST_API_KEY_ROOM_ID, incidentId);

        final JsonObject encryptedParams = new JsonObject();
        encryptedParams.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedData(jsonData));
        encryptedParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        getService().updateMessages(encryptedParams).enqueue(checkResponse(callBackSuccess, callBackError));
    }

    public void getTotalUnreadMessagesCount(final IApiCallBackSuccess<StopitResponse<String>> callBackSuccess,
                               final IApiCallBackError callBackError) {

        callBackError.showApiInProgressView();

        final String intellicode = DBHelper.getIntellicode();
        final JsonObject jsonData = new JsonObject();
        jsonData.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        final JsonObject encryptedParams = new JsonObject();
        encryptedParams.addProperty(ST_API_KEY_DATA, EncryptionHelper.getEncryptedData(jsonData));
        encryptedParams.addProperty(ST_API_KEY_INTELLICODE, intellicode);

        Log.d("BroadcastApiAdapter", "params: " + encryptedParams);
        getService().getTotalUnreadMessagesCount(encryptedParams).enqueue(checkResponse(callBackSuccess, callBackError));
    }
}