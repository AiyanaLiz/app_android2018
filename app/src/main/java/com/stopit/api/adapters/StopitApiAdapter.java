package com.stopit.api.adapters;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.stopit.api.IApiCallBackError;
import com.stopit.api.IApiCallBackSuccess;
import com.stopit.api.deserializers.BroadcastMessageDeserializer;
import com.stopit.db.DBHelper;
import com.stopit.models.BroadcastMessage;
import com.stopit.models.data_wrappers.StopitResponse;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.stopit.api.ApiKeys.ST_API_KEY_INTELLICODE;

public abstract class StopitApiAdapter<ApiService> extends BaseApiAdapter<ApiService> {

    JsonObject getParamsWithIntellicode() {
        JsonObject jsonParams = new JsonObject();
        jsonParams.addProperty(ST_API_KEY_INTELLICODE, DBHelper.getIntellicode());
        return jsonParams;
    }

    /* *
     * Check response and transfer it to incomingCallback. If incomingCallback == null,
     * or apiCallbackHandler.needToHandle() return false - response not be transfer.
     */
    <T> Callback<StopitResponse<T>> checkResponse(final IApiCallBackSuccess<StopitResponse<T>> callbackSuccess, final IApiCallBackError callbackError) {
        return new Callback<StopitResponse<T>>() {
            @Override
            public void onResponse(@NonNull final Call<StopitResponse<T>> call, @NonNull final Response<StopitResponse<T>> response) {
                android.util.Log.i("StopitApiAdapter", String.format(Locale.US, "Success = %b, Code = %d", response.isSuccessful(), response.code()));
                callbackError.hideApiInProgressView();
                if (response.isSuccessful()) {
                    Log.d("response ", "responce string " + response.toString());
                    StopitResponse<T> responseBody = response.body();
                    if (responseBody == null || (responseBody.getData() == null && responseBody.getCode() != 100000)) {
                        if (callbackError.needToHandle()) {
                            if (responseBody == null)
                                callbackError.onError(call, new ApiError(response));
                            else
                                callbackError.onError(call, new ApiError(responseBody));
                        }
                    } else
                        callbackSuccess.onApiSuccess(responseBody);

                } else {
                    callbackError.onError(call, new ApiError(response));
                }
            }

            @Override
            public void onFailure(@NonNull final Call<StopitResponse<T>> call, @NonNull final Throwable t) {
                callbackError.hideApiInProgressView();
                android.util.Log.i("StopitApiAdapter", "onFailure " + t);
                if (callbackError.needToHandle()) {
                    callbackError.onError(call, new ApiError(t));
                }
            }
        };
    }


    <T> Callback<T> checkDownloadResponse(final IApiCallBackSuccess<T> callbackSuccess, final IApiCallBackError callbackError) {
        return new Callback<T>() {
            @Override
            public void onResponse(@NonNull final Call<T> call, @NonNull final Response<T> response) {
                android.util.Log.i("StopitApiAdapter", String.format(Locale.US, "Success = %b, Code = %d", response.isSuccessful(), response.code()));
                callbackError.hideApiInProgressView();
                if (response.isSuccessful()) {
                    Log.d("response ", "responce string " + response.toString());
                    T responseBody = response.body();
                    if (responseBody == null) {
                        // in case if its null but <T> is Void request succeed
                        // but if <T> is any other type tna Void its error
                        // its ugly solution but it works
                        try {
                            Response<Void> voidResponse = (Response<Void>) response;
                            // its Void so its succeed
                            callbackSuccess.onApiSuccess(responseBody);
                        } catch (ClassCastException e) {
                            // T is not Void so its error
                            if (callbackError.needToHandle())
                                callbackError.onError(call, new ApiError(response));
                        }
                    } else {
                        callbackSuccess.onApiSuccess(responseBody);
                    }
                } else if (callbackError.needToHandle()) {
                    callbackError.onError(call, new ApiError(response));
                }
            }

            @Override
            public void onFailure(@NonNull final Call<T> call, @NonNull final Throwable t) {
                callbackError.hideApiInProgressView();
                android.util.Log.i("StopitApiAdapter", "onFailure " + t);
                if (callbackError.needToHandle()) {
                    callbackError.onError(call, new ApiError(t));
                }

            }
        };
    }


    @Override
    void registerTypeAdapters(final GsonBuilder gsonBuilder) {
        gsonBuilder
                .registerTypeAdapter(BroadcastMessage.class, new BroadcastMessageDeserializer<BroadcastMessage>());
               // .registerTypeAdapter(BroadcastMessage.class, new InboxStatusDeserializer<BroadcastMessage>());
//                .registerTypeAdapter(new TypeToken<String>() {}.getType(), new RealmPrimitiveArrayDeserializer())
//                .registerTypeAdapter(new TypeToken<List<String>>() {
//                }.getType(), new TypeAdapter<List<String>>() {
//
//                    @Override
//                    public void write(JsonWriter out, List<String> value) throws IOException {
//                        // Ignore
//                    }
//
//                    @Override
//                    public List<String> read(JsonReader in) throws IOException {
//                        List<String> list = new ArrayList<>();
//                        in.beginArray();
//                        while (in.hasNext()) {
//                            list.add(in.nextString());
//                        }
//                        in.endArray();
//                        return list;
//                    }
//                })
//                .registerTypeAdapter(new TypeToken<List<Integer>>() {
//                        }.getType(), new TypeAdapter<List<Integer>>() {
//
//                            @Override
//                            public void write(JsonWriter out, List<Integer> value) throws IOException {
//                                // Ignore
//                            }
//
//                            @Override
//                            public List<Integer> read(JsonReader in) throws IOException {
//                                List<Integer> list = new ArrayList<>();
//                                in.beginArray();
//                                while (in.hasNext()) {
//                                    list.add(in.nextInt());
//                                }
//                                in.endArray();
//                                return list;
//                            }
//                        }
//                );
    }

    /*@Override
        void addInterceptors(final OkHttpClient.Builder okClient) {
            super.addInterceptors(okClient);
            okClient.addInterceptor(new AuthenticationInterceptor());
    }*/
}
