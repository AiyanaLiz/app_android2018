package com.stopit.api.adapters;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;
import com.stanko.tools.ResHelper;
import com.stopit.R;
import com.stopit.models.data_wrappers.StopitResponse;
import com.stopit.models.data_wrappers.VersionStatusWrapper;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.security.cert.CertPathValidatorException;

import javax.net.ssl.SSLHandshakeException;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class ApiError {

    public static final int ST_INVALID_ACCESS_CODE = 40030010;
    private static final int DEFAULT_NO_IC = 1024;
    private static final int RESPONSE_IS_NULL = 1025;
    private static final int JSON_PARSE_EXCEPTION = 1026;
    private static final int SERVER_NOT_RESPONDING = 1027;
    private static final int SOCKET_TIMEOUT = 1028;
    private static final int UNKNOWN_HOST = 1029;
    private static final int SSL_PROBLEM = 1030;
    private static final int SERVER_IO = 1032;
    private static final int UNKNOWN_ERROR = 1033;
    private static final int ST_SERVER_ERR = 100005;
    private static final int ST_INVALID_INPUT = 100001;
    private static final int ST_NO_DB_CONNECTION = 100002;
    private static final int ST_INVALID_INTELLICODE = 100003;


    //private static final
    private int mResponseCode;

    @SerializedName("code")
    private int mCode = DEFAULT_NO_IC;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("message")
    private String mMessage;

    public ApiError() {
    }

    public ApiError(Throwable t) {
        if (t != null) {
            t.printStackTrace();
            if (t instanceof JsonSyntaxException)
                this.mCode = JSON_PARSE_EXCEPTION;
            else if (t instanceof ConnectException)
                this.mCode = SERVER_NOT_RESPONDING;
            else if (t instanceof UnknownHostException)
                this.mCode = UNKNOWN_HOST;
            else if (t instanceof SocketTimeoutException) {
                this.mCode = SOCKET_TIMEOUT;
            } else if (t instanceof SSLHandshakeException || t instanceof CertPathValidatorException) {
                //javax.net.ssl.SSLHandshakeException: SSL handshake aborted: ssl=0x7f75e98980: I/O error during system call, Connection reset by peer
                this.mCode = SSL_PROBLEM;
            } else {
                this.mCode = SERVER_IO;
            }
            this.mTitle = t.getClass().getSimpleName();
            this.mMessage = t.getMessage();
        }
    }

    public <T> ApiError(Response<T> response, int code) {
        ApiError apiError = null;
        if (response != null)
            try {
                apiError = parseServerError(response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        if (apiError == null) {
            this.mTitle = "Null body() in response";
            this.mCode = code;
            this.mMessage = this.mTitle;
        } else {
            this.mTitle = apiError.getTitle();
            this.mCode = code;
            this.mMessage = apiError.getMessage();
        }
    }

    public <T> ApiError(Response<T> response) {
        // try to parse error first
        ApiError apiError = null;
        if (response != null)
            try {
                apiError = parseServerError(response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        if (apiError == null) {
            if (response == null) {
                this.mCode = RESPONSE_IS_NULL;
            } else {
                this.mCode = response.code();
                final ResponseBody errorBody = response.errorBody();
                if (errorBody != null) {
                    try {
                        this.mMessage = errorBody.string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (TextUtils.isEmpty(this.mMessage)) {
                    this.mMessage = response.message();
                }
            }
            // handle http errors like 500..503
            if (mCode > 499 && mCode < 999) {
                mCode = 50000;
            }
        } else {
            this.mTitle = apiError.getTitle();
            this.mCode = apiError.getCode();
            this.mMessage = apiError.getMessage();
        }
    }

    public ApiError(@NonNull StopitResponse response) {
        this.mCode = response.getCode();
        switch (mCode) {
            case ST_INVALID_INPUT:
                this.mMessage = ResHelper.getString(R.string.api_err_100001);
                break;
            case ST_NO_DB_CONNECTION:
                this.mMessage = ResHelper.getString(R.string.api_err_100002);
                break;
            case ST_INVALID_INTELLICODE:
                this.mMessage = ResHelper.getString(R.string.api_err_100003);
                break;
            case ST_SERVER_ERR:
                this.mMessage = ResHelper.getString(R.string.api_err_100005);
                break;
            default:
                this.mMessage = response.getMessage();
        }
    }

    public ApiError(@NonNull VersionStatusWrapper response) {
        this.mMessage = "";
    }

    public int getResponseCode() {
        return mResponseCode;
    }

    public void setResponseCode(int responseCode) {
        this.mResponseCode = responseCode;
    }

    public int getCode() {
        return mCode;
    }

    public void setCode(int code) {
        this.mCode = code;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getMessage() {
        switch (mCode) {
            case 60080040:
            case 60070040:
            case 60060040:
            case 60050040:
            case 60040040:
            case 60030040:
            case 60020040:
            case 60010040:
                return ResHelper.getString(R.string.api_error_del_obj);

            case 40080040:
            case 40070040:
            case 40060040:
            case 40050040:
            case 40040040:
            case 40030040:
            case 40020040:
            case 40010040:
                return ResHelper.getString(R.string.api_error_del_info_missing);

            case 30080040:
            case 30070040:
            case 30060040:
            case 30050040:
            case 30040040:
            case 30030040:
            case 30020040:
            case 30010040:
                return ResHelper.getString(R.string.api_error_del_failure);

            case 60080030:
            case 60070030:
            case 60060030:
            case 60050030:
            case 60040030:
            case 60030030:
            case 60020030:
            case 60010030:
                return ResHelper.getString(R.string.api_error_update_no_permissions);

            case 60080020:
            case 60070020:
            case 60060020:
            case 60050020:
            case 60040020:
            case 60020020:
            case 60010020:
                return ResHelper.getString(R.string.api_error_no_permissions);

            case 60030020: //new api deactivated user err
            case 100022: //old api deactivated user err
                return ResHelper.getString(R.string.api_err_deactivated_user);

            case 40080030:
            case 40070030:
            case 40060030:
            case 40050030:
            case 40040030:
            case 40030030:
            case 40020030:
            case 40010030:
                return ResHelper.getString(R.string.api_error_update_info_missing);

            case 30080030:
            case 30070030:
            case 30060030:
            case 30050030:
            case 30040030:
            case 30030030:
            case 30020030:
            case 30010030:
                return ResHelper.getString(R.string.api_error_update_failure);

            case 40080020:
            case 40070020:
            case 40060020:
            case 40050020:
            case 40040020:
            case 40030020:
            case 40020020:
            case 40010020:
                return ResHelper.getString(R.string.api_error_retrieve_info_missing);

            case 30080020:
            case 30070020:
            case 30060020:
            case 30050020:
            case 30040020:
            case 30030020:
            case 30020020:
            case 30010020:
                return ResHelper.getString(R.string.api_error_retrieve_failure);

            case 60080010:
            case 60070010:
            case 60060010:
            case 60050010:
            case 60040010:
            case 60030010:
            case 60020010:
            case 60010010:
                return ResHelper.getString(R.string.api_error_save_no_permissions);

            case 40080010:
            case 40070010:
            case 40060010:
            case 40050010:
            case 40040010:
            case 40030010:
            case 40020010:
            case 40010010:
                return ResHelper.getString(R.string.api_error_save_info_missing);

            case 30080010:
            case 30070010:
            case 30060010:
            case 30050010:
            case 30040010:
            case 30030010:
            case 30020010:
            case 30010010:
                return ResHelper.getString(R.string.api_error_save_failure);

            default:
                return mMessage;
        }
    }


    public void setMessage(String message) {
        this.mMessage = message;
    }

    @Override
    public String toString() {
        return "ResponseCode=" + mResponseCode +
                ", Title='" + mTitle + '\'' +
                ", Code=" + mCode +
                ", Message='" + mMessage;
    }

    private ApiError parseServerError(Response<?> response) {
        ApiError error = null;
        if (response != null) {
            final ResponseBody responseBody = response.errorBody();
            if (responseBody != null) {
                String responseBodyString = null;
                try {
                    responseBodyString = responseBody.string();
                } catch (IOException e) {
                    //Crashlytics.log(e.getMessage());
                }
                if (responseBodyString != null)
                    try {
                        error = new Gson().fromJson(responseBodyString, ApiError.class);
                    } catch (JsonSyntaxException e) {
                        //Crashlytics.log(e.getMessage());
                    }
                if (error == null) {
                    error = new ApiError();
                    error.setMessage(responseBodyString);
                    error.setCode(response.code() * 100);
                }
                error.setResponseCode(response.code());
                if (error.getCode() == ApiError.DEFAULT_NO_IC) {
                    error.setCode(response.code() * 100);
                }
            }
        }
        return error;
    }


}
