package com.stopit.api.adapters;


import android.support.annotation.CallSuper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.readystatesoftware.chuck.ChuckInterceptor;
import com.stopit.BuildConfig;
import com.stopit.app.StopitApp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

abstract class BaseApiAdapter<ApiService> {

    public static final int TIME_OUT = 10000; // ms
    public static String PROD_URL = "https://api-uat.stopit.io";
    public static String PREPROD_URL = "https://api-uat.stopit.io";
//    public static String STAGING_URL = "https://api-staging.stopit.fm";

    private final ApiService mApiService;
    private final Gson mGson;

    BaseApiAdapter() {
        final OkHttpClient.Builder okClientBuilder = new OkHttpClient.Builder()
                .connectTimeout(TIME_OUT, TimeUnit.MILLISECONDS)
                .readTimeout(TIME_OUT, TimeUnit.MILLISECONDS)
                .writeTimeout(TIME_OUT, TimeUnit.MILLISECONDS);
        //TODO uncomment if Chuck needed
        addInterceptors(okClientBuilder);

        final GsonBuilder gsonBuilder = new GsonBuilder();
        registerTypeAdapters(gsonBuilder.setLenient());
        mGson = gsonBuilder.create();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(mGson))
                .client(okClientBuilder.build())
                .build();
        mApiService = retrofit.create(provideServiceClass());
    }

    static String getBaseUrl() {
        if (StopitApp.isProd)
            return PROD_URL;
        else
            return PREPROD_URL;//STAGING_URL;
    }

    public static boolean writeResponseBody(ResponseBody body, String path) {
        try {

            File file = new File(path);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];
                //long fileSize = body.contentLength();
                //long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(file);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }
                    outputStream.write(fileReader, 0, read);
                    //fileSizeDownloaded += read;
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    abstract Class<ApiService> provideServiceClass();

    @CallSuper
    void addInterceptors(final OkHttpClient.Builder okClient) {
        if (BuildConfig.DEBUG) {
            okClient
//                    .addInterceptor(
//                            new LoggingInterceptor.Builder()
//                                    .loggable(true)
//                                    .setLevel(Level.BASIC)
//                                    .log(Platform.INFO)
//                                    .request("API_Request")
//                                    .response("API_Response")
//                                    .build()
//                    )
                    .addInterceptor(new ChuckInterceptor(StopitApp.getAppContext()));
        }
    }

    void registerTypeAdapters(final GsonBuilder gsonBuilder) {
    }

    ApiService getService() {
        return mApiService;
    }

    Gson getGson() {
        return mGson;
    }
}
