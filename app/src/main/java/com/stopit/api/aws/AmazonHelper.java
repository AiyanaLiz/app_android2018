package com.stopit.api.aws;

import android.content.Context;
import android.util.Log;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.stopit.activity.OnUploadFinishedCallback;
import com.stopit.db.DBHelper;
import com.stopit.models.AWS;
import com.stopit.models.Organization;

import java.io.File;

/**
 * Created by Alenka on 19.06.2018.
 */

public class AmazonHelper /*extends AsyncTask<String,Void ,Boolean>*/ {
    private static final String STATE_COMPLETED = "COMPLETED";
    private static final String ORGS_STR = "orgs/";
    private static final String INCIDENTS_STR = "/incidents/";
    private static final String ATTACHMENTS_STR = "/attachments/";
    private static final String DELIMITER_STR = "/";

    public static void sendMedia(final Context context, final File file, long incidentId, final OnUploadFinishedCallback callback) {
        final AWS aws = DBHelper.getCurrentConfiguration().getAws();

        final String dirName = getAttachmentsDirectory(incidentId, file.getName());

        Log.d("AWS", "aws cognitoPoolId " + aws.getCognitoPoolId());
        Log.d("AWS", "aws region " + aws.getRegion());
        Log.d("AWS", "aws bucket " + aws.getBucket());

        Log.d("AWS", "aws file exists " + file.exists());
        Log.d("AWS", "aws file can read " + file.canRead());
        Log.d("AWS", "aws dirName " + dirName);
        Log.d("AWS", "aws file " + file.getName());
        Log.d("AWS", "aws file abs path " + file.getAbsolutePath());

        AmazonS3Client s3Client = new AmazonS3Client(getCredentialsProvider(context, aws), getAWSConfiguration());
        TransferUtility transferUtility = new TransferUtility(s3Client, context);
        TransferObserver observer = transferUtility.upload(aws.getBucket(), dirName, file);
        Log.d("AWS", "aws dirName " + dirName);
        Log.d("AWS", "aws file " + file.getName());
        Log.d("AWS", "aws file abs path " + file.getAbsolutePath());

        try {
            observer.setTransferListener(new TransferListener() {

                @Override
                public void onStateChanged(int id, TransferState state) {
                    Log.i("TransferUtility", "TransferUtility state " + state.toString());
                    if (state.toString().equals(STATE_COMPLETED)) {
                        if (callback != null)
                            callback.onUploadFinished(file, dirName);
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    Log.i("TransferUtility", "TransferUtility onProgressChanged");
                }

                @Override
                public void onError(int id, Exception ex) {
                    Log.e("TransferUtility", "Error " + ex.toString());
                    if (callback != null)
                        callback.onUploadFailed();
//                    String error = "";
//                    if (ex != null && ex.getCause() != null) {
//                        error = "Incident Id : " + incdentId + ", Image Id : " + id + ", Error Cause : " + ex.getCause().toString();
//                    } else if (ex != null && ex.getMessage() != null) {
//                        error = "Incident Id : " + incdentId + ", Image Id : " + id + ", Error Cause : " + ex.getMessage().toString();
//                    } else {
//                        error = "Incident Id : " + incdentId + ", Image Id : " + id + ", Error Cause : Error cause null";
//                    }
//
//                    AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_S3Upload_Error, AnalyticsManager.On_Report, error, true, null);
                }
            });
        } catch (Exception ex) {
            Log.e("TransferUtility", "TransferUtility Exception");
            if (callback != null)
                callback.onUploadFailed();
        }
    }

    private static CognitoCachingCredentialsProvider getCredentialsProvider(final Context context, final AWS aws) {
        return new CognitoCachingCredentialsProvider(
                context,
                aws.getCognitoPoolId(),
                aws.getRegion_code());
    }

    private static ClientConfiguration getAWSConfiguration() {
        ClientConfiguration s3Config = new ClientConfiguration();
        s3Config.setMaxErrorRetry(10);
        return s3Config;
    }

    private static String getAttachmentsDirectory(final long incidentId, final String fileName) {
        final long currentMillis = System.currentTimeMillis();
        final Organization currentOrg = DBHelper.getCurrentOrganization();
        if (currentOrg == null)
            return "";
        else
            return ORGS_STR + currentOrg.getId() + INCIDENTS_STR + incidentId + DELIMITER_STR
                    + currentMillis + ATTACHMENTS_STR + fileName;
//            return "orgs/1070/incidents/227159/1533632730885/attachments/file0.jpg";


    }
}
