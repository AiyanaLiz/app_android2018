package com.stopit.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public abstract class IncidentsBaseFragment extends Fragment{

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        final EventBus eventBus = EventBus.getDefault();
        if (!eventBus.isRegistered(this)) eventBus.register(this);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        final EventBus eventBus = EventBus.getDefault();
        if (eventBus.isRegistered(this)) eventBus.unregister(this);
    }


    @LayoutRes
    protected abstract int getContentView();

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        return inflater.inflate(getContentView(), container, false);
    }

    @CallSuper
    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
        setUI(savedInstanceState);
    }

    protected abstract void initUI();

    protected abstract void setUI(@Nullable final Bundle savedInstanceState);


    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(Object event) {
    }


}
