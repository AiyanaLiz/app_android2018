package com.stopit.fragment.tabs;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.andexert.library.RippleView;
import com.stopit.R;
import com.stopit.activity.MainActivity;
import com.stopit.adapter.GetHelpListAdapter;
import com.stopit.db.DBHelper;
import com.stopit.models.CrisisCenter;
import com.stopit.models.HelpOption;
import com.stopit.utils.EmailHelper;
import com.stopit.views.HelpOptionsPopup;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;

import static com.stopit.app.Constants.HELP_OPTION_MAKE_A_CALL;
import static com.stopit.app.Constants.HELP_OPTION_SEND_AN_EMAIL;
import static com.stopit.app.Constants.HELP_OPTION_SEND_SMS;
import static com.stopit.app.Constants.HELP_OPTION_URL;

public class HelpFragment extends BaseFragment implements HelpOptionsPopup.OnHelpOptionChoiceListener {

    private RecyclerView mRecyclerView;
    private GetHelpListAdapter mAdapter;
    private RealmResults<CrisisCenter> helpContactsList;
    private HelpOptionsPopup popup;

    private RippleView.OnRippleCompleteListener itemSelected = new RippleView.OnRippleCompleteListener() {
        @Override
        public void onComplete(RippleView rippleView) {
            final Object tag = rippleView.getTag();
            if (tag == null || !(tag instanceof CrisisCenter))
                return;

            final CrisisCenter center = (CrisisCenter) tag;
            final List<HelpOption> optionsList = getOptionsList(center);
            final int optionsCount = optionsList.size();
            if (optionsCount == 0)
                return;

            if (optionsCount == 1) {
                final HelpOption option = optionsList.get(0);
                handleOptionSelection(option.getType(), option.getValue());

            } else if (getActivity() != null) {
                if (popup == null) {
                    popup = new HelpOptionsPopup(getActivity());
                    popup.setOnHelpOptionChoiceListener(HelpFragment.this);
                }
                popup.show(center.getName(), optionsList);
            }
        }
    };


    @LayoutRes
    protected int getContentView() {
        return R.layout.fragment_get_help;
    }

    @Override
    protected void initUI() {
        final View root = getView();
        if (root == null)
            return;

        mRecyclerView = root.findViewById(R.id.help_centers_recycler_view);
    }

    @Override
    protected void setUI(@Nullable Bundle savedInstanceState) {
        populateContactsList();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new GetHelpListAdapter<>(helpContactsList, itemSelected);
        mAdapter.bindToRecyclerView(mRecyclerView);
    }

    private void populateContactsList() {
        helpContactsList = DBHelper.getHelpContactsList();
        helpContactsList.addChangeListener(new RealmChangeListener<RealmResults<CrisisCenter>>() {
            @Override
            public void onChange(RealmResults<CrisisCenter> centers) {
                if (getActivity() != null && !getActivity().isFinishing())
                    mAdapter.notifyDataSetChanged();
            }
        });
    }

    private List<HelpOption> getOptionsList(final CrisisCenter center) {
        final List<HelpOption> options = new ArrayList<>();
        if (!TextUtils.isEmpty(center.getPhoneNumber())) {
            options.add(new HelpOption(HELP_OPTION_MAKE_A_CALL, center.getPhoneNumber()));
        }
        if (!TextUtils.isEmpty(center.getMobilePhoneNumber())) {
            options.add(new HelpOption(HELP_OPTION_SEND_SMS, center.getMobilePhoneNumber()));
        }
        if (center.isValidEmail()) {
            options.add(new HelpOption(HELP_OPTION_SEND_AN_EMAIL, center.getValidEmail()));
        }
        if (!center.isValidEmail() && center.isValidURL()) {
            options.add(new HelpOption(HELP_OPTION_URL, center.getCrisisCenterWebUrl()));
        }
        return options;
    }

    @Override
    public void handleHelpOptionsChoice(HelpOptionsPopup popup, int actionId, final String value) {
        handleOptionSelection(actionId, value);
    }

    @Override
    public void handleHelpOptionsCancel() {

    }

    private void handleOptionSelection(final int optionId, final String value) {
        switch (optionId) {
            case HELP_OPTION_MAKE_A_CALL:
                makeACall(value);
                break;
            case HELP_OPTION_SEND_SMS:
                sendSMS(value);
                break;
            case HELP_OPTION_SEND_AN_EMAIL:
                sendEmail(value);
                break;
            case HELP_OPTION_URL:
                openUrl(value);
                break;
        }
    }

    private void sendEmail(final String email) {
        if (getActivity() == null)
            return;
        final String subject = "";
        EmailHelper.sendEmail(getActivity(), email, subject, getString(R.string.email_helper_security_exc), getString(R.string.email_helper_no_app_exc));
    }

    private void sendSMS(final String mobilePhone) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("sms:" + mobilePhone));
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            ex.printStackTrace();
            showFailureAlert(R.string.not_available_sms);
        }
    }

    private void openUrl(final String centerUrl) {
        try {
            if (!TextUtils.isEmpty(centerUrl)) {
                Uri uri = Uri.parse(centerUrl);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        } catch (ActivityNotFoundException ex) {
            ex.printStackTrace();
            showFailureAlert(R.string.not_available_browser);
        }
    }

    private void makeACall(final String phoneNumber) {
        try {
            Uri call = Uri.parse("tel:" + phoneNumber);
            Intent intent = new Intent(Intent.ACTION_DIAL, call);
            //Intent intent = new Intent(Intent.ACTION_CALL, call);
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            ex.printStackTrace();
            showFailureAlert(R.string.not_available_tel);
        }
    }

    private void showFailureAlert(final int messageRes) {
        final MainActivity parent = (MainActivity) getActivity();
        if (parent != null && !parent.isFinishing())
            parent.showAlertDialog("", getString(messageRes));
    }
}
