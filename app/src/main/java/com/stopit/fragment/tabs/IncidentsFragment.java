package com.stopit.fragment.tabs;


import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.stopit.R;
import com.stopit.activity.MainActivity;
import com.stopit.db.DBHelper;
import com.stopit.event.RightButtonClickEvent;
import com.stopit.event.RightButtonInterceptEvent;
import com.stopit.event.RightButtonVisibleEvent;
import com.stopit.event.UnreadChatMessageEvent;
import com.stopit.event.UnreadInboxMessageEvent;
import com.stopit.fragment.BroadcastsFragment;
import com.stopit.fragment.IncidentsListFragment;
import com.stopit.models.BroadcastMessage;
import com.stopit.utils.BrandingHelper;
import com.stopit.views.StopitTextView;
import com.stopit.views.brandable.BrandableTabLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.realm.RealmResults;

import static com.stopit.app.Constants.EB_KEY_IS_INCIDENTS_MODE;
import static com.stopit.app.Constants.EB_KEY_UNREAD_CHAT_COUNT;
import static com.stopit.app.Constants.EB_KEY_UNREAD_INBOX_COUNT;

public class IncidentsFragment extends BaseFragment implements ViewPager.OnPageChangeListener {

    private static boolean isNotificationReceived;
    private BrandableTabLayout mTabLayout;
    private ViewPager mViewPager;
    private RealmResults<BroadcastMessage> notifications;
    private View.OnClickListener rightBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            manageRightButton(mViewPager.getCurrentItem());
        }
    };

    @LayoutRes
    protected int getContentView() {
        return R.layout.fragment_incidents;
    }

    @Override
    protected void initUI() {
        final View root = getView();
        if (root == null)
            return;

        mTabLayout = root.findViewById(R.id.incidents_tab_layout);
        mViewPager = root.findViewById(R.id.incidents_view_pager);
    }

    @Override
    protected void setUI(@Nullable Bundle savedInstanceState) {
        final IncidentsPagerAdapter adapter = new IncidentsPagerAdapter();
        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.addOnPageChangeListener(this);

        mTabLayout.setupWithViewPager(mViewPager);
        setCustomTab();

        notifications = DBHelper.getBroadcastMessagesList();

        final MainActivity parentActivity = ((MainActivity) getActivity());
        if (parentActivity != null) {
            final StopitTextView toolbarBtn = parentActivity.getToolbarRigntBtn();
            toolbarBtn.setOnClickListener(rightBtnClickListener);
        }
        //onPageSelected(/*isNotificationReceived ? 1 : */0);
    }

    private void setRightButton() {
        final MainActivity parentActivity = ((MainActivity) getActivity());
        if (parentActivity != null) {
            final StopitTextView toolbarBtn = parentActivity.getToolbarRigntBtn();
            toolbarBtn.setOnClickListener(rightBtnClickListener);
            interceptRightButton(mViewPager.getCurrentItem());
        }
    }

    private void manageRightButton(final int viewPagerPosition) {
        final boolean isIncidentsMode = viewPagerPosition == 0;
        //if messages list empty - nothing to edit, ignore click
        if (!isIncidentsMode && (getActivity() == null || notifications.isEmpty()))
            return;

        final Bundle extras = new Bundle();
        extras.putBoolean(EB_KEY_IS_INCIDENTS_MODE, isIncidentsMode);
        EventBus.getDefault().post(new RightButtonClickEvent(extras));
    }

    private void interceptRightButton(final int viewPagerPosition) {
        final boolean isIncidentsMode = viewPagerPosition == 0;

        final Bundle extras = new Bundle();
        extras.putBoolean(EB_KEY_IS_INCIDENTS_MODE, isIncidentsMode);
        EventBus.getDefault().post(new RightButtonInterceptEvent(extras));
    }


    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RightButtonVisibleEvent event) {
        if (getActivity() == null || getActivity().isFinishing() || event == null)
            return;

        setRightButton();
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UnreadChatMessageEvent event) {
        if (getActivity() == null || getActivity().isFinishing() || event == null || event.extras == null)
            return;

        final int unreadCount = event.extras.getInt(EB_KEY_UNREAD_CHAT_COUNT);
        manageTabCounter(0, unreadCount);
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UnreadInboxMessageEvent event) {
        if (getActivity() == null || getActivity().isFinishing() || event == null || event.extras == null)
            return;

        final int unreadCount = event.extras.getInt(EB_KEY_UNREAD_INBOX_COUNT);
        manageTabCounter(1, unreadCount);
    }

//    @SuppressWarnings("unused")
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEvent(BroadcastMessageEvent event) {
////        if (getActivity() == null || getActivity().isFinishing() || event == null)
////            return;
//
//        isNotificationReceived = true;
//    }

    private void setCustomTab() {
        final String[] tabTitles = getResources().getStringArray(R.array.incidents_tab_titles);
        final LayoutInflater inflater = getLayoutInflater();
        final ColorStateList stateColors = BrandingHelper.getTextStatesColors(getContext(), R.color.tab_gray_opacity_50);
        final int tabsCount = mTabLayout.getTabCount();
        for (int position = 0; position < tabsCount; position++) {
            final TabLayout.Tab tab = mTabLayout.getTabAt(position);
            if (tab != null) {
                final View customTab = inflater.inflate(R.layout.custom_tab, null, false);
                final StopitTextView tabTitle = customTab.findViewById(R.id.tab_title_txt);
                tabTitle.setTextColor(stateColors);
                tabTitle.setText(tabTitles[position]);

                tab.setCustomView(customTab);
            }
        }
    }


    private void manageTabCounter(final int position, final int count) {
        final TabLayout.Tab tab = mTabLayout.getTabAt(position);
        if (tab != null && tab.getCustomView() != null) {
            final View customView = tab.getCustomView();
            final TextView counter = customView.findViewById(R.id.tab_counter_txt);
            if(count == 0){
                counter.setVisibility(View.GONE);
                counter.setText("");
            }else{
                counter.setVisibility(View.VISIBLE);
                counter.setText(String.valueOf(count));
            }
        }
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        interceptRightButton(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private class IncidentsPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 2;

        IncidentsPagerAdapter() {
            super(getChildFragmentManager());
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 1:
                    return new BroadcastsFragment();

                case 0:
                default:
                    return new IncidentsListFragment();
            }
        }
    }
}
