package com.stopit.fragment.tabs;


import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.stopit.R;
import com.stopit.activity.MainActivity;
import com.stopit.adapter.ResourcesListAdapter;
import com.stopit.db.DBHelper;
import com.stopit.models.InformationResource;
import com.stopit.views.StopitTextView;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class ResourcesFragment extends BaseFragment {

    private RecyclerView mRecyclerView;
    private ResourcesListAdapter mAdapter;
    private StopitTextView emptyTxt;
    private RealmResults<InformationResource> resources;


    private View.OnClickListener resourceClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View clickedView) {
            final MainActivity parent = (MainActivity) getActivity();
            final InformationResource tag = (InformationResource) clickedView.getTag();
            if (parent == null || parent.isFinishing() || tag == null || TextUtils.isEmpty(tag.getWebUrl()))
                return;

            parent.startResDetailsActivity(tag.getWebUrl());
        }
    };


    @LayoutRes
    protected int getContentView() {
        return R.layout.fragment_resources;

    }

    @Override
    protected void initUI() {
        final View root = getView();
        if (root == null)
            return;

        mRecyclerView = root.findViewById(R.id.resources_recycler_view);
        emptyTxt = root.findViewById(R.id.resources_empty_view);
    }

    @Override
    protected void setUI(@Nullable Bundle savedInstanceState) {
        populateInformationResources();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new ResourcesListAdapter<>(getActivity(), resources, resourceClickedListener);
        mAdapter.bindToRecyclerView(mRecyclerView);

        manageListVisibility();
    }

    private void populateInformationResources() {
        final MainActivity parent = (MainActivity) getActivity();
        if (parent == null)
            return;

        resources = DBHelper.getResourcesList();
        resources.addChangeListener(new RealmChangeListener<RealmResults<InformationResource>>() {
            @Override
            public void onChange(RealmResults<InformationResource> informationResources) {
                if (getActivity() == null || mAdapter == null)
                    return;

                mAdapter.notifyDataSetChanged();
                manageListVisibility();
            }
        });
    }

    private void manageListVisibility() {
        if (resources.isEmpty()) {
            mRecyclerView.setVisibility(View.GONE);
            emptyTxt.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyTxt.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {

    }

//    @SuppressWarnings("unused")
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEvent(ResourcesUpdatedEvent event) {
//        if (getActivity() == null || getActivity().isFinishing())
//            return;
//
//        populateInformationResources();
//    }

}
