package com.stopit.fragment.tabs;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;

import com.andexert.library.RippleView;
import com.stopit.R;
import com.stopit.activity.MainActivity;
import com.stopit.activity.ReportActivity;
import com.stopit.db.DBHelper;
import com.stopit.fragment.NewsPageFragment;
import com.stopit.models.InformationResource;
import com.stopit.models.Organization;
import com.stopit.utils.BrandingHelper;
import com.stopit.views.StopitTextView;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;


public class HomeFragment extends BaseFragment{

    private RippleView sendBtnRipple;
    private ViewPager newsViewPager;
    private RealmResults<InformationResource> newsList;
    private StopitTextView homeTxt;
    private NewsPagerAdapter mAdapter;


    @LayoutRes
    protected int getContentView() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initUI() {
        final View root = getView();
        if (root == null)
            return;

        newsViewPager = root.findViewById(R.id.home_news_view_pager);
        homeTxt = root.findViewById(R.id.home_emergency_txt_view);
        sendBtnRipple = root.findViewById(R.id.home_report_ripple);
    }

    @Override
    protected void setUI(@Nullable Bundle savedInstanceState) {
        final String homeMsg = BrandingHelper.getHomeScreenMessage();
        if (!TextUtils.isEmpty(homeMsg))
            homeTxt.setText(homeMsg);

        sendBtnRipple.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                final MainActivity parent = (MainActivity) getActivity();
                if (parent == null || parent.isFinishing())
                    return;

                final Organization currentOrg = DBHelper.getCurrentOrganization();
                if (currentOrg == null) {
                    parent.showAlertDialog("", getString(R.string.unknown_err_msg));
                    return;
                }

                if (currentOrg.isReportingEnabled())
                    startActivity(new Intent(getActivity(), ReportActivity.class));
                else {
                    parent.showOutOfSessionAlert();
                }
            }
        });

        //TODO uncomment when resources are ready
        /*populateNews();
        mAdapter = new NewsPagerAdapter();
        newsViewPager.setAdapter(mAdapter);
        newsViewPager.setCurrentItem(0);
        manageNewsVisibility();*/
    }


    private void populateNews() {
        final MainActivity parent = (MainActivity) getActivity();
        if (parent == null)
            return;

        newsList = DBHelper.getResourcesList();
        newsList.addChangeListener(new RealmChangeListener<RealmResults<InformationResource>>() {
            @Override
            public void onChange(RealmResults<InformationResource> informationResources) {
                if (getActivity() == null || mAdapter == null)
                    return;

                mAdapter.notifyDataSetChanged();
                manageNewsVisibility();
            }
        });
    }

    private void manageNewsVisibility() {
        if (newsList.isEmpty()) {
            newsViewPager.setVisibility(View.INVISIBLE);
            newsViewPager.setAdapter(null);
        } else {
            newsViewPager.setVisibility(View.VISIBLE);
            newsViewPager.setAdapter(mAdapter);
            newsViewPager.setCurrentItem(0);
        }
    }

//    @SuppressWarnings("unused")
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEvent(ResourcesUpdatedEvent event) {
//        if (getActivity() == null || getActivity().isFinishing())
//            return;
//
//        manageNews();
//    }

    private class NewsPagerAdapter extends FragmentPagerAdapter {

        NewsPagerAdapter() {
            super(getChildFragmentManager());
        }

        @Override
        public int getCount() {
            return newsList.size();
        }

        @Override
        public Fragment getItem(int position) {
            final NewsPageFragment currentFragment = new NewsPageFragment();
            currentFragment.setInformation(newsList.get(position));
            return currentFragment;
        }
    }
}
