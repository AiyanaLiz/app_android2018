package com.stopit.fragment.tabs;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.andexert.library.RippleView;
import com.stopit.R;
import com.stopit.activity.MainActivity;
import com.stopit.activity.TermsOfUseDetailsActivity;
import com.stopit.db.DBHelper;
import com.stopit.models.Organization;
import com.stopit.utils.EmailHelper;
import com.stopit.views.StopitTextView;

import static com.stopit.app.Constants.ST_EXTRAS_KEY_READ_ONLY;

public class MoreFragment extends BaseFragment implements RippleView.OnRippleCompleteListener {

    //private RippleView option1Ripple;
    private RippleView option2Ripple;
    private RippleView option3Ripple;
    private RippleView option4Ripple;
    private StopitTextView versionTxt;
    private StopitTextView buildTxt;

    @LayoutRes
    protected int getContentView() {
        return R.layout.fragment_more;
    }

    @Override
    protected void initUI() {
        final View root = getView();
        if (root == null)
            return;

        //option1Ripple = root.findViewById(R.id.more_language_ripple);
        option2Ripple = root.findViewById(R.id.more_logout_ripple);
        option3Ripple = root.findViewById(R.id.more_terms_ripple);
        option4Ripple = root.findViewById(R.id.more_feedback_ripple);

        versionTxt = root.findViewById(R.id.more_about_version_value_txt);
        buildTxt = root.findViewById(R.id.more_about_build_value_txt);
    }

    @Override
    protected void setUI(@Nullable Bundle savedInstanceState) {
        //option1Ripple.setOnRippleCompleteListener(this);
        option2Ripple.setOnRippleCompleteListener(this);
        option3Ripple.setOnRippleCompleteListener(this);
        option4Ripple.setOnRippleCompleteListener(this);

        setAppInfo();
    }

    private void setAppInfo() {
        final MainActivity parent = (MainActivity) getActivity();
        if (parent == null || parent.isFinishing())
            return;

        try {
            PackageInfo pInfo = parent.getPackageManager().getPackageInfo(parent.getPackageName(), 0);
            if (pInfo != null) {
                versionTxt.setText(pInfo.versionName);
                buildTxt.setText(String.valueOf(pInfo.versionCode));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void changeLanguage() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setClassName("com.android.settings", "com.android.settings.LanguageSettings");
        startActivityForResult(intent, 55);
    }

    private void sendFeedback() {
        final MainActivity parent = (MainActivity) getActivity();
        if (parent == null || parent.isFinishing())
            return;

        EmailHelper.sendEmail(parent, "feedback@stopitsolutions.com", getString(R.string.feedback_subject),
                getString(R.string.email_helper_security_exc), getString(R.string.email_helper_no_app_exc));
    }

    private void showTermsOfUse() {
        final MainActivity parent = (MainActivity) getActivity();
        if (parent != null) {
            final Intent intent = new Intent(parent, TermsOfUseDetailsActivity.class);
            intent.putExtra(ST_EXTRAS_KEY_READ_ONLY, true);
            startActivity(intent);
        }
    }

    private void showLogoutDialog() {
        final MainActivity parent = (MainActivity) getActivity();
        if (parent == null || parent.isFinishing())
            return;

        String orgName = "";
        final Organization org = DBHelper.getCurrentOrganization();
        if (org != null && !TextUtils.isEmpty(org.getName()))
            orgName = org.getName();

        parent.showTwoBtnsAlertDialog(getString(R.string.logout_alert_title),
                getString(R.string.logout_alert_question, orgName),
                R.string.logout_alert_pos_label,
                R.string.button_dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        parent.logout();
                    }
                });
    }

    @Override
    public void onComplete(RippleView rippleView) {
        /*if (rippleView == option1Ripple) {
         *//*LocaleHelper.setLocale(getActivity(), "ja");*//*
            changeLanguage();

        } else */
        if (rippleView == option2Ripple) {
            showLogoutDialog();

        } else if (rippleView == option3Ripple) {
            showTermsOfUse();

        } else if (rippleView == option4Ripple) {
            sendFeedback();
        }
    }
}
