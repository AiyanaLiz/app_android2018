package com.stopit.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.andexert.library.RippleView;
import com.stopit.R;
import com.stopit.activity.BroadcastDetailsActivity;
import com.stopit.activity.MainActivity;
import com.stopit.adapter.BroadcastsListAdapter;
import com.stopit.db.DBHelper;
import com.stopit.event.RightButtonClickEvent;
import com.stopit.event.RightButtonInterceptEvent;
import com.stopit.models.BroadcastMessage;
import com.stopit.views.StopitTextView;
import com.stopit.views.brandable.RippleWithBackground;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;

import static com.stopit.app.Constants.BROADCAST_MODE_DEFAULT;
import static com.stopit.app.Constants.BROADCAST_MODE_EDIT;
import static com.stopit.app.Constants.EB_KEY_IS_INCIDENTS_MODE;
import static com.stopit.app.Constants.FBA_SCREEN_BROADCASTS_LIST;
import static com.stopit.app.Constants.FBA_SCREEN_INCIDENTS_LIST;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_BROADCAST_MESSAGE_ID;

public class BroadcastsFragment extends IncidentsBaseFragment implements View.OnClickListener, RippleView.OnRippleCompleteListener {

    private RecyclerView mRecyclerView;
    private BroadcastsListAdapter<BroadcastMessage> mAdapter;
    private RealmResults<BroadcastMessage> messages;
    private LinearLayout buttonsContainer;
    private View emptyView;
    private View unreadBtn;
    private View deleteBtn;
    private RippleWithBackground unreadRipple;
    private RippleWithBackground deleteRipple;

    private int mode = BROADCAST_MODE_DEFAULT;

    private RippleView.OnRippleCompleteListener messageItemListener = new RippleView.OnRippleCompleteListener() {
        @Override
        public void onComplete(RippleView rippleView) {
            final Object tag = rippleView.getTag();
            if (tag == null)
                return;

            final BroadcastMessage message = (BroadcastMessage) tag;
            if (mode == BROADCAST_MODE_DEFAULT) {
                startDetailsActivity(message);
            } else {
                DBHelper.setBroadcastMessageSelected(message, !message.isSelected());
                mAdapter.notifyDataSetChanged();
                boolean isAnyItemSelected = false;
                boolean isAnyReadItemSelected = false;
                for (BroadcastMessage msg : messages) {
                    if (msg.isSelected()) {
                        isAnyItemSelected = true;
                        if (msg.isRead())
                            isAnyReadItemSelected = true;
                    }
                }
                manageDeleteButtonsState(isAnyItemSelected);
                manageUnreadButtonsState(isAnyReadItemSelected);
            }
        }
    };

    @LayoutRes
    protected int getContentView() {
        return R.layout.fragment_broadcasts;
    }

    @Override
    protected void initUI() {
        final View root = getView();
        if (root == null)
            return;

        mRecyclerView = root.findViewById(R.id.broadcasts_recycler_view);
        buttonsContainer = root.findViewById(R.id.broadcasts_bottom_buttons);

        unreadBtn = root.findViewById(R.id.broadcasts_unread_btn);
        deleteBtn = root.findViewById(R.id.broadcasts_delete_btn);
        emptyView = root.findViewById(R.id.broadcasts_empty_view);

        unreadRipple = root.findViewById(R.id.broadcast_unread_ripple);
        deleteRipple = root.findViewById(R.id.broadcast_delete_ripple);
    }

    @Override
    protected void setUI(@Nullable Bundle savedInstanceState) {
        populateMessages();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new BroadcastsListAdapter<>(getActivity(), messages, messageItemListener);
        mRecyclerView.setAdapter(mAdapter);
        manageListVisibility();

        unreadRipple.setOnRippleCompleteListener(this);
        deleteRipple.setOnRippleCompleteListener(this);

        manageUnreadButtonsState(false);
        manageDeleteButtonsState(false);
    }

    private void populateMessages() {
        final MainActivity parent = (MainActivity) getActivity();
        if (parent == null)
            return;

        messages = DBHelper.getBroadcastMessagesList();
        messages.addChangeListener(new RealmChangeListener<RealmResults<BroadcastMessage>>() {
            @Override
            public void onChange(RealmResults<BroadcastMessage> broadcastMessages) {
                if (getActivity() == null || mAdapter == null)
                    return;

                mAdapter.notifyDataSetChanged();
                manageListVisibility();
            }
        });
    }

    private void manageListVisibility() {
        if (messages.isEmpty()) {
            mRecyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RightButtonClickEvent event) {
        if (getActivity() == null || getActivity().isFinishing() || event == null || event.extras == null)
            return;

        final Boolean isIncidentsMode = (Boolean) event.extras.get(EB_KEY_IS_INCIDENTS_MODE);
        if (isIncidentsMode == null || isIncidentsMode || mAdapter == null)
            return;

        final StopitTextView rightBtn = ((MainActivity) getActivity()).getToolbarRigntBtn();
        if (mode == BROADCAST_MODE_DEFAULT) {
            mode = BROADCAST_MODE_EDIT;
            rightBtn.setText(R.string.done_btn_label);
        } else {
            mode = BROADCAST_MODE_DEFAULT;
            rightBtn.setText(R.string.edit_btn_label);
            clearItemsSelection();
            manageDeleteButtonsState(false);
            manageUnreadButtonsState(false);
        }
        mAdapter.setMode(mode);
        buttonsContainer.setVisibility(mode == BROADCAST_MODE_EDIT ? View.VISIBLE : View.GONE);
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RightButtonInterceptEvent event) {
        final MainActivity parent = (MainActivity) getActivity();
        if (parent == null || parent.isFinishing() || event == null || event.extras == null)
            return;

        final Boolean isIncidentsMode = (Boolean) event.extras.get(EB_KEY_IS_INCIDENTS_MODE);
        if (isIncidentsMode == null || isIncidentsMode || mAdapter == null)
            return;

        final StopitTextView rightBtn = ((MainActivity) getActivity()).getToolbarRigntBtn();
        rightBtn.setText(mode == BROADCAST_MODE_DEFAULT ? R.string.edit_btn_label : R.string.done_btn_label);
        parent.logScreenChangeEvent(FBA_SCREEN_BROADCASTS_LIST);
    }

    private void startDetailsActivity(final BroadcastMessage message) {
        final Activity parentActivity = getActivity();
        if (parentActivity == null || parentActivity.isFinishing())
            return;

        final Intent intent = new Intent(parentActivity, BroadcastDetailsActivity.class);
        intent.putExtra(ST_EXTRAS_KEY_BROADCAST_MESSAGE_ID, message.getId());
        startActivity(intent);
    }

    @Override
    public void onClick(View clickedView) {
        final MainActivity parent = (MainActivity) getActivity();
        if (parent == null)
            return;

        if (clickedView == unreadBtn) {
            final List<BroadcastMessage> selectedMessages = collectUserSelectionForUpdate();
            if (selectedMessages.size() == 0) {
                parent.showAlertDialog("", getString(R.string.no_messages_selection_err));
                return;
            }
            parent.markBroadcastMessagesUnread(selectedMessages);

        } else if (clickedView == deleteBtn) {
            final List<BroadcastMessage> selectedMessages = collectUserSelectionForDelete();
            final int selectedMessagesCount = selectedMessages.size();
            if (selectedMessagesCount == 0) {
                parent.showAlertDialog("", getString(R.string.no_messages_selection_err));
                return;
            }
            //            final int deleteWarningRes = selectedMessagesCount > 1 ? R.string.delete_messages_warning : R.string.delete_message_warning;
//                parent.showAlertDialog("", getString(deleteWarningRes), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        parent.deleteBroadcastMessages(selectedMessages);
//                    }
//                });
            parent.deleteBroadcastMessages(selectedMessages);
        }

        final StopitTextView rightBtn = ((MainActivity) getActivity()).getToolbarRigntBtn();
        mode = BROADCAST_MODE_DEFAULT;
        rightBtn.setText(R.string.edit_btn_label);
        clearItemsSelection();
        manageDeleteButtonsState(false);
        manageUnreadButtonsState(false);
        mAdapter.setMode(mode);
        buttonsContainer.setVisibility(mode == BROADCAST_MODE_EDIT ? View.VISIBLE : View.GONE);
    }

    private List<BroadcastMessage> collectUserSelectionForUpdate() {
        final List<BroadcastMessage> selectedMessages = new ArrayList<>();
        for (BroadcastMessage message : messages) {
            if (message.isSelected() && message.isRead())
                selectedMessages.add(message);
        }
        return selectedMessages;
    }

    private List<BroadcastMessage> collectUserSelectionForDelete() {
        final List<BroadcastMessage> selectedMessages = new ArrayList<>();
        for (BroadcastMessage message : messages) {
            if (message.isSelected())
                selectedMessages.add(message);
        }
        return selectedMessages;
    }

    private void manageUnreadButtonsState(final boolean isAnyItemSelected) {
        unreadBtn.setEnabled(isAnyItemSelected);
        unreadRipple.setEnabled(isAnyItemSelected);
        unreadRipple.setOnRippleCompleteListener(isAnyItemSelected ? this : null);
    }

    private void manageDeleteButtonsState(final boolean isAnyItemSelected) {
        deleteBtn.setEnabled(isAnyItemSelected);
        deleteRipple.setEnabled(isAnyItemSelected);
        deleteRipple.setOnRippleCompleteListener(isAnyItemSelected ? this : null);
    }


    private void clearItemsSelection() {
        for (BroadcastMessage message : messages) {
            if (message.isSelected()) {
                DBHelper.setBroadcastMessageSelected(message, false);
            }
        }
    }


    @Override
    public void onComplete(RippleView rippleView) {
        final MainActivity parent = (MainActivity) getActivity();
        if (parent == null)
            return;

        if (rippleView == unreadRipple) {
            final List<BroadcastMessage> selectedMessages = collectUserSelectionForUpdate();
            if (selectedMessages.size() == 0) {
                parent.showAlertDialog("", getString(R.string.no_messages_selection_err));
                return;
            }
            parent.markBroadcastMessagesUnread(selectedMessages);


        } else if (rippleView == deleteRipple) {
            final List<BroadcastMessage> selectedMessages = collectUserSelectionForDelete();
            final int selectedMessagesCount = selectedMessages.size();
            if (selectedMessagesCount == 0) {
                parent.showAlertDialog("", getString(R.string.no_messages_selection_err));
                return;
            }
            //            final int deleteWarningRes = selectedMessagesCount > 1 ? R.string.delete_messages_warning : R.string.delete_message_warning;
//                parent.showAlertDialog("", getString(deleteWarningRes), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        parent.deleteBroadcastMessages(selectedMessages);
//                    }
//                });
            parent.deleteBroadcastMessages(selectedMessages);
        }

        final StopitTextView rightBtn = ((MainActivity) getActivity()).getToolbarRigntBtn();
        mode = BROADCAST_MODE_DEFAULT;
        rightBtn.setText(R.string.edit_btn_label);
        clearItemsSelection();
        manageDeleteButtonsState(false);
        manageUnreadButtonsState(false);
        mAdapter.setMode(mode);
        buttonsContainer.setVisibility(mode == BROADCAST_MODE_EDIT ? View.VISIBLE : View.GONE);
    }
}
