package com.stopit.fragment;


import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.stopit.R;
import com.stopit.activity.MainActivity;
import com.stopit.models.InformationResource;
import com.stopit.utils.Imager;

public class NewsPageFragment extends Fragment implements View.OnClickListener {

    private View readMoreTxt;
    private TextView titleTxt;
    private TextView descriptionTxt;
    private ImageView imageView;
    private InformationResource infoResource;


    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_news_page, container, false);
    }

    @CallSuper
    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
        setUI(savedInstanceState);
    }

    protected void initUI() {
        final View root = getView();
        if (root == null)
            return;

        readMoreTxt = root.findViewById(R.id.news_read_more_txt);
        titleTxt = root.findViewById(R.id.news_title_txt);
        descriptionTxt = root.findViewById(R.id.news_description_txt);
        imageView = root.findViewById(R.id.news_image_view);
    }


    protected void setUI(@Nullable final Bundle savedInstanceState) {
        if (infoResource == null)
            return;

        titleTxt.setText(infoResource.getTitle());
        descriptionTxt.setText(infoResource.getDescription());
        readMoreTxt.setOnClickListener(this);
        imageView.setImageResource(R.drawable.ic_placeholder_small);
        Imager.loadNewsIcon(getContext(), infoResource.getImageUrl(), imageView);
    }

    public void setInformation(final InformationResource information) {
        this.infoResource = information;
    }

    @Override
    public void onClick(View clickedView) {
        if (clickedView == readMoreTxt) {
            final MainActivity parent = (MainActivity) getActivity();
            if (parent != null && !TextUtils.isEmpty(infoResource.getWebUrl()))
                parent.startResDetailsActivity(infoResource.getWebUrl());
        }
    }
}
