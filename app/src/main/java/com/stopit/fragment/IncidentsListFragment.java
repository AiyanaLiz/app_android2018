package com.stopit.fragment;


import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.andexert.library.RippleView;
import com.stopit.R;
import com.stopit.activity.MainActivity;
import com.stopit.adapter.IncidentsListAdapter;
import com.stopit.db.DBHelper;
import com.stopit.event.RightButtonClickEvent;
import com.stopit.event.RightButtonInterceptEvent;
import com.stopit.models.Incident;
import com.stopit.models.Organization;
import com.stopit.views.StopitEditText;
import com.stopit.views.StopitTextView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;

import static com.stopit.app.Constants.EB_KEY_IS_INCIDENTS_MODE;
import static com.stopit.app.Constants.FBA_SCREEN_INCIDENTS_LIST;
import static com.stopit.app.Constants.FBA_SCREEN_INCIDENT_RETRIEVE;
import static com.stopit.app.Constants.INCIDENTS_MODE_DEFAULT;
import static com.stopit.app.Constants.INCIDENTS_MODE_SEARCH;
import static com.stopit.app.Constants.INCIDENT_ID_MIN_LENGTH;
import static com.stopit.app.Constants.PASSWORD_MIN_LENGTH;

public class IncidentsListFragment extends IncidentsBaseFragment implements TextWatcher, RippleView.OnRippleCompleteListener {
    private RecyclerView mRecyclerView;
    private IncidentsListAdapter<Incident> mAdapter;
    private RealmResults<Incident> incidentsList;
    private RelativeLayout listContainer;
    private LinearLayout searchContainer;
    private StopitEditText incidentIdEdt;
    private StopitEditText passwordEdt;
    private StopitTextView emptyView;
    private View viewBtn;
    private RippleView searchIncidentRipple;
    private int mode = INCIDENTS_MODE_DEFAULT;


    private RippleView.OnRippleCompleteListener viewIncidentListener = new RippleView.OnRippleCompleteListener() {
        @Override
        public void onComplete(RippleView rippleView) {
            final Object tag = rippleView.getTag();
            final MainActivity parent = ((MainActivity) getActivity());
            if (tag == null || !(tag instanceof Long) || parent == null)
                return;

            parent.startIncidentDetailsActivity((Long) tag);
        }
    };

    private RippleView.OnRippleCompleteListener messengerBtnListener = new RippleView.OnRippleCompleteListener() {
        @Override
        public void onComplete(RippleView rippleView) {
            final Object tag = rippleView.getTag();
            final MainActivity parent = ((MainActivity) getActivity());
            if (tag == null || !(tag instanceof Long) || parent == null)
                return;

            parent.startMessengerActivity((Long) tag);
        }
    };

    private RippleView.OnRippleCompleteListener addMediaListener = new RippleView.OnRippleCompleteListener() {
        @Override
        public void onComplete(RippleView rippleView) {
            final Object tag = rippleView.getTag();
            if (tag == null || !(tag instanceof Long))
                return;

            final MainActivity parent = ((MainActivity) getActivity());
            if (parent == null)
                return;

            final Organization currentOrg = DBHelper.getCurrentOrganization();
            if (currentOrg == null) {
                parent.showAlertDialog("", getString(R.string.unknown_err_msg));
                return;
            }

            if (currentOrg.isReportingEnabled()) {
                parent.setIncidentToUpdateId((Long) tag);
                parent.showOptionsPopup(null);
            } else
                parent.showOutOfSessionAlert();
        }
    };

    @LayoutRes
    protected int getContentView() {
        return R.layout.fragment_incidents_list;
    }

    @Override
    protected void initUI() {
        View rootView = getView();
        if (rootView == null)
            return;

        listContainer = rootView.findViewById(R.id.incidents_list_container);
        mRecyclerView = listContainer.findViewById(R.id.incidents_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        searchContainer = rootView.findViewById(R.id.incidents_search_container);
        incidentIdEdt = searchContainer.findViewById(R.id.incident_id_edt);
        passwordEdt = searchContainer.findViewById(R.id.incident_password_edt);
        viewBtn = searchContainer.findViewById(R.id.incident_search_btn);
        searchIncidentRipple = searchContainer.findViewById(R.id.incident_search_ripple);
        emptyView = rootView.findViewById(R.id.incidents_empty_view);
    }

    @Override
    protected void setUI(@Nullable Bundle savedInstanceState) {
        populateIncidentsList();
        mAdapter = new IncidentsListAdapter<>(incidentsList, viewIncidentListener, addMediaListener, messengerBtnListener);
        mAdapter.bindToRecyclerView(mRecyclerView);
        manageListVisibility();

        incidentIdEdt.addTextChangedListener(this);
        passwordEdt.addTextChangedListener(this);
        searchContainer.setVisibility(View.GONE);
        manageViewIncidentBtn(false);
    }

    private void populateIncidentsList() {
        final MainActivity parent = ((MainActivity) getActivity());
        if (parent != null) {
            incidentsList = DBHelper.getIncidentsList();
            incidentsList.addChangeListener(new RealmChangeListener<RealmResults<Incident>>() {
                @Override
                public void onChange(RealmResults<Incident> incidents) {
                    if (getActivity() == null || mAdapter == null)
                        return;

                    mAdapter.notifyDataSetChanged();
                    manageListVisibility();
                }
            });
        }
    }


    private void manageListVisibility() {
        if (incidentsList.isEmpty()) {
            mRecyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);

        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }
    }


    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RightButtonClickEvent event) {
        final MainActivity parent = (MainActivity) getActivity();
        if (parent == null || parent.isFinishing() || event == null || event.extras == null)
            return;

        parent.hideKeyboardIfShown();

        final Boolean isIncidentsMode = (Boolean) event.extras.get(EB_KEY_IS_INCIDENTS_MODE);
        if (isIncidentsMode == null || !isIncidentsMode)
            return;

        mode = mode == INCIDENTS_MODE_DEFAULT ? INCIDENTS_MODE_SEARCH : INCIDENTS_MODE_DEFAULT;

        final StopitTextView rightBtn = ((MainActivity) getActivity()).getToolbarRigntBtn();
        if (mode == INCIDENTS_MODE_SEARCH) {
            rightBtn.setText(R.string.list_btn_label);
            listContainer.setVisibility(View.GONE);
            searchContainer.setVisibility(View.VISIBLE);
            parent.logScreenChangeEvent(FBA_SCREEN_INCIDENT_RETRIEVE);
        } else {
            rightBtn.setText(R.string.retrieve_btn_label);
            listContainer.setVisibility(View.VISIBLE);
            searchContainer.setVisibility(View.GONE);
            parent.logScreenChangeEvent(FBA_SCREEN_INCIDENTS_LIST);
        }
    }


    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RightButtonInterceptEvent event) {
        final MainActivity parent = (MainActivity) getActivity();
        if (parent == null || parent.isFinishing() || event == null || event.extras == null)
            return;

        parent.hideKeyboardIfShown();
        final Boolean isIncidentsMode = (Boolean) event.extras.get(EB_KEY_IS_INCIDENTS_MODE);
        if (isIncidentsMode == null || !isIncidentsMode)
            return;

        final StopitTextView rightBtn = ((MainActivity) getActivity()).getToolbarRigntBtn();
        if (mode == INCIDENTS_MODE_SEARCH) {
            rightBtn.setText(R.string.list_btn_label);
            parent.logScreenChangeEvent(FBA_SCREEN_INCIDENT_RETRIEVE);
        } else {
            rightBtn.setText(R.string.retrieve_btn_label);
            parent.logScreenChangeEvent(FBA_SCREEN_INCIDENTS_LIST);
        }
    }


    private boolean isIncidentDetailsValid() {
        final String idString = incidentIdEdt.getText().toString().trim();
        final String passwordString = passwordEdt.getText().toString().trim();
        return (!TextUtils.isEmpty(idString) && idString.length() > INCIDENT_ID_MIN_LENGTH &&
                !TextUtils.isEmpty(passwordString) && passwordString.length() > PASSWORD_MIN_LENGTH);

    }

    private void manageViewIncidentBtn() {
        final boolean isEnabled = isIncidentDetailsValid();
        manageViewIncidentBtn(isEnabled);
    }

    private void manageViewIncidentBtn(final boolean isEnabled) {
        viewBtn.setEnabled(isEnabled);
        searchIncidentRipple.setEnabled(isEnabled);
        searchIncidentRipple.setOnRippleCompleteListener(isEnabled ? this : null);
    }

    private void retrieveIncidentById() {
        final MainActivity parent = (MainActivity) getActivity();
        if (parent == null || parent.isFinishing())
            return;

        try {
            parent.getIncidentByIdAndCode(Long.valueOf(incidentIdEdt.getText().toString().trim()), passwordEdt.getText().toString().trim());
        } catch (NumberFormatException e) {
            parent.showAlertDialog("", getString(R.string.invalid_incident_id_err));
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        manageViewIncidentBtn();
    }

    @Override
    public void onComplete(RippleView rippleView) {
        if (rippleView == searchIncidentRipple)
            retrieveIncidentById();
    }
}
