package com.stopit.db;

import android.content.Context;
import android.os.Looper;
import android.util.Log;

import com.stopit.models.Branding;
import com.stopit.models.BroadcastMessage;
import com.stopit.models.Configuration;
import com.stopit.models.CrisisCenter;
import com.stopit.models.Incident;
import com.stopit.models.IncidentPriorities;
import com.stopit.models.InformationResource;
import com.stopit.models.Organization;
import com.stopit.models.OrgsTreeEntity;
import com.stopit.models.Sources;
import com.stopit.models.User;
import com.stopit.models.data_wrappers.UserData;
import com.stopit.models.messenger.ChatAdmin;
import com.stopit.models.messenger.ChatMessage;
import com.stopit.models.messenger.ChatServer;
import com.stopit.models.messenger.LocalUser;
import com.stopit.models.messenger.Messenger;
import com.stopit.utils.BooleanLock;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

import static com.stopit.app.Constants.INBOX_MSG_STATUS_NEW;
import static com.stopit.app.Constants.INBOX_MSG_STATUS_READ;
import static com.stopit.app.Constants.ST_DEFAULT_MESSENGER_PORT;


public class DBHelper {

    private static final int schemaVersion = 0;

    final private static AtomicInteger aiRealmInstancesCount = new AtomicInteger();
    private static final BooleanLock blInitTask = new BooleanLock();
    private static RealmConfiguration sRealmConfiguration;
    private static Realm sMainUIRealmInstance;
    /*private static Migration sMigration = new Migration();*/

    public static void init(final Context context) {
        // to avoid whole class locking while this method should be synchronized
        synchronized (blInitTask) {
            blInitTask.setRunning();
            Realm.init(context);
            if (sRealmConfiguration == null) {
                sRealmConfiguration = new RealmConfiguration.Builder()
                        .name("StopitRealmProdDB")
                        // Automatically run sMigration if needed
                        .schemaVersion(schemaVersion)
                        .deleteRealmIfMigrationNeeded()
                        //.migration(sMigration) //TODO: apply migration in future instead of delete
                        .build();
                if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                    // Or you can add the sMigration code to the configuration.
                    // This will run the sMigration code without throwing a RealmMigrationNeededException.
                    // Realm.init(context.getApplicationContext()); // from version 2.0.0
                    sMainUIRealmInstance = Realm.getInstance(sRealmConfiguration);
                    aiRealmInstancesCount.incrementAndGet(); // +1 for mainUI instance
                } else {
                    Log.e("DBHelper", "DBHelper.init() called from non UI thread!");
                }
            } else {
                Log.i("DBHelper", "DBHelper.init() call unnecessary! Called from: ");
            }
            blInitTask.setFinished();
        }
    }

    public static Realm getRealmInstance() {
        // for UI-threads we will share the same Realm instance
        // Looper.myLooper() == Looper.getMainLooper()
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            final int count = aiRealmInstancesCount.get();
            Log.i("DBHelper", "Realm instances count (1 is MainUIRealm): " + count);
            if (sMainUIRealmInstance == null) {
                Log.e("DBHelper", "sMainUIRealmInstance is null -> NPE");
                try {
                    sMainUIRealmInstance = Realm.getInstance(sRealmConfiguration);
                } catch (IllegalArgumentException e) {
                    // Configurations cannot be different if used to open the same file. The most
                    // likely cause is that equals() and hashCode() are not overridden in the
                    // sMigration class
                    //Log.e("DBHelper", ""+e.t);
                    e.printStackTrace();
                }
                aiRealmInstancesCount.incrementAndGet(); // +1 for mainUI instance
            }
            return sMainUIRealmInstance;
        } else { // for non-UI threads we will return its own instance
            final int count = aiRealmInstancesCount.incrementAndGet();
            Log.i("DBHelper", "Realm instances count: " + count);
            Realm realm = null;
            try {
                realm = Realm.getInstance(sRealmConfiguration);
            } catch (IllegalArgumentException e) {
                // Configurations cannot be different if used to open the same file. The most
                // likely cause is that equals() and hashCode() are not overridden in the
                // sMigration class: ua.novaposhtaa.db.Migration
                e.printStackTrace();
            }
            if (sRealmConfiguration == null || realm == null) {
                final String msg = "sRealmConfiguration is null? " + (sRealmConfiguration == null)
                        + " OR realm is null? " + (realm == null);
                Log.e("DBHelper", msg);
            }
            return realm;
        }
    }

    public static void closeRealmInstance(Realm realmInstanceToClose) {
        // no close for UI-thread Realm instance
        if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
            final int count = aiRealmInstancesCount.decrementAndGet();
            Log.i("DBHelper", "Realm instances count: " + count);
            realmInstanceToClose.close();
        }
    }

    public static void compact() {
//        if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
//            new Exception("Compact should be called from main UI thread").printStackTrace();
//        } else {
//            sMainUIRealmInstance.close(); // must be main UI instance
//            Realm.compactRealm(sRealmConfiguration);
//            init(PocketDoctorApp.getAppContext());
//        }
    }

    public static void save(RealmModel item) {
        final Realm realm = getRealmInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(item);
        realm.commitTransaction();
        closeRealmInstance(realm);
    }

    public static <E extends RealmModel> void save(List<E> items) {
        final Realm realm = getRealmInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(items);
        realm.commitTransaction();
        closeRealmInstance(realm);
    }

    public static <E extends RealmModel> E copyFromRealm(E realmModel) {
        final Realm realm = getRealmInstance();
        E copied = realm.copyFromRealm(realmModel);
        closeRealmInstance(realm);
        return copied;
    }

    public static <E extends RealmModel> List<E> copyFromRealm(RealmResults<E> realmResults) {
        final Realm realm = getRealmInstance();
        List<E> copied = realm.copyFromRealm(realmResults);
        closeRealmInstance(realm);
        return copied;
    }

    public static void delete(RealmModel object) {
        if (object == null) {
            Log.e("DBHelper", "object is null !");
            return;
        }
        sMainUIRealmInstance.beginTransaction();
        RealmObject.deleteFromRealm(object);
        sMainUIRealmInstance.commitTransaction();
    }

    public static void onLogout() {
        clearAllUserData();
        DBHelper.compact();
    }

    public static void clearAllUserData() {
        sMainUIRealmInstance.beginTransaction();

        sMainUIRealmInstance.delete(User.class);
        sMainUIRealmInstance.delete(Branding.class);
        sMainUIRealmInstance.delete(Configuration.class);
        sMainUIRealmInstance.delete(Sources.class);
        sMainUIRealmInstance.delete(IncidentPriorities.class);

        sMainUIRealmInstance.delete(ChatAdmin.class);
        sMainUIRealmInstance.delete(ChatServer.class);
        sMainUIRealmInstance.delete(LocalUser.class);
        sMainUIRealmInstance.delete(Messenger.class);

        sMainUIRealmInstance.delete(CrisisCenter.class);
        sMainUIRealmInstance.delete(InformationResource.class);
        sMainUIRealmInstance.delete(BroadcastMessage.class);
        sMainUIRealmInstance.delete(ChatMessage.class);
        sMainUIRealmInstance.delete(Incident.class);

        sMainUIRealmInstance.commitTransaction();
    }


    public static void clearUserData() {
        sMainUIRealmInstance.beginTransaction();

        sMainUIRealmInstance.delete(User.class);
        sMainUIRealmInstance.delete(Branding.class);
        sMainUIRealmInstance.delete(Configuration.class);
        sMainUIRealmInstance.delete(Sources.class);
        sMainUIRealmInstance.delete(IncidentPriorities.class);
        sMainUIRealmInstance.delete(ChatAdmin.class);
        sMainUIRealmInstance.delete(ChatServer.class);
        sMainUIRealmInstance.delete(CrisisCenter.class);
        sMainUIRealmInstance.delete(LocalUser.class);
        sMainUIRealmInstance.delete(Messenger.class);

        sMainUIRealmInstance.commitTransaction();
    }


    public static void deleteAllFromRealm(RealmResults<? extends RealmModel> realmResults) {
        sMainUIRealmInstance.beginTransaction();
        realmResults.deleteAllFromRealm();
        sMainUIRealmInstance.commitTransaction();
    }

    public static <E extends RealmModel> E getFirst(Class<E> clazz) {
        return sMainUIRealmInstance
                .where(clazz)
                .findFirst();
    }

    public static void saveAllUserData(final UserData userData) {
        //clears all user invalidated data
        clearAllUserData();
        saveUserRelatedData(userData);
    }

    public static void saveUserData(final UserData userData) {
        //clear old invalidated data
        //do not clears chatMessages, inboxMessages, incidents, infoResources
        clearUserData();
        saveUserRelatedData(userData);
    }

    private static void saveUserRelatedData(final UserData userData) {

        final User user = userData.getUser();
        final Organization org = userData.getOrganization();

        user.setCurrentOrgId(org.getId());
        save(user);

        org.setIntellicode(user.getIntelliCode());
        save(org);

        final Configuration config = userData.getConfiguration();
        if (config != null)
            save(config);

        final Messenger messenger = userData.getMessenger();
        if (messenger != null)
            save(messenger);

        final List<CrisisCenter> helpContacts = userData.getHelpContacts();
        if (helpContacts != null)
            save(helpContacts);

        final Branding branding = userData.getBranding();
        if (branding != null) {
            //set id = 1 == the only instance of branding will be saved
            branding.setId(1);
            save(branding);
        }
    }

    public static void clear(final Class className) {
        sMainUIRealmInstance.beginTransaction();
        sMainUIRealmInstance.delete(className);
        sMainUIRealmInstance.commitTransaction();
    }

    public static long getCurrentUserId() {
        final User user = getFirst(User.class);
        if (user != null) {
            return user.getId();
        } else
            return 0;
    }


    public static Organization getCurrentOrganization() {
        final long currentOrgId = getCurrentOrganizationId();
        if (currentOrgId == 0)
            return null;

        return sMainUIRealmInstance
                .where(Organization.class)
                .equalTo("id", currentOrgId)
                .findFirst();
    }

    public static long getCurrentOrganizationId() {
        final User user = getFirst(User.class);
        if (user != null) {
            //Log.d("DBHelper", "current org id " + user.getCurrentOrgId());
            return user.getCurrentOrgId();
        } else
            return 0;
    }

    public static boolean isGPSEnabled() {
        final Organization currentOrg = getCurrentOrganization();
        return currentOrg != null && currentOrg.isGPSEnabled();
    }

    public static boolean isMediaUploadAllowed() {
        final Organization currentOrg = getCurrentOrganization();
        return currentOrg != null && currentOrg.isMediaUploadAllowed();
    }

    public static RealmResults<Organization> getAllOrganizationsList() {
        return sMainUIRealmInstance
                .where(Organization.class)
                .findAll();
    }

    public static Organization getOrganizationById(final long id) {
        return sMainUIRealmInstance
                .where(Organization.class)
                .equalTo("id", id)
                .findFirst();
    }

    public static List<Organization> getHistoryList(final long currentOrgId) {
        final List<Organization> historyList = new ArrayList<>();
        RealmResults<Organization> results = sMainUIRealmInstance
                .where(Organization.class)
                .notEqualTo("id", currentOrgId)
                .findAll();

        if (!results.isEmpty()) {
            historyList.addAll(results);
        }

        return historyList;
    }

    public static OrgsTreeEntity getOrgsTreeEntityById(final long id) {
        return sMainUIRealmInstance
                .where(OrgsTreeEntity.class)
                .equalTo("id", id)
                .findFirst();
    }

    public static List<OrgsTreeEntity> getOrgsTreeByParentId(final long parentId) {
        final List<OrgsTreeEntity> treeList = new ArrayList<>();
        RealmResults<OrgsTreeEntity> results = sMainUIRealmInstance
                .where(OrgsTreeEntity.class)
                .equalTo("parentOrgId", parentId)
                .findAll();

        //need to copy from realm to be able to filter
        if (!results.isEmpty()) {
            for (OrgsTreeEntity entity : results) {
                treeList.add(sMainUIRealmInstance.copyFromRealm(entity));
            }
        }

        return treeList;
    }

    public static List<OrgsTreeEntity> getOrgsTreeByNameKey(final String nameKey) {
        final List<OrgsTreeEntity> treeList = new ArrayList<>();
        RealmResults<OrgsTreeEntity> results = sMainUIRealmInstance
                .where(OrgsTreeEntity.class)
                .contains("name", nameKey, Case.INSENSITIVE)
                .findAll();

        //need to copy from realm to be able to filter
        if (!results.isEmpty()) {
            for (OrgsTreeEntity entity : results) {
                treeList.add(sMainUIRealmInstance.copyFromRealm(entity));
            }
        }

        return treeList;
    }

    public static void setBroadcastMessageSelected(final BroadcastMessage message, final boolean isSelected) {
        sMainUIRealmInstance.beginTransaction();
        message.setSelected(isSelected);
        sMainUIRealmInstance.copyToRealmOrUpdate(message);
        sMainUIRealmInstance.commitTransaction();
    }

    public static void updateBroadcastMessageReadState(String messageId, boolean isRead) {
        final BroadcastMessage message = sMainUIRealmInstance.where(BroadcastMessage.class).equalTo("id", messageId).findFirst();
        if (message != null) {
            updateBroadcastMessageReadState(message, isRead);
        }
    }

    public static void updateBroadcastMessageReadState(final BroadcastMessage message, final boolean isRead) {
        final int status = isRead ? INBOX_MSG_STATUS_READ : INBOX_MSG_STATUS_NEW;
        sMainUIRealmInstance.beginTransaction();
        message.setStatusId(status);
        sMainUIRealmInstance.copyToRealmOrUpdate(message);
        sMainUIRealmInstance.commitTransaction();
    }

    public static RealmResults<BroadcastMessage> getBroadcastMessagesList() {
        RealmResults<BroadcastMessage> results = sMainUIRealmInstance
                .where(BroadcastMessage.class)
                .findAll().sort("statusId", Sort.DESCENDING, "date", Sort.DESCENDING);

        return results;
    }

    public static void deleteBroadcastMessage(String id) {
        sMainUIRealmInstance.beginTransaction();
        sMainUIRealmInstance
                .where(BroadcastMessage.class)
                .equalTo("id", id)
                .findAll()
                .deleteAllFromRealm();
        sMainUIRealmInstance.commitTransaction();
    }

    public static Configuration getCurrentConfiguration() {
        return getFirst(Configuration.class);
    }

    public static Branding getBranding() {
        return sMainUIRealmInstance
                .where(Branding.class)
                .findFirst();
    }

    public static String getChatUserPassword() {
        final Messenger messenger = getMessenger();
        if (messenger == null)
            return null;

        final LocalUser user = messenger.getSelf();
        return (user == null ? null : user.getPassword());
    }


    public static String getChatUserName() {
        final Messenger messenger = getMessenger();
        if (messenger == null)
            return null;

        final LocalUser user = messenger.getSelf();
        return (user == null ? null : user.getUserName());
    }

    public static String getChatUrl() {
        final Messenger messenger = getMessenger();
        if (messenger == null)
            return null;

        final ChatServer server = messenger.getServer();
        return (server == null ? null : server.getUrl());
    }

    public static int getChatPort() {
        final int defaultPort = ST_DEFAULT_MESSENGER_PORT;
        final Messenger messenger = getMessenger();
        if (messenger == null)
            return defaultPort;

        final ChatServer server = messenger.getServer();
        return (server == null ? defaultPort : server.getPort());
    }

    public static String getDocitAdminName() {
        final Messenger messenger = getMessenger();
        if (messenger == null)
            return null;

        final ChatAdmin admin = messenger.getAdmin();
        return (admin == null ? null : admin.getName());
    }

    public static Messenger getMessenger() {
        return sMainUIRealmInstance
                .where(Messenger.class)
                .findFirst();
    }

    public static RealmResults<ChatMessage> getChatMessagesList(final long incidentId) {
        return sMainUIRealmInstance
                .where(ChatMessage.class)
                .equalTo("incidentId", incidentId)
                .findAll().sort("messageDate", Sort.ASCENDING);
    }

    public static void clearMessages(long incidentId) {
        sMainUIRealmInstance.beginTransaction();
        sMainUIRealmInstance
                .where(ChatMessage.class)
                .equalTo("incidentId", incidentId)
                .findAll()
                .deleteAllFromRealm();
        sMainUIRealmInstance.commitTransaction();
    }

    public static boolean isChatMessageWithId(final String uniqueId) {
        final Realm realm = getRealmInstance();
        final ChatMessage savedMessage = realm
                .where(ChatMessage.class)
                .equalTo("uniqueId", uniqueId)
                .findFirst();
        closeRealmInstance(realm);
        return savedMessage != null;
    }

    public static String getIntellicode() {
        final User user = getUser();
        if (user != null) return user.getIntelliCode();
        else return null;
    }

    public static User getUser() {
        return getFirst(User.class);
    }

    public static RealmResults<CrisisCenter> getHelpContactsList() {
        return sMainUIRealmInstance
                .where(CrisisCenter.class)
                .findAll().sort("isPriorityContact", Sort.ASCENDING, "sortOrder", Sort.ASCENDING);
    }

    public static RealmResults<Incident> getIncidentsList() {
        return sMainUIRealmInstance
                .where(Incident.class)
                .findAll().sort("status", Sort.DESCENDING, "id", Sort.DESCENDING);
    }

    public static RealmResults<InformationResource> getResourcesList() {
        return sMainUIRealmInstance
                .where(InformationResource.class)
                .findAll();
    }

    public static Incident getIncidentById(final long id) {
        return sMainUIRealmInstance
                .where(Incident.class)
                .equalTo("id", id)
                .findFirst();
    }


}