package com.stopit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.stopit.R;
import com.stopit.adapter.OrganizationsBaseAdapter;
import com.stopit.db.DBHelper;
import com.stopit.models.Branding;
import com.stopit.models.Organization;
import com.stopit.models.OrgsTreeEntity;
import com.stopit.models.User;
import com.stopit.models.data_wrappers.UserData;
import com.stopit.utils.BrandingHelper;
import com.stopit.views.StopitTextView;
import com.stopit.views.StopitToolbar;

import java.util.List;

import static com.stopit.app.Constants.FBA_SCREEN_SEARCH_ORG;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_IS_TOP_ORG;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_ORGANIZATION_ID;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_ORGANIZATION_NAME;

public class SearchOrganizationActivity extends StopitActivity /*implements BaseQuickAdapter.OnItemClickListener*/ {


    private StopitToolbar toolBar;
    private StopitTextView headquartersTitleTxt;
    private EditText orgSearchEdt;
    private RecyclerView mRecyclerView;
    //private OrganizationsListAdapter mAdapter;
    private OrganizationsBaseAdapter mAdapter;
    //private OrganizationsExpandableListAdapter mAdapter;
    //private List<MultiItemEntity> organizationsList;


    private List<OrgsTreeEntity> organizationsList;
    private long parentOrgId;
    private String parentOrgName;
    private OrgsTreeEntity parentOrg;
    private long selectedOrgId;
    protected View.OnClickListener onOrganizationSelected = new View.OnClickListener() {
        @Override
        public void onClick(View clickedView) {
            final Object tag = clickedView.getTag();
            if (tag == null || !(tag instanceof OrgsTreeEntity))
                return;

            final OrgsTreeEntity entity = (OrgsTreeEntity) tag;
            if (entity.isParent()) {
                startOrganizationSearchActivity(entity.getId());

            } else {
                prepareToLogIn(entity.getId());
//                performLogin(entity.getId());
            }
        }
    };

    @Override
    protected boolean isIntentTransactionSuccessful(Intent intent) {
        if (intent == null || !intent.hasExtra(ST_EXTRAS_KEY_ORGANIZATION_ID))
            return false;

        parentOrgId = intent.getLongExtra(ST_EXTRAS_KEY_ORGANIZATION_ID, 0);
        if (parentOrgId == 0)
            return false;

        if (intent.getBooleanExtra(ST_EXTRAS_KEY_IS_TOP_ORG, false)) {
            parentOrgName = intent.getStringExtra(ST_EXTRAS_KEY_ORGANIZATION_NAME);
            parentOrg = new OrgsTreeEntity(parentOrgId, parentOrgName, true);
        } else
            parentOrg = DBHelper.getOrgsTreeEntityById(parentOrgId);

        return parentOrg != null;
    }

    @LayoutRes
    int getContentView() {
        return R.layout.activity_search_organization;
    }

    void initUI() {
        toolBar = initToolbar();
        headquartersTitleTxt = findViewById(R.id.search_headquarters_title);
        orgSearchEdt = findViewById(R.id.search_org_edt);
        mRecyclerView = findViewById(R.id.search_headered_list);
    }

    void initData() {
        organizationsList = DBHelper.getOrgsTreeByParentId(parentOrgId);
    }

    void setUI(final Bundle savedInstanceState) {
        toolBar.setBackButton(this);

        if (parentOrg != null && !TextUtils.isEmpty(parentOrg.getName()))
            headquartersTitleTxt.setText(parentOrg.getName());

        mAdapter = new OrganizationsBaseAdapter(this, organizationsList, onOrganizationSelected);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);

        orgSearchEdt.addTextChangedListener(this);
        applyBranding();
    }

    @Override
    void setAnalyticsProperties() {
    }

    @Override
    String getScreenName(){
        return FBA_SCREEN_SEARCH_ORG;
    }

    private void applyBranding() {
        if (!BrandingHelper.isBrandingColor())
            return;

        BrandingHelper.setTextColor(headquartersTitleTxt);
    }

    @Override
    public void afterTextChanged(Editable s) {
        String key = orgSearchEdt.getText().toString();
        //mAdapter.getFilter().filter(text);
        organizationsList.clear();
        if (!TextUtils.isEmpty(key))
            organizationsList.addAll(DBHelper.getOrgsTreeByNameKey(key));
        else
            organizationsList.addAll(DBHelper.getOrgsTreeByParentId(parentOrgId));

        if (mAdapter != null) mAdapter.notifyDataSetChanged();
    }

    @Override
    void onLoginSuccess(final UserData userData, final String accessCode) {
        final User user = userData.getUser();
        final Organization organization = userData.getOrganization();
        if (user == null || TextUtils.isEmpty(user.getIntelliCode()) || organization == null) {
            showAlertDialog("", getString(R.string.unknown_err_msg));
            return;
        }

        BrandingHelper.clearBranding(true);

        //TODO check
        //TODO check
        //TODO check
        /*setUserData(userData);*/
        DBHelper.saveAllUserData(userData);
        //TODO check
        //TODO check
        //TODO check

        final Branding branding = DBHelper.getBranding();
        //if no branding to download
        if (branding == null || !branding.isUrlsToDownload()) {
            //skip, go to the next screen
            startTermsOfUseActivity();

        } else {
            downloadBrandingAssets(branding);
        }
    }


    private void prepareToLogIn(final long selectedId) {
        final Organization historicalOrg = DBHelper.getOrganizationById(selectedId);
        if (historicalOrg != null && !TextUtils.isEmpty(historicalOrg.getIntellicode())) {
            selectedOrgId = selectedId;
            requestOrganizationChange(historicalOrg.getIntellicode());
        } else
            performLogin(selectedId);
    }

    @Override
    void onOrganizationChangeFailure(final String message) {
        if (selectedOrgId != 0)
            performLogin(selectedOrgId);
        else
            showAlertDialog("", getString(R.string.unknown_err_msg));
    }
}
