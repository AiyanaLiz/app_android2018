package com.stopit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.text.TextUtils;
import android.view.View;

import com.stopit.R;
import com.stopit.views.StopitOrgSelector;
import com.stopit.views.StopitToolbar;
import com.stopit.views.StopitWebView;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.stopit.app.Constants.FBA_SCREEN_PRIVACY_POLICY;
import static com.stopit.app.Constants.FBA_SCREEN_RESOURCE_DETAILS;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_POLICY_URL;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_RESOURCE_URL;

/**
 * Created by Alenka on 07.07.2018.
 */

public class PrivacyPolicyActivity extends StopitActivity implements StopitWebView.OnScrollChangedCallback {

    private StopitToolbar toolbar;
    private StopitWebView mWebView;
    private View loadingProgressView;
    private String url;

    @LayoutRes
    int getContentView() {
        return R.layout.activity_privacy_policy;
    }

    protected boolean isIntentTransactionSuccessful(final Intent intent) {
        if (intent == null || !intent.hasExtra(ST_EXTRAS_KEY_POLICY_URL))
            return false;

        url = intent.getStringExtra(ST_EXTRAS_KEY_POLICY_URL);
        return !TextUtils.isEmpty(url);
    }

    void initUI() {
        toolbar = initToolbar();

        mWebView = findViewById(R.id.privacy_policy_web_view);
        loadingProgressView = findViewById(R.id.privacy_policy_progress_layout);
    }


    void setUI(final Bundle savedInstanceState) {
        toolbar.setBackButton(this);
        mWebView.setOnScrollChangedCallback(this);
        mWebView.clearCache(true);
        loadUrl();
    }

    @Override
    String getScreenName(){
        return FBA_SCREEN_PRIVACY_POLICY;
    }

    private void loadUrl() {
        if (checkInternetConnectionAndInform()) {
            Map<String, String> headers = new HashMap<>();
            headers.put("Accept-Language", Locale.getDefault().getLanguage());
            mWebView.loadUrl(url, headers);
        }
    }

    @Override
    public void onScrollEndReached() {
    }

    @Override
    public void onLoadPageStarted() {
        loadingProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoadPageFinished() {
        loadingProgressView.setVisibility(View.GONE);
    }
}
