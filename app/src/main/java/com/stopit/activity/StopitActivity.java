package com.stopit.activity;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.stopit.R;
import com.stopit.api.ApiManager;
import com.stopit.api.IApiCallBackError;
import com.stopit.api.IApiCallBackSuccess;
import com.stopit.api.adapters.ApiError;
import com.stopit.api.aws.AmazonHelper;
import com.stopit.api.chat.SmackService;
import com.stopit.app.Constants;
import com.stopit.db.DBHelper;
import com.stopit.event.AppOnForegroundEvent;
import com.stopit.models.Branding;
import com.stopit.models.BroadcastMessage;
import com.stopit.models.Configuration;
import com.stopit.models.DynamicQuestion;
import com.stopit.models.Incident;
import com.stopit.models.IncidentAttachment;
import com.stopit.models.InformationResource;
import com.stopit.models.MediaItem;
import com.stopit.models.Organization;
import com.stopit.models.OrgsTreeEntity;
import com.stopit.models.User;
import com.stopit.models.data_wrappers.BroadcastAttachmentWrapper;
import com.stopit.models.data_wrappers.BroadcastListWrapper;
import com.stopit.models.data_wrappers.BroadcastMessageWrapper;
import com.stopit.models.data_wrappers.IncidentDataWrapper;
import com.stopit.models.data_wrappers.ResourcesListDataWrapper;
import com.stopit.models.data_wrappers.StopitResponse;
import com.stopit.models.data_wrappers.UnreadInboxCounter;
import com.stopit.models.data_wrappers.UnreadMessengerCounter;
import com.stopit.models.data_wrappers.UserData;
import com.stopit.models.messenger.ChatMessage;
import com.stopit.utils.BooleanLock;
import com.stopit.utils.BrandingHelper;
import com.stopit.utils.CameraHelper;
import com.stopit.utils.DateUtils;
import com.stopit.utils.Imager;
import com.stopit.utils.NetworkStateHelper;
import com.stopit.utils.NotificationHelper;
import com.stopit.utils.ParseHelper;
import com.stopit.utils.SharedPrefsHelper;
import com.stopit.utils.StopitPermissionsHelper;
import com.stopit.views.OptionsPopup;
import com.stopit.views.StopitOrgSelector;
import com.stopit.views.StopitToolbar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import io.realm.RealmResults;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;

import static android.view.View.FOCUS_DOWN;
import static com.stopit.api.adapters.ApiError.ST_INVALID_ACCESS_CODE;
import static com.stopit.app.Constants.CAMERA_FOR_PHOTO_REQUEST_PERMISSION_CODE;
import static com.stopit.app.Constants.CAMERA_FOR_QR_SCANNER_REQUEST_PERMISSION_CODE;
import static com.stopit.app.Constants.CAMERA_FOR_VIDEO_REQUEST_PERMISSION_CODE;
import static com.stopit.app.Constants.EXTERNAL_STORAGE_FOR_PHOTO_GALLERY_REQUEST_PERMISSION_CODE;
import static com.stopit.app.Constants.EXTERNAL_STORAGE_FOR_PHOTO_REQUEST_PERMISSION_CODE;
import static com.stopit.app.Constants.EXTERNAL_STORAGE_FOR_VIDEO_GALLERY_REQUEST_PERMISSION_CODE;
import static com.stopit.app.Constants.EXTERNAL_STORAGE_FOR_VIDEO_REQUEST_PERMISSION_CODE;
import static com.stopit.app.Constants.FBA_USER_PROPERTY_KEY_ORG_ID;
import static com.stopit.app.Constants.FBA_USER_PROPERTY_KEY_ORG_NAME;
import static com.stopit.app.Constants.GALLERY_PHOTO_PICK_REQUEST_CODE;
import static com.stopit.app.Constants.GALLERY_VIDEO_PICK_REQUEST_CODE;
import static com.stopit.app.Constants.LOCATION_REQUEST_PERMISSION_CODE;
import static com.stopit.app.Constants.OPTIONS_POPUP_ACTION_PICK_PHOTO;
import static com.stopit.app.Constants.OPTIONS_POPUP_ACTION_PICK_VIDEO;
import static com.stopit.app.Constants.OPTIONS_POPUP_ACTION_TAKE_PHOTO;
import static com.stopit.app.Constants.OPTIONS_POPUP_ACTION_TAKE_VIDEO;
import static com.stopit.app.Constants.ST_DEACTIVARED_USER_ERR_CODE;
import static com.stopit.app.Constants.ST_DEACTIVARED_USER_ERR_CODE_V2;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_IS_CLEAR_TASK;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_IS_TOP_ORG;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_NOTIFICATION_ALERT;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_NOTIFICATION_MSG_ID;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_ORGANIZATION_ID;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_ORGANIZATION_NAME;
import static com.stopit.app.Constants.TAKE_PHOTO_REQUEST_CODE;
import static com.stopit.app.Constants.VIDEO_RECORD_REQUEST_CODE;

public abstract class StopitActivity extends AppCompatActivity implements View.OnClickListener, OrgTitleClickListener,
        EasyPermissions.PermissionCallbacks, TextWatcher,
        IApiCallBackError, OptionsPopup.OnOptionsPopupChoiceListener,
        OnDownloadFinishedCallback, OnUploadFinishedCallback {

    private static final String MEDIA_FILE_PREFIX = "incident_";
    private static final String MEDIA_FILE_SUFFIX = ".jpg";

    final AtomicInteger downloadAssetsCount = new AtomicInteger();
    final AtomicInteger uploadFilesCount = new AtomicInteger();
    public View apiInProgressView;
    public View apiInProgressBarView;
    OptionsPopup optionsPopup;
    String currentPhotoPath;
    BooleanLock statusBeingChecked = new BooleanLock();
    BooleanLock incidentsBeingRefreshed = new BooleanLock();
    BooleanLock broadcastsBeingRefreshed = new BooleanLock();
    FirebaseAnalytics mFirebaseAnalytics;
    private List<IncidentAttachment> attachments = new ArrayList<>();
    private long incidentToUpdateId;
    //private static boolean isNoNeedToCheckStatus

    private BroadcastReceiver messengerMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String notificationText = intent.getStringExtra(ST_EXTRAS_KEY_NOTIFICATION_ALERT);
            if (!TextUtils.isEmpty(notificationText)) {
                if (!(StopitActivity.this instanceof MessengerActivity)) {
                    NotificationHelper.notifyUser(StopitActivity.this, notificationText);
                    retrieveUnreadChatMessagesCount();
                    //showAlertDialog("", getString(R.string.new_messenger_message_alert));
                }
            }
        }
    };
    private BooleanLock deactivatingUserTask = new BooleanLock();
    private BroadcastReceiver inboxMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String notificationText = intent.getStringExtra(ST_EXTRAS_KEY_NOTIFICATION_ALERT);
            String notificationMessageId = intent.getStringExtra(ST_EXTRAS_KEY_NOTIFICATION_MSG_ID);
            if (!TextUtils.isEmpty(notificationText)) {
                if (!TextUtils.isEmpty(notificationMessageId)) {
                    retrieveInboxUnreadCount();
                    requestBroadcastMessages();
                }
                NotificationHelper.notifyUserNewBroadcastMessage(context, notificationText);
            }
        }
    };

    public static void hideKeyboardIfShown(ViewGroup rootView) {
        ArrayList<View> focusables = rootView.getFocusables(FOCUS_DOWN);
        for (View focusable : focusables)
            if (focusable instanceof EditText && focusable.isFocused()) {
                hideKeyboard(focusable);
                break;
            }
    }

    public static void hideKeyboard(View v) {
        v.clearFocus();
        v.requestLayout();
        final InputMethodManager inputManager = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputManager != null)
            inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Intent intent = getIntent();
        if (!isIntentTransactionSuccessful(intent)) {
            android.util.Log.e(getClass().getSimpleName(), "Wrong or missed extras");
            finish();
            return;
        }

        final EventBus eventBus = EventBus.getDefault();
        if (!eventBus.isRegistered(this)) eventBus.register(this);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        //setting an organization name and id property for analytics instance
        setAnalyticsProperties();

        setContentView(getContentView());
        initUI();
        initData();
        setUI(savedInstanceState);
        logScreenViewEvent(getScreenName());
    }

    void setAnalyticsProperties() {
        if (mFirebaseAnalytics == null)
            return;

        final Organization currentOrg = DBHelper.getCurrentOrganization();
        if (currentOrg != null) {
            if (!TextUtils.isEmpty(currentOrg.getName()))
                mFirebaseAnalytics.setUserProperty(FBA_USER_PROPERTY_KEY_ORG_NAME, currentOrg.getName());
            String dToken = FirebaseInstanceId.getInstance().getToken();
            boolean dToken_flag = false;
            if(dToken != null && !dToken.isEmpty())
                dToken_flag= true;


            Log.i("Firebase","Token in Anlytics: " + dToken);
            mFirebaseAnalytics.setUserProperty("deviceToken", dToken.substring(0,36));
            mFirebaseAnalytics.setUserProperty("valid_deviceToken",String.valueOf(dToken_flag));
            mFirebaseAnalytics.setUserProperty(FBA_USER_PROPERTY_KEY_ORG_ID, String.valueOf(currentOrg.getId()));
            mFirebaseAnalytics.setUserId(String.valueOf(DBHelper.getUser().getId()));


            Log.d("StopitActivity", "userProperty orgId " + currentOrg.getId());
        } else {
            mFirebaseAnalytics.setUserProperty(FBA_USER_PROPERTY_KEY_ORG_NAME, "");
            mFirebaseAnalytics.setUserProperty(FBA_USER_PROPERTY_KEY_ORG_ID, "");
            mFirebaseAnalytics.setUserId("");
        }
    }

    @LayoutRes
    abstract int getContentView();

    void initData() {
    }

    abstract void initUI();

    abstract void setUI(final Bundle savedInstanceState);

    protected boolean isIntentTransactionSuccessful(final Intent intent) {
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        final EventBus eventBus = EventBus.getDefault();
        if (eventBus.isRegistered(this))
            eventBus.unregister(this);
    }

    @Override
    public void onClick(View view) {
    }

    public StopitToolbar initToolbar() {
        final StopitToolbar toolbar = findViewById(R.id.stopit_toolbar);
        return toolbar;
    }

    public StopitOrgSelector initOrgSelectorView() {
        return findViewById(R.id.stopit_organization_selector);
    }

    @Override
    public void onTitleClicked() {
    }

    public void showAlertDialog(final String title, final String message) {
        showAlertDialog(title, message, false, null);
    }

    public void showAlertDialog(final String title, final String message, final DialogInterface.OnClickListener clickListener) {
        if (isFinishing())
            return;

        //TODO need to handle branding for custom dialog
        new AlertDialog.Builder(this, R.style.AlertDialogCustom)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok_btn_label, clickListener)
                .show();
    }

    public void showAlertDialog(final String title, final String message, final boolean cancelable, final DialogInterface.OnClickListener clickListener) {
        if (isFinishing())
            return;

        //TODO need to handle branding for custom dialog
        new AlertDialog.Builder(this, R.style.AlertDialogCustom)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok_btn_label, clickListener)
                .setCancelable(cancelable)
                .show();
    }

    public void showAlertDialog(final String title, final String message, final int positiveBtnRes, final DialogInterface.OnClickListener clickListener) {
        if (isFinishing())
            return;

        //TODO need to handle branding for custom dialog
        new AlertDialog.Builder(this, R.style.AlertDialogCustom)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveBtnRes, clickListener)
                .show();
    }

    public void showAlertDialog(final String title, final String message, final int positiveBtnRes, final boolean cancelable, final DialogInterface.OnClickListener clickListener) {
        if (isFinishing())
            return;

        //TODO need to handle branding for custom dialog
        new AlertDialog.Builder(this, R.style.AlertDialogCustom)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveBtnRes, clickListener)
                .setCancelable(false)
                .show();
    }

    public void showTwoBtnsAlertDialog(final String title, final String message, final int positiveBtnLabelRes,
                                       final int negativeBtnRes, final DialogInterface.OnClickListener positiveClicker) {
        //TODO need to handle branding for custom dialog
        new AlertDialog.Builder(this, R.style.AlertDialogCustom)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveBtnLabelRes, positiveClicker)
                .setNegativeButton(negativeBtnRes, null)
                .show();
    }

    public void showOutOfSessionAlert() {
        showAlertDialog("", getString(R.string.reporting_disabled_err));
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////  API CALLS /////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void afterTextChanged(Editable s) {
    }

    public boolean checkInternetConnectionAndInform() {
        if (!NetworkStateHelper.isNetworkAvailable(this)) {
            showAlertDialog("", getString(R.string.no_network_err));
            return false;
        }
        return true;
    }

    public boolean isInternetConnection() {
        return NetworkStateHelper.isNetworkAvailable(this);
    }

    /////////////////////////// IApiCallBackError ////////////////////////////////
    @Override
    public boolean needToHandle() {
        return true;
    }

    private void addApiInProgressView() {
        if (apiInProgressView == null)
            apiInProgressView = findViewById(R.id.apiInProgress);
        if (apiInProgressView == null) {
            final ViewGroup viewGroup = findViewById(android.R.id.content);
            final LayoutInflater layoutInflater = getLayoutInflater();
            apiInProgressView = layoutInflater.inflate(R.layout.api_in_progress, null);
            viewGroup.addView(apiInProgressView);
        }
        apiInProgressBarView = apiInProgressView.findViewById(R.id.progressBar);
    }

    @Override
    public void showApiInProgressView() {
        hideKeyboardIfShown();
        addApiInProgressView();
        apiInProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideApiInProgressView() {
        if (apiInProgressView != null)
            apiInProgressView.setVisibility(View.GONE);
    }

    public void hideKeyboardIfShown() {
        hideKeyboardIfShown((ViewGroup) findViewById(android.R.id.content));
    }

    @Override
    public void onError(@NonNull Call call, @NonNull ApiError error) {
        if (isUserActive(error))
            showAlertDialog("", error.getMessage());
        else //user was deactivated
            deactivateUser(error);
    }

    @Override
    public void onFailure(@NonNull Call call, @NonNull Throwable t) {

    }

    boolean isUserActive(@NonNull ApiError error) {
        final int code = error.getCode();
        //Toast.makeText(this, "isUserActive check - API error = " + code, Toast.LENGTH_SHORT).show();
        return (code != ST_DEACTIVARED_USER_ERR_CODE && code != ST_DEACTIVARED_USER_ERR_CODE_V2);
    }

    void deactivateUser(@NonNull ApiError error) {
        if (deactivatingUserTask.isRunning())
            return;

        deactivatingUserTask.setRunning();
        showAlertDialog("", error.getMessage(), false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deactivatingUserTask.setFinished();
                requestLogout();
            }
        });
    }

    String getIntellicode() {
        return DBHelper.getIntellicode();
    }

    boolean isResponseUserDataValid(final UserData userData) {
        if (userData == null)
            return false;

        final User user = userData.getUser();
        if (user == null || TextUtils.isEmpty(user.getIntelliCode()))
            return false;

        final Organization currentOrg = userData.getOrganization();
        if (currentOrg == null || currentOrg.getId() == 0)
            return false;

        return true;
    }

    public void changeOrganization(final long selectedOrgId) {
        if (!checkInternetConnectionAndInform())
            return;

        //add new organization option selected
        if (selectedOrgId == 0) {
            BrandingHelper.clearBranding(true);
            showStartActivity(false);
        } else {
            //switch to historical organization selected
            final Organization orgToSwitchTo = DBHelper.getOrganizationById(selectedOrgId);
            final String savedIntellicode = orgToSwitchTo.getIntellicode();
            if (orgToSwitchTo != null && !TextUtils.isEmpty(savedIntellicode)) {
                Log.d("StopitActivity", "org id " + orgToSwitchTo.getId() + " intellicode " + orgToSwitchTo.getIntellicode());
                requestOrganizationChange(savedIntellicode);
            }
        }
    }

    void requestOrganizationChange(final String intellicode) {
        if (!checkInternetConnectionAndInform())
            return;

        ApiManager.getAccountAdapter().changeOrganization(new IApiCallBackSuccess<StopitResponse<String>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<String> response) {
                final UserData userData = response.getUserDataDecrypted();
                if (userData == null)
                    onOrganizationChangeFailure(getString(R.string.change_org_err));
                else
                    onOrganizationChangeSuccess(response.getUserDataDecrypted());

            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
                StopitActivity.this.showApiInProgressView();
            }

            @Override
            public void hideApiInProgressView() {
                StopitActivity.this.hideApiInProgressView();
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                if (isUserActive(error))
                    onOrganizationChangeFailure(error.getMessage());
                else //user was deactivated
                    deactivateUser(error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                onOrganizationChangeFailure(getString(R.string.change_org_err));
            }
        }, intellicode);
    }

    void onOrganizationChangeSuccess(UserData userData) {
        BrandingHelper.clearBranding(true);
        DBHelper.saveAllUserData(userData);
        final Branding newBranding = DBHelper.getBranding();
        if (newBranding != null && newBranding.isUrlsToDownload())
            downloadBrandingAssets(newBranding);
        else
            startMainActivity();
    }

    void onOrganizationChangeFailure(final String message) {
        showAlertDialog("", message);
    }

    public void statusCheck() {
        statusCheck(DBHelper.getIntellicode());
    }

    public void statusCheck(String intellicode) {
        if (!isInternetConnection() || statusBeingChecked.isRunning())
            return;

        statusBeingChecked.setRunning();
        ApiManager.getAccountAdapter().statusCheck(new IApiCallBackSuccess<StopitResponse<String>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<String> response) {
                statusBeingChecked.setFinished();
                final UserData userData = response.getUserDataDecrypted();
                if (userData == null)
                    onStatusCheckFailure();
                else
                    onStatusCheckSuccess(userData);

            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
                StopitActivity.this.showApiInProgressView();
            }

            @Override
            public void hideApiInProgressView() {
                StopitActivity.this.hideApiInProgressView();
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                statusBeingChecked.setFinished();
                if (isUserActive(error))
                    onStatusCheckFailure();
                else
                    deactivateUser(error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                statusBeingChecked.setFinished();
                onStatusCheckFailure();
            }
        }, intellicode);
    }

    void onStatusCheckFailure() {
    }

    void onStatusCheckSuccess(final UserData userData) {
        if (userData == null) {
            showAlertDialog("", "Error");
            return;
        }

        //if status check response is invalid - ignore the response
        if (!isResponseUserDataValid(userData))
            return;

        //get previous branding assets urls and compare with new ones
        String logoUrl = null;
        String bgUrl = null;
        final Branding currentBranding = DBHelper.getBranding();
        if (currentBranding != null) {
            logoUrl = currentBranding.getLogoUrl();
            bgUrl = currentBranding.getBgImageUrl();
        }

        BrandingHelper.clearBranding(false);
        //DBHelper.saveAllUserData(userData);
        DBHelper.saveUserData(userData);
        final Branding updatedBranding = DBHelper.getBranding();
        if (updatedBranding != null) {
            if (!TextUtils.equals(logoUrl, updatedBranding.getLogoUrl())
                    || !TextUtils.equals(bgUrl, updatedBranding.getBgImageUrl())) {
                downloadBrandingAssets(updatedBranding);
            }
        }
    }

    void performLogin(final String accessCode) {
        if (!checkInternetConnectionAndInform())
            return;

        ApiManager.getAccountAdapter().register(new IApiCallBackSuccess<StopitResponse<UserData>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<UserData> response) {
                onLoginSuccess(response.getData(), accessCode);
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
                StopitActivity.this.showApiInProgressView();
            }

            @Override
            public void hideApiInProgressView() {
                StopitActivity.this.hideApiInProgressView();
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                if (error.getCode() == ST_INVALID_ACCESS_CODE)
                    showAlertDialog("", getString(R.string.invalid_accesscode_err));
                else
                    StopitActivity.this.onError(call, error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                onLoginFailure();
            }
        }, accessCode);
    }

    void performLogin(final long selectedOrgId) {
        if (!checkInternetConnectionAndInform())
            return;

        ApiManager.getAccountAdapter().register(new IApiCallBackSuccess<StopitResponse<UserData>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<UserData> response) {
                onLoginSuccess(response.getData(), null);
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
                StopitActivity.this.showApiInProgressView();
            }

            @Override
            public void hideApiInProgressView() {
                StopitActivity.this.hideApiInProgressView();
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                StopitActivity.this.onError(call, error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                onLoginFailure();
            }
        }, selectedOrgId);
    }


    void onLoginSuccess(final UserData userData, final String accessCode) {
    }

    void onLoginFailure() {
        showAlertDialog("", getString(R.string.unknown_err_msg));
    }

    //logout from current org
    public void requestLogout() {
        if (!checkInternetConnectionAndInform())
            return;
        ApiManager.getAccountAdapter().logout(new IApiCallBackSuccess<StopitResponse<UserData>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<UserData> response) {
                onLogoutSuccess();
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
                StopitActivity.this.showApiInProgressView();
            }

            @Override
            public void hideApiInProgressView() {
                StopitActivity.this.hideApiInProgressView();
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                if (isUserActive(error))
                    showAlertDialog("", error.getMessage(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            onLogoutFailure();
                        }
                    });
                else
                    deactivateUser(error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                onLogoutFailure();
            }
        });
    }

    public void onLogoutSuccess() {
        //clearAnalyticsProperties();
        clearCurrentOrgInfo();

        //check for saved logged in orgs
        final RealmResults<Organization> savedOrganizations = DBHelper.getAllOrganizationsList();
        if (savedOrganizations.isEmpty()) {
            showStartActivity(true);
            return;
        }

        String savedIntellicode = null;
        for (Organization org : savedOrganizations) {
            if (org == null || TextUtils.isEmpty(org.getIntellicode()))
                continue;

            savedIntellicode = org.getIntellicode();
            break;
        }
        if (!TextUtils.isEmpty(savedIntellicode)) {
            requestOrganizationChange(savedIntellicode);
        } else
            showStartActivity(true);
    }

    void clearCurrentOrgInfo() {
        BrandingHelper.clearBranding(true);
        ParseHelper.unsubscribeForParsePush();

        final Organization currentOrg = DBHelper.getCurrentOrganization();
        if (currentOrg != null) {
            SharedPrefsHelper.setTermsOfUseDeclined(this, currentOrg.getId());
            DBHelper.delete(currentOrg);
            onLogout();
        }
    }

    void onLogout() {
        DBHelper.onLogout();
        DBHelper.clear(OrgsTreeEntity.class);
    }

    void onLogoutFailure() {
    }

    void showStartActivity(final boolean isClearTask) {
        final Intent intent = new Intent(this, StartActivity.class);
        if (isClearTask)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        intent.putExtra(ST_EXTRAS_KEY_IS_CLEAR_TASK, isClearTask);
        startActivity(intent);
    }

    void acceptTermsOfUse() {
        if (!checkInternetConnectionAndInform())
            return;

        //TODO check
        /*if(userData == null || userData.getUser() == null){
            showAlertDialog("", getString(R.string.unknown_err_msg));
            return;
        }
*/
        final String intellicode = DBHelper.getIntellicode();//userData.getUser().getIntelliCode();
        /*if(TextUtils.isEmpty(intellicode)){
            showAlertDialog("", getString(R.string.unknown_err_msg));
            return;
        }*/

        ApiManager.getAccountAdapter().acceptTermsOfUse(new IApiCallBackSuccess<StopitResponse<Void>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<Void> response) {
                onAcceptTermsOfUseSuccess();
            }
        }, this, intellicode);
    }

    void onAcceptTermsOfUseSuccess() {
        //TODO check
        /*if (userData == null) {
            showAlertDialog("", getString(R.string.unknown_err_msg));
            return;
        }*/

        //DBHelper.saveAllUserData(userData);
        ParseHelper.subscribeForParsePush();
        SharedPrefsHelper.setTermsOfUseAccepted(this, DBHelper.getCurrentOrganizationId());
        startMainActivity();
        finish();

    }

    void submitIncident(final Incident incident) {
        ApiManager.getIncidentsAdapter().submitIncident(new IApiCallBackSuccess<StopitResponse<IncidentDataWrapper>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<IncidentDataWrapper> response) {
                final IncidentDataWrapper dataWrapper = response.getData();
                final Incident incident = dataWrapper.getIncident();
                final String afterHoursMessage = dataWrapper.getAfterHoursMessage();
                if (incident != null)
                    onSubmitIncidentSuccess(incident, afterHoursMessage);
                else {
                    onSubmitIncidentFailure();
                }
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
                StopitActivity.this.showApiInProgressView();
            }

            @Override
            public void hideApiInProgressView() {
                StopitActivity.this.hideApiInProgressView();
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                if (isUserActive(error))
                    showAlertDialog("", error.getMessage(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            onSubmitIncidentFailure();
                        }
                    });
                else
                    deactivateUser(error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                onSubmitIncidentFailure();
            }
        }, incident);
    }

    void onSubmitIncidentSuccess(final Incident incidentWrapper, final String afterHoursMessage) {
    }

    void onSubmitIncidentFailure() {
    }

    void updateIncident(final long incidentId, final List<MediaItem> mediaItems) {
        final Configuration config = DBHelper.getCurrentConfiguration();
        if (config == null || config.getAws() == null) {
            onUpdateIncidentFailure(getString(R.string.incident_update_err));
            return;
        }

        //clearing from previous incident update
        attachments.clear();
        //incidentToUpdate = incident;//DBHelper.copyFromRealm(incident);
        incidentToUpdateId = incidentId;
        uploadFilesCount.set(mediaItems.size());
        showApiInProgressView();

        for (MediaItem item : mediaItems) {
            final File fileToSend = new File(item.getPath());
            AmazonHelper.sendMedia(getBaseContext(), fileToSend, incidentId, this);
        }
    }

    public void onUploadFinished(final File file, final String filePath) {
        //String extension = filePath.substring(filePath.lastIndexOf(".")).replace(".", "");
        attachments.add(new IncidentAttachment(this, file, filePath));
        if (uploadFilesCount.decrementAndGet() == 0) {
            requestIncidentUpdate(incidentToUpdateId, attachments);
        }
    }

    public void onUploadFailed() {
        if (uploadFilesCount.decrementAndGet() == 0) {
            if (attachments.size() > 0) {
                requestIncidentUpdate(incidentToUpdateId, attachments);
            } else {
                //files were not uploaded - no attachments to update an incident
                hideApiInProgressView();
                onUpdateIncidentFailure(getString(R.string.incident_update_err));
            }
        }
    }

    void requestIncidentUpdate(final long incidentId, final List<IncidentAttachment> attachments) {
        ApiManager.getIncidentsAdapter().updateIncident(new IApiCallBackSuccess<StopitResponse<IncidentDataWrapper>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<IncidentDataWrapper> response) {
                onUpdateIncidentSuccess(response.getData().getIncident());
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
                StopitActivity.this.showApiInProgressView();
            }

            @Override
            public void hideApiInProgressView() {
                StopitActivity.this.hideApiInProgressView();
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                if (isUserActive(error))
                    onUpdateIncidentFailure(error.getMessage());
                else
                    deactivateUser(error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                onUpdateIncidentFailure(getString(R.string.incident_update_err));
            }
        }, incidentId, attachments);
    }

    void onUpdateIncidentSuccess(final Incident incident) {
    }

    void onUpdateIncidentFailure(final String errorMessage) {
    }

    void requestIncidentById(final long incidentId) {
        //failure handled in the ReportDetailsActivity
        if (!checkInternetConnectionAndInform())
            return;

        ApiManager.getIncidentsAdapter().getIncidentById(new IApiCallBackSuccess<StopitResponse<String>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<String> response) {
                onRequestIncidentByIdSuccess(response.getIncidentDecrypted());
            }
        }, this, incidentId);
    }

    void onRequestIncidentByIdSuccess(final Incident incident) {
    }

    void requestIncidentsList() {
        if (incidentsBeingRefreshed.isRunning())
            return;

        incidentsBeingRefreshed.setRunning();
        ApiManager.getIncidentsAdapter().requestIncidentsList(new IApiCallBackSuccess<StopitResponse<String>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<String> response) {
                incidentsBeingRefreshed.setFinished();
                onRequestIncidentsSuccess(response.getIncidentsListDecrypted());
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
                StopitActivity.this.showApiInProgressView();
            }

            @Override
            public void hideApiInProgressView() {
                StopitActivity.this.hideApiInProgressView();
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                incidentsBeingRefreshed.setFinished();
                if (isUserActive(error))
                    onRequestIncidentsFailure(error.getMessage());
                else
                    deactivateUser(error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                incidentsBeingRefreshed.setFinished();
                onRequestIncidentsFailure(getString(R.string.incidents_list_request_failed));
            }
        });
    }

    void onRequestIncidentsSuccess(List<Incident> incidents) {
        //overrided in Main Activity
    }

    void onRequestIncidentsFailure(final String errorMessage) {
        //overrided in Main Activity
    }

    void requestIncidentByIdAndCode(final long incidentId, final String code) {
        if (!checkInternetConnectionAndInform())
            return;

        ApiManager.getIncidentsAdapter().getIncidentByIdAndCode(new IApiCallBackSuccess<StopitResponse<IncidentDataWrapper>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<IncidentDataWrapper> response) {
                onRequestIncidentByIdAndCodeSuccess(response.getData().getIncident());
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
                StopitActivity.this.showApiInProgressView();
            }

            @Override
            public void hideApiInProgressView() {
                StopitActivity.this.hideApiInProgressView();
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                StopitActivity.this.onError(call, error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                onRequestIncidentByIdAndCodeFailure();
            }
        }, incidentId, code);
    }

    void onRequestIncidentByIdAndCodeSuccess(final Incident incident) {
    }

    void onRequestIncidentByIdAndCodeFailure() {
        showAlertDialog("", getString(R.string.incident_by_id_and_code_err));
    }

    void requestDynamicQuestions() {
        ApiManager.getIncidentsAdapter().getDynamicQuestions(new IApiCallBackSuccess<StopitResponse<String>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<String> response) {
                onRequestDynamicQuestionsSuccess(response.getDynamicQuestionsDecrypted());
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
                StopitActivity.this.showApiInProgressView();
            }

            @Override
            public void hideApiInProgressView() {
                StopitActivity.this.hideApiInProgressView();
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                if (isUserActive(error))
                    onRequestDynamicQuestionsFailure(error.getMessage());
                else
                    deactivateUser(error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                onRequestDynamicQuestionsFailure(getString(R.string.dynamic_questions_request_failure));
            }
        });
    }

    void onRequestDynamicQuestionsSuccess(final List<DynamicQuestion> questions) {
    }

    void onRequestDynamicQuestionsFailure(final String message) {
    }

    void requestBroadcastMessages() {
        if (broadcastsBeingRefreshed.isRunning())
            return;

        broadcastsBeingRefreshed.setRunning();
        ApiManager.getBroadcastsAdapter().getMessagesList(new IApiCallBackSuccess<StopitResponse<BroadcastListWrapper>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<BroadcastListWrapper> response) {
                broadcastsBeingRefreshed.setFinished();
                onRetrieveBroadcastMessagesSuccess(response.getData().getMessages());
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
                StopitActivity.this.showApiInProgressView();
            }

            @Override
            public void hideApiInProgressView() {
                StopitActivity.this.hideApiInProgressView();
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                broadcastsBeingRefreshed.setFinished();
                if (isUserActive(error))
                    onRetrieveBroadcastMessagesFailure(error.getMessage());
                else
                    deactivateUser(error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                broadcastsBeingRefreshed.setFinished();
                onRetrieveBroadcastMessagesFailure(getString(R.string.unknown_broadcasts_err_msg));
            }
        });
    }

    void onRetrieveBroadcastMessagesSuccess(final List<BroadcastMessage> messages) {
        Log.d("StopitActivity", "Broadcast" + messages);
    }

    void onRetrieveBroadcastMessagesFailure(final String errorMessage) {
    }

    void updateBroadcastMessage(final String messageId, final boolean isMessageRead) {
        ApiManager.getBroadcastsAdapter().updateMessage(new IApiCallBackSuccess<StopitResponse<BroadcastMessageWrapper>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<BroadcastMessageWrapper> response) {
                onUpdateBroadcastMessageSuccess(messageId);
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
            }

            @Override
            public void hideApiInProgressView() {
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                if (isUserActive(error))
                    showAlertDialog("", error.getMessage(), false, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            onUpdateBroadcastMessageFailure();
                        }
                    });
                else
                    deactivateUser(error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                showAlertDialog("", getString(R.string.update_inbox_message_failure), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onUpdateBroadcastMessageFailure();
                    }
                });
            }
        }, messageId, isMessageRead);
    }

    void onUpdateBroadcastMessageSuccess(final String messageId) {
    }

    void onUpdateBroadcastMessageFailure() {
    }

    void requestBroadcastMessageById(final String messageId) {
        Log.d("StopitActivity", "details for a broadcast message id " + messageId);
        ApiManager.getBroadcastsAdapter().getMessageById(new IApiCallBackSuccess<StopitResponse<BroadcastMessageWrapper>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<BroadcastMessageWrapper> response) {
                onRetrieveBroadcastMessageSuccess(response.getData().getMessage());
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
                StopitActivity.this.showApiInProgressView();
            }

            @Override
            public void hideApiInProgressView() {
                StopitActivity.this.hideApiInProgressView();
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                if (isUserActive(error))
                    onRetrieveBroadcastMessageFailure(error.getMessage());
                else
                    deactivateUser(error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                onRetrieveBroadcastMessageFailure("");
            }
        }, messageId);
    }

    void onRetrieveBroadcastMessageSuccess(final BroadcastMessage message) {
    }

    void onRetrieveBroadcastMessageFailure(final String message) {
    }

    void deleteBroadcastMessageById(final String messageId) {
        ApiManager.getBroadcastsAdapter().deleteMessageById(new IApiCallBackSuccess<StopitResponse<BroadcastMessageWrapper>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<BroadcastMessageWrapper> response) {
                onDeleteBroadcastMessageSuccess(messageId);
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
            }

            @Override
            public void hideApiInProgressView() {
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                if (isUserActive(error))
                    showAlertDialog("", error.getMessage(), false, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            onDeleteBroadcastMessageFailure();
                        }
                    });
                else
                    deactivateUser(error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                showAlertDialog("", getString(R.string.delete_inbox_message_failure), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onDeleteBroadcastMessageFailure();
                    }
                });
            }
        }, messageId);
    }

    void onDeleteBroadcastMessageSuccess(final String messageId) {
    }

    void onDeleteBroadcastMessageFailure() {
    }

    void retrieveBroadcastMessageMediaUrl(final String messageId) {
        Log.d("StopitActivity", "URL for a broadcast message id " + messageId);
        if (!isInternetConnection())
            return;

        ApiManager.getBroadcastsAdapter().getMessageMediaUrl(new IApiCallBackSuccess<StopitResponse<String>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<String> response) {
//                final BroadcastAttachmentWrapper wrapper = response.getBroadcastAttachmentDecrypted();
//                if (wrapper != null)
//                    Log.d("StopitActivity", "attachment for the broadcast message id " + wrapper.getMimeType() + " url " + wrapper.getUrl());
                onRetrieveMessageURLSuccess(response.getBroadcastAttachmentDecrypted());
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
                StopitActivity.this.showApiInProgressView();
            }

            @Override
            public void hideApiInProgressView() {
                StopitActivity.this.hideApiInProgressView();
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                if (isUserActive(error))
                    onRetrieveMessageURLFailure(error.getMessage());
                else
                    deactivateUser(error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                onRetrieveMessageURLFailure("");
            }
        }, messageId);
    }

    void onRetrieveMessageURLSuccess(final BroadcastAttachmentWrapper mediaWrapper) {
    }

    void onRetrieveMessageURLFailure(final String message) {
    }

    void retrieveInboxUnreadCount() {
        if (!isInternetConnection())
            return;

        ApiManager.getBroadcastsAdapter().getUnreadInboxCount(new IApiCallBackSuccess<StopitResponse<String>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<String> response) {
                onRetrieveInboxUnreadCountSuccess(response.getUnreadInboxCounter());
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
                StopitActivity.this.showApiInProgressView();
            }

            @Override
            public void hideApiInProgressView() {
                StopitActivity.this.hideApiInProgressView();
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                if (isUserActive(error))
                    onRetrieveInboxUnreadCountFailure();
                else
                    deactivateUser(error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                onRetrieveInboxUnreadCountFailure();
            }
        });
    }

    void onRetrieveInboxUnreadCountSuccess(final UnreadInboxCounter counter) {
    }

    void onRetrieveInboxUnreadCountFailure() {
    }


    void retrieveChatHistory(final long incidentId) {
        ApiManager.getMessengerApiAdapter().getChatHistory(new IApiCallBackSuccess<StopitResponse<String>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<String> response) {
                onRetrieveChatHistorySuccess(response.getChatHistoryDecrypted().getAllMessages());
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
            }

            @Override
            public void hideApiInProgressView() {
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                if (error.getCode() == 100017)
                    onRetrieveChatHistorySuccess(new ArrayList<ChatMessage>());
                else
                    StopitActivity.this.onError(call, error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                onRetrieveChatHistoryFailure();
            }
        }, incidentId);
    }

    void onRetrieveChatHistorySuccess(final List<ChatMessage> chatHistory) {
    }

    void onRetrieveChatHistoryFailure() {
    }

    void updateChatMessages(final long incidentId) {
        ApiManager.getMessengerApiAdapter().updateMessages(new IApiCallBackSuccess<StopitResponse<Void>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<Void> response) {
                onUpdateChatMessagesSuccess();
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
            }

            @Override
            public void hideApiInProgressView() {
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                StopitActivity.this.onError(call, error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                onUpdateChatMessagesFailure();
            }
        }, incidentId);
    }

    void onUpdateChatMessagesSuccess() {
    }

    void onUpdateChatMessagesFailure() {
    }

    void retrieveUnreadChatMessagesCount() {
        ApiManager.getMessengerApiAdapter().getTotalUnreadMessagesCount(new IApiCallBackSuccess<StopitResponse<String>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<String> response) {
                retrieveTotalUnreadMessagesCountSuccess(response.getUnreadMessengerCounter());
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
            }

            @Override
            public void hideApiInProgressView() {
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                StopitActivity.this.onError(call, error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                retrieveTotalUnreadMessagesCountFailure();
            }
        });
    }

    void retrieveTotalUnreadMessagesCountSuccess(final UnreadMessengerCounter counter) {
    }

    void retrieveTotalUnreadMessagesCountFailure() {
    }


    void requestResources() {
        ApiManager.getResourcesAdapter().getInformationResources(new IApiCallBackSuccess<StopitResponse<ResourcesListDataWrapper>>() {
            @Override
            public void onApiSuccess(@NonNull StopitResponse<ResourcesListDataWrapper> response) {
                onRequestResourcesSuccess(response.getData().getResources());
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
                StopitActivity.this.showApiInProgressView();
            }

            @Override
            public void hideApiInProgressView() {
                StopitActivity.this.hideApiInProgressView();
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                if (isUserActive(error))
                    onRequestResourcesFailure(error.getMessage());
                else
                    deactivateUser(error);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                onRequestResourcesFailure(getString(R.string.unknown_resources_err_msg));
            }
        });
    }

    void onRequestResourcesSuccess(final List<InformationResource> resources) {
    }

    void onRequestResourcesFailure(final String errorMessage) {
    }

    public void startMessengerActivity(final long incidentId) {
        final Intent intent = new Intent(this, MessengerActivity.class);
        intent.putExtra(Constants.ST_EXTRAS_KEY_INCIDENT_ID, (long) incidentId);
        startActivity(intent);
    }

    void startOrganizationSearchActivity(final long parentOrgId, final String parentOrgName, final boolean isTopOrg) {
        final Intent intent = new Intent(this, SearchOrganizationActivity.class);
        intent.putExtra(ST_EXTRAS_KEY_IS_TOP_ORG, isTopOrg);
        intent.putExtra(ST_EXTRAS_KEY_ORGANIZATION_ID, parentOrgId);
        intent.putExtra(ST_EXTRAS_KEY_ORGANIZATION_NAME, parentOrgName);
        startActivity(intent);
    }

    void startOrganizationSearchActivity(final long parentOrgId) {
        final Intent intent = new Intent(this, SearchOrganizationActivity.class);
        intent.putExtra(ST_EXTRAS_KEY_ORGANIZATION_ID, parentOrgId);
        startActivity(intent);
    }

    void startTermsOfUseActivity() {
        startActivity(new Intent(this, TermsOfUseActivity.class));
    }

    public void startMainActivity() {
        DBHelper.clear(OrgsTreeEntity.class);

        //setting an organization name property for analytics
//        final Organization currentOrg = DBHelper.getCurrentOrganization();
//        if (currentOrg != null && !TextUtils.isEmpty(currentOrg.getName())) {
//            mFirebaseAnalytics.setUserProperty(FBA_USER_PROPERTY_KEY_ORG_NAME, currentOrg.getName());
//            mFirebaseAnalytics.setUserProperty(FBA_USER_PROPERTY_KEY_ORG_ID, String.valueOf(currentOrg.getId()));
//            mFirebaseAnalytics.setUserId(String.valueOf(DBHelper.getUser().getId()));
//            Log.d("StopitActivity", "userProperty orgId "+currentOrg.getId());
//        }

        final Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////  camera gallery popup  ////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(Object event) {
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AppOnForegroundEvent event) {
        if (isFinishing() || event == null)
            return;

        final User user = DBHelper.getUser();
        if (user == null)
            return;

        final String currentIntellicode = user.getIntelliCode();
        if (TextUtils.isEmpty(currentIntellicode))
            return;

        statusCheck();
    }

    public void showOptionsPopup(final Object tag) {
        if (optionsPopup == null) {
            optionsPopup = new OptionsPopup(this);
            optionsPopup.setOnOptionsPopupChoiceListener(this);
        }

        if (tag != null)
            optionsPopup.setTag(tag);

        optionsPopup.show();
    }

    public void handleOptionsPopupChoice(OptionsPopup popup, int actionId) {
        if (isMediaRestricted()) {
            showAlertDialog("", getString(R.string.org_media_upload_restricted));
            return;
        }

        switch (actionId) {
            case OPTIONS_POPUP_ACTION_TAKE_PHOTO:
                if (isCameraRestricted(CAMERA_FOR_PHOTO_REQUEST_PERMISSION_CODE))
                    return;

                prepareToTakePhoto();
                break;

            case OPTIONS_POPUP_ACTION_TAKE_VIDEO:
                if (isCameraRestricted(CAMERA_FOR_VIDEO_REQUEST_PERMISSION_CODE))
                    return;

                prepareToRecordVideo();
                break;

            case OPTIONS_POPUP_ACTION_PICK_PHOTO:
                if (isSDCardRestricted(EXTERNAL_STORAGE_FOR_PHOTO_GALLERY_REQUEST_PERMISSION_CODE))
                    return;

                pickPhotoFromGallery();
                break;

            case OPTIONS_POPUP_ACTION_PICK_VIDEO:
                if (isSDCardRestricted(EXTERNAL_STORAGE_FOR_VIDEO_GALLERY_REQUEST_PERMISSION_CODE))
                    return;

                pickVideoFromGallery();
                break;
        }
    }

    public void handleOptionsPopupCancel() {
    }

    boolean isMediaRestricted() {
        final Organization currentOrganization = DBHelper.getCurrentOrganization();
        return !currentOrganization.isMediaUploadAllowed();
    }

    boolean isCameraRestricted(final int permissionRequestCode) {
        if (!CameraHelper.hasCamera()) {
            showAlertDialog("", getString(R.string.no_camera_available_err));
            return true;

        } else if (Build.VERSION.SDK_INT >= 23 && !StopitPermissionsHelper.isCameraPermitted(this)) {
            StopitPermissionsHelper.requestCameraPermission(this, permissionRequestCode);
            return true;

        } else
            return false;
    }

    boolean isSDCardRestricted(final int permissionRequestCode) {
        if (!CameraHelper.isExternalStorageAvailable()) {
            showAlertDialog("", getString(R.string.no_SDCard_available_err));
            return true;

        } else if (Build.VERSION.SDK_INT >= 23 && !StopitPermissionsHelper.isExternalStorageReadWritePermitted(this)) {
            StopitPermissionsHelper.requestExternalStoragePermission(this, permissionRequestCode);
            return true;

        } else
            return false;
    }

    void prepareToTakePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                showAlertDialog("", getString(R.string.unknown_err_msg));
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.stopitcyberbully.mobile.fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, TAKE_PHOTO_REQUEST_CODE);
            }
        }
    }

    public File createImageFile() throws IOException {
        // Create an image file name
        final String timeStamp = DateUtils.getMediaFilePrefixFormatted(System.currentTimeMillis());
        String imageFileName = MEDIA_FILE_PREFIX + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                MEDIA_FILE_SUFFIX,
                storageDir
        );
        Log.d("StopitActivity", "fileName " + imageFileName);

        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    void prepareToRecordVideo() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, VIDEO_RECORD_REQUEST_CODE);
        }
    }

    void pickPhotoFromGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, GALLERY_PHOTO_PICK_REQUEST_CODE);
    }

    void pickVideoFromGallery() {
        Intent videoPickerIntent = new Intent(Intent.ACTION_PICK);
        videoPickerIntent.setType("video/*");
        startActivityForResult(videoPickerIntent, GALLERY_VIDEO_PICK_REQUEST_CODE);
    }

    public String getRealPathFromURI(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;

        if (requestCode == TAKE_PHOTO_REQUEST_CODE && !TextUtils.isEmpty(currentPhotoPath)) {
            if (!isValidMimeType(currentPhotoPath)) {
                showAlertDialog("", getString(R.string.attachment_err));
                final File file = new File(currentPhotoPath);
                if (file.exists() && file.canWrite()) {
                    boolean isDeleted = file.delete();
                    Log.d("ReportActivity", "deleted file " + isDeleted + " path " + currentPhotoPath);
                }
                return;
            }

            addMediaItem(currentPhotoPath, true);

        } else if ((requestCode == GALLERY_PHOTO_PICK_REQUEST_CODE
                || requestCode == VIDEO_RECORD_REQUEST_CODE
                || requestCode == GALLERY_VIDEO_PICK_REQUEST_CODE) && data != null) {
            final Uri uri = data.getData();
            if (uri != null) {
                addMediaItem(uri, requestCode == VIDEO_RECORD_REQUEST_CODE);
            }
        }
    }

    void addMediaItem(final Uri uri, final boolean shouldBeDeleted) {
        final String path = getRealPathFromURI(uri);

        if (!isValidMimeType(path)) {
            showAlertDialog("", getString(R.string.attachment_err));
            final File file = new File(path);
            if (file.exists() && file.canWrite()) {
                boolean isDeleted = file.delete();
                Log.d("ReportActivity", "deleted file " + isDeleted + " path " + path);
            }
            return;
        }

        addMediaItem(path, shouldBeDeleted);
    }

    void addMediaItem(final String path, final boolean shouldBeDeleted) {
        //need to be implemented
    }

    boolean isValidMimeType(final String path) {
        final List<String> mimeTypes = Arrays.asList(getResources().getStringArray(R.array.supported_mime_types));
        final String mimeType = getMimeType(path);
        return mimeTypes.contains(mimeType);
    }

    String getMimeType(final String path) {
        final Uri uri = Uri.fromFile(new File(path));
        return getMimeType(uri);
    }

    String getMimeType(final Uri uri) {
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = getContentResolver();
            mimeType = cr.getType(uri);
        }

        if (!TextUtils.isEmpty(mimeType)) {
            Log.d("StopitActivity", "mimeType " + mimeType);
            return mimeType;
        }

        String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                .toString());
        mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                fileExtension.toLowerCase());

        Log.d("StopitActivity", "mimeType " + mimeType);
        return mimeType;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        switch (requestCode) {
            case LOCATION_REQUEST_PERMISSION_CODE:
                setLocation();
                break;

            case CAMERA_FOR_PHOTO_REQUEST_PERMISSION_CODE:
                Log.d("StopitActivity", "CAMERA_REQUEST_PERMISSION granted");
                if (isSDCardRestricted(EXTERNAL_STORAGE_FOR_PHOTO_REQUEST_PERMISSION_CODE))
                    return;

                prepareToTakePhoto();
                break;

            case CAMERA_FOR_VIDEO_REQUEST_PERMISSION_CODE:
                if (isSDCardRestricted(EXTERNAL_STORAGE_FOR_VIDEO_REQUEST_PERMISSION_CODE))
                    return;

                prepareToRecordVideo();
                break;

            case EXTERNAL_STORAGE_FOR_PHOTO_REQUEST_PERMISSION_CODE:
                prepareToTakePhoto();
                break;

            case EXTERNAL_STORAGE_FOR_VIDEO_REQUEST_PERMISSION_CODE:
                prepareToRecordVideo();
                break;

            case EXTERNAL_STORAGE_FOR_PHOTO_GALLERY_REQUEST_PERMISSION_CODE:
                pickPhotoFromGallery();
                break;

            case EXTERNAL_STORAGE_FOR_VIDEO_GALLERY_REQUEST_PERMISSION_CODE:
                pickVideoFromGallery();
                break;

            case CAMERA_FOR_QR_SCANNER_REQUEST_PERMISSION_CODE:
                initScanner();
                break;
        }
    }

    void setLocation() {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////  download branding assets ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d("StopitActivity", "Permission has been denied for request code " + requestCode);
    }

    void initScanner() {
    }

    void downloadBrandingAssets(final Branding branding) {
        final String bgUrl = branding.getBgImageUrl();
        final String logoUrl = branding.getLogoUrl(); //"http://www.swtest.org/wp-content/uploads/2017/12/swtw18-longlogo.png";

        final long orgId = DBHelper.getCurrentOrganizationId();
        final int downloadTasksCount = branding.getUrlsToDownloadCount();
        downloadAssetsCount.set(downloadTasksCount);
        if (!TextUtils.isEmpty(bgUrl)) {
            showApiInProgressView();
            Imager.downloadImage(getBaseContext(), bgUrl,
                    BrandingHelper.getPathForBackground(orgId), this);
        }
        if (!TextUtils.isEmpty(logoUrl)) {
            showApiInProgressView();
            Imager.downloadLogoImage(getBaseContext(), logoUrl,
                    BrandingHelper.getPathForLogo(orgId),
                    BrandingHelper.getPathForSmallLogo(orgId),
                    this);
        }
    }


    public void onDownloadFinished() {
        if (downloadAssetsCount.decrementAndGet() == 0) {
            hideApiInProgressView();
            onDownloadAssetsFinished();
            //EventBus.getDefault().post(new BrandingUpdatedEvent());
        }
    }

    void onDownloadAssetsFinished() {
        goToNextScreen();
    }

    void goToNextScreen() {
        BrandingHelper.init();
        final Organization currentOrg = DBHelper.getCurrentOrganization();
        if (currentOrg != null && SharedPrefsHelper.isTermsOfUseAccepted(this, currentOrg.getId())) {
            startMainActivity();
        } else
            startTermsOfUseActivity();
    }

    @Override
    protected void onResume() {
        IntentFilter messengerFilter = new IntentFilter();
        messengerFilter.addAction(SmackService.PUSH_NEW_MESSAGE);
        registerReceiver(messengerMessageReceiver, messengerFilter);

        IntentFilter inboxFilter = new IntentFilter();
        inboxFilter.addAction(Constants.INBOX_NOTIFICATION_BROADCAST_ID);
        registerReceiver(inboxMessageReceiver, inboxFilter);
        super.onResume();
    }

    void logScreenViewEvent(final String screenName) {
        if (TextUtils.isEmpty(screenName))
            return;

        mFirebaseAnalytics.logEvent(screenName, new Bundle());
        Log.d("Stopit Activity", "Firebase analytics event " + screenName);
    }

    String getScreenName() {
        return "";
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            unregisterReceiver(messengerMessageReceiver);
            unregisterReceiver(inboxMessageReceiver);
        } catch (Exception ex) {

        }
    }
}
