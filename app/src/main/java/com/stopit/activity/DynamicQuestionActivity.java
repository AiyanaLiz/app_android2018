package com.stopit.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.andexert.library.RippleView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.stopit.R;
import com.stopit.adapter.DynamicAnswersListAdapter;
import com.stopit.models.DynamicAnswer;
import com.stopit.models.DynamicQuestion;
import com.stopit.views.StopitOrgSelector;
import com.stopit.views.StopitTextView;
import com.stopit.views.StopitToolbar;
import com.stopit.views.brandable.RippleWithBackground;

import java.util.List;

import static com.stopit.app.Constants.DYNAMIC_QUESTION_TYPE_CHK_LIST_ID;
import static com.stopit.app.Constants.DYNAMIC_QUESTION_TYPE_DROPDOWN_ID;
import static com.stopit.app.Constants.DYNAMIC_QUESTION_TYPE_RADIO_ID;
import static com.stopit.app.Constants.DYNAMIC_QUESTION_TYPE_RATING_ID;
import static com.stopit.app.Constants.FBA_SCREEN_DYNAMIC_QUESTION_DETAILS;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_DYNAMIC_QUESTION;

public class DynamicQuestionActivity extends StopitActivity implements BaseQuickAdapter.OnItemClickListener, RippleView.OnRippleCompleteListener {

    private StopitToolbar toolbar;
    private StopitOrgSelector orgSelectorView;
    private StopitTextView headerTxt;
    private RecyclerView recyclerView;
    private RippleWithBackground clearRipple;
    private RippleWithBackground saveRipple;

    private StopitTextView saveBtn;
    private StopitTextView clearBtn;
    private DynamicQuestion question;
    private List<DynamicAnswer> answers;
    private String initialAnswersSelectionString;

    private DynamicAnswersListAdapter adapter;


    @Override
    int getContentView() {
        return R.layout.activity_dynamic_question;
    }

    protected boolean isIntentTransactionSuccessful(final Intent intent) {
        if (intent == null || !intent.hasExtra(ST_EXTRAS_KEY_DYNAMIC_QUESTION))
            return false;

        question = (DynamicQuestion) intent.getSerializableExtra(ST_EXTRAS_KEY_DYNAMIC_QUESTION);
        answers = question.getAnswers();
        if (answers.size() == 0) {
            return false;
        }

        initialAnswersSelectionString = question.getSelectionString();
        return true;
    }

    void initUI() {
        toolbar = initToolbar();
        orgSelectorView = initOrgSelectorView();
        headerTxt = findViewById(R.id.dynamic_question_header_txt);
        recyclerView = findViewById(R.id.dynamic_question_recycler_view);
        saveBtn = findViewById(R.id.activity_dynamic_question_save_btn);
        clearBtn = findViewById(R.id.activity_dynamic_question_clear_btn);
        clearRipple = findViewById(R.id.dynamic_clear_ripple);
        saveRipple = findViewById(R.id.dynamic_save_ripple);
    }

    @Override
    void setUI(Bundle savedInstanceState) {
        toolbar.setBackButton(this);
        orgSelectorView.setTitleActive(this);
        headerTxt.setText(question.getRequiredLabel(this));

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (question.getTypeId() == DYNAMIC_QUESTION_TYPE_CHK_LIST_ID)
            adapter = new DynamicAnswersListAdapter<>(answers);
        else
            adapter = new DynamicAnswersListAdapter<>(answers, true);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(this);

        clearRipple.setOnRippleCompleteListener(this);
        saveRipple.setOnRippleCompleteListener(this);

        manageSaveBtn(false);
        manageClearBtn();
    }

    @Override
    String getScreenName(){
        return FBA_SCREEN_DYNAMIC_QUESTION_DETAILS;
    }

    @Override
    public void onClick(View clickedView) {
        if (clickedView == clearBtn) {
            for (DynamicAnswer answer : answers) {
                answer.setSelected(false);
            }
        }

        final Intent intent = new Intent();
        intent.putExtra(ST_EXTRAS_KEY_DYNAMIC_QUESTION, question);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void manageSaveBtn() {
        manageSaveBtn(question.isAnyChanges(initialAnswersSelectionString));
    }

    private void manageSaveBtn(final boolean isEnabled) {
        saveRipple.setEnabled(isEnabled);
        saveRipple.setOnRippleCompleteListener(isEnabled ? this : null);
        saveBtn.setEnabled(isEnabled);
    }

    private void manageClearBtn() {
        manageClearBtn(question.isUserAnswer());
    }

    private void manageClearBtn(final boolean isEnabled) {
        clearRipple.setEnabled(isEnabled);
        clearRipple.setOnRippleCompleteListener(isEnabled? this : null);
        clearBtn.setEnabled(isEnabled);
    }


    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        final DynamicAnswer selectedAnswer = answers.get(position);
        if (selectedAnswer == null)
            return;

        switch (question.getTypeId()) {
            case DYNAMIC_QUESTION_TYPE_RADIO_ID:
            case DYNAMIC_QUESTION_TYPE_RATING_ID:
            case DYNAMIC_QUESTION_TYPE_DROPDOWN_ID:
                if (selectedAnswer.isSelected())
                    return;

                for (DynamicAnswer answer : answers) {
                    if (selectedAnswer.equals(answer))
                        selectedAnswer.setSelected(!selectedAnswer.isSelected());
                    else
                        answer.setSelected(false);
                }
                break;

            default:
                selectedAnswer.setSelected(!selectedAnswer.isSelected());
        }

        adapter.notifyDataSetChanged();
        manageSaveBtn();
        manageClearBtn();
    }

    @Override
    public void onComplete(RippleView rippleView) {
        if (rippleView == clearRipple) {
            for (DynamicAnswer answer : answers) {
                answer.setSelected(false);
            }
        }

        final Intent intent = new Intent();
        intent.putExtra(ST_EXTRAS_KEY_DYNAMIC_QUESTION, question);
        setResult(RESULT_OK, intent);
        finish();
    }
}
