package com.stopit.activity;

import java.io.File;

public interface OnUploadFinishedCallback {

    void onUploadFinished(final File file, final String filePath);
    void onUploadFailed();

}
