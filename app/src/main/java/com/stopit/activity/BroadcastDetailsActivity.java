package com.stopit.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.andexert.library.RippleView;
import com.stopit.R;
import com.stopit.app.Constants;
import com.stopit.db.DBHelper;
import com.stopit.models.BroadcastMessage;
import com.stopit.models.data_wrappers.BroadcastAttachmentWrapper;
import com.stopit.views.StopitOrgSelector;
import com.stopit.views.StopitTextView;
import com.stopit.views.StopitToolbar;
import com.stopit.views.brandable.RippleWithBackground;

import static com.stopit.app.Constants.FBA_SCREEN_BROADCAST_DETAILS;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_BROADCAST_MESSAGE_ID;

public class BroadcastDetailsActivity extends StopitActivity implements RippleView.OnRippleCompleteListener {
    private StopitToolbar toolbar;
    private StopitOrgSelector orgSelector;
    private BroadcastMessage message;
    private String messageId;

    private StopitTextView titleTxt;
    private StopitTextView dateTxt;
    private StopitTextView messageTxt;

    private ImageView mediaPreview;
    private StopitTextView mediaBtn;
    private RippleWithBackground mediaRipple;
    private BroadcastAttachmentWrapper attachment;

    private View.OnClickListener deleteBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showTwoBtnsAlertDialog("", getString(R.string.delete_msg_warning),
                    R.string.delete_msg_btn_label, R.string.button_dialog_cancel,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deleteBroadcastMessageById(messageId);
                        }
                    });
        }
    };

    @Override
    protected boolean isIntentTransactionSuccessful(Intent intent) {
        if (intent == null || !intent.hasExtra(ST_EXTRAS_KEY_BROADCAST_MESSAGE_ID))
            return false;

        messageId = intent.getStringExtra(ST_EXTRAS_KEY_BROADCAST_MESSAGE_ID);
        return !TextUtils.isEmpty(messageId);
    }

    @LayoutRes
    int getContentView() {
        return R.layout.activity_broadcast_details;
    }

    void initUI() {
        toolbar = initToolbar();
        orgSelector = initOrgSelectorView();

        titleTxt = findViewById(R.id.broadcast_details_title_txt);
        dateTxt = findViewById(R.id.broadcast_details_date_txt);
        messageTxt = findViewById(R.id.broadcast_details_message_txt);

        mediaPreview = findViewById(R.id.broadcast_details_thumbnail);
        mediaBtn = findViewById(R.id.broadcast_details_media_btn);
        mediaRipple = findViewById(R.id.broadcast_details_media_ripple);
    }

    @Override
    void initData() {
        requestBroadcastMessageById(messageId);
    }

    void setUI(final Bundle savedInstanceState) {
        orgSelector.setTitleActive(this);
        toolbar.setBackButton(this);
        toolbar.setRightButton(R.string.delete_msg_label, deleteBtnListener);
    }

    @Override
    String getScreenName(){
        return FBA_SCREEN_BROADCAST_DETAILS;
    }

    private void populateMessageDetails() {
        titleTxt.setText(message.getTitle());
        dateTxt.setText(message.getDateFormatted());
        messageTxt.setText(message.getMessage());
    }

    ///////////////////////////////////// API calls //////////////////////////////

    @Override
    void onRetrieveBroadcastMessageSuccess(final BroadcastMessage message) {
        if (message == null) {
            showRequestMessageFailureDialog(getString(R.string.get_inbox_msg_err));
            return;
        }
        this.message = message;
        populateMessageDetails();

        retrieveBroadcastMessageMediaUrl(messageId);
    }

    @Override
    void onRetrieveMessageURLSuccess(final BroadcastAttachmentWrapper attachment) {
        if (attachment == null || TextUtils.isEmpty(attachment.getUrl()))
            return;

        this.attachment = attachment;
        mediaRipple.setVisibility(View.VISIBLE);
        mediaRipple.setOnRippleCompleteListener(this);

        final int btnLabelRes;
        if (attachment.isImage())
            btnLabelRes = R.string.show_image_btn_label;
        else if (attachment.isVideo())
            btnLabelRes = R.string.show_video_btn_label;
        else
            btnLabelRes = R.string.open_url_btn_label;

        mediaBtn.setText(btnLabelRes);
    }

    @Override
    void onRetrieveMessageURLFailure(final String message) {
        //ignore
    }

    @Override
    void onRetrieveBroadcastMessageFailure(final String message) {
        showRequestMessageFailureDialog(message);
    }

    private void showRequestMessageFailureDialog(final String message) {
        showAlertDialog("", message, false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
    }

    @Override
    void onUpdateBroadcastMessageSuccess(final String messageId) {
        DBHelper.updateBroadcastMessageReadState(message, true);
        populateMessageDetails();
    }

    @Override
    void onUpdateBroadcastMessageFailure() {
        //ignore?
    }

    @Override
    void onDeleteBroadcastMessageSuccess(final String messageId) {
        DBHelper.deleteBroadcastMessage(messageId);
        showAlertDialog("", getString(R.string.delete_msg_success_label), false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
    }

    @Override
    void onDeleteBroadcastMessageFailure() {
        showAlertDialog("", getString(R.string.delete_msg_err_label));
    }

    @Override
    public void onComplete(RippleView rippleView) {
        if (rippleView == mediaRipple) {
            final Intent intent = new Intent(this, MediaViewerActivity.class);

            intent.putExtra(Constants.ST_EXTRAS_KEY_ATTACHMENT_TYPE, attachment.getMimeTypeString());
            intent.putExtra(Constants.ST_EXTRAS_KEY_ATTACHMENT_URL, attachment.getUrl());
            startActivity(intent);
        }
    }
}
