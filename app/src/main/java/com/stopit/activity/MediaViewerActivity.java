package com.stopit.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.VideoView;

import com.stopit.R;
import com.stopit.utils.Imager;
import com.stopit.views.StopitToolbar;

import static com.stopit.app.Constants.FBA_SCREEN_MEDIA_VIEWER;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_ATTACHMENT_TYPE;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_ATTACHMENT_URL;
import static com.stopit.app.Constants.VIDEO_MIME_TYPE;
import static com.stopit.app.Constants.WEB_PAGE_TYPE;

/**
 * Created by Alenka on 07.07.2018.
 */

public class MediaViewerActivity extends StopitActivity {

    private StopitToolbar toolbar;
    private ImageView mImageView;
    private VideoView mVideoView;
    private WebView mWebView;
    private LinearLayout videoContainer;
    private String mimeType;
    private String url;

    @LayoutRes
    int getContentView() {
        return R.layout.activity_photo_viewer;
    }

    protected boolean isIntentTransactionSuccessful(final Intent intent) {
        if (intent == null || !intent.hasExtra(ST_EXTRAS_KEY_ATTACHMENT_TYPE) || !intent.hasExtra(ST_EXTRAS_KEY_ATTACHMENT_URL))
            return false;

        mimeType = intent.getStringExtra(ST_EXTRAS_KEY_ATTACHMENT_TYPE);
        url = intent.getStringExtra(ST_EXTRAS_KEY_ATTACHMENT_URL);
        return !TextUtils.isEmpty(mimeType) && !TextUtils.isEmpty(url);
    }

    void initUI() {
        toolbar = initToolbar();
        mImageView = findViewById(R.id.image_viewer);
        mVideoView = findViewById(R.id.video_viewer);
        mWebView = findViewById(R.id.web_viewer);
        videoContainer = findViewById(R.id.video_container);
    }


    void setUI(final Bundle savedInstanceState) {
        toolbar.setBackButton(this);

        switch (mimeType) {
            case VIDEO_MIME_TYPE:
                loadVideo();
                break;

            case WEB_PAGE_TYPE:
                loadWebUrl();
                break;

            default:
                loadImage();
        }
    }

    @Override
    String getScreenName(){
        return FBA_SCREEN_MEDIA_VIEWER;
    }

    private void loadImage() {
        mImageView.setVisibility(View.VISIBLE);
        Imager.loadImageFromFile(this, url, mImageView);
    }

    private void loadVideo() {
        videoContainer.setVisibility(View.VISIBLE);
        Uri videoURI = Uri.parse(url);

        MediaController mc = new MediaController(this);
        mc.setAnchorView(mVideoView);

        mVideoView.setMediaController(mc);
        mVideoView.setVideoURI(videoURI);
        mVideoView.requestFocus();
        mVideoView.start();
    }

    private void loadWebUrl() {
        final View progressBar = findViewById(R.id.web_progress_view);
        mWebView.setVisibility(View.VISIBLE);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
            }

            // This method will be triggered when the Page loading is completed

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
            }

            // This method will be triggered when error page appear

            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                progressBar.setVisibility(View.GONE);
                super.onReceivedError(view, errorCode, description, failingUrl);
                showAlertDialog("", getString(R.string.unknown_err_msg), false, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            }
        });
        mWebView.loadUrl(url);
    }

    @Override
    public void onPause() {
        if (mWebView != null && mWebView.getVisibility() == View.VISIBLE) {
            mWebView.onPause();
            mWebView.pauseTimers();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mWebView != null && mWebView.getVisibility() == View.VISIBLE) {
            mWebView.resumeTimers();
            mWebView.onResume();
        }
    }


    @Override
    protected void onDestroy() {
        if (mWebView != null && mWebView.getVisibility() == View.VISIBLE) {
            mWebView.destroy();
            mWebView = null;
        }
        super.onDestroy();
    }
}
