package com.stopit.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.CompoundButton;

import com.andexert.library.RippleView;
import com.stopit.R;
import com.stopit.db.DBHelper;
import com.stopit.models.Organization;
import com.stopit.utils.BrandingHelper;
import com.stopit.views.StopitBackground;
import com.stopit.views.StopitBigLogo;
import com.stopit.views.StopitTextView;
import com.stopit.views.brandable.RippleWithBackground;

import static com.stopit.app.Constants.FBA_SCREEN_TERMS_OF_USE;
import static com.stopit.app.Constants.ST_DELIMITER;

/**
 * Created by Alenka on 07.07.2018.
 */

public class TermsOfUseActivity extends StopitActivity implements CompoundButton.OnCheckedChangeListener,
        RippleView.OnRippleCompleteListener {

    private StopitTextView orgTitleTxt;
    private AppCompatCheckBox termsOfUseChk;
    private StopitTextView termsOfUseTxt;
    private StopitTextView nextBtn;
    private RippleWithBackground nextBtnRipple;
    private RippleView backRipple;
    private Organization currentOrganization;


    @LayoutRes
    int getContentView() {
        return R.layout.activity_terms_of_use;
    }


    void initUI() {
        orgTitleTxt = findViewById(R.id.terms_org_title_txt);
        termsOfUseChk = findViewById(R.id.terms_of_use_chk);
        termsOfUseTxt = findViewById(R.id.terms_privacy_policy_txt);
        nextBtn = findViewById(R.id.terms_start_btn);
        nextBtnRipple = findViewById(R.id.terms_go_btn_ripple);
        backRipple = findViewById(R.id.terms_back_btn_ripple);
    }

    void initData() {
        currentOrganization = DBHelper.getCurrentOrganization();
    }

    void setUI(final Bundle savedInstanceState) {
        termsOfUseChk.setOnCheckedChangeListener(this);

        if (currentOrganization != null)
            orgTitleTxt.setText(currentOrganization.getName());

        String source = getString(R.string.terms_label);
        final int spanStart = source.indexOf(ST_DELIMITER);
        final int spanEnd = source.lastIndexOf(ST_DELIMITER) - 1;
        source = source.replaceAll(ST_DELIMITER, "");

        SpannableString spannable = new SpannableString(source);
        spannable.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TermsOfUseActivity.this, TermsOfUseDetailsActivity.class));
            }
        }, spanStart, spanEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new ForegroundColorSpan(Color.WHITE), spanStart, spanEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        termsOfUseTxt.setText(spannable);
        termsOfUseTxt.setMovementMethod(LinkMovementMethod.getInstance());

        setNextBtnEnabled(false);
        backRipple.setOnRippleCompleteListener(this);
        applyBranding();
    }

    @Override
    void setAnalyticsProperties() {
    }

    @Override
    String getScreenName() {
        return FBA_SCREEN_TERMS_OF_USE;
    }

    private void applyBranding() {
        if (!BrandingHelper.isBrandingColor())
            return;

        BrandingHelper.setCheckBoxBtnColor(termsOfUseChk);
        BrandingHelper.setTextColor(nextBtn);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        setNextBtnEnabled(isChecked);
    }

    private void setNextBtnEnabled(final boolean isEnabled) {
        nextBtn.setEnabled(isEnabled);
        nextBtnRipple.setEnabled(isEnabled);
        nextBtnRipple.setOnRippleCompleteListener(isEnabled ? this : null);
    }

    @Override
    public void onComplete(RippleView rippleView) {
        if (rippleView == nextBtnRipple) {
            acceptTermsOfUse();

        } else if (rippleView == backRipple) {
            requestLogout();
            //TODO check
            //TODO check
            //TODO check
            //TODO check
            //clearUserData();
            onBackPressed();
        }
    }

    @Override
    public void onLogoutSuccess() {
        clearCurrentOrgInfo();
        TermsOfUseActivity.this.onBackPressed();
    }

    void onLogout() {
        DBHelper.onLogout();
    }

    @Override
    void onLogoutFailure() {
        clearCurrentOrgInfo();
        TermsOfUseActivity.this.onBackPressed();
    }
}
