package com.stopit.activity;

/**
 * Created by Alenka on 27.06.2018.
 */

public interface OrgTitleClickListener {
    void onTitleClicked();
    void changeOrganization(final long organizationId);
}
