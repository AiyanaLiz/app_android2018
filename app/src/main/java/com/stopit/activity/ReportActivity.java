package com.stopit.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.andexert.library.RippleView;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;
import com.stopit.R;
import com.stopit.db.DBHelper;
import com.stopit.models.Branding;
import com.stopit.models.DynamicAnswer;
import com.stopit.models.DynamicQuestion;
import com.stopit.models.DynamicQuestionResult;
import com.stopit.models.Incident;
import com.stopit.models.IncidentGPSCoords;
import com.stopit.models.MediaItem;
import com.stopit.models.Organization;
import com.stopit.utils.BrandingHelper;
import com.stopit.utils.EmailHelper;
import com.stopit.utils.StopitPermissionsHelper;
import com.stopit.views.MediaPreviewLayout;
import com.stopit.views.StopitOrgSelector;
import com.stopit.views.StopitTextView;
import com.stopit.views.StopitToolbar;
import com.stopit.views.dynamic_questions_views.CheckboxQuestionView;
import com.stopit.views.dynamic_questions_views.DateQuestionView;
import com.stopit.views.dynamic_questions_views.EmailQuestionView;
import com.stopit.views.dynamic_questions_views.QuestionView;
import com.stopit.views.dynamic_questions_views.TextQuestionView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static com.stopit.app.Constants.DYNAMIC_QUESTION_ACTIVITY_REQUEST_CODE;
import static com.stopit.app.Constants.DYNAMIC_QUESTION_TYPE_CHECKBOX_ID;
import static com.stopit.app.Constants.DYNAMIC_QUESTION_TYPE_DATE_ID;
import static com.stopit.app.Constants.DYNAMIC_QUESTION_TYPE_EMAIL_ID;
import static com.stopit.app.Constants.DYNAMIC_QUESTION_TYPE_TEXTBOX_ID;
import static com.stopit.app.Constants.FBA_SCREEN_REPORT;
import static com.stopit.app.Constants.GALLERY_PHOTO_PICK_REQUEST_CODE;
import static com.stopit.app.Constants.GALLERY_VIDEO_PICK_REQUEST_CODE;
import static com.stopit.app.Constants.MAX_TOTAL_ATTACHMENTS_SIZE;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_DYNAMIC_QUESTION;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_INCIDENT_ID;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_INCIDENT_PASSWORD;
import static com.stopit.app.Constants.TAKE_PHOTO_REQUEST_CODE;
import static com.stopit.app.Constants.VIDEO_RECORD_REQUEST_CODE;

public class ReportActivity extends StopitActivity implements QuestionView.OnAnswerChangedListener {

    final String CALENDAR_DIALOG_TAG = "REPORT_ACTIVITY_CALENDAR_DIALOG";
    private StopitToolbar toolbar;
    private StopitOrgSelector orgSelector;
    private EditText incidentMsgTxt;
    private MediaPreviewLayout photoLayout;
    private LinearLayout questionsContainer;
    private RippleView submitRipple;
    private List<DynamicQuestion> dynamicQuestions;
    private List<MediaItem> mediaItems = new ArrayList<>();
    private StopitTextView disclaimerTxt;
    private Incident incident;
    private SimpleDateFormat newIncidentDateFormat = new SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault());


    //dynamic question view click listener (radiobtn, chklist, dropdown)
    private View.OnClickListener questionClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final int position = ((QuestionView) view).getQuestionPosition();
            final DynamicQuestion question = dynamicQuestions.get(position);

            final Intent intent = new Intent(ReportActivity.this, DynamicQuestionActivity.class);
            intent.putExtra(ST_EXTRAS_KEY_DYNAMIC_QUESTION, question);
            startActivityForResult(intent, DYNAMIC_QUESTION_ACTIVITY_REQUEST_CODE);
        }
    };
    //dynamic date question view click listener
    private View.OnClickListener dateQuestionClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final int questionPosition = ((QuestionView) view).getQuestionPosition();
            showDatePicker(questionPosition);
        }
    };
    //dynamic single chk question view click listener
    private View.OnClickListener chkQuestionClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final int questionPosition = ((QuestionView) view).getQuestionPosition();
            final DynamicQuestion question = dynamicQuestions.get(questionPosition);
            final boolean newAnswer = !question.getChkValue();
            question.setChkValue(newAnswer);
            ((CheckboxQuestionView) view).setChkAnswer(newAnswer);
        }
    };
    //photo layout add btn click listener
    private View.OnClickListener mediaClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //check if media allowed
            if (DBHelper.isMediaUploadAllowed())
                showOptionsPopup(null);
            else
                showAlertDialog("", getString(R.string.media_not_allowed_err));
        }
    };

    @LayoutRes
    int getContentView() {
        return R.layout.activity_report;
    }

    void initUI() {
        toolbar = initToolbar();
        orgSelector = initOrgSelectorView();

        incidentMsgTxt = findViewById(R.id.send_report_msg_edt);
        photoLayout = findViewById(R.id.send_report_media_layout);
        questionsContainer = findViewById(R.id.dynamic_questions_container);
        submitRipple = findViewById(R.id.report_submit_ripple);
        disclaimerTxt = findViewById(R.id.report_disclaimer_txt);
    }

    @Override
    void initData() {
        incident = new Incident();
        if (isInternetConnection())
            requestDynamicQuestions();
        else
            onRequestDynamicQuestionsFailure(getString(R.string.no_network_err));

    }

    void onRequestDynamicQuestionsFailure(final String message) {
        showAlertDialog("", message, false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
    }

    //////////////////// DYNAMIC QUESTIONS //////////////////////////////

    void setUI(final Bundle savedInstanceState) {
        toolbar.setBackButton(this);
        orgSelector.setTitleActive(this);
        photoLayout.init(mediaItems, mediaClickListener);
        submitRipple.setOnClickListener(this);
        if (DBHelper.isGPSEnabled()) {
            setLocation();
        }

        final Branding currentBranding = BrandingHelper.getBranding();
        if(currentBranding != null && !TextUtils.isEmpty(currentBranding.getDisclaimerText())){
            disclaimerTxt.setText(currentBranding.getDisclaimerText());
        }
    }

    void onRequestDynamicQuestionsSuccess(final List<DynamicQuestion> questions) {
        dynamicQuestions = questions;
        if (dynamicQuestions == null || dynamicQuestions.size() == 0)
            questionsContainer.setVisibility(View.GONE);
        else
            populateDynamicQuestions();
    }

    private void populateDynamicQuestions() {
        populateQuestionsContainer();
    }

    private void populateQuestionsContainer() {
        questionsContainer.setVisibility(View.VISIBLE);
        Collections.sort(dynamicQuestions);
        final int questionsCount = dynamicQuestions.size();
        for (int position = 0; position < questionsCount; position++) {
            questionsContainer.addView(getQuestionView(position));
        }
    }

    private View getQuestionView(final int questionPosition) {
        final DynamicQuestion question = dynamicQuestions.get(questionPosition);
        final QuestionView questionView;
        switch (question.getTypeId()) {
            //hardcoded values for question types
            //textbox
            case DYNAMIC_QUESTION_TYPE_TEXTBOX_ID:
                questionView = new TextQuestionView(this);
                questionView.setQuestionHint(question.getRequiredLabel(this));
                questionView.setOnAnswerChangedListener(this);
                break;
            //email
            case DYNAMIC_QUESTION_TYPE_EMAIL_ID:
                questionView = new EmailQuestionView(this);
                questionView.setQuestionHint(question.getRequiredLabel(this));
                questionView.setOnAnswerChangedListener(this);
                break;
            //date
            case DYNAMIC_QUESTION_TYPE_DATE_ID:
                questionView = new DateQuestionView(this);
                questionView.setOnClickListener(dateQuestionClickListener);
                break;

            case DYNAMIC_QUESTION_TYPE_CHECKBOX_ID:
                questionView = new CheckboxQuestionView(this);
                question.setChkValue(false);
                questionView.setOnClickListener(chkQuestionClickListener);
                break;

            //radiobtn, checkbox list, etc
            default:
                questionView = new QuestionView(this);
                questionView.setOnClickListener(questionClickListener);
        }

        questionView.setQuestionPosition(questionPosition);
        questionView.setQuestionLabel(question.getRequiredLabel(this));
        return questionView;
    }

    @Override
    String getScreenName() {
        return FBA_SCREEN_REPORT;
    }

    //////////////////// DYNAMIC QUESTIONS //////////////////////////////

    private void showDatePicker(final int position) {
        final Locale defLocale = Locale.getDefault();
        final Calendar calendar = Calendar.getInstance(defLocale);
        final int currentYear = calendar.get(Calendar.YEAR);
        final int currentMonth = calendar.get(Calendar.MONTH);
        final int currentDay = calendar.get(Calendar.DAY_OF_MONTH);

        CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                .setOnDateSetListener(new CalendarDatePickerDialogFragment.OnDateSetListener() {
                    @Override
                    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
                        setDateAnswer(position, year, monthOfYear, dayOfMonth);
                    }
                })
                .setFirstDayOfWeek(Calendar.SUNDAY)
                .setPreselectedDate(currentYear, currentMonth, currentDay)
                .setDateRange(null, new MonthAdapter.CalendarDay(currentYear, currentMonth, currentDay))
                .setDoneText(getString(R.string.button_dialog_ok))
                .setCancelText(getString(R.string.button_dialog_cancel));

        if (!BrandingHelper.isBrandingColor()) {
            cdp.setThemeCustom(R.style.BetterPickersDialogDefaultStyle);
        } else
            cdp.setThemeCustom(R.style.BetterPickersDialogStyle);

        cdp.show(getSupportFragmentManager(), CALENDAR_DIALOG_TAG);
    }

    //set date selection result
    private void setDateAnswer(final int questionPosition, final int year, final int monthOfYear, final int dayOfMonth) {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        //setting questions dateAnswer value
        final DynamicQuestion question = dynamicQuestions.get(questionPosition);
        if (question == null)
            return;

        final String dateString = newIncidentDateFormat.format(calendar.getTimeInMillis());
        question.setTextAnswer(dateString);

        final QuestionView childView = (QuestionView) questionsContainer.getChildAt(questionPosition);
        if (childView instanceof DateQuestionView) {
            childView.setAnswer(dateString);
        }
    }

    @Override
    public void manageAnswerChanged(int questionPosition, String userInput) {
        final DynamicQuestion question = dynamicQuestions.get(questionPosition);
        question.setTextAnswer(userInput);
    }

    @Override
    public void onClick(View clickedView) {
        if (clickedView == submitRipple) {
            hideKeyboardIfShown();
            prepareToSendIncident();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;

        // dynamic question answer received
        if (requestCode == DYNAMIC_QUESTION_ACTIVITY_REQUEST_CODE && data != null) {
            final DynamicQuestion doneQuestion = (DynamicQuestion) data.getSerializableExtra(ST_EXTRAS_KEY_DYNAMIC_QUESTION);
            if (doneQuestion == null || !dynamicQuestions.contains(doneQuestion))
                return;

            final int questionPosition = dynamicQuestions.indexOf(doneQuestion);
            dynamicQuestions.set(questionPosition, doneQuestion);
            final QuestionView viewToUpdate = (QuestionView) questionsContainer.getChildAt(questionPosition);
            if (viewToUpdate == null)
                return;

            final StringBuilder answerString = new StringBuilder("");
            final List<DynamicAnswer> answers = doneQuestion.getAnswers();
            for (DynamicAnswer answer : answers) {
                if (!answer.isSelected())
                    continue;

                if (answerString.length() > 0)
                    answerString.append(", ");

                answerString.append(answer.getLabel());
            }
            final String result = answerString.toString();
            viewToUpdate.setAnswer(result);

        } else if (requestCode == TAKE_PHOTO_REQUEST_CODE && !TextUtils.isEmpty(currentPhotoPath)) {
            addMediaItem(currentPhotoPath, true);

        } else if ((requestCode == GALLERY_PHOTO_PICK_REQUEST_CODE
                || requestCode == VIDEO_RECORD_REQUEST_CODE
                || requestCode == GALLERY_VIDEO_PICK_REQUEST_CODE) && data != null) {
            final Uri uri = data.getData();
            if (uri != null) {
                addMediaItem(uri, requestCode == VIDEO_RECORD_REQUEST_CODE);
            }
        }
    }

    /////////////////////////  photo handling ///////////////////////////

    private void prepareToSendIncident() {
        if (!validateIncident())
            return;

        incident.setReportMessage(incidentMsgTxt.getText().toString().trim());
        incident.setOrgId(DBHelper.getCurrentOrganizationId());
        populateQuestionsResults();

        if (checkInternetConnectionAndInform())
            submitIncident(incident);
    }


    //////////////////////// incident validation ///////////////////

    private boolean validateIncident() {
        final String message = incidentMsgTxt.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            showAlertDialog("", getString(R.string.report_txt_empty_err));
            return false;
        }

        final int addedMediaCount = mediaItems.size();
        if (addedMediaCount > 0 && !checkFileSize()) {
            showAlertDialog("", getString(R.string.max_attachment_size_exceeded_err));
            return false;

        } else if (!isRequiredDynamicAnswersOk()) {
            showAlertDialog("", getString(R.string.report_required_questions_error));
            return false;
        }

        //email questions validation
        for (DynamicQuestion question : dynamicQuestions) {
            //check if valid email adress
            if (question.getTypeId() == DYNAMIC_QUESTION_TYPE_EMAIL_ID) {
                final String emailValue = question.getTextAnswer();
                if (!TextUtils.isEmpty(emailValue) && !EmailHelper.isValidEmail(question.getTextAnswer())) {
                    showAlertDialog("", getString(R.string.report_invalid_email_error));
                    return false;
                }
            }
        }

        return true;
    }

    private boolean isRequiredDynamicAnswersOk() {
        boolean isAnswersOk = true;
        for (DynamicQuestion question : dynamicQuestions) {
            if (question.isRequired() && !question.isUserAnswer()) {
                isAnswersOk = false;
                break;
            }
        }
        return isAnswersOk;
    }

    private void populateQuestionsResults() {
        if (dynamicQuestions == null || dynamicQuestions.size() == 0)
            return;

        final List<DynamicQuestionResult> questionResults = new ArrayList<>();
        for (DynamicQuestion question : dynamicQuestions) {
            switch (question.getTypeId()) {
                case DYNAMIC_QUESTION_TYPE_EMAIL_ID:
                case DYNAMIC_QUESTION_TYPE_TEXTBOX_ID:
                case DYNAMIC_QUESTION_TYPE_DATE_ID:
                    if (TextUtils.isEmpty(question.getTextAnswer()))
                        break;

                    questionResults.add(new DynamicQuestionResult(question.getId(), question.getLabel(), 0, question.getTextAnswer()));
                    break;

                case DYNAMIC_QUESTION_TYPE_CHECKBOX_ID:
                    //if not required & there is no user answer should skip this question
                    final boolean chkValue = question.getChkValue();
                    questionResults.add(new DynamicQuestionResult(question.getId(), question.getLabel(), 0, chkValue ? getString(R.string.YES) : getString(R.string.NO)));
                    break;

                default:
                    final List<DynamicAnswer> answers = question.getAnswers();
                    for (DynamicAnswer answer : answers) {
                        if (answer.isSelected()) {
                            questionResults.add(new DynamicQuestionResult(question.getId(), question.getLabel(), answer.getId(), answer.getLabel()));

                        }
                    }
            }
        }
        incident.setQuestionsResults(questionResults);
    }


    @Override
    void onSubmitIncidentSuccess(final Incident incidentWrapper, final String afterHoursMessage) {
        incident = incidentWrapper;
        DBHelper.save(incident);

        if (mediaItems.size() > 0) {
            if (isInternetConnection()) {
                updateIncident(incident.getId(), mediaItems);
            } else {
                showAlertDialog("", getString(R.string.media_upload_err), false, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startResultActivity(incident);
                    }
                });
            }
        } else {
            if (!TextUtils.isEmpty(afterHoursMessage)) {
                showAlertDialog("", afterHoursMessage, false, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startResultActivity(incident);
                    }
                });
            } else
                startResultActivity(incident);
        }
    }

    @Override
    void onSubmitIncidentFailure() {
        handleSubmitOrUpdateFailure(R.string.incident_submit_err);
    }

    @Override
    void onUpdateIncidentSuccess(final Incident incident) {
        DBHelper.save(incident);
        showAlertDialog("", getString(R.string.incident_submit_success), false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startResultActivity(incident);
            }
        });
    }

    @Override
    void onUpdateIncidentFailure(final String errorMessage) {
        handleSubmitOrUpdateFailure(R.string.incident_update_err);
    }

    private void handleSubmitOrUpdateFailure(final int errorRes) {
        showAlertDialog("", getString(errorRes), false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startResultActivity(incident);
            }
        });
    }

    private void startResultActivity(final Incident incident) {
        final Intent intent = new Intent(this, SubmitIncidentResultActivity.class);
        intent.putExtra(ST_EXTRAS_KEY_INCIDENT_ID, incident.getId());
        intent.putExtra(ST_EXTRAS_KEY_INCIDENT_PASSWORD, incident.getRetrievalCode());
        startActivity(intent);
        finish();
    }

    @Override
    void addMediaItem(final String path, final boolean shouldBeDeleted) {
        File file = new File(path);
        if (file.exists() && file.canRead()) {
            final MediaItem item = new MediaItem(path, shouldBeDeleted);
            mediaItems.add(item);
            photoLayout.addPhotoView(this, item);
        }
    }

    private boolean checkFileSize() {
        if (mediaItems.size() == 0)
            return true;

        int totalLength = 0;
        for (MediaItem item : mediaItems) {
            final File file = new File(item.getPath());
            if (file.exists() && file.canRead()) {
                totalLength += file.length();
                //Log.d("ReportActivity", ">>>>>>>> total" + totalLength);
            }
        }

        return totalLength <= MAX_TOTAL_ATTACHMENTS_SIZE;
    }

    @Override
    protected void onDestroy() {
        clearTempMediaItems();
        super.onDestroy();
    }

    private void clearTempMediaItems() {
        if (mediaItems == null || mediaItems.size() == 0)
            return;

        for (MediaItem items : mediaItems) {
            if (items.isShouldBeDeleted() && !TextUtils.isEmpty(items.getPath())) {
                final File file = new File(items.getPath());
                if (file.exists() && file.canWrite()) {
                    boolean isDeleted = file.delete();
                    Log.d("ReportActivity", "deleted file " + isDeleted + " path " + items.getPath());
                }
            }
        }
    }

    public void setLocation() {
        Location location = null;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            StopitPermissionsHelper.requestLocationPermission(this);
            return;
        }

        final LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager == null)
            return;

        // getting GPS status
        boolean isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        // getting network status
        boolean isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled && !isNetworkEnabled)
            // no network provider is enabled
            return;

        // First get location from Network Provider
        if (isNetworkEnabled) {
//                    locationManager.requestLocationUpdates(
//                            LocationManager.NETWORK_PROVIDER,
//                            MIN_TIME_BW_UPDATES,
//                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
            Log.d("Network", "Network");
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//                if (location != null) {
//                    latitude = location.getLatitude();
//                    longitude = location.getLongitude();
//                }
        }

        if (location != null) {
            incident.setGpsCoords(new IncidentGPSCoords(location.getLongitude(), location.getLatitude()));
            return;
        }

        // if GPS Enabled get lat/long using GPS Services
        if (isGPSEnabled) {
            //                    locationManager.requestLocationUpdates(
//                            LocationManager.GPS_PROVIDER,
//                            MIN_TIME_BW_UPDATES,
//                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
            Log.d("GPS Enabled", "GPS Enabled");
            location = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                incident.setGpsCoords(new IncidentGPSCoords(location.getLongitude(), location.getLatitude()));
            }
        }
    }

    public void checkLocationPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            StopitPermissionsHelper.requestLocationPermission(this);
        } else {
            setLocation();
        }
    }
}
