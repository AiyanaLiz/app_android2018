package com.stopit.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.zxing.integration.android.IntentIntegrator;
import com.stopit.R;
import com.stopit.db.DBHelper;
import com.stopit.models.Branding;
import com.stopit.models.Organization;
import com.stopit.models.OrgsTreeEntity;
import com.stopit.models.User;
import com.stopit.models.data_wrappers.UserData;
import com.stopit.utils.BrandingHelper;
import com.stopit.utils.CameraHelper;
import com.stopit.utils.SharedPrefsHelper;
import com.stopit.utils.StopitPermissionsHelper;
import com.stopit.views.StopitEditText;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import static com.stopit.app.Constants.ACCESS_CODE_MAX_LENGTH;
import static com.stopit.app.Constants.ACCESS_CODE_MIN_LENGTH;
import static com.stopit.app.Constants.CAMERA_FOR_QR_SCANNER_REQUEST_PERMISSION_CODE;
import static com.stopit.app.Constants.FBA_SCREEN_START;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_IS_CLEAR_TASK;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_ORGANIZATION_ID;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_SCAN_RESULT;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_SCAN_RESULT_FORMAT;

public class StartActivity extends StopitActivity implements TextView.OnEditorActionListener, RippleView.OnRippleCompleteListener {

    private StopitEditText accessCodeEdt;
    private RippleView qrRipple;
    private RippleView backBtnRipple;
    private boolean isBackBtnAvailable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Intent intent = getIntent();
        if (!isIntentTransactionSuccessful(intent)) {
            android.util.Log.e(getClass().getSimpleName(), "Wrong or missed extras");
            finish();
            return;
        }

        final EventBus eventBus = EventBus.getDefault();
        if (!eventBus.isRegistered(this)) eventBus.register(this);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        //setting an organization name and id property for analytics instance
        setAnalyticsProperties();

        setContentView(getContentView());
        initUI();
        initData();
        setUI(savedInstanceState);
    }

    @Override
    protected boolean isIntentTransactionSuccessful(Intent intent) {
        if (intent != null)
            isBackBtnAvailable = !intent.getBooleanExtra(ST_EXTRAS_KEY_IS_CLEAR_TASK, false);

        return true;
    }


    @LayoutRes
    int getContentView() {
        return R.layout.activity_start;
    }

    void initUI() {
        accessCodeEdt = findViewById(R.id.start_access_code_edt);
        backBtnRipple = findViewById(R.id.start_back_btn_ripple);
        qrRipple = findViewById(R.id.start_barcode_btn_ripple);
    }

    void initData() {
    }

    void setUI(final Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.containsKey(ST_EXTRAS_KEY_ORGANIZATION_ID)) {
            //Toast.makeText(this, "Toast ", Toast.LENGTH_SHORT).show();
        }

        /*if(isBackBtnAvailable){
            backBtnRipple.setVisibility(View.VISIBLE);
            backBtnRipple.setOnRippleCompleteListener(this);
        }
*/
        accessCodeEdt.setOnEditorActionListener(this);
        accessCodeEdt.addTextChangedListener(this);
        qrRipple.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {

            @Override
            public void onComplete(RippleView rippleView) {
                if (!CameraHelper.hasCamera()) {
                } else if (Build.VERSION.SDK_INT >= 23) {
                    if (StopitPermissionsHelper.isCameraPermitted(StartActivity.this)) {
                        initScanner();
                    } else {
                        StopitPermissionsHelper.requestCameraPermission(StartActivity.this, CAMERA_FOR_QR_SCANNER_REQUEST_PERMISSION_CODE);
                    }
                } else {
                    initScanner();
                }
            }
        });

        doBranding();
    }

    private void doBranding() {
        if (!BrandingHelper.isBrandingColor())
            return;

        BrandingHelper.setTextColor(accessCodeEdt);
    }

//    @Override
//    void setAnalyticsProperties() {
//    }

    @Override
    String getScreenName() {
        return FBA_SCREEN_START;
    }


    void initScanner() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
        integrator.setCaptureActivity(VerticalScannerActivity.class);
        integrator.setOrientationLocked(false);
        integrator.setPrompt(getString(R.string.qr_code_scanner_info));
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK || data == null)
            return;

        if (requestCode == IntentIntegrator.REQUEST_CODE) {
            String scanResult = data.getStringExtra(ST_EXTRAS_KEY_SCAN_RESULT);
            String format = data.getStringExtra(ST_EXTRAS_KEY_SCAN_RESULT_FORMAT);
            Log.d("MainActivity", "Barcode Result: " + scanResult);
            Log.d("MainActivity", "Barcode Format: " + format);

            if (TextUtils.isEmpty(scanResult)) {
                showAlertDialog("", getString(R.string.no_qr_scan_result_msg));
                return;
            }

            prepareToLogin(scanResult.trim());
        }
    }

    ////////////////////////////   access code edt handling  //////////////////////////////////

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() > 0) {
            // position the text type in the left top corner
            accessCodeEdt.setGravity(Gravity.LEFT | Gravity.TOP);
        } else {
            // no text entered. Center the hint text.
            accessCodeEdt.setGravity(Gravity.CENTER);
        }
    }

    @Override
    public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_GO) {
            final Editable accessCode = accessCodeEdt.getText();
            if (accessCode == null)
                return true;

            prepareToLogin(accessCode.toString().trim());
            return true;
        }
        // Return true if you have consumed the action, else false.
        return false;
    }

    ////////////////////////// LOGIN /////////////////////////////////////

    private void prepareToLogin(final String accessCode) {
        if (!isAccessCodeValid(accessCode))
            return;

        //check for org id for access code
        final long savedOrgId = SharedPrefsHelper.getOrgIdByAccessCode(this, accessCode);
        if (savedOrgId == 0) {
            performLogin(accessCode);
            return;
        }

        //check for historical organization with id
        final Organization historicalOrg = DBHelper.getOrganizationById(savedOrgId);
        if (historicalOrg != null && !TextUtils.isEmpty(historicalOrg.getIntellicode()))
            requestOrganizationChange(historicalOrg.getIntellicode());
        else
            performLogin(accessCode);
    }

    @Override
    void onOrganizationChangeFailure(final String message) {
        final String accessCode = accessCodeEdt.getText().toString().trim();
        if (!TextUtils.isEmpty(accessCode))
            performLogin(accessCode);
        else
            showAlertDialog("", getString(R.string.unknown_err_msg));
    }


    @Override
    void onLoginSuccess(final UserData userData, final String accessCode) {
        if (userData == null) {
            showAlertDialog("", getString(R.string.unknown_err_msg));
            return;
        }

        final User user = userData.getUser();
        final List<OrgsTreeEntity> organizationsTree = userData.getOrganizations();
        if (user == null || TextUtils.isEmpty(user.getIntelliCode())) {
            if (organizationsTree != null && organizationsTree.size() > 0) {
                DBHelper.save(organizationsTree);

                final long parentOrgId = userData.getParentOrgId();
                final String parentOrgName = userData.getParentOrgName();

                if (parentOrgId == 0) {
                    showAlertDialog("", getString(R.string.unknown_err_msg));
                    return;
                }

                startOrganizationSearchActivity(parentOrgId, parentOrgName, true);

            } else {
                showAlertDialog("", getString(R.string.unknown_err_msg));
                return;
            }
        } else {
            final Organization organization = userData.getOrganization();
            if (organization == null) {
                showAlertDialog("", getString(R.string.unknown_err_msg));
                return;
            }
            if (!TextUtils.isEmpty(accessCode)) {
                SharedPrefsHelper.saveAccessCodeForOrganization(this, accessCode, organization.getId());
            }

            BrandingHelper.clearBranding(true);
            //TODO check
            //TODO check
            //TODO check
            DBHelper.saveAllUserData(userData);
            //setUserData(userData);
            //TODO check
            //TODO check
            //TODO check

            final Branding branding = DBHelper.getBranding();
            //if no branding to download
            if (branding == null || !branding.isUrlsToDownload()) {
                //skip, go to the next screen
                startTermsOfUseActivity();

            } else {
                downloadBrandingAssets(branding);
            }
        }
    }


    private boolean isAccessCodeValid(final String accessCode) {
        if (TextUtils.isEmpty(accessCode)) {
            showAlertDialog("", getString(R.string.empty_access_code));
            accessCodeEdt.requestFocus();
            return false;
        }

        final int accessCodeLength = accessCode.trim().length();
        if (accessCodeLength < ACCESS_CODE_MIN_LENGTH || accessCodeLength > ACCESS_CODE_MAX_LENGTH) {
            showAlertDialog("", getString(R.string.invalid_access_code));
            accessCodeEdt.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onComplete(RippleView rippleView) {
        onBackPressed();
    }
}
