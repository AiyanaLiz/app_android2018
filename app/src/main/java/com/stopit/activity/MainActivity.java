package com.stopit.activity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.stopit.R;
import com.stopit.db.DBHelper;
import com.stopit.event.InboxMessageEvent;
import com.stopit.event.RightButtonVisibleEvent;
import com.stopit.event.UnreadChatMessageEvent;
import com.stopit.event.UnreadInboxMessageEvent;
import com.stopit.fragment.tabs.HelpFragment;
import com.stopit.fragment.tabs.HomeFragment;
import com.stopit.fragment.tabs.IncidentsFragment;
import com.stopit.fragment.tabs.MoreFragment;
import com.stopit.models.BroadcastMessage;
import com.stopit.models.Incident;
import com.stopit.models.InformationResource;
import com.stopit.models.MediaItem;
import com.stopit.models.data_wrappers.UnreadInboxCounter;
import com.stopit.models.data_wrappers.UnreadMessengerCounter;
import com.stopit.utils.BrandingHelper;
import com.stopit.views.StopitOrgSelector;
import com.stopit.views.StopitTextView;
import com.stopit.views.StopitToolbar;
import com.stopit.views.brandable.BrandableTabLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.stopit.app.Constants.EB_KEY_UNREAD_CHAT_COUNT;
import static com.stopit.app.Constants.EB_KEY_UNREAD_INBOX_COUNT;
import static com.stopit.app.Constants.FBA_SCREEN_GET_HELP;
import static com.stopit.app.Constants.FBA_SCREEN_HOME;
import static com.stopit.app.Constants.FBA_SCREEN_MORE;
import static com.stopit.app.Constants.INBOX_NOTIFICATION_BROADCAST_ACTION;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_BROADCAST_MESSAGE_ID;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_INCIDENT;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_INCIDENT_ID;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_READ_ONLY;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_RESOURCE_URL;

public class MainActivity extends StopitActivity implements ViewPager.OnPageChangeListener {

    final AtomicInteger messagesToUpdateCounter = new AtomicInteger();
    final AtomicInteger messagesToDeleteCounter = new AtomicInteger();
    private StopitToolbar toolBar;
    private StopitOrgSelector orgTitleView;
    private ViewPager mViewPager;
    //private RadioGroup bottomNavigator;
//    private StopitRadioButton homeBtn;
//    private StopitRadioButton incidentsBtn;
//    private StopitRadioButton resourcesBtn;
//    private StopitRadioButton helpBtn;
//    private StopitRadioButton moreBtn;
    private long incidentToUpdateId;
    private BrandableTabLayout mTabLayout;

    private int unreadChatMessagesCount;
    private int unreadInboxMessagesCount;
    private AtomicInteger unreadRequestTask = new AtomicInteger();

    @LayoutRes
    int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    void initUI() {
        toolBar = initToolbar();
        orgTitleView = initOrgSelectorView();
        mViewPager = findViewById(R.id.main_view_pager);
        mTabLayout = findViewById(R.id.incidents_tab_layout);

//        bottomNavigator = findViewById(R.id.main_bottom_navigator);
//        homeBtn = findViewById(R.id.navigation_home_btn);
//        incidentsBtn = findViewById(R.id.navigation_activity_btn);
//        /*resourcesBtn = findViewById(R.id.navigation_res_btn);*/
//        helpBtn = findViewById(R.id.navigation_help_btn);
//        moreBtn = findViewById(R.id.navigation_more_btn);

    }

    @Override
    void initData() {
//        retrieveUnreadChatMessagesCount();
//        retrieveInboxUnreadCount();
        //requestData();
    }

    @Override
    void setUI(final Bundle savedInstanceState) {
        orgTitleView.setTitleActive(this);

        final MainPagerAdapter mAdapter = new MainPagerAdapter();
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(5);
        mViewPager.addOnPageChangeListener(this);

        mTabLayout.setupWithViewPager(mViewPager);
        setCustomTab();

        //bottomNavigator.setOnCheckedChangeListener(this);
        //bottomNavigator.check(R.id.navigation_home_btn);
        //homeBtn.setChecked(true);

        //applyBranding();
        final Intent intent = getIntent();
        if (intent.getAction() != null && TextUtils.equals(intent.getAction(), INBOX_NOTIFICATION_BROADCAST_ACTION)) {
            //incidentsBtn.setChecked(true);
            // EventBus.getDefault().post(new BroadcastMessageEvent());
            onPageSelected(1);
        } else
            onPageSelected(0);

    }

    private void setCustomTab() {
        final String[] tabTitles = getResources().getStringArray(R.array.main_tabs_titles);
        final LayoutInflater inflater = getLayoutInflater();
        final ColorStateList stateColors = BrandingHelper.getTextStatesColors(this, R.color.tab_text_gray);
        final int tabsCount = mTabLayout.getTabCount();
        for (int position = 0; position < tabsCount; position++) {
            final TabLayout.Tab tab = mTabLayout.getTabAt(position);
            if (tab != null) {
                final View customTab = inflater.inflate(R.layout.main_custom_tab, null, false);
                final ImageView icon = customTab.findViewById(R.id.main_tab_icon);
                icon.setImageDrawable(getIconResForPosition(position));

                final StopitTextView tabTitle = customTab.findViewById(R.id.main_tab_title);
                tabTitle.setTextColor(stateColors);
                tabTitle.setText(tabTitles[position]);

                tab.setCustomView(customTab);
            }
        }
    }

    private StateListDrawable getIconResForPosition(final int position) {
        switch (position) {
            case 1:
                return BrandingHelper.getTabDrawableStateList(this, R.drawable.ic_activity_inactive, R.drawable.ic_activity_active);

            case 2:
                return BrandingHelper.getTabDrawableStateList(this, R.drawable.ic_help_inactive, R.drawable.ic_help_active);

            case 3:
                return BrandingHelper.getTabDrawableStateList(this, R.drawable.ic_more_inactive, R.drawable.ic_more_active);

            default:
                return BrandingHelper.getTabDrawableStateList(this, R.drawable.ic_home_inactive, R.drawable.ic_home_active);
        }
    }

    @Override
    protected void onResume() {
        requestData();
        super.onResume();
    }

    void requestData() {
        if (isInternetConnection()) {
            //TODO uncomment for resources
            /*requestResources();*/

            requestIncidentsList();
            requestBroadcastMessages();
            requestUnreadCounts();
        }
    }

    @Override
    void onRequestResourcesSuccess(final List<InformationResource> resources) {
        DBHelper.clear(InformationResource.class);
        if (resources != null && resources.size() > 0) {
            DBHelper.save(resources);
        }
    }

    @Override
    void onRequestResourcesFailure(final String errorMessage) {
        showAlertDialog("", errorMessage);
    }

    @Override
    void onRequestIncidentsSuccess(final List<Incident> incidents) {
        DBHelper.clear(Incident.class);
        if (incidents != null && incidents.size() > 0) {
            DBHelper.save(incidents);
        }
    }

    @Override
    void onRequestIncidentsFailure(final String message) {
        showAlertDialog("", message);
    }

    void onRetrieveBroadcastMessagesSuccess(final List<BroadcastMessage> messages) {
        DBHelper.clear(BroadcastMessage.class);
        if (messages != null && messages.size() > 0) {
            DBHelper.save(messages);
        }
    }

    void onRetrieveBroadcastMessagesFailure(final String errorMessage) {
        showAlertDialog("", errorMessage);
    }

    void requestUnreadCounts() {
        unreadRequestTask.set(2);
        retrieveUnreadChatMessagesCount();
        retrieveInboxUnreadCount();
    }

    void retrieveTotalUnreadMessagesCountSuccess(final UnreadMessengerCounter counter) {
        //Toast.makeText(this, "Messenger count: " + counter.getUnreadCount(), Toast.LENGTH_SHORT).show();
        unreadChatMessagesCount = counter.getUnreadCount();

        final Bundle bundle = new Bundle();
        bundle.putInt(EB_KEY_UNREAD_CHAT_COUNT, unreadChatMessagesCount);
        EventBus.getDefault().post(new UnreadChatMessageEvent(bundle));

        onRetrieveUnreadFinished();
    }

    void retrieveTotalUnreadMessagesCountFailure() {
        onRetrieveUnreadFinished();
    }

    void onRetrieveInboxUnreadCountSuccess(final UnreadInboxCounter counter) {
        //Toast.makeText(this, "Inbox count: " + counter.getUnreadCount(), Toast.LENGTH_SHORT).show();
        unreadInboxMessagesCount = counter.getUnreadCount();

        final Bundle bundle = new Bundle();
        bundle.putInt(EB_KEY_UNREAD_INBOX_COUNT, unreadInboxMessagesCount);
        EventBus.getDefault().post(new UnreadInboxMessageEvent(bundle));

        onRetrieveUnreadFinished();
    }

    void onRetrieveInboxUnreadCountFailure() {
        onRetrieveUnreadFinished();
    }

    private void onRetrieveUnreadFinished() {
        if (unreadRequestTask.decrementAndGet() <= 0) {
            manageUnreadTabCounter();
        }
    }

    private void manageUnreadTabCounter() {
        final boolean isUnreadMessages = (unreadChatMessagesCount + unreadInboxMessagesCount) > 0;
        //hardcoded value
        final TabLayout.Tab tab = mTabLayout.getTabAt(1);
        if (tab != null && tab.getCustomView() != null) {
            final View tabView = tab.getCustomView();
            final View counter = tabView.findViewById(R.id.main_tab_counter_txt);
            counter.setVisibility(isUnreadMessages ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onTitleClicked() {
    }


    public StopitTextView getToolbarRigntBtn() {
        return toolBar.getRightBtn();
    }

    public void startIncidentDetailsActivity(final long incidentId) {
        final Intent intent = new Intent(this, ReportDetailsActivity.class);
        intent.putExtra(ST_EXTRAS_KEY_INCIDENT_ID, incidentId);
        startActivity(intent);
    }

    public void logout() {
        requestLogout();
    }

    public void startIncidentDetailsActivity(final Incident incident) {
        final Intent intent = new Intent(this, ReportDetailsActivity.class);
        intent.putExtra(ST_EXTRAS_KEY_INCIDENT, incident);
        intent.putExtra(ST_EXTRAS_KEY_READ_ONLY, true);
        startActivity(intent);
    }

    public void startResDetailsActivity(final String url) {
        final Intent intent = new Intent(this, ResourceDetailsActivity.class);
        intent.putExtra(ST_EXTRAS_KEY_RESOURCE_URL, url);
        startActivity(intent);
    }

    public void getIncidentByIdAndCode(final long id, final String code) {
        requestIncidentByIdAndCode(id, code);
    }

    @Override
    void onRequestIncidentByIdAndCodeSuccess(Incident incident) {
        if (incident != null) {
            //DBHelper.save(incident);
            startIncidentDetailsActivity(incident);
        } else {
            onRequestIncidentByIdAndCodeFailure();
        }
    }

    public void setIncidentToUpdateId(final long incidentId) {
        this.incidentToUpdateId = incidentId;
    }

    @Override
    void addMediaItem(final String path, final boolean shouldBeDeleted) {
        File file = new File(path);
        if (file.exists() && file.canRead() && incidentToUpdateId > 0) {
            final MediaItem item = new MediaItem(path, shouldBeDeleted);
            final List<MediaItem> mediaItems = new ArrayList<>();
            mediaItems.add(item);
            updateIncident(incidentToUpdateId, mediaItems);
        }
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(InboxMessageEvent event) {
        if (isFinishing() || event == null || event.extras == null)
            return;

        final String newMessageId = event.extras.getString(ST_EXTRAS_KEY_BROADCAST_MESSAGE_ID);
        if (!TextUtils.isEmpty(newMessageId))
            requestBroadcastMessageById(newMessageId);
    }

    @Override
    void onRetrieveBroadcastMessageSuccess(final BroadcastMessage message) {
        if (message != null)
            DBHelper.save(message);
    }

    @Override
    void onRetrieveBroadcastMessageFailure(final String errMessage) {
    }

    @Override
    void onUpdateIncidentSuccess(final Incident incident) {
        DBHelper.save(incident);
        showAlertDialog("", getString(R.string.incident_update_success));
    }

    @Override
    void onUpdateIncidentFailure(final String errorMessage) {
        showAlertDialog("", errorMessage);
    }

    public void markBroadcastMessagesUnread(final List<BroadcastMessage> selectedMessages) {
        int messagesCounter = 0;
        final List<BroadcastMessage> messagesToUpdate = new ArrayList<>();
        for (BroadcastMessage message : selectedMessages) {
            if (!message.isRead())
                continue;

            messagesToUpdate.add(message);
            messagesCounter++;
        }

        messagesToUpdateCounter.set(messagesCounter);
        showApiInProgressView();
        for (BroadcastMessage message : messagesToUpdate) {
            updateBroadcastMessage(message.getId(), true);
        }
    }

    @Override
    void onUpdateBroadcastMessageSuccess(final String messageId) {
        if (!TextUtils.isEmpty(messageId))
            DBHelper.updateBroadcastMessageReadState(messageId, false);
        if (messagesToUpdateCounter.decrementAndGet() == 0) {
            hideApiInProgressView();
            showAlertDialog("", getString(R.string.update_inbox_message_success));
        }
    }

    @Override
    void onUpdateBroadcastMessageFailure() {
        if (messagesToUpdateCounter.decrementAndGet() == 0) {
            hideApiInProgressView();
        }
    }

    public void deleteBroadcastMessages(final List<BroadcastMessage> messagesTodelete) {
        messagesToDeleteCounter.set(messagesTodelete.size());
        showApiInProgressView();
        for (BroadcastMessage message : messagesTodelete) {
            deleteBroadcastMessageById(message.getId());
        }
    }

    @Override
    void onDeleteBroadcastMessageSuccess(final String messageId) {
        if (!TextUtils.isEmpty(messageId))
            DBHelper.deleteBroadcastMessage(messageId);

        if (messagesToDeleteCounter.decrementAndGet() == 0) {
            hideApiInProgressView();
            showAlertDialog("", getString(R.string.delete_inbox_message_success));
        }
    }

    @Override
    void onDeleteBroadcastMessageFailure() {
        if (messagesToDeleteCounter.decrementAndGet() == 0) {
            hideApiInProgressView();
        }
    }


    public void logScreenChangeEvent(final String screenName) {
        logScreenViewEvent(screenName);
    }

    @Override
    String getScreenName() {
        return "";
    }


    //show/hide right toolbar btn
    public void manageRightButton(final boolean isVisible) {
        if (isVisible){
            toolBar.showRightButton();
            EventBus.getDefault().post(new RightButtonVisibleEvent());
        } else
            toolBar.hideRightButton();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        final String screenName;
        switch (position) {
            case 1:
                if (isInternetConnection()) {
                    requestIncidentsList();
                    requestBroadcastMessages();
                    requestUnreadCounts();
                }
                screenName = "";
                break;
            //TODO uncomment for resources page
            /*case R.id.navigation_res_btn:
                currentItemPosition = 2;
                screenName = FBA_SCREEN_RESOURCES;
                break;*/
            case 2:
                screenName = FBA_SCREEN_GET_HELP;
                break;
            case 3:
                screenName = FBA_SCREEN_MORE;
                break;
            default:
                screenName = FBA_SCREEN_HOME;
                break;
        }

        //hardcoded value 1
        manageRightButton(position == 1);
        logScreenViewEvent(screenName);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private class MainPagerAdapter extends FragmentPagerAdapter {
        //TODO change for resources page
        private final int tabsCount = 4; //5;

        MainPagerAdapter() {
            super(getSupportFragmentManager());
        }

        @Override
        public int getCount() {
            return tabsCount;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 1:
                    return new IncidentsFragment();
                //TODO uncomment for resources page
               /* case 2:
                    return new ResourcesFragment();*/
                case 2:
                    return new HelpFragment();
                case 3:
                    return new MoreFragment();
                default:
                    return new HomeFragment();
            }
        }
    }

//    @Override
//    public void onCheckedChanged(RadioGroup group, int checkedId) {
//        final int currentItemPosition;
//        final String screenName;
//        switch (checkedId) {
//            case R.id.navigation_activity_btn:
//                currentItemPosition = 1;
//                if (isInternetConnection()) {
//                    requestIncidentsList();
//                    requestBroadcastMessages();
//                }
//                screenName = "";
//                break;
//            //            /*case R.id.navigation_res_btn:
//                currentItemPosition = 2;
//                screenName = FBA_SCREEN_RESOURCES;
//                break;*/
//            case R.id.navigation_help_btn:
//                currentItemPosition = 2;
//                screenName = FBA_SCREEN_GET_HELP;
//                break;
//            case R.id.navigation_more_btn:
//                currentItemPosition = 3;
//                screenName = FBA_SCREEN_MORE;
//                break;
//            default:
//                currentItemPosition = 0;
//                screenName = FBA_SCREEN_HOME;
//                break;
//        }
//
//        //hardcoded value 1
//        manageRightButton(currentItemPosition == 1);
//        mViewPager.setCurrentItem(currentItemPosition);
//        logScreenViewEvent(screenName);
//    }

}
