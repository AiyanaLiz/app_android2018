package com.stopit.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;

import com.andexert.library.RippleView;
import com.stopit.R;
import com.stopit.db.DBHelper;
import com.stopit.models.Configuration;
import com.stopit.utils.BrandingHelper;
import com.stopit.views.StopitTextView;
import com.stopit.views.StopitToolbar;
import com.stopit.views.StopitWebView;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.stopit.app.Constants.FBA_SCREEN_TERMS_OF_USE_DETAILS;
import static com.stopit.app.Constants.ST_DELIMITER;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_POLICY_URL;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_READ_ONLY;

/**
 * Created by Alenka on 07.07.2018.
 */

public class TermsOfUseDetailsActivity extends StopitActivity implements StopitWebView.OnScrollChangedCallback,
        RippleView.OnRippleCompleteListener {

    private String defaultTermsUrl;
    private String defaultPolicyUrl;
    private StopitToolbar toolbar;
    private StopitTextView titleTxt;
    private StopitTextView descriptionTxt;
    private StopitTextView acceptBtn;
    private StopitWebView mWebView;
    private View progressView;
    private View bottomButtonsContainer;
    private RippleView acceptBtnRipple;
    private RippleView declineBtnRipple;
    private boolean isReadOnlyMode;

    @Override
    protected boolean isIntentTransactionSuccessful(Intent intent) {
        if (intent != null) {
            isReadOnlyMode = intent.getBooleanExtra(ST_EXTRAS_KEY_READ_ONLY, false);
        }
        return true;
    }

    @LayoutRes
    int getContentView() {
        return R.layout.activity_terms_of_use_details;
    }


    void initUI() {
        toolbar = initToolbar();
        descriptionTxt = findViewById(R.id.terms_details_descriprion_txt);
        acceptBtn = findViewById(R.id.terms_details_accept_btn);
        mWebView = findViewById(R.id.terms_details_web_view);
        progressView = findViewById(R.id.terms_details_progress_view);
        bottomButtonsContainer = findViewById(R.id.terms_details_btns_inner_container);
        acceptBtnRipple = findViewById(R.id.terms_details_accept_ripple);
        declineBtnRipple = findViewById(R.id.terms_details_decline_ripple);
    }

    void setUI(final Bundle savedInstanceState) {
        toolbar.setBackButton(this);

        if (isReadOnlyMode) {
            bottomButtonsContainer.setVisibility(View.GONE);
        } else {
            declineBtnRipple.setOnRippleCompleteListener(this);
            setAcceptBtnEnabled(false);
            initPrivacyPolicyLink();
        }

        defaultTermsUrl = getString(R.string.terms_and_conditions_link);
        defaultPolicyUrl = getString(R.string.privacy_policy_link);

        mWebView.setOnScrollChangedCallback(this);
        mWebView.clearCache(true);
        loadUrl();
    }

    @Override
    void setAnalyticsProperties() {
    }

    @Override
    String getScreenName(){
        return FBA_SCREEN_TERMS_OF_USE_DETAILS;
    }

    private void setAcceptBtnEnabled(final boolean isEnabled) {
        if (isEnabled) {
            acceptBtn.setEnabled(true);
            acceptBtnRipple.setEnabled(true);
            acceptBtnRipple.setOnRippleCompleteListener(this);
        } else {
            acceptBtn.setEnabled(false);
            acceptBtnRipple.setEnabled(false);
            acceptBtnRipple.setOnRippleCompleteListener(null);
        }
    }

    private String getTermsAndCondUrl() {
        final Configuration config = DBHelper.getCurrentConfiguration();
        if (config != null && !TextUtils.isEmpty(config.getTermsOfUseUrl()))
            return config.getTermsOfUseUrl();
        else
            return defaultTermsUrl;
    }

    private void initPrivacyPolicyLink() {
        String initialString = getString(R.string.terms_description_txt);
        final int spanStart = initialString.indexOf(ST_DELIMITER);
        final int spanEnd = initialString.lastIndexOf(ST_DELIMITER)-1;
        initialString = initialString.replaceAll(ST_DELIMITER, "");
        SpannableString ppText = new SpannableString(initialString);
        ppText.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(TermsOfUseDetailsActivity.this, PrivacyPolicyActivity.class);
                intent.putExtra(ST_EXTRAS_KEY_POLICY_URL, getString(R.string.privacy_policy_link));
                startActivity(intent);
            }
        }, spanStart, spanEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        if (BrandingHelper.isBranding()) {
            final int mainColor = BrandingHelper.getMainColor(this);
            ppText.setSpan(new ForegroundColorSpan(mainColor), spanStart, spanEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        ppText.setSpan(new BackgroundColorSpan(Color.TRANSPARENT), spanStart, spanEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        descriptionTxt.setText(ppText);
        descriptionTxt.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void loadUrl() {
        if (checkInternetConnectionAndInform()) {
            Map<String, String> headers = new HashMap<>();
            headers.put("Accept-Language", Locale.getDefault().getLanguage());
            mWebView.loadUrl(getTermsAndCondUrl(), headers);
        }
    }

    @Override
    public void onScrollEndReached() {
        setAcceptBtnEnabled(true);
    }

    @Override
    public void onLoadPageStarted() {
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoadPageFinished() {
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void onComplete(RippleView rippleView) {
        if (rippleView == declineBtnRipple) {
            showAlertDialog("", getString(R.string.decline_terms_alert_msg), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    requestLogout();
                }
            });

        } else {
            acceptTermsOfUse();
        }
    }
}
