package com.stopit.activity;

public interface OnDownloadFinishedCallback {

    void onDownloadFinished();

}
