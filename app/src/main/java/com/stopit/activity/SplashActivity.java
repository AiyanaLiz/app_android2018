package com.stopit.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.stopit.R;
import com.stopit.api.ApiManager;
import com.stopit.api.IApiCallBackError;
import com.stopit.api.IApiCallBackSuccess;
import com.stopit.api.adapters.ApiError;
import com.stopit.db.DBHelper;
import com.stopit.event.AppOnForegroundEvent;
import com.stopit.models.Branding;
import com.stopit.models.Organization;
import com.stopit.models.User;
import com.stopit.models.VersionStatus;
import com.stopit.models.data_wrappers.UserData;
import com.stopit.models.data_wrappers.VersionStatusWrapper;
import com.stopit.utils.BooleanLock;
import com.stopit.utils.BrandingHelper;
import com.stopit.utils.NetworkStateHelper;
import com.stopit.utils.SharedPrefsHelper;
import com.stopit.utils.VersionController;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import retrofit2.Call;

import static com.stopit.app.Constants.ST_EXTRAS_KEY_IS_CLEAR_TASK;

public class SplashActivity extends StopitActivity {

    private boolean ifMigrationFromPrevApp = false;
    private BooleanLock appVersionBeingChecked = new BooleanLock();

    @LayoutRes
    int getContentView() {
        return R.layout.activity_splash;
    }

    @Override
    void initUI() {
    }

    void initData() {
    }

    @Override
    void setUI(Bundle savedInstanceState) {
        showApiInProgressView();
    }

    @Override
    protected void onResume() {
        checkAppVersion();
        super.onResume();
    }

    public boolean checkInternetConnectionAndInform() {
        if (!NetworkStateHelper.isNetworkAvailable(this)) {
            showAlertDialog("", getString(R.string.no_network_err), false, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            return false;
        }
        return true;
    }

    private void checkAppVersion() {
        if (!checkInternetConnectionAndInform() || appVersionBeingChecked.isRunning())
            return;

        appVersionBeingChecked.setRunning();
        ApiManager.getConfigApiAdapter().requestAppInfo(new IApiCallBackSuccess<VersionStatusWrapper>() {
            @Override
            public void onApiSuccess(@NonNull VersionStatusWrapper statusWrapper) {
                appVersionBeingChecked.setFinished();
                onRequestAppInfoSuccess(statusWrapper.getAndroidVersionStatus());
            }
        }, new IApiCallBackError() {
            @Override
            public boolean needToHandle() {
                return true;
            }

            @Override
            public void showApiInProgressView() {
            }

            @Override
            public void hideApiInProgressView() {
            }

            @Override
            public void onError(@NonNull Call call, @NonNull ApiError error) {
                appVersionBeingChecked.setFinished();
                onRequestAppInfoFailure();
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                appVersionBeingChecked.setFinished();
                onRequestAppInfoFailure();
            }
        });
    }

    private void onRequestAppInfoSuccess(final VersionStatus versionStatus) {
        //something went wrong - ignore versions check
        if (versionStatus == null) {
            onVersionCheckSuccess();
            return;
        }

        if (!VersionController.isDeviceOsSupported(versionStatus)) {
            hideApiInProgressView();
            onOSVersionCheckFailure();
            return;
        }

        if (!VersionController.isAppVersionSupported(this, versionStatus)) {
            hideApiInProgressView();
            onAppVersionFailure();
            return;
        }

        onVersionCheckSuccess();
    }

    private void onRequestAppInfoFailure() {
        checkIsLoggedIn();
    }

    private void onOSVersionCheckFailure() {
        showAlertDialog("", getString(R.string.unsupported_os_version), false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!isFinishing())
                    finish();
            }
        });
    }

    private void onAppVersionFailure() {
        showAlertDialog("", getString(R.string.unsupported_app_version), R.string.update_btn_label, false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final String appPackageName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
    }

    private void onVersionCheckSuccess() {
        //Toast.makeText(this, "app version check success", Toast.LENGTH_SHORT).show();
        checkIsLoggedIn();
    }

    private void checkIsLoggedIn() {
        if (!checkInternetConnectionAndInform())
            return;

        //check if updated from app version 9.6
        final String savedIntellicode = SharedPrefsHelper.getOldAppIntellicodeSaved(this);
        if (!TextUtils.isEmpty(savedIntellicode)) {
            Log.d("Splash", "Stopit 9.7 terms accepted status check with an old intellicode " + savedIntellicode);
            ifMigrationFromPrevApp = true;
            statusCheck(savedIntellicode);
            return;
        }

        final User user = DBHelper.getUser();
        //no saved user found or intellicode is invalid
        if (user == null || TextUtils.isEmpty(user.getIntelliCode())) {
            showStartScreen();
            return;
        }

        final Organization currentOrg = DBHelper.getCurrentOrganization();
        //org is invalid
        if (currentOrg == null || currentOrg.getId() == 0) {
            showStartScreen();
            return;
        }

        statusCheck();
    }


    @Override
    void onStatusCheckSuccess(final UserData userData) {
        Toast.makeText(this, "onStatusCheckSuccess", Toast.LENGTH_SHORT).show();
        statusBeingChecked.setFinished();
        Log.d("Splash", "status check with an old intellicode success");

        if (!isResponseUserDataValid(userData)) {
            if (ifMigrationFromPrevApp) {
                SharedPrefsHelper.removeOldAppIntellicode(this);
                showAlertDialog("", getString(R.string.app_migration_err), false, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showStartScreen();
                    }
                });
            } else
                showStartScreen();

            return;
        }

        //get previous branding assets urls and compare with new ones
        String logoUrl = null;
        String bgUrl = null;
        final Branding currentBranding = DBHelper.getBranding();
        if (currentBranding != null) {
            logoUrl = currentBranding.getLogoUrl();
            bgUrl = currentBranding.getBgImageUrl();
        }

        BrandingHelper.clearBranding(false);
        DBHelper.saveAllUserData(userData);
        if (ifMigrationFromPrevApp) {
            SharedPrefsHelper.removeOldAppIntellicode(this);
            SharedPrefsHelper.setTermsOfUseAccepted(this, DBHelper.getCurrentOrganizationId());
            Log.d("RemoteDataManager", "Stopit 9.7 terms accepted " + DBHelper.getCurrentOrganizationId());
        }
        final Branding updatedBranding = DBHelper.getBranding();
        //if branding changed
        if (updatedBranding != null && (!TextUtils.equals(logoUrl, updatedBranding.getLogoUrl())
                || !TextUtils.equals(bgUrl, updatedBranding.getBgImageUrl()))) {

            downloadBrandingAssets(updatedBranding);

        } else {
            goToNextScreen();
            finish();
        }
    }

    void onDownloadAssetsFinished() {
        goToNextScreen();
        finish();
    }

    void onStatusCheckFailure() {
        //Toast.makeText(this, "onStatusCheckFailure", Toast.LENGTH_SHORT).show();
        statusBeingChecked.setFinished();
        if (ifMigrationFromPrevApp) {
            SharedPrefsHelper.removeOldAppIntellicode(this);
            showAlertDialog("", getString(R.string.app_migration_err), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    showStartScreen();
                }
            });
        } else
            showStartScreen();
    }

    private void showStartScreen() {
        final Intent intent = new Intent(this, StartActivity.class);
        intent.putExtra(ST_EXTRAS_KEY_IS_CLEAR_TASK, true);
        startActivity(intent);
        finish();
    }

    @Override
    public void showApiInProgressView() {
    }

    @Override
    public void hideApiInProgressView() {
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AppOnForegroundEvent event) {
        //ignore
    }
}
