package com.stopit.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.view.View;

import com.andexert.library.RippleView;
import com.stopit.R;
import com.stopit.utils.BrandingHelper;
import com.stopit.views.StopitOrgSelector;
import com.stopit.views.StopitTextView;
import com.stopit.views.StopitToolbar;
import com.stopit.views.brandable.BrandableTextView;
import com.stopit.views.brandable.RippleWithBackground;

import static com.stopit.app.Constants.FBA_SCREEN_SUBMIT_INCIDENT_RESULT;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_INCIDENT_ID;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_INCIDENT_PASSWORD;

/**
 * Created by Alenka on 07.07.2018.
 */

public class SubmitIncidentResultActivity extends StopitActivity implements RippleView.OnRippleCompleteListener {

    private StopitToolbar toolbar;
    private StopitOrgSelector orgTitleView;
    private BrandableTextView idTxt;
    private BrandableTextView passwordTxt;
    private StopitTextView descriptionTxt;

//    private StopitButton messagesBtn;
//    private StopitButton okBtn;

    private RippleWithBackground messagesRipple;
    private RippleWithBackground okRipple;

    private long incidentId;
    private String password;


    @LayoutRes
    int getContentView() {
        return R.layout.activity_submit_incident_details;
    }

    protected boolean isIntentTransactionSuccessful(final Intent intent) {
        if (intent == null || !intent.hasExtra(ST_EXTRAS_KEY_INCIDENT_ID) || !intent.hasExtra(ST_EXTRAS_KEY_INCIDENT_PASSWORD))
            return false;

        incidentId = intent.getLongExtra(ST_EXTRAS_KEY_INCIDENT_ID, 0);
        password = intent.getStringExtra(ST_EXTRAS_KEY_INCIDENT_PASSWORD);

        return (incidentId != 0 && !TextUtils.isEmpty(password));
    }


    void initUI() {
        toolbar = initToolbar();
        toolbar.hideBackButton();

        orgTitleView = initOrgSelectorView();
        idTxt = findViewById(R.id.submit_details_id_txt);
        passwordTxt = findViewById(R.id.submit_details_password_txt);
        descriptionTxt = findViewById(R.id.submit_details_descriprion_txt);

//        messagesBtn = findViewById(R.id.submit_details_messages_btn);
//        okBtn = findViewById(R.id.submit_details_ok_btn);

        messagesRipple = findViewById(R.id.incident_result_messages_ripple);
        okRipple = findViewById(R.id.incident_result_ok_ripple);
    }

    void initData() {

    }

    void setUI(final Bundle savedInstanceState) {
        orgTitleView.setTitleActive(this);

        idTxt.setText(String.valueOf(incidentId));
        passwordTxt.setText(password);

        messagesRipple.setOnRippleCompleteListener(this);
        okRipple.setOnRippleCompleteListener(this);

        initWebLink();
    }

    @Override
    String getScreenName(){
        return FBA_SCREEN_SUBMIT_INCIDENT_RESULT;
    }

    private void initWebLink() {
        SpannableString ppText = new SpannableString(getString(R.string.submit_description_txt));
        final int spanStartIndex = ppText.length() - 38;
        final int spanEndIndex = ppText.length();
        final Resources res = getResources();
        int mainColor = res.getColor(R.color.colorPrimary);

        if (BrandingHelper.isBrandingColor()) {
            mainColor = BrandingHelper.getMainColor(this);
            //ppText.setSpan(new ForegroundColorSpan(mainColor), spanStartIndex, spanEndIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        final int normalColor = mainColor;
        final int pressedColor = res.getColor(R.color.colorPrimaryDark);

        ppText.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://web.stopitsolutions.com/lookup"));
                startActivity(browserIntent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                if (descriptionTxt.isPressed()) {
                    ds.setColor(pressedColor);
                } else {
                    ds.setColor(normalColor);
                }
                descriptionTxt.invalidate();
            }
        }, spanStartIndex, spanEndIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        ppText.setSpan(new BackgroundColorSpan(Color.TRANSPARENT), spanStartIndex, spanEndIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        descriptionTxt.setText(ppText);
        descriptionTxt.setMovementMethod(LinkMovementMethod.getInstance());
        descriptionTxt.setHighlightColor(Color.TRANSPARENT);
    }

    @Override
    public void onComplete(RippleView rippleView) {
        if (rippleView == messagesRipple) {
            startMessengerActivity(incidentId);
        }

        finish();
    }
}
