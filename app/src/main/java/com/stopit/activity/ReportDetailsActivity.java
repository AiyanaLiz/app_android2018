package com.stopit.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.andexert.library.RippleView;
import com.stopit.R;
import com.stopit.api.adapters.ApiError;
import com.stopit.db.DBHelper;
import com.stopit.event.AppOnForegroundEvent;
import com.stopit.models.DynamicQuestionResult;
import com.stopit.models.Incident;
import com.stopit.models.MediaItem;
import com.stopit.models.Organization;
import com.stopit.views.StopitOrgSelector;
import com.stopit.views.StopitTextView;
import com.stopit.views.StopitToolbar;
import com.stopit.views.brandable.RippleWithBackground;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit2.Call;

import static com.stopit.app.Constants.FBA_SCREEN_INCIDENT_DETAILS;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_INCIDENT;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_INCIDENT_ID;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_READ_ONLY;

public class ReportDetailsActivity extends StopitActivity implements RippleView.OnRippleCompleteListener {
    private StopitToolbar toolbar;
    private StopitOrgSelector orgSelector;
    private Incident incident;

    private StopitTextView incidentIdTxt;
    private StopitTextView incidentDateTxt;
    private StopitTextView statusTxt;
    private LinearLayout detailsContainer;


    private RippleView addMediaRipple;
    private StopitTextView mediaCounterTxt;
    private long incidentId;
    private StopitTextView messengerBtn;
    private RippleWithBackground messengerRipple;
    private boolean isReadOnly;


    @Override
    protected boolean isIntentTransactionSuccessful(Intent intent) {
        if (intent == null)
            return false;

        incidentId = intent.getLongExtra(ST_EXTRAS_KEY_INCIDENT_ID, 0);
        isReadOnly = intent.getBooleanExtra(ST_EXTRAS_KEY_READ_ONLY, false);
        if (incidentId == 0) {
            incident = (Incident) intent.getSerializableExtra(ST_EXTRAS_KEY_INCIDENT);
            if (incident != null)
                incidentId = incident.getId();
        }
        return incidentId != 0 || incident != null;
    }

    @LayoutRes
    int getContentView() {
        return R.layout.activity_report_details;
    }

    void initUI() {
        toolbar = initToolbar();
        orgSelector = initOrgSelectorView();

        incidentIdTxt = findViewById(R.id.details_incident_id);
        incidentDateTxt = findViewById(R.id.details_incident_date);
        statusTxt = findViewById(R.id.details_incident_status_txt);
        detailsContainer = findViewById(R.id.details_questions_container);
        mediaCounterTxt = findViewById(R.id.details_media_counter);
        addMediaRipple = findViewById(R.id.details_add_media_ripple);
        messengerBtn = findViewById(R.id.incident_details_messenger_btn);
        messengerRipple = findViewById(R.id.incident_details_messenger_ripple);
    }

    void initData() {
        if (incident == null)
            requestIncidentById(incidentId);
        else
            onRequestIncidentByIdSuccess(incident);
    }

    @Override
    void onRequestIncidentByIdSuccess(final Incident incident) {
        if (incident != null) {
            this.incident = incident;
            populateIncidentDetails();
        }
    }

    @Override
    public void onFailure(@NonNull Call call, @NonNull Throwable t) {
        showAlertDialog("", t.getMessage(), false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
    }

    @Override
    public void onError(@NonNull Call call, @NonNull ApiError error) {
        if (isUserActive(error))
            showAlertDialog("", error.getMessage(), false, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        else //user was deactivated
            deactivateUser(error);
    }

    void setUI(final Bundle savedInstanceState) {
        toolbar.setBackButton(this);
    }

    @Override
    String getScreenName() {
        return FBA_SCREEN_INCIDENT_DETAILS;
    }

    private void populateIncidentDetails() {
        if (!incident.isCurrentOrgIncident()) {
            final String orgName = incident.getOrgName();
            if (!TextUtils.isEmpty(orgName))
                orgSelector.setOrganizationsTitle(orgName);

        } else {
            orgSelector.setTitleActive(this);
        }

        setMediaCounter();

        if (incident.isClosed() || isReadOnly) {
            addMediaRipple.setVisibility(View.INVISIBLE);
            addMediaRipple.setOnRippleCompleteListener(null);

        } else {
            addMediaRipple.setVisibility(View.VISIBLE);
            addMediaRipple.setOnRippleCompleteListener(this);
            addMediaRipple.setEnabled(true);
        }

        final int messagesCount = incident.getMessagesCount();
        messengerBtn.setText(messagesCount == 0 ?
                getString(R.string.messages_btn_label) :
                getString(R.string.messenger_btn_format, messagesCount));

        messengerRipple.setOnRippleCompleteListener(isReadOnly ? null : this);
        messengerRipple.setEnabled(!isReadOnly);

        setIncidentHeader();
        populateDetailsContainer();
    }

    private void setIncidentHeader() {
        incidentIdTxt.setText(incident.getIdFormatted());
        incidentDateTxt.setText(incident.getDateFormatted(incident.getCreationDate()));
        final int bgRes;
        final int txtRes;
        if (incident.isClosed()) {
            bgRes = R.drawable.incident_status_closed_bg;
            txtRes = R.string.incident_status_closed_label;
        } else {
            bgRes = R.drawable.incident_status_open_bg;
            txtRes = R.string.incident_status_open_label;
        }

        statusTxt.setText(txtRes);
        statusTxt.setBackgroundResource(bgRes);
    }

    private void populateDetailsContainer() {
        final String description = incident.getReportMessage();
        if (!TextUtils.isEmpty(description)) {
            detailsContainer.addView(getIncidentDescriptionView(description));
        }

        final List<DynamicQuestionResult> questions = incident.getQuestionsResults();
        if (questions != null && questions.size() > 0) {
            final List<DynamicQuestionResult> questionsToDisplay = new ArrayList<>();
            final Set<String> questionsLabels = new HashSet<>();
            for (DynamicQuestionResult questionResult : questions) {
                final String label = questionResult.getReadOnlyLabel();
                if (!TextUtils.isEmpty(label))
                    questionsLabels.add(label);
            }

            StringBuilder answerBuilder;
            for (String questionLabel : questionsLabels) {
                answerBuilder = new StringBuilder("");
                for (DynamicQuestionResult question : questions) {
                    if (TextUtils.equals(questionLabel, question.getReadOnlyLabel())) {
                        if (answerBuilder.length() > 0)
                            answerBuilder.append(", ");

                        answerBuilder.append(question.getReadOnlyAnswer());
                    }
                }
                questionsToDisplay.add(new DynamicQuestionResult(questionLabel, answerBuilder.toString()));
            }

            for (DynamicQuestionResult question : questionsToDisplay) {
                detailsContainer.addView(getIncidentDetailsView(question));
            }
        }
    }

    private void setMediaCounter() {
        final int totalMediaCount = incident.getAttachmentsCount();
        mediaCounterTxt.setText(getString(R.string.media_counter_format, totalMediaCount));
    }

    //////////////////// DYNAMIC QUESTIONS //////////////////////////////

    private View getIncidentDescriptionView(final String description) {
        final View view = getLayoutInflater().inflate(R.layout.item_incident_details, null);
        final StopitTextView question = view.findViewById(R.id.details_question_txt);
        question.setVisibility(View.GONE);
        final StopitTextView answer = view.findViewById(R.id.details_answer_txt);
        answer.setText(description);

        return view;
    }

    private View getIncidentDetailsView(final DynamicQuestionResult question) {
        final View view = getLayoutInflater().inflate(R.layout.item_incident_details, null);
        final StopitTextView questionTxt = view.findViewById(R.id.details_question_txt);
        final StopitTextView answerTxt = view.findViewById(R.id.details_answer_txt);

        questionTxt.setText(question.getReadOnlyLabel());
        final String answerLabel = question.getReadOnlyAnswer();
        answerTxt.setText(TextUtils.isEmpty(answerLabel) ? getString(R.string.no_answer_label) : answerLabel);

        return view;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

//    private void clearTempMediaItems() {
//        if (mediaItems == null || mediaItems.size() == 0)
//            return;
//
//        for (MediaItem items : mediaItems) {
//            if (items.isShouldBeDeleted() && !TextUtils.isEmpty(items.getPath())) {
//                final File file = new File(items.getPath());
//                if (file.exists() && file.canWrite()) {
//                    boolean isDeleted = file.delete();
//                    Log.d("ReportDetailsActivity", "deleted file " + isDeleted + " path " + items.getPath());
//                }
//            }
//        }
//    }

    @Override
    public void onComplete(RippleView rippleView) {
        if (rippleView == messengerRipple) {
            startMessengerActivity(incident.getId());

        } else if (rippleView == addMediaRipple) {
            final Organization currentOrg = DBHelper.getCurrentOrganization();
            if (currentOrg == null) {
                showAlertDialog("", getString(R.string.unknown_err_msg));

            } else if (currentOrg.isReportingEnabled())
                showOptionsPopup(incident);

            else
                showOutOfSessionAlert();
        }
    }

    @Override
    void addMediaItem(final String path, final boolean shouldBeDeleted) {
        File file = new File(path);
        if (file.exists() && file.canRead()) {
            final MediaItem item = new MediaItem(path, shouldBeDeleted);
            final List<MediaItem> mediaItems = new ArrayList<>();
            mediaItems.add(item);
            updateIncident(incidentId, mediaItems);
        }
    }

    @Override
    void onUpdateIncidentSuccess(final Incident updatedIncident) {
        if(incident != null) {
            DBHelper.save(updatedIncident);
            this.incident = updatedIncident;
            setMediaCounter();
            showAlertDialog("", getString(R.string.incident_update_success));
        }
    }

    @Override
    void onUpdateIncidentFailure(final String errorMessage) {
        showAlertDialog("", errorMessage);
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AppOnForegroundEvent event) {
    }
}
