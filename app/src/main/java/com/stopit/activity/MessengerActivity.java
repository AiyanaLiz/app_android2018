package com.stopit.activity;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.stopit.R;
import com.stopit.adapter.MessengerAdapter;
import com.stopit.api.chat.SmackConnection;
import com.stopit.api.chat.SmackService;
import com.stopit.db.DBHelper;
import com.stopit.event.AppOnForegroundEvent;
import com.stopit.models.Branding;
import com.stopit.models.Incident;
import com.stopit.models.Organization;
import com.stopit.models.User;
import com.stopit.models.messenger.ChatMessage;
import com.stopit.utils.BrandingHelper;
import com.stopit.views.StopitEditText;
import com.stopit.views.StopitOrgSelector;
import com.stopit.views.StopitTextView;
import com.stopit.views.StopitToolbar;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.UUID;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;

import static com.stopit.app.Constants.FBA_SCREEN_MESSENGER;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_INCIDENT_ID;

public class MessengerActivity extends StopitActivity /*implements View.OnFocusChangeListener */ {

    public SmackService mService = null;
    private boolean mBound;

    private StopitOrgSelector orgSelectorView;
    private StopitToolbar toolbar;
    private View edtContainer;
    private StopitEditText messageEdt;
    private View sendBtn;
    private RelativeLayout edtBackground;
    private StopitTextView incidentIdTxt;
    private StopitTextView incidentDate;
    private StopitTextView incidentStatusTxt;
    private RecyclerView messagesRecycler;
    private MessengerAdapter mAdapter;
    private RealmResults<ChatMessage> messagesList;


    private long incidentId;
    private Incident incident;

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the mSmackConnection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            SmackService.LocalBinder binder = (SmackService.LocalBinder) service;
            mService = binder.getService();
            mService.restart();
            mBound = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the mSmackConnection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mBound = false;
        }
    };

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("MessengerActivity", "receiver.onReceive()");
//            //Reset unread message count
//            //RemoteDataManager.setUnreadMessageCount(context, 0);
//            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.cancel(1);
//
//            runOnUiThread(new Runnable() {
//                public void run() {
//                    messageListAdapter.notifyDataSetChanged();
//                }
//            });
        }
    };
    private BroadcastReceiver beforeXmppReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showApiInProgressView();
            Log.d("MessengerActivity", "beforeXmppReceiver.onReceive()");

        }
    };
    private BroadcastReceiver xmppRoomJoinedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //TODO
            //sendMessage();
            hideApiInProgressView();
            Log.d("MessengerActivity", "xmppRoomJoinedReceiver.onReceive()");
        }
    };
    private BroadcastReceiver xmppRoomJoiningFailed = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("MessengerActivity", "xmppRoomJoinedReceiver.onReceive()");
            showAlertDialog(getString(R.string.joining_room_failed_title), getString(R.string.joining_room_failed_message), false, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        }
    };
    private BroadcastReceiver afterXmppReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("MessengerActivity", "afterXmppReceiver.onReceive()");
            hideApiInProgressView();
        }
    };

    private BroadcastReceiver errorXmppReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("MessengerActivity", "errorXmppReceiver.onReceive()");
            showAlertDialog(getString(R.string.xmpp_error_title), getString(R.string.xmpp_error_message));
        }
    };

    private BroadcastReceiver messageSentFailureReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("MessengerActivity", "errorXmppReceiver.onReceive()");
            showAlertDialog(getString(R.string.xmpp_error_title), getString(R.string.message_not_sent_err));
        }
    };

    public String getRoomUrl() {
        return incidentId + "@conference." + DBHelper.getChatUrl();
    }

    @Override
    protected boolean isIntentTransactionSuccessful(Intent intent) {
        if (intent == null || !intent.hasExtra(ST_EXTRAS_KEY_INCIDENT_ID)) {
            return false;
        }

        incidentId = intent.getLongExtra(ST_EXTRAS_KEY_INCIDENT_ID, 0);
        if (incidentId != 0)
            incident = DBHelper.getIncidentById(incidentId);

        return incidentId != 0;
    }

    @LayoutRes
    int getContentView() {
        return R.layout.activity_messenger;
    }

    void initUI() {
        orgSelectorView = initOrgSelectorView();
        toolbar = initToolbar();

        incidentIdTxt = findViewById(R.id.messenger_incident_id);
        incidentStatusTxt = findViewById(R.id.messenger_incident_status);
        incidentDate = findViewById(R.id.messenger_incident_date);
        edtBackground = findViewById(R.id.messenger_edt_bg_view);
        messagesRecycler = findViewById(R.id.messenger_recycler_view);
        messageEdt = findViewById(R.id.messenger_edt);
        sendBtn = findViewById(R.id.messenger_send_btn);
        edtContainer = findViewById(R.id.messenger_send_container);
    }

    @Override
    void initData() {
        messagesList = DBHelper.getChatMessagesList(incidentId);
       /* if (checkInternetConnectionAndInform()) {
            retrieveChatHistory(incidentId);
        }*/
    }

//    @Override
//    public void onFocusChange(View v, boolean hasFocus) {
//        if (hasFocus) {
//            edtBackground.setBackgroundResource(R.drawable.btn_white_blue_border_bg);
//        } else {
//            edtBackground.setBackgroundResource(R.drawable.edt_gray_border_bg);
//        }
//    }

    @Override
    void onRetrieveChatHistorySuccess(final List<ChatMessage> messageList) {
        DBHelper.clearMessages(incidentId);
        if (messageList.size() > 0) {
            DBHelper.save(messageList);
            updateChatMessages(incidentId);
        }
    }

    @Override
    void onRetrieveChatHistoryFailure() {
        showAlertDialog("", getString(R.string.history_update_err));
    }

    void setUI(final Bundle savedInstanceState) {
        orgSelectorView.setTitleActive(this);
        toolbar.setBackButton(this);

        final LinearLayoutManager mManager = new LinearLayoutManager(this);
        //mManager.setStackFromEnd(true);
        messagesRecycler.setLayoutManager(mManager);
        mAdapter = new MessengerAdapter(messagesList);
        messagesRecycler.setAdapter(mAdapter);
        messagesRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        populateHeader();

        messagesList.addChangeListener(new RealmChangeListener<RealmResults<ChatMessage>>() {
            @Override
            public void onChange(@NonNull RealmResults<ChatMessage> chatMessages) {
                final RealmResults<ChatMessage> messages = DBHelper.getChatMessagesList(incidentId);
                //Log.d("messenger", "size "+messages.size());
                if (!isFinishing() && mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                    messagesRecycler.scrollToPosition(messagesList.size()-1);
                }
            }
        });

        if (incident != null && incident.isClosed()) {
            edtContainer.setVisibility(View.GONE);
        } else {
            messageEdt.addTextChangedListener(this);
            sendBtn.setOnClickListener(this);
            //messageEdt.setOnFocusChangeListener(this);
        }
    }

    @Override
    String getScreenName(){
        return FBA_SCREEN_MESSENGER;
    }

    private void populateHeader() {
        if (incident != null) {
            incidentIdTxt.setText(incident.getIdFormatted());
            incidentStatusTxt.setText(incident.getStatusTxtResource());
            incidentStatusTxt.setBackgroundResource(incident.getStatusBackground());
            incidentDate.setText(incident.getDateFormatted(incident.getCreationDate()));
        }
    }

    @Override
    public void onClick(View view) {
        if (view == sendBtn) {
            sendMessage();
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (TextUtils.isEmpty(editable) || TextUtils.isEmpty(editable.toString().trim()))
            sendBtn.setVisibility(View.INVISIBLE);
        else
            sendBtn.setVisibility(View.VISIBLE);
    }

    private void sendMessage() {
        if (!checkInternetConnectionAndInform())
            return;

        if (SmackService.getState().equals(SmackConnection.ConnectionState.DISCONNECTED)) {
            startChatService();
        }

        final String messageBody = messageEdt.getText().toString();
//        Date todayDate = new Date();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //"2017-10-17T13:20:22.000Z"
//        final String todayDateStr = sdf.format(todayDate);
        final String uuid_string = UUID.randomUUID().toString();
        final String roomId = getRoomUrl();

        //send the message to the queue
        Intent intent = new Intent(SmackService.SEND_MESSAGE);
        intent.setPackage(MessengerActivity.this.getPackageName());
        intent.putExtra(SmackService.BUNDLE_MESSAGE_BODY, messageBody);
        intent.putExtra(SmackService.MESSAGE_UUID, uuid_string);
        intent.putExtra(SmackService.MESSAGE_DATE, System.currentTimeMillis());
        intent.putExtra(SmackService.MESSAGE_INCIDENT_ID, incidentId);
        if (!TextUtils.isEmpty(roomId)) {
            intent.putExtra(SmackService.BUNDLE_TO, roomId);
        }
        intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);

        MessengerActivity.this.sendBroadcast(intent);

        //Check if we need to show after hours message
        showAfterHoursMessageIfNeeded();
        clearMessageEdt();
    }

    private void clearMessageEdt() {
        messageEdt.setText("");
        hideKeyboardIfShown();
    }

    private void showAfterHoursMessageIfNeeded() {
        final Branding orgBranding = BrandingHelper.getBranding();
        if (orgBranding != null) {
            String afterHoursMessage = orgBranding.getAutoResponseText();
            if (!TextUtils.isEmpty(afterHoursMessage)) {
                showAlertDialog("", afterHoursMessage);
            }
        }
    }

    @Override
    protected void onResume() {
        IntentFilter newmsgFilter = new IntentFilter();
        newmsgFilter.addAction(SmackService.NEW_MESSAGE);
        registerReceiver(receiver, newmsgFilter);

        IntentFilter beforeXmppConnectionfilter = new IntentFilter();
        beforeXmppConnectionfilter.addAction(SmackService.BEFORE_CONNECTION);
        registerReceiver(beforeXmppReceiver, beforeXmppConnectionfilter);

        IntentFilter afterXmppConnectionfilter = new IntentFilter();
        afterXmppConnectionfilter.addAction(SmackService.AFTER_CONNECTION);
        registerReceiver(afterXmppReceiver, afterXmppConnectionfilter);

        IntentFilter errorXmppConnectionfilter = new IntentFilter();
        errorXmppConnectionfilter.addAction(SmackService.XMPP_ERROR);
        registerReceiver(errorXmppReceiver, errorXmppConnectionfilter);

        IntentFilter roomXmppConnectionReceiver = new IntentFilter();
        roomXmppConnectionReceiver.addAction(SmackService.XMPP_ROOM_JOINED_FAILED);
        registerReceiver(xmppRoomJoiningFailed, roomXmppConnectionReceiver);

        IntentFilter messageSentFailure = new IntentFilter();
        roomXmppConnectionReceiver.addAction(SmackService.OUTCOMING_MESSAGE_NOT_SENT);
        registerReceiver(messageSentFailureReceiver, messageSentFailure);

        //if incident closed - inform, else - start service
        if (checkIsMessagingAvailableAndInform()) {
            showApiInProgressView();
            startChatService();

            IntentFilter roomJoinedXMPPConnectionReceiver = new IntentFilter();
            roomJoinedXMPPConnectionReceiver.addAction(SmackService.XMPP_ROOM_JOINED);
            registerReceiver(xmppRoomJoinedReceiver, roomJoinedXMPPConnectionReceiver);
        }

        super.onResume();
        if (checkInternetConnectionAndInform()) {
            retrieveChatHistory(incidentId);
        }
    }

    private boolean checkIsMessagingAvailableAndInform() {
        if (incident == null || incident.isClosed()) {
            showAlertDialog("", getString(R.string.joining_room_closed_message));
            return false;
        }

        final Organization currentOrg = DBHelper.getCurrentOrganization();
        if(currentOrg == null || !currentOrg.isReportingEnabled()){
            showAlertDialog("", getString(R.string.reporting_disabled_err));
            return false;
        }
        return true;
    }

    @Override
    protected void onPause() {

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(1);

        try {
            unregisterReceiver(receiver);
            unregisterReceiver(beforeXmppReceiver);
            unregisterReceiver(afterXmppReceiver);
            unregisterReceiver(errorXmppReceiver);
            unregisterReceiver(xmppRoomJoinedReceiver);
            unregisterReceiver(xmppRoomJoiningFailed);
            unregisterReceiver(messageSentFailureReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (this.mService != null)
                this.mService.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (this.mService != null)
            this.mService.stop();

        super.onDestroy();
    }

    //Start Chat if not started already
    void startChatService() {
        try {
            if (SmackService.getState().equals(SmackConnection.ConnectionState.DISCONNECTED)) {
                final String chatUserName = DBHelper.getChatUserName();
                final String chatServerUrl = DBHelper.getChatUrl();
                final String chatUserPassword = DBHelper.getChatUserPassword();
                final int chatPort = DBHelper.getChatPort();
                final String roomUrl = getRoomUrl();

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                prefs.edit()
                        .putString("xmpp_jid", chatUserName + "@" + chatServerUrl)
                        .putString("xmpp_password", chatUserPassword)
                        .putString("xmpp_room_url", roomUrl)
                        .putLong("xmpp_room_id", incidentId)
                        .putInt("xmpp_port", chatPort)
                        .apply();

                if (!TextUtils.isEmpty(DBHelper.getChatUserName())) {
                    //Check state again
                    if (SmackService.getState().equals(SmackConnection.ConnectionState.DISCONNECTED)) {
                        if (mService == null) {
                            Intent startIntent = new Intent(this, SmackService.class);
                            mBound = bindService(startIntent, mConnection, Context.BIND_AUTO_CREATE);
                        } else {
                            mService.restart();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

            //wild card catch
            //TODO handle
            //NetworkUtil.handleMessengerServerErrorDialog(AppBaseActivity.this);
//            String codeLine = Integer.toString(Thread.currentThread().getStackTrace()[2].getLineNumber());
//            AnalyticsManager.sendLocalyticsEvent(AnalyticsManager.Localytics_XMPP_Error, AnalyticsManager.On_App_Base, codeLine, true, null);
        }
    }

    private void roomJoiningFailed() {
        String alertTitle = getResources().getString(R.string.joining_room_failed_title);
        String alertMessage = getResources().getString(R.string.joining_room_failed_message);

        showAlertDialog(alertTitle, alertMessage, false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AppOnForegroundEvent event) {
       //ignore
    }
}
