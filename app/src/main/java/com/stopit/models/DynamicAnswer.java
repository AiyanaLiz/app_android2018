package com.stopit.models;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Alenka on 14.03.2018.
 */

public class DynamicAnswer implements Serializable, Comparable<DynamicAnswer> {

    @SerializedName("id")
    private long id;

    @SerializedName("question_id")
    private long questionId;

    @SerializedName("label")
    private String label;

    private int order;
    private boolean isSelected;

    public DynamicAnswer(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    @Override
    public int compareTo(@NonNull DynamicAnswer other) {
        return Integer.valueOf(this.order).compareTo(other.order);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DynamicAnswer)) return false;

        DynamicAnswer that = (DynamicAnswer) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
