package com.stopit.models;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Alenka on 11.07.2018.
 */

public class Registration {

    @SerializedName("regis_code")
    private final String accessCode;

    @SerializedName("serial_number")
    private final String deviceId;

    private Registration(final String accessCode, final String password) {
        this.accessCode = accessCode;
        this.deviceId = password;
    }

    public static JsonObject getJson(Gson gson, final String login, final String password) {
        return gson.toJsonTree(new Registration(login, password), Registration.class).getAsJsonObject();
    }

}
