package com.stopit.models;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.stopit.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

import static com.stopit.app.Constants.DYNAMIC_QUESTION_TYPE_CHECKBOX_ID;
import static com.stopit.app.Constants.DYNAMIC_QUESTION_TYPE_DATE_ID;
import static com.stopit.app.Constants.DYNAMIC_QUESTION_TYPE_EMAIL_ID;
import static com.stopit.app.Constants.DYNAMIC_QUESTION_TYPE_TEXTBOX_ID;

/**
 * Created by Alenka on 14.03.2018.
 */


public class IncidentQuestion /*implements Serializable*/ {

   /* private static final String QUESTION_LABEL_FORMAT = "%1$s (%2$s)";

    @SerializedName("label")
    private String label;

    @SerializedName("answer")
    private String answer;

    public IncidentQuestion(){}


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }*/
}
