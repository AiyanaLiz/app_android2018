package com.stopit.models;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.stopit.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.stopit.app.Constants.DYNAMIC_QUESTION_TYPE_CHECKBOX_ID;
import static com.stopit.app.Constants.DYNAMIC_QUESTION_TYPE_DATE_ID;
import static com.stopit.app.Constants.DYNAMIC_QUESTION_TYPE_EMAIL_ID;
import static com.stopit.app.Constants.DYNAMIC_QUESTION_TYPE_TEXTBOX_ID;

/**
 * Created by Alenka on 14.03.2018.
 */

public class DynamicQuestion implements Serializable, Comparable<DynamicQuestion> {

    private static final String QUESTION_LABEL_FORMAT = "%1$s (%2$s)";

    @SerializedName("question_id")
    private long id;

    @SerializedName("question")
    private String label;

    @SerializedName("question_type_id")
    private int typeId;

    @SerializedName("required")
    private boolean required;

    @SerializedName("answers")
    private List<DynamicAnswer> answers;

    private int active;
    private int order;

    //for textbox, email, date question type
    private String textAnswer;
    //for single check box type
    private boolean chkValue;


    public DynamicQuestion() {
        answers = new ArrayList<>();
        textAnswer = "";
    }

    public DynamicQuestion getCopiedQuestion() {
        final DynamicQuestion copy = new DynamicQuestion();
        copy.id = this.id;
        copy.label = this.label;
        copy.typeId = this.typeId;
        copy.active = this.active;
        copy.order = this.order;
        copy.required = this.required;
        copy.answers = this.answers;
        return copy;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRequiredLabel(Context context) {
        if (TextUtils.isEmpty(label))
            return "";

        if (isRequired() /*&& typeId != DYNAMIC_QUESTION_TYPE_CHECKBOX_ID*/) {
            final String requiredLabel = context.getString(R.string.required_question_label);
            return String.format(QUESTION_LABEL_FORMAT, label, requiredLabel);
        } else return label;
    }


    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public List<DynamicAnswer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<DynamicAnswer> answers) {
        this.answers = answers;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getTextAnswer() {
        return textAnswer;
    }

    public void setTextAnswer(String textAnswer) {
        this.textAnswer = textAnswer;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    /////////////// custom ///////////////////

    public Boolean getChkValue() {
        return chkValue;
    }

    public void setChkValue(Boolean positive) {
        chkValue = positive;
    }

    public boolean isUserAnswer() {
        switch (typeId) {
            case DYNAMIC_QUESTION_TYPE_DATE_ID:
            case DYNAMIC_QUESTION_TYPE_TEXTBOX_ID:
            case DYNAMIC_QUESTION_TYPE_EMAIL_ID:
                return !TextUtils.isEmpty(textAnswer);

            case DYNAMIC_QUESTION_TYPE_CHECKBOX_ID:
                return chkValue;

            default:
                boolean isAnswer = false;
                for (DynamicAnswer answer : answers) {
                    if (answer.isSelected()) {
                        isAnswer = true;
                        break;
                    }
                }
                return isAnswer;
        }
    }

    public boolean isEmailQuestion() {
        //hardcoded value for email question type id
        return typeId == DYNAMIC_QUESTION_TYPE_EMAIL_ID;
    }

    @Override
    public int compareTo(@NonNull DynamicQuestion otherQuestion) {
        return Integer.valueOf(this.order).compareTo(otherQuestion.order);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DynamicQuestion)) return false;

        DynamicQuestion that = (DynamicQuestion) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    public String getSelectionString() {
        final StringBuilder builder = new StringBuilder("");
        for (DynamicAnswer answer : answers) {
            builder.append(answer.isSelected());
        }
        builder.append(String.valueOf(chkValue));
        return builder.toString();
    }

    public boolean isAnyChanges(final String initialAnswersSelectionString) {
//        Log.d("DynamicQuestion ", "initial " + initialAnswersSelectionString);
//        Log.d("DynamicQuestion ", "current state " + getSelectionString());
        return !TextUtils.equals(getSelectionString(), initialAnswersSelectionString);
    }
}
