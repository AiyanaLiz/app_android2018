package com.stopit.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Alenka on 14.03.2018.
 */

public class DynamicQuestionResult implements Serializable {

    @SerializedName("question_id")
    private long questionId;

    @SerializedName("question_text")
    private String questionText;

    @SerializedName("answer_id")
    private long answerId;

    @SerializedName("answer_text")
    private String answerText;

    @SerializedName("label")
    private String readOnlyLabel;

    @SerializedName("answer")
    private String readOnlyAnswer;

    public DynamicQuestionResult() {
    }

    public DynamicQuestionResult(long questionId, String questionText, long answerId, String answerText) {
        this.questionId = questionId;
        this.questionText = questionText;
        this.answerId = answerId;
        this.answerText = answerText;
    }

    public DynamicQuestionResult(final String questionText, final String answerText) {
        this.readOnlyLabel = questionText;
        this.readOnlyAnswer = answerText;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(long answerId) {
        this.answerId = answerId;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public String getReadOnlyLabel() {
        return readOnlyLabel;
    }

    public void setReadOnlyLabel(String readOnlyLabel) {
        this.readOnlyLabel = readOnlyLabel;
    }

    public String getReadOnlyAnswer() {
        return readOnlyAnswer;
    }

    public void setReadOnlyAnswer(String readOnlyAnswer) {
        this.readOnlyAnswer = readOnlyAnswer;
    }

    /*
    *  public List<String> getReadOnlyAnswer() {
        return readOnlyAnswersList;
    }

    public void setReadOnlyAnswer(List<String> readOnlyAnswer) {
        this.readOnlyAnswersList = readOnlyAnswer;
    }

    public String getRedOnlyAnswerString(){
        if(readOnlyAnswersList == null || readOnlyAnswersList.size() == 0)
            return "";

        final StringBuilder builder = new StringBuilder();
        for(String value : readOnlyAnswersList){
            builder.append(value).append(",");
        }
        builder.substring(0, builder.length() -2);
        return builder.toString();
    }
    *
    * */
}
