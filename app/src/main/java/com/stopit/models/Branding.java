package com.stopit.models;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Alenka on 11.07.2018.
 */
@RealmClass
public class Branding implements RealmModel {

    @PrimaryKey
    private long id;

    @SerializedName("logo_path")
    private String logoUrl;

    @SerializedName("background_image_path")
    private String bgImageUrl;

    @SerializedName("home_screen_message")
    private String homeMessage;

    @SerializedName("disclaimer_text")
    private String disclaimerText;

    @SerializedName("out_of_office_message")
    private String closedText;

    @SerializedName("out_of_office_messenger_message")
    private String closedMessengerText;

    @SerializedName("auto_response_message")
    private String autoResponseText;

    private long organizationId;


    public Branding() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getBgImageUrl() {
        return bgImageUrl;
    }

    public void setBgImageUrl(String bgImageUrl) {
        this.bgImageUrl = bgImageUrl;
    }

    public String getHomeMessage() {
        return homeMessage;
    }

    public void setHomeMessage(String homeMessage) {
        this.homeMessage = homeMessage;
    }

    public String getDisclaimerText() {
        return disclaimerText;
    }

    public void setDisclaimerText(String disclaimerText) {
        this.disclaimerText = disclaimerText;
    }

    public String getClosedText() {
        return closedText;
    }

    public void setClosedText(String closedText) {
        this.closedText = closedText;
    }

    public String getClosedMessengerText() {
        return closedMessengerText;
    }

    public void setClosedMessengerText(String closedMessengerText) {
        this.closedMessengerText = closedMessengerText;
    }

    public String getAutoResponseText() {
        return autoResponseText;
    }

    public void setAutoResponseText(String autoResponseText) {
        this.autoResponseText = autoResponseText;
    }

    public long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(long organizationId) {
        this.organizationId = organizationId;
    }

    public List<String> getURLsToDownload(){
        final List<String> urls = new ArrayList<>();
        if(!TextUtils.isEmpty(logoUrl)) {
            urls.add(logoUrl);
        }else
            Log.d("StopitActivity", "isNeedToDownloadBranding branding LOGO url is null");

        if(!TextUtils.isEmpty(bgImageUrl))
            urls.add(bgImageUrl);
        else
            Log.d("StopitActivity", "isNeedToDownloadBranding branding BG url is null");

        return urls;
    }

    public boolean isUrlsToDownload(){
        return getUrlsToDownloadCount() > 0;
    }

    public int getUrlsToDownloadCount(){
        return getURLsToDownload().size();
    }
}
