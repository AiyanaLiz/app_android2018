package com.stopit.models.data_wrappers;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.stopit.models.Incident;

import java.io.Serializable;

/**
 * Created by Alenka on 18.06.2018.
 */

public class IncidentDataWrapper implements Serializable{

    @SerializedName("incident")
    private Incident incident;

    @SerializedName("out_of_office_message")
    private String afterHoursMessage;

    public IncidentDataWrapper(){}

    public Incident getIncident() {
        return incident;
    }

    public void setIncident(Incident incident) {
        this.incident = incident;
    }

    public String getAfterHoursMessage(){
        return afterHoursMessage;
    }

    public void setAfterHoursMessage(String afterHoursMessage) {
        this.afterHoursMessage = afterHoursMessage;
    }

    public boolean isAfterHoursMessage() {
        return !TextUtils.isEmpty(afterHoursMessage);
    }
}
