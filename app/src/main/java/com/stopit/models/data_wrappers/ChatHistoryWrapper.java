package com.stopit.models.data_wrappers;

import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.stopit.models.messenger.ChatMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alenka on 18.06.2018.
 */

public class ChatHistoryWrapper {

    @SerializedName("read")
    private List<ChatMessage> readMessages;

    @SerializedName("unread")
    private List<ChatMessage> unreadMessages;

    public ChatHistoryWrapper() {
    }

    public List<ChatMessage> getReadMessages() {
        return readMessages;
    }

    public void setReadMessages(List<ChatMessage> readMessages) {
        this.readMessages = readMessages;
    }

    public List<ChatMessage> getUnreadMessages() {
        return unreadMessages;
    }

    public void setUnreadMessages(List<ChatMessage> unreadMessages) {
        this.unreadMessages = unreadMessages;
    }

    public List<ChatMessage> getAllMessages() {
        final List<ChatMessage> messages = new ArrayList<>();
        messages.addAll(unreadMessages);
        messages.addAll(readMessages);
        for(ChatMessage message : messages){
            Log.d("ChatHistoryWrapper", "Message hist "+ message.toString());
        }
        return messages;
    }
}
