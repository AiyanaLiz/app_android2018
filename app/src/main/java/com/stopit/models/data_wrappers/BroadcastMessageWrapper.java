package com.stopit.models.data_wrappers;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.stopit.api.deserializers.BroadcastMessageDeserializer;
import com.stopit.models.BroadcastMessage;
import com.stopit.utils.EncryptionHelper;

/**
 * Created by Alenka on 18.06.2018.
 */

public class BroadcastMessageWrapper {


    @SerializedName("message_object")
    private String encryptedString;


    public BroadcastMessageWrapper() {
    }

    public BroadcastMessage getMessage() {
        final String decrypted = EncryptionHelper.getDecryptedData(encryptedString);
        if (TextUtils.isEmpty(decrypted))
            return null;

        final Gson gson = new GsonBuilder()
                .registerTypeAdapter(BroadcastMessage.class, new BroadcastMessageDeserializer<BroadcastMessage>())
                .create();
        BroadcastMessage message = gson.fromJson(decrypted, BroadcastMessage.class);
        return message;
    }
}
