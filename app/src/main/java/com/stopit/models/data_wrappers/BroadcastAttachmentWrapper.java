package com.stopit.models.data_wrappers;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import static com.stopit.app.Constants.IMAGE_MIME_TYPE;
import static com.stopit.app.Constants.VIDEO_MIME_TYPE;
import static com.stopit.app.Constants.WEB_PAGE_TYPE;

/**
 * Created by Alenka on 18.06.2018.
 */

public class BroadcastAttachmentWrapper implements Serializable {


    @SerializedName("url")
    private String url;

    @SerializedName("mime_type")
    private String mimeType;

    public BroadcastAttachmentWrapper() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getMimeTypeString() {
        if (isImage())
            return IMAGE_MIME_TYPE;
        else if (isVideo())
            return VIDEO_MIME_TYPE;
        else
            return WEB_PAGE_TYPE;
    }

    public boolean isImage() {
        return (!TextUtils.isEmpty(mimeType) && mimeType.contains(IMAGE_MIME_TYPE));
    }

    public boolean isVideo() {
        return (!TextUtils.isEmpty(mimeType) && mimeType.contains(VIDEO_MIME_TYPE));
    }
}
