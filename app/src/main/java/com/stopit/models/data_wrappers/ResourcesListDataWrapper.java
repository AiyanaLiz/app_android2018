package com.stopit.models.data_wrappers;

import com.google.gson.annotations.SerializedName;
import com.stopit.models.Incident;
import com.stopit.models.InformationResource;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Alenka on 18.06.2018.
 */

public class ResourcesListDataWrapper implements Serializable{

    @SerializedName("resources")
    private List<InformationResource> resources;

    public ResourcesListDataWrapper(){}

    public List<InformationResource> getResources() {
        return resources;
    }

    public void setResources(List<InformationResource> resources) {
        this.resources = resources;
    }
}
