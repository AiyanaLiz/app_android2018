package com.stopit.models.data_wrappers;

import com.google.gson.annotations.SerializedName;
import com.stopit.models.Incident;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Alenka on 18.06.2018.
 */

public class IncidentListDataWrapper implements Serializable{

    @SerializedName("incidents")
    private List<Incident> incidents;

    public IncidentListDataWrapper(){}

    public List<Incident> getIncidents() {
        return incidents;
    }

    public void setIncidents(List<Incident> incidents) {
        this.incidents = incidents;
    }
}
