package com.stopit.models.data_wrappers;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.stopit.api.deserializers.ChatMessageDeserializer;
import com.stopit.models.BroadcastMessage;
import com.stopit.models.DynamicQuestion;
import com.stopit.models.Incident;
import com.stopit.models.messenger.ChatMessage;
import com.stopit.utils.EncryptionHelper;

import java.util.List;

/**
 * Created by Alenka on 19.06.2018.
 */

public class StopitResponse<T> {

    @SerializedName("code")
    int code;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    T data;

    public StopitResponse() {
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserData getUserDataDecrypted() {
        final String encryptedString = (String) data;
        if (TextUtils.isEmpty(encryptedString))
            return null;

        String decrypted = null;
        try {
            decrypted = EncryptionHelper.decrypt(EncryptionHelper.getSpecialKey(), encryptedString);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        if (TextUtils.isEmpty(decrypted))
            return null;

        final UserData data = new Gson().fromJson(decrypted, UserData.class);
        return data; //new Gson().fromJson(decrypted, UserData.class);
    }

    public ChatHistoryWrapper getChatHistoryDecrypted() {
        final String encryptedString = (String) data;
        if (TextUtils.isEmpty(encryptedString))
            return null;

        String decrypted = null;
        try {
            decrypted = EncryptionHelper.decrypt(EncryptionHelper.getEncryptionSalt(), encryptedString);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        if (TextUtils.isEmpty(decrypted))
            return null;

        final Gson gson = new GsonBuilder()
                .registerTypeAdapter(ChatMessage.class, new ChatMessageDeserializer<>())
                .create();

        final ChatHistoryWrapper chatHistory = gson.fromJson(decrypted, ChatHistoryWrapper.class);
        return chatHistory;
    }

    public List<DynamicQuestion> getDynamicQuestionsDecrypted() {
        final String decrypted = EncryptionHelper.getDecryptedData((String) data);
        if (TextUtils.isEmpty(decrypted))
            return null;

        final QuestionsDataWrapper questionsWrapper = new Gson().fromJson(decrypted, QuestionsDataWrapper.class);
        return questionsWrapper.getQuestions();
    }

    public List<Incident> getIncidentsListDecrypted() {
        final String decrypted = EncryptionHelper.getDecryptedData((String) data);
        if (TextUtils.isEmpty(decrypted))
            return null;

        final IncidentListDataWrapper incidentsWrapper = new Gson().fromJson(decrypted, IncidentListDataWrapper.class);
        return incidentsWrapper.getIncidents();
    }

    public Incident getIncidentDecrypted() {
        final String decrypted = EncryptionHelper.getDecryptedData((String) data);
        if (TextUtils.isEmpty(decrypted))
            return null;

        final IncidentDataWrapper incidentWrapper = new Gson().fromJson(decrypted, IncidentDataWrapper.class);
        return incidentWrapper.getIncident();
    }

    public BroadcastAttachmentWrapper getBroadcastAttachmentDecrypted() {
        final String decrypted = EncryptionHelper.getDecryptedData((String) data);
        if (TextUtils.isEmpty(decrypted))
            return null;

        final BroadcastAttachmentWrapper wrapper = new Gson().fromJson(decrypted, BroadcastAttachmentWrapper.class);
        return wrapper;
    }

    public UnreadInboxCounter getUnreadInboxCounter() {
        final String decrypted = EncryptionHelper.getDecryptedData((String) data);
        if (TextUtils.isEmpty(decrypted))
            return null;

        return new Gson().fromJson(decrypted, UnreadInboxCounter.class);
    }

    public UnreadMessengerCounter getUnreadMessengerCounter() {
        final String decrypted = EncryptionHelper.getDecryptedData((String) data);
        if (TextUtils.isEmpty(decrypted))
            return null;

        return new Gson().fromJson(decrypted, UnreadMessengerCounter.class);
    }
}
