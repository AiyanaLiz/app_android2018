package com.stopit.models.data_wrappers;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.stopit.api.deserializers.BroadcastMessageDeserializer;
import com.stopit.models.BroadcastMessage;
import com.stopit.utils.EncryptionHelper;

import java.util.List;

/**
 * Created by Alenka on 18.06.2018.
 */

public class BroadcastListWrapper {

    private List<BroadcastMessage> messages;

    @SerializedName("inbox_list")
    private String encryptedString;


    public BroadcastListWrapper() {
    }

    public List<BroadcastMessage> getMessages() {
        final String decrypted = EncryptionHelper.getDecryptedData(encryptedString);
        if (TextUtils.isEmpty(decrypted))
            return null;

        final Gson gson = new GsonBuilder()
                .registerTypeAdapter(BroadcastMessage.class, new BroadcastMessageDeserializer<BroadcastMessage>())
                .create();


        messages = gson.fromJson(decrypted, new TypeToken<List<BroadcastMessage>>() {
        }.getType());
        return messages;
    }
}
