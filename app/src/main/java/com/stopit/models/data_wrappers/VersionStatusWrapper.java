package com.stopit.models.data_wrappers;

import com.google.gson.annotations.SerializedName;
import com.stopit.models.VersionStatus;

/**
 * Created by Alenka on 18.06.2018.
 */

public class VersionStatusWrapper {

//    @SerializedName("ios")
//    private VersionStatus iosVersionStatus;

    @SerializedName("android")
    private VersionStatus androidVersionStatus;


    public VersionStatusWrapper() {
    }


    public VersionStatus getAndroidVersionStatus() {
        return androidVersionStatus;
    }
}
