package com.stopit.models.data_wrappers;

import com.google.gson.annotations.SerializedName;
import com.stopit.models.Branding;
import com.stopit.models.Configuration;
import com.stopit.models.CrisisCenter;
import com.stopit.models.messenger.Messenger;
import com.stopit.models.Organization;
import com.stopit.models.OrgsTreeEntity;
import com.stopit.models.User;

import java.util.List;

/**
 * Created by Alenka on 19.06.2018.
 */

public class UserData {

    @SerializedName("user")
    private User user;

    @SerializedName("org")
    private Organization organization;

    @SerializedName("branding")
    private Branding branding;

    @SerializedName("get_help")
    private List<CrisisCenter> helpContacts;

    @SerializedName("messenger")
    private Messenger messenger;

    @SerializedName("configuration")
    private Configuration configuration;

    ////////// register multilevel result ///////

    @SerializedName("parent_org_id")
    private long parentOrgId;

    @SerializedName("parent_org_name")
    private String parentOrgName;

    @SerializedName("orgs")
    private List<OrgsTreeEntity> organizations;


    public UserData(){}

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public List<CrisisCenter> getHelpContacts() {
        return helpContacts;
    }

    public void setHelpContacts(List<CrisisCenter> helpContacts) {
        this.helpContacts = helpContacts;
    }

    public Messenger getMessenger() {
        return messenger;
    }

    public void setMessenger(Messenger messenger) {
        this.messenger = messenger;
    }

    public List<OrgsTreeEntity> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(List<OrgsTreeEntity> organizations) {
        this.organizations = organizations;
    }

    public Branding getBranding() {
        return branding;
    }

    public void setBranding(Branding branding) {
        this.branding = branding;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public long getParentOrgId() {
        return parentOrgId;
    }

    public void setParentOrgId(long parentOrgId) {
        this.parentOrgId = parentOrgId;
    }

    public String getParentOrgName() {
        return parentOrgName;
    }

    public void setParentOrgName(String parentOrgName) {
        this.parentOrgName = parentOrgName;
    }
}
