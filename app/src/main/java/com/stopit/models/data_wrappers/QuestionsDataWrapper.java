package com.stopit.models.data_wrappers;

import com.google.gson.annotations.SerializedName;
import com.stopit.models.DynamicQuestion;

import java.util.List;

/**
 * Created by Alenka on 19.06.2018.
 */

public class QuestionsDataWrapper {

    @SerializedName("incident_questions")
    List<DynamicQuestion> questions;

    public QuestionsDataWrapper() {
    }

    public List<DynamicQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(List<DynamicQuestion> questions) {
        this.questions = questions;
    }
}
