package com.stopit.models.data_wrappers;

import com.google.gson.annotations.SerializedName;

public class UnreadMessengerCounter {

    @SerializedName("count")
    private int unreadCount;

    public int getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }
}
