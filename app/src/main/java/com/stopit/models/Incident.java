package com.stopit.models;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.stopit.R;
import com.stopit.app.StopitApp;
import com.stopit.db.DBHelper;
import com.stopit.utils.DateUtils;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmModel;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Alenka on 18.06.2018.
 */

@RealmClass
public class Incident implements Serializable, RealmModel, Comparable<Incident> {
    private static final String STATUS_OPEN = "Open";
    private static final String STATUS_CLOSED = "Closed";

    @SerializedName("id")
    @PrimaryKey
    private long id;

    @SerializedName("org_id")
    private long orgId;

    @SerializedName("org_name")
    private String orgName;

    @SerializedName("incident_status")
    private String status;

    @SerializedName("priority")
    private String priority;

    @SerializedName("retrieval_code")
    private String retrievalCode;

    @SerializedName("report_message")
    private String reportMessage;

    @SerializedName("attachment_count")
    private int attachmentsCount;

    @SerializedName("message_count")
    private int messagesCount;

    @SerializedName("created_date")
    private String creationDate;

    @SerializedName("last_updated_date")
    private String lastUpdatedDate;

    @Ignore
    @SerializedName("attachments")
    private List<IncidentAttachment> attachments;

    @Ignore
    @SerializedName("gps_coords")
    private IncidentGPSCoords gpsCoords;

//    @Ignore
//    @SerializedName("questions")
//    private List<IncidentQuestion> questions;

    @Ignore
    @SerializedName("questions")
    private List<DynamicQuestionResult> questionsResults;

    public Incident() {
    }

    public Incident(final long id, final long date, final String status, final String description) {
        this.id = id;

        this.status = status;
        this.reportMessage = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOrgId() {
        return orgId;
    }

    public void setOrgId(long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getReportMessage() {
        return reportMessage;
    }

    public void setReportMessage(String reportMessage) {
        this.reportMessage = reportMessage;
    }

    public int getAttachmentsCount() {
        return attachmentsCount;
    }

    public void setAttachmentsCount(int attachmentsCount) {
        this.attachmentsCount = attachmentsCount;
    }

    public int getMessagesCount() {
        return messagesCount;
    }

    public void setMessagesCount(int messagesCount) {
        this.messagesCount = messagesCount;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(String lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getRetrievalCode() {
        return retrievalCode;
    }

    public void setRetrievalCode(String retrievalCode) {
        this.retrievalCode = retrievalCode;
    }

    public IncidentGPSCoords getGpsCoords() {
        return gpsCoords;
    }

    public void setGpsCoords(IncidentGPSCoords gpsCoords) {
        this.gpsCoords = gpsCoords;
    }

    public List<DynamicQuestionResult> getQuestionsResults() {
        return questionsResults;
    }

    public void setQuestionsResults(List<DynamicQuestionResult> questionsResults) {
        this.questionsResults = questionsResults;
    }

    public List<IncidentAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<IncidentAttachment> attachments) {
        this.attachments = attachments;
    }

    public boolean isClosed() {
        return TextUtils.isEmpty(status) || TextUtils.equals(status, STATUS_CLOSED);
    }

    public String getIdFormatted() {
        return StopitApp.getAppContext().getString(R.string.incident_id_format, id);
    }

    public String getDateFormatted(final String dateString) {
        return DateUtils.getIncidentDateFormatted(dateString);
    }

    public int getStatusTxtResource() {
        return isClosed() ? R.string.incident_status_closed_label : R.string.incident_status_open_label;
    }

    public int getStatusBackground() {
        return isClosed() ? R.drawable.incident_status_closed_bg : R.drawable.incident_status_open_bg;
    }

    @Override
    public int compareTo(@NonNull Incident o) {
        return Long.compare(id, o.getId());
    }

    public boolean isCurrentOrgIncident() {
        return orgId == DBHelper.getCurrentOrganizationId();
    }

}
