package com.stopit.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Alenka on 19.06.2018.
 */

@RealmClass
public class OrgsTreeEntity implements Serializable, RealmModel {


    @PrimaryKey
    @SerializedName("id")
    private long id;

    @SerializedName("top_org_id")
    private long topOrgId;

    @SerializedName("parent_org_id")
    private long parentOrgId;

    @SerializedName("is_parent")
    private boolean isParent;

    @SerializedName("is_leaf")
    private boolean isLeaf;

    @SerializedName("left_index")
    private int leftIndex;

    @SerializedName("right_index")
    private int rightIndex;

    @SerializedName("org_name")
    private String name;

    @SerializedName("country_code")
    private String countryCode;

    @SerializedName("org_status_id")
    private int statusId;

    @SerializedName("depth")
    private int depth;


    public OrgsTreeEntity(){}

    public OrgsTreeEntity(final long id, final String name, final boolean isParent){
        this.id = id;
        this.name = name;
        this.isParent = isParent;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTopOrgId() {
        return topOrgId;
    }

    public void setTopOrgId(long topOrgId) {
        this.topOrgId = topOrgId;
    }

    public long getParentOrgId() {
        return parentOrgId;
    }

    public void setParentOrgId(long parentOrgId) {
        this.parentOrgId = parentOrgId;
    }

    public int getLeftIndex() {
        return leftIndex;
    }

    public void setLeftIndex(int leftIndex) {
        this.leftIndex = leftIndex;
    }

    public int getRightIndex() {
        return rightIndex;
    }

    public void setRightIndex(int rightIndex) {
        this.rightIndex = rightIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public boolean isParent() {
        return isParent;
    }

    public void setParent(boolean parent) {
        isParent = parent;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public void setLeaf(boolean leaf) {
        isLeaf = leaf;
    }
}
