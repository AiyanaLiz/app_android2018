package com.stopit.models;

/**
 * Created by Alenka on 18.06.2018.
 */

public class IncidentMedia {

    private long id;
    private long date;
    private int status;

    public IncidentMedia(final long id, final long date, final int status){
        this. id = id;
        this.date = date;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
