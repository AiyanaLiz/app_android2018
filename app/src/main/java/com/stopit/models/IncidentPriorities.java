package com.stopit.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

/**
 * Created by Alenka on 11.07.2018.
 */
@RealmClass
public class IncidentPriorities implements RealmModel {

    @SerializedName("high")
    private int highId;

    @SerializedName("medium")
    private int mediumId;

    @SerializedName("low")
    private int lowId;

    @SerializedName("default")
    private int defaultId;

    public IncidentPriorities(){}

    public int getHighId() {
        return highId;
    }

    public void setHighId(int highId) {
        this.highId = highId;
    }

    public int getMediumId() {
        return mediumId;
    }

    public void setMediumId(int mediumId) {
        this.mediumId = mediumId;
    }

    public int getLowId() {
        return lowId;
    }

    public void setLowId(int lowId) {
        this.lowId = lowId;
    }

    public int getDefaultId() {
        return defaultId;
    }

    public void setDefaultId(int defaultId) {
        this.defaultId = defaultId;
    }
}
