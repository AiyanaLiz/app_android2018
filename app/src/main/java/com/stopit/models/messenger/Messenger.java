package com.stopit.models.messenger;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Alenka on 11.07.2018.
 */
@RealmClass
public class Messenger implements RealmModel {

    @PrimaryKey
    private long id;

    @SerializedName("is_enabled")
    private Boolean isEnabled;

    @SerializedName("server")
    private ChatServer server;

    @SerializedName("admin")
    private ChatAdmin admin;

    @SerializedName("local_user")
    private LocalUser self;

    @SerializedName("history_message_count")
    private int historyLength;

    public Messenger() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ChatServer getServer() {
        return server;
    }

    public void setServer(ChatServer server) {
        this.server = server;
    }

    public ChatAdmin getAdmin() {
        return admin;
    }

    public void setAdmin(ChatAdmin admin) {
        this.admin = admin;
    }

    public LocalUser getSelf() {
        return self;
    }

    public void setSelf(LocalUser self) {
        this.self = self;
    }

    public int getHistoryLength() {
        return historyLength;
    }

    public void setHistoryLength(int historyLength) {
        this.historyLength = historyLength;
    }
}
