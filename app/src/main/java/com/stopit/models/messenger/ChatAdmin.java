package com.stopit.models.messenger;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Alenka on 11.07.2018.
 */
@RealmClass
public class ChatAdmin implements RealmModel {

    @PrimaryKey
    @SerializedName("username")
    private String name;

    @SerializedName("label")
    private String label;

    public ChatAdmin(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
