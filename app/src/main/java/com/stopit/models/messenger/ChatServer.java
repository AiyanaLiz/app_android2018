package com.stopit.models.messenger;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Alenka on 11.07.2018.
 */

@RealmClass
public class ChatServer implements RealmModel {

    @PrimaryKey
    @SerializedName("host")
    private String url;

    @SerializedName("port")
    private int port;


    public ChatServer(){}

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
