package com.stopit.models.messenger;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.google.gson.annotations.SerializedName;
import com.stopit.utils.DateUtils;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

import static com.stopit.app.Constants.MESSAGE_TYPE_ADMIN_MESSAGE;
import static com.stopit.app.Constants.MESSAGE_TYPE_USER_MESSAGE;

@RealmClass
public class ChatMessage implements RealmModel, MultiItemEntity {

    @PrimaryKey
    @SerializedName("stanza")
    private String uniqueId;

    private long id;

    @SerializedName("body")
    private String message;

    @SerializedName("sentDate")
    private long messageDate;

    @SerializedName("fromJID")
    private String from;

    @SerializedName("toJID")
    private String to;

    @SerializedName("incoming")
    private boolean incoming;

    @SerializedName("conversationID")
    private long conversationId;

    @SerializedName("incidentId")
    private long incidentId;

    public ChatMessage() {
    }

    public ChatMessage(String message, long dateMillis, final String uniqueID, final long incidentId, final boolean incoming) {
        this.message = message;
        this.messageDate = dateMillis;
        this.uniqueId = uniqueID;
        this.incidentId = incidentId;
        this.incoming = incoming;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(long messageDate) {
        this.messageDate = messageDate;
    }

    public boolean isIncoming() {
        return incoming;
    }

    public void setIncoming(boolean incoming) {
        this.incoming = incoming;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public long getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(long incidentId) {
        this.incidentId = incidentId;
    }

    public long getConversationId() {
        return conversationId;
    }

    public void setConversationId(long conversationId) {
        this.conversationId = conversationId;
    }

    @Override
    public int getItemType() {
        return incoming ? MESSAGE_TYPE_ADMIN_MESSAGE : MESSAGE_TYPE_USER_MESSAGE;
    }

    public String getMessageDateFormatted() {
        return DateUtils.getChatDateFormatted(messageDate);
    }

    @Override
    public String toString() {
        return " uniqueId: " + uniqueId + "\n" +
                " id: " + id +
                " incidentId: " + incidentId +
                " incoming: " + incoming +
                " body: " + message +
                " date: " + messageDate +
                " fromJID: " + from +
                " toJid: " + to +
                " convId: " + conversationId;
    }
}
