package com.stopit.models.messenger;

/**
 * Created by MShah on 8/10/17.
 */

public class Conversation {

    private String conversationId;
    private String conversationDateStamp;
    private String conversationUnread;
    private String incidentStatus;


    public Conversation(String id, String dateStamp, String unread, String incidentstatus){
        this.conversationId = id;
        this.conversationDateStamp = dateStamp;
        this.conversationUnread = unread;
        this.incidentStatus = incidentstatus;
    }

    public String getConversationID (){
        return conversationId;
    }

    public String getConversationDateStamp (){
        return conversationDateStamp;
    }

    public String getConversationUnreadCount () {
        return conversationUnread;
    }

    public String getConversationsIncidentStatus (){
        return incidentStatus;
    }

}
