package com.stopit.models;

import com.amazonaws.regions.Regions;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Object model to store data related to Amazon Web Services upload.
 */
@RealmClass
public class AWS implements RealmModel {

    @PrimaryKey
    @SerializedName("cognito_id")
    private String cognitoPoolId;

    @SerializedName("region")
    private String region;

    @SerializedName("bucket")
    private String bucket;

    @Ignore
    private Regions region_code;

    public AWS() {
    }

    public AWS(final String cognitoId, final String region, final String bucket) {
        this.cognitoPoolId = cognitoId;
        this.region = region;
        this.bucket = bucket;
    }

    public Regions getRegion_code() {
        Regions rc = Regions.US_EAST_1;

        switch (this.region) {
            case "us-east-1":
                rc = Regions.US_EAST_1;
                break;
            case "eu-west-1":
                rc = Regions.EU_WEST_1;
                break;
            case "ap-northeast-1":
                rc = Regions.AP_NORTHEAST_1;
                break;
        }

        return rc;
    }

    public Regions getBucketRegion_code() {
        Regions rc = getRegion_code();

        if (getRegion_code() == Regions.EU_WEST_1) {
            rc = Regions.US_WEST_2;
        }

        return rc;
    }

    public String getCognitoPoolId() {
        return cognitoPoolId;
    }

    public void setCognitoPoolId(String cognitoPoolId) {
        this.cognitoPoolId = cognitoPoolId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }
}
