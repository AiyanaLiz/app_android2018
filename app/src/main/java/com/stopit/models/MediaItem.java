package com.stopit.models;

import android.text.TextUtils;
import android.util.Log;

import java.net.URLConnection;

import static com.stopit.app.Constants.IMAGE_MIME_TYPE;
import static com.stopit.app.Constants.VIDEO_MIME_TYPE;

/**
 * Created by Alenka on 19.07.2018.
 */

public class MediaItem {

    private final String path;

    //if temp file
    private boolean shouldBeDeleted;

    public MediaItem(final String path, final boolean shouldBeDeleted) {
        this.path = path;
        this.shouldBeDeleted = shouldBeDeleted;
        Log.d("MediaItem", "path " + path);
    }

    public String getPath() {
        return path;
    }

    public boolean isShouldBeDeleted() {
        return shouldBeDeleted;
    }

    public void setShouldBeDeleted(boolean shouldBeDeleted) {
        this.shouldBeDeleted = shouldBeDeleted;
    }

    public String getFileType() {
        if (TextUtils.isEmpty(path))
            return null;

        String mimeType = URLConnection.guessContentTypeFromName(path);
        if (mimeType.startsWith(IMAGE_MIME_TYPE))
            return IMAGE_MIME_TYPE;

        else if (mimeType.startsWith(VIDEO_MIME_TYPE))
            return VIDEO_MIME_TYPE;

        return null;
    }

    public boolean isVideo(){
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType.startsWith(VIDEO_MIME_TYPE);
    }
}
