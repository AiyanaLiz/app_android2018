package com.stopit.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Alenka on 11.07.2018.
 */

@RealmClass
public class Configuration implements RealmModel {

    @PrimaryKey
    private long id;

    @SerializedName("terms_of_use_url")
    private String termsOfUseUrl;

    @SerializedName("sources")
    private Sources sources;

    @SerializedName("incident_priorities")
    private IncidentPriorities priorities;

    @SerializedName("aws")
    private AWS aws;

    public Configuration() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTermsOfUseUrl() {
        return termsOfUseUrl;
    }

    public void setTermsOfUseUrl(String termsOfUseUrl) {
        this.termsOfUseUrl = termsOfUseUrl;
    }

    public Sources getSources() {
        return sources;
    }

    public void setSources(Sources sources) {
        this.sources = sources;
    }

    public IncidentPriorities getPriorities() {
        return priorities;
    }

    public void setPriorities(IncidentPriorities priorities) {
        this.priorities = priorities;
    }

    public AWS getAws() {
        return aws;
    }

    public void setAws(AWS aws) {
        this.aws = aws;
    }
}
