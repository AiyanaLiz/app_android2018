package com.stopit.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Alenka on 19.06.2018.
 */

@RealmClass
public class Organization implements RealmModel {

    @PrimaryKey
    @SerializedName("org_id")
    private long id;

    @SerializedName("org_name")
    private String name;

    @SerializedName("active")
    private Boolean isActive;

    @SerializedName("org_type_id")
    private int typeId;

    @SerializedName("is_off_hours")
    private Boolean isOffHours;

    @SerializedName("allow_media_upload")
    private boolean isMediaUploadAllowed;

    @SerializedName("is_auto_response_enabled")
    private boolean isAutoResponseEnabled;

    @SerializedName("is_gps_enabled")
    private boolean isGPSEnabled;

    //if org has vactions - false, if org is working - true
    @SerializedName("is_reporting_enabled")
    private boolean isReportingEnabled;

    @SerializedName("country_code")
    private String countryCode;

    private String intellicode;


    public Organization() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public Boolean getOffHours() {
        return isOffHours;
    }

    public void setOffHours(Boolean offHours) {
        isOffHours = offHours;
    }

    public boolean isMediaUploadAllowed() {
        return isMediaUploadAllowed;
    }

    public void setMediaUploadAllowed(boolean mediaUploadAllowed) {
        isMediaUploadAllowed = mediaUploadAllowed;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getIntellicode() {
        return intellicode;
    }

    public void setIntellicode(String intellicode) {
        this.intellicode = intellicode;
    }

    public boolean isAutoResponseEnabled() {
        return isAutoResponseEnabled;
    }

    public void setAutoResponseEnabled(boolean autoResponseEnabled) {
        isAutoResponseEnabled = autoResponseEnabled;
    }

    public boolean isGPSEnabled() {
        return isGPSEnabled;
    }

    public void setGPSEnabled(boolean GPSEnabled) {
        isGPSEnabled = GPSEnabled;
    }

    public boolean isReportingEnabled() {
        return isReportingEnabled;
    }

    public void setReportingEnabled(boolean reportingEnabled) {
        isReportingEnabled = reportingEnabled;
    }
}
