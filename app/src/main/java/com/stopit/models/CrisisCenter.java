package com.stopit.models;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.stopit.R;
import com.stopit.utils.EmailHelper;

import io.realm.RealmModel;
import io.realm.annotations.Ignore;
import io.realm.annotations.RealmClass;

import static com.stopit.app.Constants.CRISIS_CENTER_URL_PREFIX;
import static com.stopit.app.Constants.CRISIS_CENTER_URL_PREFIX_PART;
import static com.stopit.app.Constants.CRISIS_CENTER_URL_PREFIX_PART_1;
import static com.stopit.app.Constants.CRISIS_CENTER_URL_PREFIX_PART_2;
import static com.stopit.app.Constants.MAILTO_PREFIX;

/**
 * Created by Alenka on 19.06.2018.
 */

@RealmClass
public class CrisisCenter implements RealmModel/*, Comparable<CrisisCenter> */ {

    @SerializedName("name")
    private String name;

    @SerializedName("phone")
    private String phoneNumber;

    @SerializedName("mobile_phone")
    private String mobilePhoneNumber;

    @SerializedName("email")
    private String email;

    @SerializedName("website_address")
    private String url;

    @SerializedName("sort_order")
    private int sortOrder;

    @SerializedName("is_priority_contact")
    private boolean isPriorityContact;

    @Ignore
    private int optionsCount = -1;


    public CrisisCenter() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    public void setMobilePhoneNumber(String mobilePhoneNumber) {
        this.mobilePhoneNumber = mobilePhoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getCrisisCenterWebUrl() {
        if (TextUtils.isEmpty(url) || url.contains(CRISIS_CENTER_URL_PREFIX)
                || url.contains(CRISIS_CENTER_URL_PREFIX_PART) || url.contains(CRISIS_CENTER_URL_PREFIX_PART_1)
                || url.contains(CRISIS_CENTER_URL_PREFIX_PART_2))
            return url;
        else
            //return CRISIS_CENTER_URL_PREFIX + url;
            return CRISIS_CENTER_URL_PREFIX_PART_2 + url;
    }

    public boolean isValidEmail() {
        return !TextUtils.isEmpty(getValidEmail());
    }

    public boolean isValidURL() {
        return !TextUtils.isEmpty(getCrisisCenterWebUrl());
    }

    public String getValidEmail() {
        if (EmailHelper.isValidEmail(url))
            return url;
        else if (!TextUtils.isEmpty(url) && url.contains(MAILTO_PREFIX)) {
            final String mailtoEmail = url.replace(MAILTO_PREFIX, "");
            if (EmailHelper.isValidEmail(mailtoEmail))
                return mailtoEmail;
            else return null;
        } else return null;
    }

    public boolean isEmailOrUrl() {
        return (isValidEmail() || isValidURL());
    }

    public int getIconRes() {
        int iconRes = R.drawable.ic_phone;
        int variantsCount = 0;

        if (isValidEmail()) {
            iconRes = isPriorityContact ? R.drawable.ic_mail_black : R.drawable.ic_mail;
            variantsCount++;
        }

        if (!isValidEmail() && isValidURL()) {
            iconRes = isPriorityContact ? R.drawable.ic_www_black : R.drawable.ic_www;
            variantsCount++;
        }

        if (!TextUtils.isEmpty(phoneNumber)) {
            iconRes = isPriorityContact ? R.drawable.ic_phone_black : R.drawable.ic_phone;
            variantsCount++;
        }

        if (!TextUtils.isEmpty(mobilePhoneNumber)) {
            iconRes = isPriorityContact ? R.drawable.ic_sms_black : R.drawable.ic_sms;
            variantsCount++;
        }

        if (variantsCount > 1) {
            return isPriorityContact ? R.drawable.ic_more_options_black : R.drawable.ic_more_options_blue;
        } else
            return iconRes;
    }

    public boolean isPriorityContact() {
        return isPriorityContact;
    }

    public void setPriorityContact(boolean priorityContact) {
        isPriorityContact = priorityContact;
    }
}
