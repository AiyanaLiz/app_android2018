package com.stopit.models;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.annotations.Ignore;

public class IncidentImage implements Serializable {

    private String id;
    //@SerializedName("image_url")
    private String imageUrl;
    //@SerializedName("thumb_url")
    private String thumbUrl;
    //@SerializedName("is_pdf")
    private boolean isPdf;

    @Ignore
    private String localPath;

    public IncidentImage() {

    }

    public IncidentImage(String path) {
        localPath = path;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public boolean isPdf() {
        return isPdf;
    }

    public void setPdf(boolean pdf) {
        isPdf = pdf;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    /**
     * @return true if this Image was added from local resource, so its not needed to use API
     */
    public boolean isLocal() {
        return !TextUtils.isEmpty(localPath);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof IncidentImage)) return false;

        final IncidentImage image = (IncidentImage) o;

        return id != null ? id.equals(image.id) : TextUtils.equals(image.localPath, localPath);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : localPath.hashCode();
    }

}
