package com.stopit.models;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.google.gson.annotations.SerializedName;

import java.io.File;


public class IncidentAttachment {

    @SerializedName("attachment_type")
    private String type;

    @SerializedName("attachment_upload_path")
    private String uploadPath;

    public IncidentAttachment() {
    }

    public IncidentAttachment(final Context context, final File file, final String path) {
        this.type = getMimeType(context, file);
        this.uploadPath = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    private String getMimeType(final Context context, final File file) {
        if(file == null)
            return null;

        final Uri uri = Uri.fromFile(file);
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getContentResolver();
            mimeType = cr.getType(uri);
        }

        if (!TextUtils.isEmpty(mimeType)) {
            Log.d("IncidentAttachment", "IncidentAttachment mimeType " + mimeType);
            return mimeType;
        }

        String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                .toString());
        mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                fileExtension.toLowerCase());

        Log.d("IncidentAttachment", "IncidentAttachment mimeType " + mimeType);
        return mimeType;
    }
}
