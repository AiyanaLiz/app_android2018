package com.stopit.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Alenka on 19.06.2018.
 */

@RealmClass
public class User implements RealmModel {

    @PrimaryKey
    @SerializedName("user_id")
    private long id;

    @SerializedName("intellicode")
    private String intelliCode;

    @SerializedName("salt")
    private String salt;

    private long currentOrgId;

    public User(){}

    public String getIntelliCode() {
        return intelliCode;
    }

    public void setIntelliCode(String intelliCode) {
        this.intelliCode = intelliCode;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCurrentOrgId() {
        return currentOrgId;
    }

    public void setCurrentOrgId(long currentOrgId) {
        this.currentOrgId = currentOrgId;
    }
}
