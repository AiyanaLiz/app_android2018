package com.stopit.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alenka on 18.06.2018.
 */

public class VersionStatus {

    @SerializedName("min_os_version")
    private int minOsVersion;

    @SerializedName("minversion")
    private int minVersion;

    @SerializedName("currversion")
    private float currentVersion;

    @SerializedName("updatelink")
    private String urlForUpdate;

    @SerializedName("active_now")
    private int activeNow;

    @SerializedName("inactive_message")
    private String inactiveMsgUrl;

    public VersionStatus() {
    }

    public int getMinOsVersion() {
        return minOsVersion;
    }

    public void setMinOsVersion(int minOsVersion) {
        this.minOsVersion = minOsVersion;
    }

    public int getMinVersion() {
        return minVersion;
    }

    public void setMinVersion(int minVersion) {
        this.minVersion = minVersion;
    }

    public float getCurrentVersion() {
        return currentVersion;
    }

    public void setCurrentVersion(int currentVersion) {
        this.currentVersion = currentVersion;
    }

    public String getUrlForUpdate() {
        return urlForUpdate;
    }

    public void setUrlForUpdate(String urlForUpdate) {
        this.urlForUpdate = urlForUpdate;
    }

    public int getActiveNow() {
        return activeNow;
    }

    public void setActiveNow(int activeNow) {
        this.activeNow = activeNow;
    }

    public String getInactiveMsgUrl() {
        return inactiveMsgUrl;
    }

    public void setInactiveMsgUrl(String inactiveMsgUrl) {
        this.inactiveMsgUrl = inactiveMsgUrl;
    }
}
