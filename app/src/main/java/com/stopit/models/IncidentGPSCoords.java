package com.stopit.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Alenka on 14.03.2018.
 */


public class IncidentGPSCoords implements Serializable {

    @SerializedName("long")
    private double longitude;

    @SerializedName("lat")
    private double latitude;

    public IncidentGPSCoords() {
    }

    public IncidentGPSCoords(final double longitude, final double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
