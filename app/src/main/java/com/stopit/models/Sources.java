package com.stopit.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

/**
 * Created by Alenka on 11.07.2018.
 */
@RealmClass
public class Sources implements RealmModel {

    @SerializedName("stopit_app")
    private int appSourceId;

    @SerializedName("stopit_web")
    private int webSourceId;

    @SerializedName("stopit_admin")
    private int adminSourceId;

    public Sources() {
    }

    public int getAppSourceId() {
        return appSourceId;
    }

    public void setAppSourceId(int appSourceId) {
        this.appSourceId = appSourceId;
    }

    public int getWebSourceId() {
        return webSourceId;
    }

    public void setWebSourceId(int webSourceId) {
        this.webSourceId = webSourceId;
    }

    public int getAdminSourceId() {
        return adminSourceId;
    }

    public void setAdminSourceId(int adminSourceId) {
        this.adminSourceId = adminSourceId;
    }
}
