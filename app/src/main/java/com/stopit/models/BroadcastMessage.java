package com.stopit.models;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.stopit.utils.DateUtils;

import java.io.Serializable;

import io.realm.RealmModel;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

import static com.stopit.app.Constants.INBOX_MSG_STATUS_NEW;
import static com.stopit.app.Constants.INBOX_MSG_STATUS_READ;
import static com.stopit.app.Constants.ST_INBOX_ATTACHMENT_TYPE_IMAGE;

@RealmClass
public class BroadcastMessage implements Serializable, RealmModel{


    @PrimaryKey
    @SerializedName("message_id")
    private String id;

    @SerializedName("subject")
    private String title;

    @Ignore
    @SerializedName("body")
    private String message;

    @SerializedName("created_date")
    private long date;

    @SerializedName("message_status_id")
    private int statusId;

    @SerializedName("mime_type")
    private int mimeType = 0;

    private boolean isSelected;

    public BroadcastMessage() {
    }

    public BroadcastMessage(
            final String id,
            final String title,
            final String message,
            final long date, final int statusId) {

        this.id = id;
        this.title = title;
        this.message = message;
        this.date = date;
        this.isSelected = false;
        this.statusId = statusId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDateFormatted() {
        return DateUtils.getDateFormatted(date);
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public int getMimeType() {
        return mimeType;
    }

    public void setMimeType(int mimeType) {
        this.mimeType = mimeType;
    }

//    int INBOX_MSG_STATUS_NEW = 3;
//    int INBOX_MSG_STATUS_READ = 2;
//    int INBOX_MSG_STATUS_DELETED = 1;
//
    public boolean isRead() {
        return statusId == INBOX_MSG_STATUS_READ;
    }

    public boolean isMediaAttached(){
        return mimeType > 0;
    }

    public boolean isImageAttached(){
        return mimeType == ST_INBOX_ATTACHMENT_TYPE_IMAGE ;
    }

//    switch (status) {
//        case 0:
//            return INBOX_MESSAGE_STATUS_NEW;
//        case 1:
//            return INBOX_MESSAGE_STATUS_NEW;
//        case 2:
//        case 7:
//            return INBOX_MESSAGE_STATUS_READ;
//        case 3:
//            return INBOX_MESSAGE_STATUS_NEW;
//        case 4:
//            return INBOX_MESSAGE_STATUS_NEW;
//        case 5:
//            return INBOX_MESSAGE_STATUS_DELETE;
//        default:
//            return INBOX_MESSAGE_STATUS_NEW;
//    }


}
