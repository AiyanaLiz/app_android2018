package com.stopit.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

@RealmClass
public class ResourceCategory implements RealmModel{

    @SerializedName("name")
    private String name;

    public ResourceCategory(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
