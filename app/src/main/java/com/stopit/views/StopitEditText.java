package com.stopit.views;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.widget.TextView;

import com.stopit.R;
import com.stopit.utils.BrandingHelper;
import com.stopit.utils.FontHelper;

import java.lang.reflect.Field;

/**
 * Created by Alenka on 05.07.2018.
 */

public class StopitEditText extends AppCompatEditText {
    public StopitEditText(Context context) {
        super(context);
        applyBranding();
    }

    public StopitEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        FontHelper.setCustomFont(this, context, attrs);
        applyBranding();
    }

    public StopitEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        FontHelper.setCustomFont(this, context, attrs);
        applyBranding();
    }

    private void applyBranding() {
        if (!BrandingHelper.isBranding())
            return;

        try {
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(this, R.drawable.cursor_drawable);
        } catch (Exception ignored) {
            //do nothing
        }
    }
}
