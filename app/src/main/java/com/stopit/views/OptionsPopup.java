package com.stopit.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.stopit.R;

import static com.stopit.app.Constants.OPTIONS_POPUP_ACTION_PICK_PHOTO;
import static com.stopit.app.Constants.OPTIONS_POPUP_ACTION_PICK_VIDEO;
import static com.stopit.app.Constants.OPTIONS_POPUP_ACTION_TAKE_PHOTO;
import static com.stopit.app.Constants.OPTIONS_POPUP_ACTION_TAKE_VIDEO;


public class OptionsPopup implements PopupWindow.OnDismissListener {

    private final Animation animShowPopup;
    private final Animation animHidePopup;
    Context context;
    LayoutInflater inflater;
    View view;     //popup view
    private OnOptionsPopupChoiceListener listener;
    private PopupWindow popupWindow;
    private boolean isVisible;
    private Object tag;
    private View rootView;
    private RelativeLayout contentLayout;
    private final OnClickListener contentLayoutListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (listener != null)
                listener.handleOptionsPopupCancel();

            dismissPopup();
        }
    };
    private View takePhotoView;
    private View takeVideoView;
    private View pickPhotoView;
    private View pickVideoView;
    private View cancelView;

    private OnClickListener optionClickListener = new OnClickListener() {
        @Override
        public void onClick(View clickedView) {
            if (clickedView == cancelView) {
                dismissPopup();
                return;
            }

            final int actionId;
            if (clickedView == takePhotoView)
                actionId = OPTIONS_POPUP_ACTION_TAKE_PHOTO;

            else if (clickedView == takeVideoView)
                actionId = OPTIONS_POPUP_ACTION_TAKE_VIDEO;

            else if (clickedView == pickPhotoView)
                actionId = OPTIONS_POPUP_ACTION_PICK_PHOTO;

            else //(clickedView == pickVideoView)
                actionId = OPTIONS_POPUP_ACTION_PICK_VIDEO;

            processPopupChoice(actionId);
        }
    };

    public OptionsPopup(Activity context) {
        this.context = context;
        this.rootView = ((ViewGroup) context.findViewById(android.R.id.content)).getChildAt(0);
        this.inflater = context.getLayoutInflater();
        animShowPopup = AnimationUtils.loadAnimation(context, R.anim.show_popup);
        animHidePopup = AnimationUtils.loadAnimation(context, R.anim.hide_popup);
        animHidePopup.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        popupWindow.dismiss();
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void dismissPopup() {
        if (!isVisible)
            return;
        isVisible = false;
        contentLayout.startAnimation(animHidePopup);
    }

    public void show() {
        if (popupWindow != null && popupWindow.isShowing())
            return;

        contentLayout = new RelativeLayout(context);
        contentLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
//        contentLayout.setOrientation(LinearLayout.VERTICAL);
        contentLayout.setGravity(Gravity.CENTER);
        contentLayout.setBackgroundColor(context.getResources().getColor(R.color.popup_gray_bg));
        contentLayout.setClickable(true);
        contentLayout.setOnClickListener(contentLayoutListener);
        contentLayout.setSoundEffectsEnabled(false);

        view = inflater.inflate(R.layout.options_popup_view, contentLayout, false);
        contentLayout.addView(view);

        popupWindow = new PopupWindow(contentLayout, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupWindow.showAtLocation(rootView, Gravity.CENTER, 0, 0);

        takePhotoView = view.findViewById(R.id.popup_option_take_photo);
        takePhotoView.setOnClickListener(optionClickListener);

        takeVideoView = view.findViewById(R.id.popup_option_take_video);
        takeVideoView.setOnClickListener(optionClickListener);

        pickPhotoView = view.findViewById(R.id.popup_option_pick_photo);
        pickPhotoView.setOnClickListener(optionClickListener);

        pickVideoView = view.findViewById(R.id.popup_option_pick_video);
        pickVideoView.setOnClickListener(optionClickListener);

        cancelView = view.findViewById(R.id.popup_option_cancel);
        cancelView.setOnClickListener(optionClickListener);

        popupWindow.setFocusable(true);
        isVisible = true;

        contentLayout.startAnimation(animShowPopup);
    }

    private void processPopupChoice(final int actionId) {
        if (listener != null)
            listener.handleOptionsPopupChoice(this, actionId);
        dismissPopup();
    }

    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }

    public void setOnOptionsPopupChoiceListener(OnOptionsPopupChoiceListener listener) {
        this.listener = listener;
    }

    @Override
    public void onDismiss() {
        // when back button pressed
        if (!isVisible)
            return;
        isVisible = false;
    }

    @Override
    protected void finalize() throws Throwable {
        this.listener = null;
        //this.popupListView = null;
        this.popupWindow = null;
        this.context = null;
        this.rootView = null;
        this.inflater = null;
        super.finalize();
    }

    public interface OnOptionsPopupChoiceListener {
        void handleOptionsPopupChoice(OptionsPopup popup, int actionId);

        void handleOptionsPopupCancel();
    }

}