package com.stopit.views;

import android.content.Context;
import android.util.AttributeSet;

import com.stopit.utils.FontHelper;


/**
 * Created by Alenka on 06.07.2018.
 */

public class StopitRadioButton extends android.support.v7.widget.AppCompatRadioButton {
    public StopitRadioButton(Context context) {
        super(context);
    }

    public StopitRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        FontHelper.setCustomFont(this, context, attrs);
    }

    public StopitRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        FontHelper.setCustomFont(this, context, attrs);
    }
}
