package com.stopit.views;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import com.stopit.R;
import com.stopit.activity.MediaViewerActivity;
import com.stopit.models.MediaItem;
import com.stopit.utils.Imager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.stopit.app.Constants.ST_EXTRAS_KEY_ATTACHMENT_TYPE;
import static com.stopit.app.Constants.ST_EXTRAS_KEY_ATTACHMENT_URL;


/**
 * Created by Alenka on 16.07.2018.
 */

public class MediaPreviewLayout extends LinearLayout {


    private final ArrayList<View> addedViews = new ArrayList<>();
    private LayoutInflater inflater;
    private HorizontalScrollView scrollView;
    private LinearLayout horizontalWrapper;
    //private View btnAdd;
    private View addRipple;
    private List<MediaItem> mItems;
    final OnClickListener deleteBtnListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            final MediaItem itemToDelete = (MediaItem) view.getTag();
            boolean success = mItems.remove(itemToDelete);
            final View parent = (View) view.getParent().getParent();
            horizontalWrapper.removeView(parent);
            if (itemToDelete.isShouldBeDeleted()) {
                File file = new File(itemToDelete.getPath());
                if (file.exists() && file.canWrite()) {
                    boolean isDeleted = file.delete();
                    Log.d("ReportActivity", "deleted file " + isDeleted + " path " + itemToDelete.getPath());
                }
            }
        }
    };

    private Context context;
    final OnClickListener photoClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            final MediaItem item = (MediaItem) ((RoundedImagePreview) view).getTaggedObject();
            if (item == null || TextUtils.isEmpty(item.getPath()))
                return;

            final String fileType = item.getFileType();
            if (TextUtils.isEmpty(fileType))
                return;

            final Intent intent = new Intent(context, MediaViewerActivity.class);
            intent.putExtra(ST_EXTRAS_KEY_ATTACHMENT_TYPE, fileType);
            intent.putExtra(ST_EXTRAS_KEY_ATTACHMENT_URL, item.getPath());
            context.startActivity(intent);
        }
    };


    public MediaPreviewLayout(Context context) {
        this(context, null);
    }

    public MediaPreviewLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs, 0);
        this.context = context;
        inflateLayout(context);
    }

    private void inflateLayout(final Context context) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = null;

        if (inflater != null)
            rootView = inflater.inflate(R.layout.media_preview_layout, this);

        if (rootView == null)
            return;

        scrollView = rootView.findViewById(R.id.media_preview_scroll_view);
        horizontalWrapper = rootView.findViewById(R.id.media_preview_internal_wrapper);
        //btnAdd = rootView.findViewById(R.id.media_preview_add_btn);
        addRipple = rootView.findViewById(R.id.media_add_btn_ripple);
    }

    public void init(final List<MediaItem> images,
                     final OnClickListener photoBtnListener) {

        this.mItems = images;
        addRipple.setOnClickListener(photoBtnListener);
    }

    public void addPhotoView(final Context context, final MediaItem item) {
        final View mediaView = inflater.inflate(R.layout.item_media_preview, null);
        final RoundedImagePreview photoView = mediaView.findViewById(R.id.rounded_image);
        photoView.setTaggedObject(item);
        photoView.setOnClickListener(photoClickListener);
        Imager.loadTumbnail(context, item.getPath(), photoView);
        if (item.isVideo()) {
            final View playBtn = mediaView.findViewById(R.id.play_video_btn);
            playBtn.setVisibility(VISIBLE);
        }

        final View deleteBtn = mediaView.findViewById(R.id.delete_image_btn);
        deleteBtn.setTag(item);
        deleteBtn.setOnClickListener(deleteBtnListener);

        horizontalWrapper.addView(mediaView);

        //scroll to last position
        postDelayed(new Runnable() {
            public void run() {
                scrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        }, 100L);
    }
}
