package com.stopit.views;

import android.content.Context;
import android.util.AttributeSet;

import com.stopit.R;
import com.stopit.utils.FontHelper;

import static com.stopit.app.Constants.STOPIT_VIEW_DISABLED_ALPHA;
import static com.stopit.app.Constants.STOPIT_VIEW_ENABLED_ALPHA;


/**
 * Created by Alenka on 05.07.2018.
 */

public class StopitButton extends android.support.v7.widget.AppCompatButton {

    public StopitButton(Context context) {
        super(context);
    }

    public StopitButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        FontHelper.setCustomFont(this, context, attrs);
        setBackgroundColor(getResources().getColor(R.color.transparent));
    }

    public StopitButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        FontHelper.setCustomFont(this, context, attrs);
        setBackgroundColor(getResources().getColor(R.color.transparent));
    }

    @Override
    public void setEnabled(boolean isEnabled) {
        super.setEnabled(isEnabled);
        setAlpha(isEnabled ? STOPIT_VIEW_ENABLED_ALPHA : STOPIT_VIEW_DISABLED_ALPHA);
//        final ViewParent parent = getParent();
//        if(parent != null && parent instanceof CardView){
//            ((CardView)parent).setCardBackgroundColor(isEnabled ? getResources().getColor(R.color.card_elevation_gray) : getResources().getColor(R.color.transparent));
//        }
    }
}
