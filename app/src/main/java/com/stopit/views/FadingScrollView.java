package com.stopit.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

import com.stopit.R;


/**
 * Created by Alenka on 26.03.2018.
 */

public class FadingScrollView extends ScrollView {
    private int fadingEdgeColor;
    private OnScrollChangedCallback callback;


    public FadingScrollView(Context context) {
        super(context);
        init(context);
    }

    public FadingScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FadingScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.fadingEdgeColor = context.getResources().getColor(R.color.white);
    }

    @Override
    protected float getTopFadingEdgeStrength() {
        return 0.0f;
    }

    @Override
    public int getSolidColor() {
        return fadingEdgeColor;
    }

//    @Override
//    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
//        if(callback != null)
//            callback.animateScrollShadow();
//        super.onScrollChanged(l, t, oldl, oldt);
//    }

    public void setOnScrollChangedCallback(final OnScrollChangedCallback callback) {
        this.callback = callback;
    }

//    @Override
//    public boolean onTouchEvent(MotionEvent ev) {
//        if (ev.getAction() == MotionEvent.ACTION_MOVE) {
//            Log.d("FadingScrollView", "motion >>>>>"+ev.getAction());
//            if (callback != null)
//                callback.animateScrollShadow();
//        }
//        return super.onTouchEvent(ev);
//    }

    public interface OnScrollChangedCallback {
        void animateScrollShadow();
    }
}
