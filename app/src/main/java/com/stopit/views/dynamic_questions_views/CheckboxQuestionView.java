package com.stopit.views.dynamic_questions_views;


import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.kyleduo.switchbutton.SwitchButton;
import com.stopit.R;


public class CheckboxQuestionView extends QuestionView {
    private SwitchButton answerSwitch;


    public CheckboxQuestionView(Context context) {
        super(context);
    }

    public CheckboxQuestionView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CheckboxQuestionView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    void init(Context context) {
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;
        if (inflater != null)
            view = inflater.inflate(R.layout.dynamic_chk_question_layout, this);

        if (view == null)
            return;

        questionTxt = view.findViewById(R.id.dynamic_question_txt);
        answerTxt = view.findViewById(R.id.dynamic_answer_txt);
        answerSwitch = view.findViewById(R.id.dynamic_answer_switch);


    }

    public void setChkAnswer(final boolean isChecked) {
        answerSwitch.setChecked(isChecked);
    }
}