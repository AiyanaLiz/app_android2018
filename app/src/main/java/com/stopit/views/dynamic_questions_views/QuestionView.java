package com.stopit.views.dynamic_questions_views;


import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stopit.R;


public class QuestionView extends LinearLayout implements TextWatcher, View.OnFocusChangeListener {
    int questionPosition;
    TextView questionTxt;
    TextView answerTxt;
    OnAnswerChangedListener answerChangedListener;

    public QuestionView(Context context) {
        super(context);
        init(context);
    }

    public QuestionView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public QuestionView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    void init(Context context) {
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = null;

        if (inflater != null)
            view = inflater.inflate(R.layout.dynamic_question_layout, this);

        if (view == null)
            return;

        questionTxt = (TextView) view.findViewById(R.id.dynamic_question_txt);
        answerTxt = (TextView) view.findViewById(R.id.dynamic_answer_txt);
    }

    public void setQuestionLabel(final String label) {
        questionTxt.setText(label);
    }

    public int getQuestionPosition() {
        return questionPosition;
    }

    public void setQuestionPosition(final int position) {
        this.questionPosition = position;
    }


    public void setAnswer(final String answer) {
        if (TextUtils.isEmpty(answer)) {
            answerTxt.setText("");
            answerTxt.setVisibility(GONE);
        } else {
            answerTxt.setText(answer);
            answerTxt.setVisibility(VISIBLE);
        }
    }

    public void setOnAnswerChangedListener(final OnAnswerChangedListener listener) {
        this.answerChangedListener = listener;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    public void setQuestionHint(final String hint) {

    }

    public interface OnAnswerChangedListener {
        void manageAnswerChanged(final int questionPosition, final String userInput);
    }

//    public void setAnswerClickListener(final OnClickListener clickListener) {
//    }
}