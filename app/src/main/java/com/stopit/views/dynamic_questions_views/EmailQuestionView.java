package com.stopit.views.dynamic_questions_views;


import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.stopit.R;


public class EmailQuestionView extends TextQuestionView {

//    private TextView errorView;
//    private String errorMsg;

    public EmailQuestionView(Context context) {
        super(context);
        init(context);
    }

    public EmailQuestionView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public EmailQuestionView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    void init(Context context) {
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = null;

        if (inflater != null)
            view = inflater.inflate(R.layout.dynamic_email_question_layout, this);

        if (view == null)
            return;

        questionTxt = (TextView) view.findViewById(R.id.dynamic_question_txt);
        answerTxt = (TextView) view.findViewById(R.id.dynamic_answer_txt);
        answerTxt.addTextChangedListener(this);

//        errorView = (TextView) view.findViewById(R.id.dynamic_error_txt);
//        errorMsg = context.getString(R.string.invalid_email_address);
    }

    public void setQuestionHint(final String hint){
        answerTxt.setHint(hint);
    }

//    @Override
//    public void afterTextChanged(Editable s) {
//        errorView.setVisibility((TextUtils.isEmpty(s) || !EmailHelper.isValidEmail(s.toString())) ? VISIBLE : INVISIBLE);
//    }
}