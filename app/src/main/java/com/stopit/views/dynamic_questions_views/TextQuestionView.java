package com.stopit.views.dynamic_questions_views;


import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.stopit.R;


public class TextQuestionView extends QuestionView {

    public TextQuestionView(Context context) {
        super(context);
    }

    public TextQuestionView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TextQuestionView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    void init(Context context) {
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;
        if (inflater != null)
            view = inflater.inflate(R.layout.dynamic_text_question_layout, this);

        if (view == null)
            return;

        questionTxt = (TextView) view.findViewById(R.id.dynamic_question_txt);
        answerTxt = (TextView) view.findViewById(R.id.dynamic_answer_txt);
        answerTxt.setImeOptions(EditorInfo.IME_ACTION_DONE);
        answerTxt.setRawInputType(InputType.TYPE_CLASS_TEXT);

        answerTxt.addTextChangedListener(this);
    }

    public void setQuestionHint(final String hint){
        answerTxt.setHint(hint);
    }

    public void setAnswer(final String answer) {
        //should ignore thid method call
        //user types answer via editText
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (answerChangedListener != null)
            answerChangedListener.manageAnswerChanged(questionPosition, s.toString().trim());
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
            final String input = answerTxt.getText().toString().trim();
            answerTxt.setText(input);
        }
    }
}