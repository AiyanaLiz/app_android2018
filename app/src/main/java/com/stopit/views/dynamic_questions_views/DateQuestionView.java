package com.stopit.views.dynamic_questions_views;


import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.stopit.R;


public class DateQuestionView extends QuestionView {

    public DateQuestionView(Context context) {
        super(context);
        init(context);
    }

    public DateQuestionView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DateQuestionView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    void init(Context context) {
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = null;

        if (inflater != null)
            view = inflater.inflate(R.layout.dynamic_date_question_layout, this);

        if (view == null)
            return;

        questionTxt = view.findViewById(R.id.dynamic_question_txt);
        answerTxt = view.findViewById(R.id.dynamic_answer_txt);
    }

    public void setAnswer(final String answer) {
        if (!TextUtils.isEmpty(answer)) {
            answerTxt.setVisibility(VISIBLE);
            answerTxt.setText(answer);
        }else{
            answerTxt.setVisibility(GONE);
            answerTxt.setText("");
        }
    }
}