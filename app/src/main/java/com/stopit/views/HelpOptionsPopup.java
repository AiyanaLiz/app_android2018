package com.stopit.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.stopit.R;
import com.stopit.models.CrisisCenter;
import com.stopit.models.HelpOption;
import com.stopit.utils.FontHelper;

import java.util.List;

import static com.stopit.app.Constants.HELP_OPTION_MAKE_A_CALL;
import static com.stopit.app.Constants.HELP_OPTION_SEND_AN_EMAIL;
import static com.stopit.app.Constants.HELP_OPTION_SEND_SMS;


public class HelpOptionsPopup implements PopupWindow.OnDismissListener {

    private final Animation animShowPopup;
    private final Animation animHidePopup;
    Context context;
    LayoutInflater inflater;
    View view;     //popup view
    private OnHelpOptionChoiceListener listener;
    private PopupWindow popupWindow;
    private boolean isVisible;
    private Object tag;
    private View rootView;
    private RelativeLayout contentLayout;
    private final OnClickListener contentLayoutListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (listener != null)
                listener.handleHelpOptionsCancel();

            dismissPopup();
        }
    };
    private View cancelBtn;


    private OnClickListener optionClickListener = new OnClickListener() {
        @Override
        public void onClick(View clickedView) {
            final Object tag = clickedView.getTag();
            if(tag == null)
                return;

            final HelpOption option = (HelpOption) tag;
            processPopupChoice(option.getType(), option.getValue());
        }
    };

    public HelpOptionsPopup(Activity context) {
        this.context = context;
        this.rootView = ((ViewGroup) context.findViewById(android.R.id.content)).getChildAt(0);
        this.inflater = context.getLayoutInflater();
        animShowPopup = AnimationUtils.loadAnimation(context, R.anim.show_popup);
        animHidePopup = AnimationUtils.loadAnimation(context, R.anim.hide_popup);
        animHidePopup.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        popupWindow.dismiss();
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void dismissPopup() {
        if (!isVisible)
            return;
        isVisible = false;
        contentLayout.startAnimation(animHidePopup);
    }

    public void show(final String title, final List<HelpOption> optionsList) {
        if (popupWindow != null && popupWindow.isShowing())
            return;

        contentLayout = new RelativeLayout(context);
        contentLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
//        contentLayout.setOrientation(LinearLayout.VERTICAL);
        contentLayout.setGravity(Gravity.CENTER);
        contentLayout.setBackgroundColor(context.getResources().getColor(R.color.popup_gray_bg));
        contentLayout.setClickable(true);
        contentLayout.setOnClickListener(contentLayoutListener);
        contentLayout.setSoundEffectsEnabled(false);

        view = inflater.inflate(R.layout.help_popup_view, contentLayout, false);
        contentLayout.addView(view);

        if (!TextUtils.isEmpty(title)) {
            final StopitTextView titleTxt = view.findViewById(R.id.hepl_option_title_view);
            titleTxt.setText(title);
        }

        popupWindow = new PopupWindow(contentLayout, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupWindow.showAtLocation(rootView, Gravity.CENTER, 0, 0);

        final RecyclerView mListView = view.findViewById(R.id.help_options_list_view);
        mListView.setLayoutManager(new LinearLayoutManager(context));
        final OptionsListAdapter<HelpOption> mAdapter = new OptionsListAdapter<>(optionsList);
        mListView.setAdapter(mAdapter);

        cancelBtn = view.findViewById(R.id.help_option_cancel_button);
        cancelBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissPopup();
            }
        });

        popupWindow.setFocusable(true);
        isVisible = true;

        contentLayout.startAnimation(animShowPopup);
    }

    private void processPopupChoice(final int actionId, final String value) {
        if (listener != null)
            listener.handleHelpOptionsChoice(this, actionId, value);
        dismissPopup();
    }

    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }

    public void setOnHelpOptionChoiceListener(OnHelpOptionChoiceListener listener) {
        this.listener = listener;
    }

    @Override
    public void onDismiss() {
        // when back button pressed
        if (!isVisible)
            return;
        isVisible = false;
    }

    @Override
    protected void finalize() throws Throwable {
        this.listener = null;
        //this.popupListView = null;
        this.popupWindow = null;
        this.context = null;
        this.rootView = null;
        this.inflater = null;
        super.finalize();
    }

    public interface OnHelpOptionChoiceListener {
        void handleHelpOptionsChoice(HelpOptionsPopup popup, int actionId, final String value);

        void handleHelpOptionsCancel();
    }

    private class OptionsListAdapter<T extends HelpOption> extends BaseQuickAdapter<T, BaseViewHolder> {

        public OptionsListAdapter(final List<T> options) {
            super(R.layout.item_help_option, options);
        }

        @Override
        protected void convert(BaseViewHolder viewHolder, HelpOption item) {
            viewHolder.itemView.setTag(item);
            viewHolder.itemView.setOnClickListener(optionClickListener);

            int textRes = 0;
            switch (item.getType()) {
                case HELP_OPTION_MAKE_A_CALL:
                    textRes = R.string.help_option_call_label;
                    break;
                case HELP_OPTION_SEND_SMS:
                    textRes = R.string.help_option_sms_label;
                    break;
                case HELP_OPTION_SEND_AN_EMAIL:
                    textRes = R.string.help_option_email_label;
                    break;
                //case HELP_OPTION_URL:
                default:
                    textRes = R.string.help_option_url_label;

            }
            final TextView txtView = viewHolder.getView(R.id.help_option_txt);
            FontHelper.setCustomFont(txtView, "fonts/Montserrat-Medium.ttf", context);
            txtView.setText(context.getString(textRes, item.getValue()));
        }
    }
}