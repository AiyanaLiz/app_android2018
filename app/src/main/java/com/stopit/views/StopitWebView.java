package com.stopit.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Custom WebView  to handle enabling/ disabling buttons
 */
public class StopitWebView extends WebView {
    private OnScrollChangedCallback mOnScrollChangedCallback;

    public StopitWebView(final Context context) {
        super(context);
        setWebClient();
        enableJavaScript();
    }

    public StopitWebView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        setWebClient();
        enableJavaScript();
    }

    public StopitWebView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        setWebClient();
        enableJavaScript();
    }

    private void enableJavaScript(){
        getSettings().setJavaScriptEnabled(true);
    }

    private void setWebClient() {
        setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                Log.d("demo url", "url " + url);
                return true;
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if(mOnScrollChangedCallback != null)
                    mOnScrollChangedCallback.onLoadPageStarted();
            }

            // This method will be triggered when the Page loading is completed

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if(mOnScrollChangedCallback != null)
                    mOnScrollChangedCallback.onLoadPageFinished();
            }

            // This method will be triggered when error page appear

            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                if(mOnScrollChangedCallback != null)
                    mOnScrollChangedCallback.onLoadPageFinished();

                super.onReceivedError(view, errorCode, description, failingUrl);
            }
        });
    }


    @Override
    protected void onScrollChanged(final int l, final int t, final int oldl, final int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (mOnScrollChangedCallback == null)
            return;

        int height = (int) Math.floor(getContentHeight() * getScale() * .5);
        if (t + getMeasuredHeight() >= height) {
            mOnScrollChangedCallback.onScrollEndReached();
        }
    }

    public void setOnScrollChangedCallback(final OnScrollChangedCallback onScrollChangedCallback) {
        mOnScrollChangedCallback = onScrollChangedCallback;
    }

    /**
     * Implement in the activity/fragment/view that you want to listen to the WebView
     */
    public interface OnScrollChangedCallback {
        void onScrollEndReached();
        void onLoadPageStarted();
        void onLoadPageFinished();
    }
}