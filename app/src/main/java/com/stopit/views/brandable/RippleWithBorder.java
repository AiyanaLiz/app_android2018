package com.stopit.views.brandable;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.andexert.library.RippleView;
import com.stopit.utils.BrandingHelper;

/**
 * Created by Alenka on 06.07.2018.
 */

public class RippleWithBorder extends RippleView {

    public RippleWithBorder(Context context) {
        super(context);
        doBranding(context);
    }

    public RippleWithBorder(Context context, AttributeSet attrs) {
        super(context, attrs);
        doBranding(context);
    }

    public RippleWithBorder(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        doBranding(context);
    }

    private void doBranding(Context context) {
        if (!BrandingHelper.isBrandingColor())
            return;

        BrandingHelper.setBorderColor(this);
    }

    @Override
    public void setEnabled(boolean isEnabled) {
        super.setEnabled(isEnabled);
        Drawable background = getBackground();
        if (background != null)
            background.setAlpha(isEnabled ? 255 : 150); //STOPIT_VIEW_ENABLED_ALPHA : STOPIT_VIEW_DISABLED_ALPHA);

        setDuration(isEnabled ? 150 : 0);
        //setAlpha(isEnabled ? STOPIT_VIEW_ENABLED_ALPHA : STOPIT_VIEW_DISABLED_ALPHA);
    }
}
