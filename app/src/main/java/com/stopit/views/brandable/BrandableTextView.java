package com.stopit.views.brandable;

import android.content.Context;
import android.util.AttributeSet;

import com.stopit.utils.BrandingHelper;
import com.stopit.utils.FontHelper;
import com.stopit.views.StopitTextView;


/**
 * Created by Alenka on 05.07.2018.
 */

public class BrandableTextView extends StopitTextView {

    public BrandableTextView(Context context) {
        super(context);
        applyBranding();
    }

    public BrandableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyBranding();
    }

    public BrandableTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyBranding();
    }

    private void applyBranding(){
        if(!BrandingHelper.isBrandingColor())
            return;

        BrandingHelper.setTextColor(this);
    }
}
