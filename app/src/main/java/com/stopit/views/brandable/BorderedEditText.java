package com.stopit.views.brandable;

import android.content.Context;
import android.util.AttributeSet;

import com.stopit.R;
import com.stopit.utils.BrandingHelper;
import com.stopit.views.StopitEditText;


/**
 * Created by Alenka on 05.07.2018.
 */

public class BorderedEditText extends StopitEditText {

    public BorderedEditText(Context context) {
        super(context);
        applyBranding(context);
    }

    public BorderedEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyBranding(context);
    }

    public BorderedEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyBranding(context);
    }

    private void applyBranding(final Context context) {
        if (!BrandingHelper.isBrandingColor())
            return;

        BrandingHelper.setTextColor(this);
        BrandingHelper.setBorderColor(this);
    }
}
