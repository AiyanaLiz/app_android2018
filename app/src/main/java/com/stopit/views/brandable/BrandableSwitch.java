package com.stopit.views.brandable;

import android.content.Context;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;

import com.kyleduo.switchbutton.SwitchButton;
import com.stopit.utils.BrandingHelper;


/**
 * Created by Alenka on 05.07.2018.
 */

public class BrandableSwitch extends SwitchButton{

    public BrandableSwitch(Context context) {
        super(context);
        applyBranding(context);
    }

    public BrandableSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyBranding(context);
    }

    public BrandableSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyBranding(context);
    }

    private void applyBranding(final Context context) {
        if (!BrandingHelper.isBrandingColor())
            return;

        BrandingHelper.setSwitchColor(context, this);
    }
}
