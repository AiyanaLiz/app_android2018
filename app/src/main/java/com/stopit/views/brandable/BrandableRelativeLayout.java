package com.stopit.views.brandable;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.stopit.utils.BrandingHelper;

public class BrandableRelativeLayout extends RelativeLayout {

    public BrandableRelativeLayout(Context context) {
        super(context);
        applyBranding();
    }

    public BrandableRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyBranding();
    }

    public BrandableRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyBranding();
    }

    private void applyBranding(){
        if(!BrandingHelper.isBrandingColor())
            return;

        BrandingHelper.setBorderColor(this);
    }
}
