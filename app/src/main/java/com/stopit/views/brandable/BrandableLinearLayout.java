package com.stopit.views.brandable;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.stopit.utils.BrandingHelper;

public class BrandableLinearLayout extends LinearLayout{
    public BrandableLinearLayout(Context context) {
        super(context);
        applyBranding();
    }

    public BrandableLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyBranding();
    }

    public BrandableLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyBranding();
    }

    private void applyBranding(){
        if(!BrandingHelper.isBrandingColor())
            return;

        BrandingHelper.setBackgroundDrawableColor(this);
    }
}
