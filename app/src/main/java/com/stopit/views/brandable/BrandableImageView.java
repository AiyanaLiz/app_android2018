package com.stopit.views.brandable;

import android.content.Context;
import android.util.AttributeSet;

import com.stopit.utils.BrandingHelper;

/**
 * Created by Alenka on 12.07.2018.
 */

public class BrandableImageView extends android.support.v7.widget.AppCompatImageView {
    public BrandableImageView(Context context) {
        super(context);
        applyBranding();
    }

    public BrandableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyBranding();
    }

    public BrandableImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyBranding();
    }

    private void applyBranding(){
        if(!BrandingHelper.isBrandingColor())
            return;

        BrandingHelper.setIconColor(this);
    }
}
