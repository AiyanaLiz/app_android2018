package com.stopit.views.brandable;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;

import com.stopit.R;
import com.stopit.utils.BrandingHelper;

/**
 * Created by Alenka on 12.07.2018.
 */

public class BrandableTabLayout extends TabLayout {
    public BrandableTabLayout(Context context) {
        super(context);
        applyBranding(context);
    }

    public BrandableTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyBranding(context);
    }

    public BrandableTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyBranding(context);
    }


    private void applyBranding(final Context context) {
        if (!BrandingHelper.isBrandingColor())
            return;

        setSelectedTabIndicatorColor(BrandingHelper.getMainColor(context));
    }

//    private ColorStateList getTextColors() {
//        final Resources res = getContext().getResources();
//        final int activeColor = BrandingHelper.isBranding() ? BrandingHelper.getMainColor(getContext())
//                : res.getColor(R.color.colorPrimary);
//        final int enabledColor = res.getColor(R.color.tab_text_gray);
//
//        return new ColorStateList(
//                new int[][]{
//                        new int[]{android.R.attr.state_checked},
//                        new int[]{android.R.attr.state_selected},
//                        new int[]{android.R.attr.state_pressed},
//                        new int[]{-android.R.attr.state_checked}},
//                new int[]{
//                        activeColor, activeColor, activeColor, enabledColor});
//    }
}
