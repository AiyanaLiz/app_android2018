package com.stopit.views.brandable;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;

import com.stopit.R;
import com.stopit.utils.BrandingHelper;

/**
 * Created by Alenka on 06.07.2018.
 */

public class BrandableBigCheckBox extends AppCompatCheckBox {

    public BrandableBigCheckBox(Context context) {
        super(context);
        doBranding(context);
    }

    public BrandableBigCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        doBranding(context);
    }

    public BrandableBigCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        doBranding(context);
    }

    private void doBranding(Context context) {
        if (!BrandingHelper.isBrandingColor())
            return;

        final int color = BrandingHelper.getMainColor(context);

        Drawable checkedDrawable = BrandingHelper.changeIconColor(context, R.drawable.ic_checkbox_big, color);
        if (checkedDrawable == null)
            return;

        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked},
                checkedDrawable);
        states.addState(new int[]{},
                context.getResources().getDrawable(R.drawable.ic_checkbox_big_inactive));
        setButtonDrawable(states);
    }
}
