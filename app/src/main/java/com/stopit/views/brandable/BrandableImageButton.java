package com.stopit.views.brandable;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

import com.stopit.utils.BrandingHelper;

/**
 * Created by Alenka on 12.07.2018.
 */

public class BrandableImageButton extends android.support.v7.widget.AppCompatImageButton {
    public BrandableImageButton(Context context) {
        super(context);
        applyBranding();
    }

    public BrandableImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyBranding();
    }

    public BrandableImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyBranding();
    }

    private void applyBranding(){
        if(!BrandingHelper.isBranding())
            return;

        BrandingHelper.setIconColor(this);
    }
}
