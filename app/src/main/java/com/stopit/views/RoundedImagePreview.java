package com.stopit.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.makeramen.roundedimageview.RoundedImageView;


public class RoundedImagePreview extends RoundedImageView {
    private Object taggedObject;

    public RoundedImagePreview(Context context) {
        super(context);
    }

    public RoundedImagePreview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RoundedImagePreview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    public Object getTaggedObject() {
        return taggedObject;
    }

    public void setTaggedObject(Object taggedObject) {
        this.taggedObject = taggedObject;
    }
}
