package com.stopit.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.stopit.R;
import com.stopit.db.DBHelper;
import com.stopit.utils.BrandingHelper;
import com.stopit.utils.Imager;

import java.io.File;

import static com.stopit.app.Constants.BRANDING_IMG_JPG_FILE_SUFIX;
import static com.stopit.app.Constants.BRANDING_IMG_PNG_FILE_SUFIX;

/**
 * Created by Alenka on 10.07.2018.
 */

public class StopitBackground extends RelativeLayout {

    private ImageView bgImageView;
    private View bgColorFilter;

    public StopitBackground(Context context) {
        super(context);
        inflateLayout(context);
    }

    public StopitBackground(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflateLayout(context);
    }

    public StopitBackground(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflateLayout(context);
    }

    private void inflateLayout(Context context) {
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View root = null;

        if (inflater != null)
            root = inflater.inflate(R.layout.stopit_background, this);

        if (root == null)
            return;

        bgImageView = root.findViewById(R.id.bg_image_view);
        bgColorFilter = root.findViewById(R.id.bg_color_filter_view);

        applyBranding(context);
    }

    private void applyBranding(final Context context) {
        if (!BrandingHelper.isBranding())
            return;

        BrandingHelper.setBackground(bgColorFilter);

        final String bgPath = BrandingHelper.getPathForBackground(DBHelper.getCurrentOrganizationId());
        final String pngPath = bgPath + BRANDING_IMG_PNG_FILE_SUFIX;
        final String jpgPath = bgPath + BRANDING_IMG_JPG_FILE_SUFIX;

        final File bgFilePNG = new File(pngPath);
        if (bgFilePNG.exists() && bgFilePNG.canRead()) {
            Imager.loadImageFromFile(context, pngPath, bgImageView);
            return;
        }

        final File bgFileJPG = new File(jpgPath);
        if (bgFileJPG.exists() && bgFileJPG.canRead()) {
            Imager.loadImageFromFile(context, jpgPath, bgImageView);
        }
    }

//    public void updateBranding(final Context context) {
//        final String bgPath = BrandingHelper.getPathForBackground(DBHelper.getCurrentOrganizationId());
//        final File bgFile = new File(bgPath);
//        if (bgFile.exists() && bgFile.canRead()) {
//            Imager.loadImageFromFile(context, bgPath, bgImageView);
//        }
//
//        BrandingHelper.setBackground(bgColorFilter);
//    }
}
