package com.stopit.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.andexert.library.RippleView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.stopit.R;
import com.stopit.activity.OrgTitleClickListener;
import com.stopit.adapter.CategorySelectorListAdapter;
import com.stopit.db.DBHelper;
import com.stopit.models.Organization;
import com.stopit.models.ResourceCategory;
import com.stopit.utils.BrandingHelper;

import java.util.List;

public class StopitCategorySelector extends RelativeLayout {

    private StopitTextView titleView;
    private CheckBox dropdownChk;
    private LinearLayout listContainer;
    private OrgTitleClickListener listener;
    private CategorySelectorListAdapter mAdapter;
    private List<ResourceCategory> categoryList;
    private RippleView titleRipple;

    private ResourceCategory selectedCategory;

    public StopitCategorySelector(Context context) {
        super(context);
        inflateLayout(context);
    }

    public StopitCategorySelector(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflateLayout(context);
    }

    public StopitCategorySelector(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        inflateLayout(context);
    }

    private void inflateLayout(Context context) {
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View selectorView = null;

        if (inflater != null)
            selectorView = inflater.inflate(R.layout.stopit_category_selector, this);

        if (selectorView == null)
            return;

        //title - always visible
        titleView = selectorView.findViewById(R.id.category_title_view);
        dropdownChk = selectorView.findViewById(R.id.category_dropdown_btn);
        titleRipple = selectorView.findViewById(R.id.category_title_ripple);

        //container view with organizations list
        listContainer = selectorView.findViewById(R.id.category_list_container);

        final RecyclerView mRecycler = selectorView.findViewById(R.id.category_recycler_view);

        populateCategoriesList();
        setCategory();
        manageDropdownBtn();

        mAdapter = new CategorySelectorListAdapter(categoryList);
        mRecycler.setLayoutManager(new LinearLayoutManager(context));
        mRecycler.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                final ResourceCategory entity = (ResourceCategory) adapter.getItem(position);
                /*final long organizationId = entity != null ? entity.getId() : 0;
                changeOrganization(organizationId);*/
            }
        });

        applyBranding();
    }

    private void populateCategoriesList() {
        //categoryList = DBHelper.getHistoryList(currentOrganizationId);
    }

    private void changeOrganization(final long orgId) {
        if (listener != null)
            listener.changeOrganization(orgId);
    }

    private void applyBranding() {
        if (!BrandingHelper.isBrandingColor())
            return;

        BrandingHelper.setDropdownBtnColor(dropdownChk);
    }

    private void manageDropdownBtn(){
        //TODO if(categoryList.size() >= 1)

    }

    public void setTitleActive(final OrgTitleClickListener listener) {
        this.listener = listener;

        titleRipple.setEnabled(true);
        titleRipple.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dropdownChk.performClick();
            }
        });

        if (dropdownChk.getVisibility() == GONE) {
            dropdownChk.setVisibility(VISIBLE);
        }
        dropdownChk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                manageOrganizationsList(isChecked);
            }
        });
    }

    private void manageOrganizationsList(final boolean isChecked) {
        listContainer.setVisibility(isChecked ? VISIBLE : GONE);
        if (listener != null)
            listener.onTitleClicked();
    }

    public void setCategory() {
        final ResourceCategory lastSelectedCategory = null;
        //TODO final ResourceCategory lastSelectedCategory = DBHelper.get ...
        if(lastSelectedCategory != null){
            //TODO set lastSelectedCategory
        }else if (categoryList.size() != 0){
            //TODO set categoryList.get(0);
        }
    }

    //public void setOrganizationsTitle(final String title) {
//        titleView.setText(title);
//    }
}
