package com.stopit.views;

import android.content.Context;
import android.util.AttributeSet;

import com.stopit.utils.FontHelper;

import static com.stopit.app.Constants.STOPIT_VIEW_DISABLED_ALPHA;
import static com.stopit.app.Constants.STOPIT_VIEW_ENABLED_ALPHA;


/**
 * Created by Alenka on 05.07.2018.
 */

public class StopitTextView extends android.support.v7.widget.AppCompatTextView {

    public StopitTextView(Context context) {
        super(context);
    }

    public StopitTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        FontHelper.setCustomFont(this, context, attrs);
    }

    public StopitTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        FontHelper.setCustomFont(this, context, attrs);
    }

    @Override
    public void setEnabled(boolean isEnabled) {
        super.setEnabled(isEnabled);
        setAlpha(isEnabled ? STOPIT_VIEW_ENABLED_ALPHA : STOPIT_VIEW_DISABLED_ALPHA);
    }
}
