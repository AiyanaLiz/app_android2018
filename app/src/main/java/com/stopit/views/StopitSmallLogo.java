package com.stopit.views;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.stopit.db.DBHelper;
import com.stopit.utils.BrandingHelper;
import com.stopit.utils.Imager;

import java.io.File;

import static com.stopit.app.Constants.BRANDING_IMG_GIF_FILE_SUFIX;
import static com.stopit.app.Constants.BRANDING_IMG_JPG_FILE_SUFIX;
import static com.stopit.app.Constants.BRANDING_IMG_PNG_FILE_SUFIX;

/**
 * Created by Alenka on 10.07.2018.
 */

public class StopitSmallLogo extends AppCompatImageView {

    public StopitSmallLogo(Context context) {
        super(context);
        applyBranding(context);
    }

    public StopitSmallLogo(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyBranding(context);
    }

    public StopitSmallLogo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyBranding(context);
    }

    private void applyBranding(final Context context) {
        if (!BrandingHelper.isBranding())
            return;

        final String logoPath = BrandingHelper.getPathForSmallLogo(DBHelper.getCurrentOrganizationId());
        final String pngPath = logoPath + BRANDING_IMG_PNG_FILE_SUFIX;
        final String jpgPath = logoPath + BRANDING_IMG_JPG_FILE_SUFIX;
        final String gifPath = logoPath + BRANDING_IMG_GIF_FILE_SUFIX;

        final File logoPngFile = new File(pngPath);
        if (logoPngFile.exists() && logoPngFile.canRead()) {
            Imager.loadImageFromFile(context, pngPath, this);
            return;
        }

        final File logoJpgFile = new File(jpgPath);
        if (logoJpgFile.exists() && logoJpgFile.canRead()) {
            Imager.loadImageFromFile(context, jpgPath, this);
        }

        final File logoGifFile = new File(gifPath);
        if (logoGifFile.exists() && logoGifFile.canRead()) {
            Imager.loadImageFromFile(context, gifPath, this);
        }
    }

//    public void updateBranding(final Context context){
//        final String logoPath = BrandingHelper.getPathForSmallLogo(DBHelper.getCurrentOrganizationId());
//        final File logoFile = new File(logoPath);
//        if (logoFile.exists() && logoFile.canRead()) {
//            Imager.loadImageFromFile(context, logoPath, this);
//        }
//    }
}
