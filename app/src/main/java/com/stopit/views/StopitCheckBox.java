package com.stopit.views;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.stopit.utils.FontHelper;

/**
 * Created by Alenka on 05.07.2018.
 */

public class StopitCheckBox extends AppCompatCheckBox{
    public StopitCheckBox(Context context) {
        super(context);
    }

    public StopitCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        FontHelper.setCustomFont(this, context, attrs);
    }

    public StopitCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        FontHelper.setCustomFont(this, context, attrs);
    }
}
