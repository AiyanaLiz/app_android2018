package com.stopit.views;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.andexert.library.RippleView;
import com.stopit.R;
import com.stopit.utils.BrandingHelper;

public class StopitToolbar extends RelativeLayout {

    private View mToolbar;
    private StopitTextView mRightBtn;
    private RippleView backBtnRipple;
    private RippleView rightBtnRipple;


    public StopitToolbar(Context context) {
        super(context);
        inflateLayout(context);
    }

    public StopitToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflateLayout(context);
    }

    public StopitToolbar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        inflateLayout(context);
    }

    private void inflateLayout(Context context) {
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View toolbar = null;

        if (inflater != null)
            toolbar = inflater.inflate(R.layout.stopit_tool_bar, this);

        if (toolbar == null)
            return;

        mToolbar = toolbar.findViewById(R.id.toolbar);
        mRightBtn = mToolbar.findViewById(R.id.right_btn);

        backBtnRipple = mToolbar.findViewById(R.id.button_back_ripple);
        rightBtnRipple = mToolbar.findViewById(R.id.right_btn_ripple);

        applyBranding();
    }

    private void applyBranding() {
        if (!BrandingHelper.isBrandingColor())
            return;

        BrandingHelper.setBackground(mToolbar);
    }


    public void setBackButton(final Activity activity) {
        if (backBtnRipple.getVisibility() == View.INVISIBLE)
            backBtnRipple.setVisibility(View.VISIBLE);

        backBtnRipple.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Workaround (cos-tyl)
                // https://code.google.com/p/android/issues/detail?can=2&q=&colspec=ID%20Type%20Status%20Owner%20Summary%20Stars&groupby=&sort=&id=183771#makechanges
                try {
                    activity.onBackPressed();
                } catch (Throwable e) {
                    //todo
                }
            }
        });
    }

    public void setRightButton(final int labelRes, final OnClickListener listener) {
        if (rightBtnRipple.getVisibility() == View.INVISIBLE)
            rightBtnRipple.setVisibility(View.VISIBLE);

        rightBtnRipple.setOnClickListener(listener);
        mRightBtn.setText(labelRes);
    }

    public void showRightButton() {
        if (rightBtnRipple.getVisibility() == View.INVISIBLE || rightBtnRipple.getVisibility() == View.GONE)
            rightBtnRipple.setVisibility(View.VISIBLE);
    }

    public StopitTextView getRightBtn() {
        return mRightBtn;
    }

    public void hideBackButton() {
        if (backBtnRipple.getVisibility() == View.VISIBLE) {
            backBtnRipple.setVisibility(View.INVISIBLE);
            backBtnRipple.setOnClickListener(null);
        }
    }

    public void hideRightButton() {
        if (rightBtnRipple.getVisibility() == View.VISIBLE) {
            rightBtnRipple.setVisibility(View.INVISIBLE);
            rightBtnRipple.setOnClickListener(null);
            mRightBtn.setText("");
        }
    }
}
